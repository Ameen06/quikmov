//
//  VIewBookingViewController.h
//  QuikMov
//
//  Created by Ajmal Khan on 2/20/16.
//  Copyright © 2016 CSCS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "UIImageView+WebCache.h"
#import "QuickMenuViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "UIImageView+WebCache.h"
#import <MessageUI/MessageUI.h>
@import GoogleMaps;

@interface VIewBookingViewController : UIViewController<CLLocationManagerDelegate,MKMapViewDelegate,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,UIGestureRecognizerDelegate,menuDelegate,GMSMapViewDelegate,MFMessageComposeViewControllerDelegate>

@property(nonatomic,strong)CLLocationManager* locationManager;
@property(nonatomic, strong)NSString* userType;
@property (nonatomic, strong) UIPanGestureRecognizer *panGest;


//+ (NSArray*)decodePolylineWithString:(NSString *)encodedString;


@end
