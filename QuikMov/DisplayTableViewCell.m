//
//  DisplayTableViewCell.m
//  QuikMov
//
//  Created by Ajmal Khan on 3/3/16.
//  Copyright © 2016 CSCS. All rights reserved.
//

#import "DisplayTableViewCell.h"
#import "SVProgressHUD.h"
#import "GlobalObjects.h"

@implementation DisplayTableViewCell

@synthesize titleLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 0, self.frame.size.width - 10, 50)];
        [self addSubview:titleLabel];
    }
    
    return self;
}



@end
