//
//  QuikMovGoogleViewController.m
//  QuikMov
//
//  Created by CSCS on 2/24/16.
//  Copyright © 2016 CSCS. All rights reserved.
//

#import "QuikMovGoogleViewController.h"
#import "DesignObj.h"
#import "SVProgressHUD.h"
#import "Reachability.h"
#import "GlobalObjects.h"
#import "LoginViewController.h"
#import "ProfileViewController.h"
#import "userObj.h"
#import "MHGallery.h"
#import "CSMarker.h"
#import "ShipmentTypeObj.h"
#import "AppDelegate.h"
#import "UICustomSwitch.h"
#import "NYTPhotosViewController.h"
#import "NYTPhoto.h"
#import "AsyncImageView.h"
#import "MWPhotoBrowser.h"
#import "RecentBookingViewController.h"


@interface QuikMovGoogleViewController ()<UINavigationControllerDelegate,UIPickerViewDataSource,UIPickerViewDelegate,MWPhotoBrowserDelegate>{
    
    CLLocationCoordinate2D* coordinates;
    CLGeocoder* forwardGeocoder;
    NSString* lat;
    NSString* log;
    GMSMapView* mapView_;
    NSString* statusCheck;
//    MKPointAnnotation* myAnnotation;
    NSMutableArray * annotations;
    NSString* forwardlat;
    NSString* forwardlong;
    UIButton* setCurrentLocation;
    GMSCameraPosition * camera;
//    GMSMarker* marker;
    CSMarker* marker;
 
    NSMutableArray * objectsArr;
    NSMutableArray * imagesArr;
    NSMutableArray * latPosition;
    NSMutableArray * longPosition;
    NSMutableArray * name;
    NSMutableArray * mailid;
    NSMutableArray * phoneno;
    
    
    UIView* shadowView;
    UIView* popView;
    UIButton* closeView;
    UIButton* shadowButton;
    
    NSString * userID;
    NSString*userdistance;
    NSString* selectType;
    
    UIButton* viewimage1;
    UIButton* viewimage2;
    UIButton* viewimage3;
    UIButton* viewimage4;
    
    UIImageView * profimg;
    UIImageView * profimg2;
    UIImageView * profimg3;
    UIImageView * profimg4;
    
    UISwitch* switchStatus;
    UIView* hideMapview;
    
    QuickMenuViewController * menuView;
    UISwipeGestureRecognizer* swipetoRight;
    UIButton *listButton;
    
    UITableView* listtableView;
    UIButton* tblcloseView;
    UIButton* tableShaddowview;
    
    UIView* panshadowView;
    BOOL isOPen;
    
    UIView* bookshadowView;
    UIView* bookpopView;
    
    UIButton* bookcloseView;
    UIButton* bookshadowButton;
    UITextField* deliveryArea;
    UITextField* shipmentType;
    UITextField* Description;
    UIButton* confirmBooking;
    UIImageView * txtbgImg1;
    UIImageView * txtbgImg2;
    UIImageView * txtbgImg3;
    
    UIButton* closingMenu;
    NSString* trukrId;
    UITableView* shipmentypeTableView;
    UIButton *contactTruck;
    
    NSUInteger tagged;
    
    NSString* serviceArr;
    
// Uiview pop
    
    
    UIView* popContactView;
    UIView* popContactshadowView;
    UIButton* popContactshadowbtn;
    UIButton* popcontactClose;
    UIButton* call;
    UIButton* callImg;
    UIButton* sms;
    UIButton* smsImg;
    NSString* phno;
    NSMutableArray* shipArr;
    UIImageView* userprofimag;
    
//    Shipment
    
    NSMutableArray * shiptypeArr;
    UIPickerView * shipmentList;
    
    CLLocationManager* locationManager;
    UIPanGestureRecognizer *panGest;
    UITapGestureRecognizer *imageViewtap;
    MHPresenterImageView *iv;
    
    UIScrollView * _scrView;
    
    UICustomSwitch* switchControlling;
    
    UIView * ViewForValuePicker;
    
//    Places & AutoComplete
    
    GMSPlacesClient * _placesClient;
    GMSAutocompleteResultsViewController * _resultsViewController;
    UISearchController * _searchController;
    UITableView * autocompleteTableView;
    NSMutableArray * autocompleteUrls;
    NSMutableArray * pastUrls;
    
//    Images Array
    
    NSMutableArray * potos;
    NSArray * setofPic;
    
    
    

}

@end

@implementation QuikMovGoogleViewController
@synthesize userType;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.navigationBarHidden = NO;
    
    self.navigationItem.hidesBackButton = YES;
    
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [DesignObj quikYellow]}];
    self.navigationController.navigationBar.barTintColor = [DesignObj quikRed];
    
    
    self.title = @"QuikMov";
    
    //    [self getcurrentLocation];
    locationManager = [[CLLocationManager alloc]init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8) {
        [locationManager requestWhenInUseAuthorization];
    }
    [locationManager startUpdatingLocation];
    
    //    annotations = [[NSMutableArray alloc]init];
    
    NSLog(@"%@",[NSString stringWithFormat:@"latitude: %f longitude: %f", locationManager.location.coordinate.latitude, locationManager.location.coordinate.longitude]);
    
    lat = [NSString stringWithFormat:@"%f",locationManager.location.coordinate.latitude];
    log = [NSString stringWithFormat:@"%f",locationManager.location.coordinate.longitude];

    
    //  Google Map View
    
    camera = [GMSCameraPosition cameraWithLatitude:[lat floatValue] longitude:[log floatValue] zoom:15];
    //    CGRectZero
    mapView_ = [GMSMapView mapWithFrame:CGRectMake(0, 0, screenWidth, screenHeight) camera:camera];
    mapView_.myLocationEnabled = YES;
    mapView_.settings.myLocationButton = YES;
    mapView_.mapType = kGMSTypeNormal;
    mapView_.delegate = self;

    self.view = mapView_;
    
    serviceArr = @"No shipment";
    
    // MapView Ends

    listButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [listButton addTarget:self action:@selector(listOpen:) forControlEvents:UIControlEventTouchUpInside]; //adding action
    [listButton setTitleColor:[DesignObj quikYellow] forState:UIControlStateNormal];
    [listButton setImage:[UIImage imageNamed:@"list.png"] forState:UIControlStateNormal];
    listButton.frame = CGRectMake(screenWidth-60 ,80,45,45);
    [self.view addSubview:listButton];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self action:@selector(menuOpen) forControlEvents:UIControlEventTouchUpInside]; //adding action
    [button setTitleColor:[DesignObj quikYellow] forState:UIControlStateNormal];
    [button setImage:[UIImage imageNamed:@"menu.png"] forState:UIControlStateNormal];
    button.frame = CGRectMake(0 ,0,15,35);
    
    UIButton *logoutBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [logoutBtn addTarget:self action:@selector(logout) forControlEvents:UIControlEventTouchUpInside]; //adding action
    [logoutBtn setTitleColor:[DesignObj quikYellow] forState:UIControlStateNormal];
    [logoutBtn setImage:[UIImage imageNamed:@"logout.png"] forState:UIControlStateNormal];
    logoutBtn.frame = CGRectMake(0 ,0,34,34);
    
    UIBarButtonItem* barbtn = [[UIBarButtonItem alloc]initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = barbtn;
    
    UIBarButtonItem* rytbarbtn = [[UIBarButtonItem alloc]initWithCustomView:logoutBtn];
    self.navigationItem.rightBarButtonItem = rytbarbtn;
    
//    Refresh button
    

//    
//    UIBarButtonItem* refreshbtn = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(refreshAction:)];
//    self.navigationItem.rightBarButtonItem = refreshbtn;
    
//    1st cell 
    
 // mapview segment
    
    UISegmentedControl *segmentControl = [[UISegmentedControl alloc]initWithItems:@[@"Terrain",@"Satelite"]];
    
    [segmentControl setBackgroundColor:[UIColor whiteColor]];
    segmentControl.frame = CGRectMake((screenWidth-200)/2, screenHeight-40, 200, 25);
    [segmentControl setTintColor:[DesignObj quikRed]];
    segmentControl.backgroundColor= [DesignObj quikYellow];
    [segmentControl addTarget:self action:@selector(segmentedControlValueDidChange:) forControlEvents:UIControlEventValueChanged];
    [segmentControl setSelectedSegmentIndex:0];
    [self.view addSubview:segmentControl];
    
    [self uploadcurrentLocation];
    
    panshadowView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screenWidth, screenHeight)];
    panshadowView.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.0];
    panshadowView.hidden = YES;
    UITapGestureRecognizer *tapIt = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                            action:@selector(tapOnShawdow:)];
    [panshadowView addGestureRecognizer:tapIt];
    
    panshadowView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:panshadowView];
    
    menuView = [[QuickMenuViewController alloc]init];
    menuView.view.frame = CGRectMake(- screenWidth, 0, screenWidth, screenHeight);
    menuView.view.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.0];
    menuView.delagte = self;
    [self.view addSubview:menuView.view];
    
    UIView * pangestureView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 20, screenHeight)];
    pangestureView.backgroundColor =[UIColor clearColor];
    [self.view addSubview:pangestureView];
    
    panGest = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(moveDrawer:)];
    panGest.maximumNumberOfTouches = 1;
    panGest.minimumNumberOfTouches = 1;
    panGest.delegate = self;
    [pangestureView addGestureRecognizer:panGest];
    
    potos = [[NSMutableArray alloc]init];
}


- (IBAction)refreshAction:(id)sender{
    [self getNearbyTrucks];
}

-(void)swipemenuOpen:(UISwipeGestureRecognizer*)rightgesture{
    
    [self.navigationController.view addSubview:menuView.view];
    float duration = MENU_DURATION/menuView.view.frame.size.width*fabs(menuView.view.center.x)+MENU_DURATION/2; // y=mx+c
    // shawdow
    
    [UIView animateWithDuration:duration
                          delay:0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         closingMenu = [DesignObj initWithButton:CGRectMake(screenWidth-50, 0, 50, screenHeight) tittle:@"" img:@""];
                         [closingMenu addTarget:self action:@selector(closeMenu) forControlEvents:UIControlEventTouchUpInside];
                         [menuView.view addSubview:closingMenu];
                         panshadowView.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:SHAWDOW_ALPHA];
                     }
                     completion:nil];
    panshadowView.hidden = NO;
    // drawer
    [UIView animateWithDuration:duration
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         menuView.view.frame = CGRectMake(0, 0, menuView.view.frame.size.width, menuView.view.frame.size.height);
                     }
                     completion:nil];
    
    isOPen= YES;
    
}

- (void) switchToggled:(id)sender {
    
    NSUserDefaults * user = [NSUserDefaults standardUserDefaults];
    
    switchStatus = (UISwitch *)sender;
    
    if ([switchStatus isOn]) {
        
        [user setBool:YES forKey:@"toggle"];
        statusCheck =@"1";
        hideMapview.hidden = YES;
        [self uploadcurrentLocation];
        [self showAlert:@"You are Active now\n(Now you are visible to your Customers)"];
        
    } else {
        
        [user setBool:NO forKey:@"toggle"];
        statusCheck =@"0";
        hideMapview.hidden = NO;
        [self uploadcurrentLocation];
        [self showAlert:@"You are InActive now\n(The Customers can't see and Book you)"];
        
    }
    [user synchronize];
}



-(void)segmentedControlValueDidChange:(UISegmentedControl *)segment
{
    switch (segment.selectedSegmentIndex) {
        case 0:{
            
            mapView_.mapType = kGMSTypeNormal;
            break;
        }
        case 1:{
            mapView_.mapType = kGMSTypeSatellite;
            break;
        }
    }
}

-(IBAction)listOpen:(id)sender{
    
    serviceArr = @"Table";
    
    tableShaddowview = [UIButton buttonWithType:UIButtonTypeCustom];
    tableShaddowview.frame = CGRectMake(0,0,screenWidth,screenHeight);
    [tableShaddowview addTarget:self action:@selector(closetblView) forControlEvents:UIControlEventTouchUpInside];
    tableShaddowview.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.3];
    [self.view addSubview:tableShaddowview];
    
    
    _scrView = [[UIScrollView alloc]init];
    _scrView.frame = CGRectMake(0, 0, listtableView.frame.size.width, listtableView.frame.size.height);
    [listtableView addSubview:_scrView];
    
//    UIImageView* bgView = [[UIImageView alloc] initWithImage:
//                            [UIImage imageNamed:@"bg.png"]];
    
    
    listtableView = [DesignObj initWithTableView:self frame:CGRectMake((screenWidth - (screenWidth - 40))/2, (screenHeight - (screenHeight - 150))/2,screenWidth - 40, screenHeight - 150)];
    [listtableView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg.png"]]];
    listtableView.separatorColor = [DesignObj quikRed];
    listtableView.dataSource =self;
    listtableView.rowHeight = 70.0f;
//    [listtableView.backgroundView addSubview:bgView];
    [self.view addSubview:listtableView];
    

    tblcloseView = [UIButton buttonWithType:UIButtonTypeCustom];
    tblcloseView.frame = CGRectMake(screenWidth-40,65,40,40);
    [tblcloseView addTarget:self action:@selector(closetblView) forControlEvents:UIControlEventTouchUpInside];
    [tblcloseView setImage:[UIImage imageNamed:@"close.png"] forState:UIControlStateNormal];
    [self.view addSubview:tblcloseView];
    
    //    [self.panGest setEnabled:NO];
    
}

-(void)closetblView{
    
    [listtableView removeFromSuperview];
    [tblcloseView removeFromSuperview];
    [tableShaddowview removeFromSuperview];
}

-(void)showCurrentLocation:(id)sender{
    
    
//    [self getcurrentLocation];
    
    
}

-(void)menuOpen{
    
    /*   if (menuView == nil) {
     menuView = [[QuickMenuViewController alloc]init];
     menuView.view.frame = CGRectMake(- screenWidth, 0, screenWidth, screenHeight);
     menuView.view.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.1];
     menuView.delagte = self;
     
     closingMenu = [DesignObj initWithButton:CGRectMake(screenWidth-50, 0, 50, screenHeight) tittle:@"" img:@""];
     [closingMenu addTarget:self action:@selector(closeMenu) forControlEvents:UIControlEventTouchUpInside];
     [menuView.view addSubview:closingMenu];
     
     [self.navigationController.view addSubview:menuView.view];
     
     [UIView animateWithDuration:0.5 animations:^{
     
     menuView.view.frame = CGRectMake(0, 0, screenWidth, screenHeight);
     menuView.view.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.5];
     
     }];
     }else{
     
     [UIView animateWithDuration:0.5 animations:^{
     menuView.view.frame = CGRectMake(- screenWidth, 0, screenWidth, screenHeight);
     
     } completion:^(BOOL finished) {
     [menuView.view removeFromSuperview];
     menuView = nil;
     }];*/
    
    //}
    [self.navigationController.view addSubview:menuView.view];
    float duration = MENU_DURATION/menuView.view.frame.size.width*fabs(menuView.view.center.x)+MENU_DURATION/2; // y=mx+c
    // shawdow
    
    [UIView animateWithDuration:duration
                          delay:0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         closingMenu = [DesignObj initWithButton:CGRectMake(screenWidth-50, 0, 50, screenHeight) tittle:@"" img:@""];
                         [closingMenu addTarget:self action:@selector(closeMenu) forControlEvents:UIControlEventTouchUpInside];
                         [menuView.view addSubview:closingMenu];
                         panshadowView.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:SHAWDOW_ALPHA];
                     }
                     completion:nil];
    panshadowView.hidden = NO;
    // drawer
    [UIView animateWithDuration:duration
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         menuView.view.frame = CGRectMake(0, 0, menuView.view.frame.size.width, menuView.view.frame.size.height);
                     }
                     completion:nil];
    
    isOPen= YES;
    
}

- (void)tapOnShawdow:(UITapGestureRecognizer *)recognizer {
    [self closeMenu];
}

-(void)moveDrawer:(UIPanGestureRecognizer *)recognizer{
    
    CGPoint translation = [recognizer translationInView:self.view];
    CGPoint velocity = [(UIPanGestureRecognizer*)recognizer velocityInView:self.view];
    //    NSLog(@"velocity x=%f",velocity.x);
    
    if([(UIPanGestureRecognizer*)recognizer state] == UIGestureRecognizerStateBegan) {
        //        NSLog(@"start");
        if ( velocity.x > MENU_TRIGGER_VELOCITY && isOPen) {
            [self menuOpen];
        }else if (velocity.x < -MENU_TRIGGER_VELOCITY && isOPen) {
            [self closeMenu];
        }
    }
    
    if([(UIPanGestureRecognizer*)recognizer state] == UIGestureRecognizerStateChanged) {
        //        NSLog(@"changing");
        float movingx = menuView.view.center.x + translation.x;
        if ( movingx > -menuView.view.frame.size.width/2 && movingx < menuView.view.frame.size.width/2){
            
            menuView.view.center = CGPointMake(movingx, menuView.view.center.y);
            [recognizer setTranslation:CGPointMake(0,0) inView:self.view];
            
            float changingAlpha = SHAWDOW_ALPHA/menuView.view.frame.size.width*movingx+SHAWDOW_ALPHA/2; // y=mx+c
            panshadowView.hidden = NO;
            panshadowView.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:changingAlpha];
        }
    }
    
    if([(UIPanGestureRecognizer*)recognizer state] == UIGestureRecognizerStateEnded) {
        //        NSLog(@"end");
        if (menuView.view.center.x>0){
            [self menuOpen];
        }else if (menuView.view.center.x<0){
            [self closeMenu];
        }
    }
    
    
    
}

-(void)closeMenu{
    
    float duration = MENU_DURATION/menuView.view.frame.size.width*fabs(menuView.view.center.x)+MENU_DURATION/2; // y=mx+c
    
    // shawdow
    [UIView animateWithDuration:duration
                          delay:0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         panshadowView.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.0f];
                     }
                     completion:^(BOOL finished){
                         [panshadowView removeFromSuperview];
                     }];
    
    // drawer
    [UIView animateWithDuration:duration
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         menuView.view.frame = CGRectMake(- menuView.view.frame.size.width, 0, menuView.view.frame.size.width, menuView.view.frame.size.height);;
                     }
                     completion:^(BOOL finished) {
                         [menuView.view removeFromSuperview];
                     }];
    isOPen= NO;
    
    
    
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    NSLog(@"Error:%@",error);
    switch([error code])
    {
        case kCLErrorNetwork: // general, network-related error
        {
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please check your network connection or that you are not in airplane mode" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        }
            break;
        case kCLErrorDenied:{
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Location Service not enabeld" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];        }
            break;
            
        case kCLErrorLocationUnknown:
            break;
        default:
            
            break;
    }
    
}


-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:YES];
    
    [self getNearbyTrucks];
    
}

-(void)getNearbyTrucks{
    
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
    [SVProgressHUD show];
    
    NSURL* url = [NSURL URLWithString:@"http://quikmov.quikmovapp.com/auth/getList"];
    NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession* session = [NSURLSession sessionWithConfiguration:config ];
    
    NSMutableURLRequest*request = [[NSMutableURLRequest alloc]initWithURL:url];
    request.HTTPMethod = @"POST";
    
    NSUserDefaults* userStoredetails = [NSUserDefaults standardUserDefaults];
    userID = [userStoredetails objectForKey:@"user_id"];
    NSLog(@"%@",userID);
    
    NSString* bodycntnt = [NSString stringWithFormat:@"userid=%@",userID];
    
    NSData *data = [bodycntnt dataUsingEncoding:NSUTF8StringEncoding];
    
    NSURLSessionUploadTask *uploadtask = [session uploadTaskWithRequest:request fromData:data completionHandler:^(NSData*data, NSURLResponse*response, NSError*error){
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
            [self serviceSuccess:data];
        });
    }];
    
    [uploadtask resume];
}

- (void)serviceSuccess:(NSData *)data{
    if (data == nil) {
        return;
    }
    NSString* str = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
    NSError * error;
    NSDictionary* dic = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    NSLog(@"%@******%@,%@",str,[dic allValues],[dic objectForKey:@"response"]);
    
    NSString * Result = [dic objectForKey:@"status"];
    
    NSLog(@"%@",Result);
    
    if ([Result isEqualToString:@"success"]){
        NSArray * arr = [dic objectForKey:@"result"];
        [objectsArr removeAllObjects];
        objectsArr = [NSMutableArray new];
        
        for ( int i = 0; i < [arr count]; i++) {
            
            //
            GlobalObjects * globleObj = [[GlobalObjects alloc]init];
            NSDictionary * objDic = [arr objectAtIndex:i];
            globleObj.latitudee = [objDic objectForKey:@"latitude"];
            
            globleObj.longitudee = [objDic objectForKey:@"longitude"];
            
            globleObj.username = [objDic objectForKey:@"user_name"];
            
            globleObj.userPhone = [objDic objectForKey:@"user_phone"];
            
            globleObj.usermailid = [objDic objectForKey:@"user_email"];
            
            globleObj.currentUsertype = [objDic objectForKey:@"usertype"];
            
            globleObj.userDistance = [objDic objectForKey:@"distance"];
            
            globleObj.userPhotoUrl = [NSString stringWithFormat:@"http://quikmov.quikmovapp.com%@",[objDic objectForKey:@"userfilepath"]];
            
            globleObj.userStatus = [objDic objectForKey:@"status"];
            
            globleObj.userId = [objDic objectForKey:@"user_id"];
            
            
            if(![userID isEqualToString:[objDic objectForKey:@"user_id"]]){
                
                if ([userType isEqualToString:@"1"]) {
                    if (![globleObj.userStatus isEqualToString:@"0"]){
                        [objectsArr addObject:globleObj];
                    }
                }else{
                    [objectsArr addObject:globleObj];
                }
                
            }
            
            
            //        GlobalObjects* gblObj = [[GlobalObjects alloc]init];
        }
        [self loadAnnotations];
        
    }else{
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Message" message:[NSString stringWithFormat:@"%@",[dic objectForKey:@"result"]] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
}

//-(void)truckimagesuccess:(NSData *)data{
//    
//   }

-(void)uploadcurrentLocation{
    
    if ([Reachability reachabilityForInternetConnection]) {
        
        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
        //        [SVProgressHUD show];
        
        NSURL* url = [NSURL URLWithString:@"http://quikmov.quikmovapp.com/auth/userLocation" ];
        NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession* session = [NSURLSession sessionWithConfiguration:config ];
        
        NSMutableURLRequest*request = [[NSMutableURLRequest alloc]initWithURL:url];
        request.HTTPMethod = @"POST";
        
        //        inputs : userid,lat,lon,status(1 or 0)
        //        url : http://truckcsoft.mapcee.com/auth/userLocation
        
        NSUserDefaults* userStoredetails = [NSUserDefaults standardUserDefaults];
        userID = [userStoredetails objectForKey:@"user_id"];
        NSLog(@"%@",userID);
        
        NSString* bodycntnt = [NSString stringWithFormat:@"userid=%@&lat=%@&lon=%@&status=%@",userID,lat,log,statusCheck];
        
        NSLog(@"%@",bodycntnt);
        
        NSData *data = [bodycntnt dataUsingEncoding:NSUTF8StringEncoding];
        
        NSURLSessionUploadTask *uploadtask = [session uploadTaskWithRequest:request fromData:data completionHandler:^(NSData*data, NSURLResponse*response, NSError*error){
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                
                NSString* str = [[NSString alloc]initWithData:data
                                                     encoding:NSUTF8StringEncoding];
                
                NSError* error;
                
                if (str.length == 0) {
                    [self showAlert:@"Server error"];
                    return;
                }
                
                NSDictionary* dic = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                
                NSLog(@"%@******%@,%@",str,[dic allValues],[dic objectForKey:@"response"]);
                
                // userId = [[dic objectForKey:@"message"] stringValue];
                NSString * Result = [dic objectForKey:@"status"];
                
                NSLog(@"%@",Result);
                
            });
            
            
        }];
        [uploadtask resume];
    }
    
    
}

-(void)loadAnnotations{
    
//    if (annotations.count > 0) {
//        [annotations removeAllObjects];
//        [mapView_ removeAnnotations:[mapView_ annotations]];
//    }
    // if bust latitude is zero we need to load only stops
    
    [mapView_ clear];
    
    for(int i = 0; i < objectsArr.count; i++)
    {
        GlobalObjects * object = (GlobalObjects*)[objectsArr objectAtIndex:i];
        
        CLLocationCoordinate2D theCoordinate1 = CLLocationCoordinate2DMake([object.latitudee floatValue], [object.longitudee floatValue]);
        
        marker = [CSMarker markerWithPosition:theCoordinate1];
//        marker.title =  [NSString stringWithFormat:@"%d",i];
//        marker.objectID = [NSString stringWithFormat:@"%@",object.userId];
        marker.icon = [UIImage imageNamed:@"pin.png"];
        marker.appearAnimation = kGMSMarkerAnimationPop;
//        marker.title = [NSString stringWithFormat:@"%d",i];
        marker.objectID = [NSString stringWithFormat:@"%d",i];
        marker.map = mapView_;
        
        }
//        [self drawMarkers];
       [listtableView reloadData];
}


-(UIView*)mapView:(GMSMapView *)mapView markerInfoWindow:(GMSMarker *)marker1 {
    
    NSInteger tag = [marker1.title integerValue];
    GlobalObjects * object = (GlobalObjects*)[objectsArr objectAtIndex:tag];
    trukrId = [NSString stringWithFormat:@"%@",object.userId];
    [self getTruckImages:tag];
    
    return nil;
    
}

-(void)getTruckImages:(NSUInteger)tag{
    
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
    [SVProgressHUD show];
    
    NSURL* url = [NSURL URLWithString:@"http://quikmov.quikmovapp.com/auth/getTruckImages"];
    NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession* session = [NSURLSession sessionWithConfiguration:config ];
    
    NSMutableURLRequest*request = [[NSMutableURLRequest alloc]initWithURL:url];
    request.HTTPMethod = @"POST";
    
    NSString* bodycntnt = [NSString stringWithFormat:@"userid=%@",trukrId];
    
    NSData *data = [bodycntnt dataUsingEncoding:NSUTF8StringEncoding];
    
    NSURLSessionUploadTask *uploadtask = [session uploadTaskWithRequest:request fromData:data completionHandler:^(NSData*data, NSURLResponse*response, NSError*error){
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
    
            NSString* str = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
            NSError * error;
            NSDictionary* dic = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            NSLog(@"%@******%@,%@",str,[dic allValues],[dic objectForKey:@"response"]);
            
            NSString * Result = [dic objectForKey:@"status"];
            
            NSLog(@"%@",Result);
            
            if ([Result isEqualToString:@"success"]){
                NSArray * arr = [dic objectForKey:@"result"];
                
                imagesArr = [[NSMutableArray alloc]init];
                
                for ( int i = 0; i < [arr count]; i++) {
                
                    //
                    
                    NSDictionary * objDic = [arr objectAtIndex:i];
                    
                    [imagesArr addObject:[NSString stringWithFormat:@"http://quikmov.quikmovapp.com%@",[objDic objectForKey:@"filepath"]]];
                    
                    
                }
                
                [self ShowDetailView:tag];
                                
                
                
            }else{
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Message" message:[NSString stringWithFormat:@"%@",[dic objectForKey:@"result"]] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alert show];
            }
        });
    }];
    
    [uploadtask resume];
    
    
}


- (void)ShowDetailView:(NSUInteger)tag{
    
    [panGest setEnabled:NO];
    listtableView.hidden = YES;
    
    GlobalObjects * object = (GlobalObjects*)[objectsArr objectAtIndex:(tag)];
    
    
    NSLog(@"%@",[imagesArr objectAtIndex:0]);
    
    shadowView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screenWidth, screenHeight)];
    shadowView.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.5f];
    [self.view addSubview:shadowView];
    
    shadowButton = [DesignObj initWithButton:CGRectMake(0, 0, screenWidth, screenHeight) tittle:@"" img:@""];
    [shadowButton setBackgroundColor:[UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.5f]];
    [shadowButton addTarget:self action:@selector(closePopView) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:shadowButton];
    
    popView = [[UIView alloc] initWithFrame:CGRectMake((screenWidth - (screenWidth - 40))/2, (screenHeight - (screenHeight - 150))/2,screenWidth - 40, screenHeight - 150)];
    popView.layer.cornerRadius = 8;
    [popView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg.png"]]];
    [self.view addSubview:popView];
    
    profimg = [[UIImageView alloc]initWithFrame:CGRectMake(20, 20, 50, 50)];
    
    //    ((popView.frame.size.width - (screenWidth - 200))/2
    dispatch_async(dispatch_get_main_queue(), ^{
        [profimg sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[imagesArr objectAtIndex:0]]] placeholderImage:[UIImage imageNamed:@"truck_login.png"]];
        
        //[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:object.userPhotoUrl]]];
    });
    
    [popView addSubview:profimg];
    
    viewimage1 = [DesignObj initWithButton:CGRectMake(20, 20, 50, 50) tittle:nil img:nil];
    [viewimage1 addTarget:self action:@selector(showPic:) forControlEvents:UIControlEventTouchUpInside];
    viewimage1.tag = 110;
    [popView addSubview:viewimage1];
    
    profimg2 = [[UIImageView alloc]initWithFrame:CGRectMake(profimg.frame.origin.x + 50 + 20, 20, 50, 50)];
    dispatch_async(dispatch_get_main_queue(), ^{
        [profimg2 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[imagesArr objectAtIndex:1]]] placeholderImage:[UIImage imageNamed:@"truck_login.png"]];
    });
    
    [popView addSubview:profimg2];
    
    viewimage2 = [DesignObj initWithButton:CGRectMake(profimg.frame.origin.x + 50 + 20, 20, 50, 50) tittle:nil img:nil];
    [viewimage2 addTarget:self action:@selector(showPic:) forControlEvents:UIControlEventTouchUpInside];
    viewimage2.tag = 120;
    [popView addSubview:viewimage2];
    
    profimg3 = [[UIImageView alloc]initWithFrame:CGRectMake(profimg2.frame.origin.x + 50 + 20, 20, 50, 50)];
    dispatch_async(dispatch_get_main_queue(), ^{
        [profimg3 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[imagesArr objectAtIndex:2]]] placeholderImage:[UIImage imageNamed:@"truck_login.png"]];
    });
    
    [popView addSubview:profimg3];
    
    viewimage3 = [DesignObj initWithButton:CGRectMake(profimg2.frame.origin.x + 50 + 20, 20, 50, 50) tittle:nil img:nil];
    [viewimage3 addTarget:self action:@selector(showPic:) forControlEvents:UIControlEventTouchUpInside];
    viewimage3.tag = 130;
    [popView addSubview:viewimage3];
    
    profimg4 = [[UIImageView alloc]initWithFrame:CGRectMake(profimg3.frame.origin.x + 50 + 20, 20, 50, 50)];
    dispatch_async(dispatch_get_main_queue(), ^{
        [profimg4 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[imagesArr objectAtIndex:3]]] placeholderImage:[UIImage imageNamed:@"truck_login.png"]];
    });
    [popView addSubview:profimg4];
    
    viewimage4 = [DesignObj initWithButton:CGRectMake(profimg3.frame.origin.x + 50 + 20, 20, 50, 50) tittle:nil img:nil];
    [viewimage4 addTarget:self action:@selector(showPic:) forControlEvents:UIControlEventTouchUpInside];
    viewimage4.tag = 140;
    [popView addSubview:viewimage4];
    
    UILabel *namelabel = [[UILabel alloc] initWithFrame:CGRectMake(30, profimg.frame.origin.y+ 50 +30, 200, 40)];
    //    CGRectMake((popView.frame.size.width-100)/2, profimg.frame.origin.y+ 70 +30, 200, 40)];
    namelabel.numberOfLines = 2;
    namelabel.textAlignment = NSTextAlignmentLeft;
    namelabel.textColor = [DesignObj quikRed];
    namelabel.font = [UIFont systemFontOfSize:16.0];
    namelabel.text = [NSString stringWithFormat:@"Name: %@",object.username];
    NSLog(@"%@",object.username);
    [popView addSubview:namelabel];
    
    UILabel *phonelabel = [[UILabel alloc] initWithFrame:CGRectMake(30, namelabel.frame.origin.y+ 40+ 20, 300, 40)];
    //    CGRectMake((popView.frame.size.width - 100)/2, namelabel.frame.origin.y+ 40+ 20, 200, 40)];
    phonelabel.numberOfLines = 2;
    phonelabel.textAlignment = NSTextAlignmentLeft;
    phonelabel.textColor = [DesignObj quikRed];
    phonelabel.font = [UIFont systemFontOfSize:16.0];
    phonelabel.text = [NSString stringWithFormat:@"Phone Number: %@",object.userPhone];
    NSLog(@"%@",object.userPhone);
    [popView addSubview:phonelabel];
    
    UILabel *emaillabel = [[UILabel alloc] initWithFrame:CGRectMake(30, phonelabel.frame.origin.y+ 40+ 20, 200, 40)];
    //    CGRectMake((popView.frame.size.width - 100)/2, phonelabel.frame.origin.y+ 40+ 20, 200, 40)]
    emaillabel.numberOfLines = 2;
    emaillabel.textAlignment = NSTextAlignmentLeft;
    emaillabel.textColor = [DesignObj quikRed];
    emaillabel.font = [UIFont systemFontOfSize:16.0];
    emaillabel.text = [NSString stringWithFormat:@"E-mail-id: %@",object.usermailid];
    NSLog(@"%@",object.usermailid);
    [popView addSubview:emaillabel];
    
    UIButton *bookNow = [DesignObj initWithButton:CGRectMake((popView.frame.size.width - 200)/2, phonelabel.frame.origin.y+ 40+ 20+70, 200, 40) tittle:@"Book Now" img:@"button.png"];
    [bookNow addTarget:self action:@selector(bookNow:) forControlEvents:UIControlEventTouchUpInside];
    [bookNow setTitleColor:[DesignObj quikYellow] forState:UIControlStateNormal];
    [popView addSubview:bookNow];
    
    closeView = [UIButton buttonWithType:UIButtonTypeCustom];
    closeView.frame = CGRectMake(popView.frame.size.width-30,0,30,30);
    [closeView addTarget:self action:@selector(closePopView) forControlEvents:UIControlEventTouchUpInside];
    [closeView setImage:[UIImage imageNamed:@"close.png"] forState:UIControlStateNormal];
    [popView addSubview:closeView];
    
    trukrId = [NSString stringWithFormat:@"%@",object.userId];
    phno = [NSString stringWithFormat:@"%@",object.userPhone];
    
    
}

-(IBAction)contactView:(id)sender{
    
    
    popContactshadowView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screenWidth, screenHeight)];
    popContactshadowView.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.5f];
    [self.view addSubview:popContactshadowView];
    
    popContactshadowbtn = [DesignObj initWithButton:CGRectMake(0, 0, screenWidth, screenHeight) tittle:@"" img:@""];
    [popContactshadowbtn setBackgroundColor:[UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.5f]];
    [popContactshadowbtn addTarget:self action:@selector(closePopcontactView) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:popContactshadowbtn];
    
    popContactView = [[UIView alloc]initWithFrame:CGRectMake((screenWidth-300)/2, (screenHeight - 150)/2, 300, 150)];
    [popContactView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg.png"]]];
    popContactView.layer.cornerRadius = 9;
    [self.view addSubview:popContactView];
    
    callImg = [DesignObj initWithButton:CGRectMake(10, (75-40)/2, 40, 40) tittle:@"" img:@"call.png"];
    [callImg addTarget:self action:@selector(callAction:) forControlEvents:UIControlEventTouchUpInside];
    [popContactView addSubview:callImg];
    
    call = [DesignObj initWithButton:CGRectMake(0,0 , 300, 75) tittle:@"Call Trucker" img:@""];
    [call setTitleColor:[DesignObj quikRed] forState:UIControlStateNormal];
    //        [call setBackgroundColor:[UIColor greenColor]];
    [call addTarget:self action:@selector(callAction:) forControlEvents:UIControlEventTouchUpInside ];
    [popContactView addSubview:call];
    
    smsImg = [DesignObj initWithButton:CGRectMake(10, 150-60, 40, 40) tittle:@"" img:@"sms.png"];
    [smsImg addTarget:self action:@selector(smsAction) forControlEvents:UIControlEventTouchUpInside];
    [popContactView addSubview:smsImg];
    
    sms = [DesignObj initWithButton:CGRectMake(0, 75, 300, 75) tittle:@"SMS Trucker" img:@""];
    [sms setTitleColor:[DesignObj quikRed] forState:UIControlStateNormal];
    //        [sms setBackgroundColor:[DesignObj quikRed]];
    [sms addTarget:self action:@selector(smsAction) forControlEvents:UIControlEventTouchUpInside ];
    [popContactView addSubview:sms];
    
    popcontactClose = [UIButton buttonWithType:UIButtonTypeCustom];
    popcontactClose.frame = CGRectMake(popContactView.frame.size.width-20,0,20,20);
    [popcontactClose addTarget:self action:@selector(closePopcontactView) forControlEvents:UIControlEventTouchUpInside];
    [popcontactClose setImage:[UIImage imageNamed:@"close.png"] forState:UIControlStateNormal];
    [popContactView addSubview:popcontactClose];
}

-(IBAction)callAction:(id)sender{
    

    NSString * phoneNumber = [@"tel://" stringByAppendingString:[NSString stringWithFormat:@"%@",phno]];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
    [self closePopcontactView];
}

-(void)smsAction{
    
    if(![MFMessageComposeViewController canSendText]) {
        UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Your device doesn't support SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [warningAlert show];
        return;
    }
    
    NSArray *recipents = @[phno];
    NSString *message = [NSString stringWithFormat:@" Hey Trucker am waiting for your Pick-Up"];
    
    MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
    messageController.messageComposeDelegate = self;
    [messageController setRecipients:recipents];
    [messageController setBody:message];
    
    // Present message view controller on screen
    [self presentViewController:messageController animated:YES completion:nil];
//    [self closePopcontactView];
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result
{
    switch (result) {
        case MessageComposeResultCancelled:
            break;
            
        case MessageComposeResultFailed:
        {
            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Failed to send SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [warningAlert show];
            break;
        }
            
        case MessageComposeResultSent:
        {
            
            UIAlertView *successAlert = [[UIAlertView alloc] initWithTitle:@"Success" message:@"Message Sent to Trucker!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [successAlert show];
        }
            break;
            
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}



-(void)closePopcontactView{
    
    [popContactshadowbtn removeFromSuperview];
    [popContactshadowView removeFromSuperview];
    [popContactView removeFromSuperview];
    [popcontactClose removeFromSuperview];
    
}

-(IBAction)showPic:(id)sender{
 
    /*[potos removeAllObjects];
    
//    UIView * checkView =[[UIView alloc]initWithFrame:CGRectMake((screenWidth - (screenWidth - 40))/2, (screenHeight - (screenHeight - 150))/2,screenWidth - 40, screenHeight - 150)];
//    [self.view addSubview:checkView];
//    
    
//    setofPic = [[NSArray alloc]initWithObjects:profimg.image,profimg2.image,profimg3.image,profimg4.image, nil];
    
//    [potos addObject:setofPic];
    
    [potos addObject:[MWPhoto photoWithImage:profimg.image]];
    [potos addObject:[MWPhoto photoWithImage:profimg2.image]];
    [potos addObject:[MWPhoto photoWithImage:profimg3.image]];
    [potos addObject:[MWPhoto photoWithImage:profimg4.image]];
    
    
    // Create browser (must be done each time photo browser is
    // displayed. Photo browser objects cannot be re-used)
    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
//    browser.view.frame = CGRectMake((screenWidth - 200)/2, (screenHeight - 200)/2, 200, 200);
    
    
    // Set options
    browser.displayActionButton = YES; // Show action button to allow sharing, copying, etc (defaults to YES)
    browser.displayNavArrows = NO; // Whether to display left and right nav arrows on toolbar (defaults to NO)
    browser.displaySelectionButtons = NO; // Whether selection buttons are shown on each image (defaults to NO)
    browser.zoomPhotosToFill = YES; // Images that almost fill the screen will be initially zoomed to fill (defaults to YES)
    browser.alwaysShowControls = NO; // Allows to control whether the bars and controls are always visible or whether they fade away to show the photo full (defaults to NO)
    browser.enableGrid = YES; // Whether to allow the viewing of all the photo thumbnails on a grid (defaults to YES)
    browser.startOnGrid = NO; // Whether to start on the grid of thumbnails instead of the first photo (defaults to NO)
    browser.autoPlayOnAppear = NO; // Auto-play first video
    
    // Customise selection images to change colours if required
    
    // Optionally set the current visible photo before displaying
    [browser setCurrentPhotoIndex:1];
    
    // Present
    
   [self.navigationController pushViewController:browser animated:YES];
//    [checkView addSubview:browser.view];
    
    // Manipulate
    [browser showNextPhotoAnimated:YES];
    [browser showPreviousPhotoAnimated:YES];
    [browser setCurrentPhotoIndex:10];
    
    */
    MHGalleryItem *landschaft1 = [[MHGalleryItem alloc]initWithImage:profimg.image];
    MHGalleryItem* landschaft2 =[MHGalleryItem itemWithImage:profimg2.image];
    MHGalleryItem* landschaft3 =[MHGalleryItem itemWithImage:profimg3.image];
    MHGalleryItem* landschaft4 =[MHGalleryItem itemWithImage:profimg4.image];
    
     NSArray *galleryItem = @[landschaft1,landschaft2,landschaft3,landschaft4];

    
   MHGalleryController *gallery = [MHGalleryController galleryWithPresentationStyle:MHGalleryViewModeImageViewerNavigationBarHidden];
    gallery.galleryItems = galleryItem;
    gallery.presentingFromImageView = nil;
    gallery.presentationIndex = 0;
    gallery.UICustomization.showOverView =NO;
    gallery.UICustomization.barStyle = UIBarStyleBlackTranslucent;
    gallery.UICustomization.hideShare = YES;
    gallery.UICustomization.barTintColor = [DesignObj quikRed];
    gallery.UICustomization.barButtonsTintColor = [DesignObj quikYellow];

    __weak MHGalleryController *blockGallery = gallery;
    
    gallery.finishedCallback = ^(NSInteger currentIndex,UIImage *image,MHTransitionDismissMHGallery *interactiveTransition,MHGalleryViewMode viewMode){
        if (viewMode == MHGalleryViewModeOverView) {
            [blockGallery dismissViewControllerAnimated:YES completion:^{
                [self setNeedsStatusBarAppearanceUpdate];
            }];
        }else{
            
            dispatch_async(dispatch_get_main_queue(), ^{

                
                [blockGallery dismissViewControllerAnimated:YES dismissImageView:nil completion:^{
                    
                    [self setNeedsStatusBarAppearanceUpdate];
                    

                }];
            });
        }
    };
//     [selfView addSubview:gallery.imageViewerViewController.view];
    [self presentMHGalleryController:gallery animated:YES completion:nil];
    

    
    
  /*  GlobalObjects* globalimg = (GlobalObjects*)[imagesArr objectAtIndex:tagged];

//    NSData * img1 = [NSData dataWithData: UIImagePNGRepresentation(profimg.image)];
//    NSData * img2 = [NSData dataWithData: UIImagePNGRepresentation(profimg2.image)];
//    NSData * img3 = [NSData dataWithData: UIImagePNGRepresentation(profimg3.image)];
//    NSData * img4 = [NSData dataWithData: UIImagePNGRepresentation(profimg4.image)];
    
//    NSArray* imagesArray = [[NSArray alloc]initWithObjects:img1,img2,img3,img4, nil];
    
    UIImage* imgg1 = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[imagesArr objectAtIndex:0]]]];
    UIImage* imgg2 = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[imagesArr objectAtIndex:1]]]];
    UIImage* imgg3 = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[imagesArr objectAtIndex:2]]]];
    UIImage* imgg4 = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[imagesArr objectAtIndex:3]]]];
    
    NSArray* imagesArray = [[NSArray alloc]initWithObjects:imgg1,imgg2,imgg3,imgg4, nil];
    
    NYTPhotosViewController *photosViewController = [[NYTPhotosViewController alloc]initWithPhotos:imagesArray];
    [self presentViewController:photosViewController animated:YES completion:nil];
    
    */
}


#pragma MWBrowser Delegate

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return imagesArr.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < imagesArr.count) {
        return [imagesArr objectAtIndex:index];
    }
    return nil;
}

#pragma mark - tableView Functions.

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if ([serviceArr isEqualToString:@"shipment"]) {
        return shipArr.count;
        
    }else{
        
    return objectsArr.count;
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    // This will create a "invisible" footer
    return 0.001;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *MyIdentifier = @"MyIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:MyIdentifier];
    }
    
    for (UIView * view in cell.contentView.subviews)
    {
        [view removeFromSuperview];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor clearColor];
    
    if ([serviceArr isEqualToString:@"shipment"]) {
        
        ShipmentTypeObj * shipmntObj = (ShipmentTypeObj*)[shipArr objectAtIndex:indexPath.row];
        
        cell.backgroundColor = [UIColor clearColor];
        
        UILabel * titlelbl = [[UILabel alloc]initWithFrame:CGRectMake(20, 10, 150, 30)];
        titlelbl.font = [UIFont boldSystemFontOfSize:15.0];
        titlelbl.textColor = [DesignObj quikRed];
        titlelbl.textAlignment = NSTextAlignmentLeft;
        [cell.contentView addSubview:titlelbl];
        
        titlelbl.text = shipmntObj.shipmentTyp;

    }else{

    GlobalObjects * globalObj = (GlobalObjects*)[objectsArr objectAtIndex:indexPath.row];

    userprofimag = [DesignObj initWithImage:CGRectMake(20, 20, 50, 50) img:@""];
    [userprofimag sd_setImageWithURL:[NSURL URLWithString:globalObj.userPhotoUrl] placeholderImage:[UIImage imageNamed:@"user.png"]];
    [cell.contentView addSubview:userprofimag];
    
    cell.backgroundColor = [UIColor clearColor];
    
    UILabel * titlelbl = [[UILabel alloc]initWithFrame:CGRectMake(cell.imageView.frame.origin.x+50 +40, 10, 150, 30)];
    titlelbl.font = [UIFont boldSystemFontOfSize:15.0];
    [cell.contentView addSubview:titlelbl];
    
    UILabel *distncelbl = [[UILabel alloc]initWithFrame:CGRectMake(listtableView.frame.size.width-100, 10, 100, 30)];
    distncelbl.textColor = [DesignObj quikRed];
    distncelbl.font = [UIFont systemFontOfSize:16.0];
    
    [cell.contentView addSubview:distncelbl];
    
    UILabel * addresslbl = [[UILabel alloc]initWithFrame:CGRectMake(100, 90, screenWidth-100, 30)];
    addresslbl.font = [UIFont boldSystemFontOfSize:15.0];
    
    [cell.contentView addSubview:addresslbl];
    
    titlelbl.text = globalObj.username;
//    distncelbl.text = [NSString stringWithFormat:@"%@Miles",globalObj.userDistance];
    
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([serviceArr isEqualToString:@"shipment"]) {
        
        ShipmentTypeObj* shiptypeObj = (ShipmentTypeObj*)[shipArr objectAtIndex:indexPath.row];
        shipmentType.text = [NSString stringWithFormat:@"%@",shiptypeObj.shipmentTyp];
        [shipmentypeTableView removeFromSuperview];
        [Description becomeFirstResponder];
        
    }else{
        GlobalObjects * globalObj = (GlobalObjects*)[objectsArr objectAtIndex:indexPath.row];
        trukrId = [NSString stringWithFormat:@"%@",globalObj.userId];
        //    [self ShowDetailView:indexPath.row];
        [self getTruckImages:indexPath.row];
        [tableShaddowview removeFromSuperview];
        [tblcloseView removeFromSuperview];
        listtableView.hidden = YES;
    }
}


# pragma pickerview method

-(void)loadpickerView{
    
    ViewForValuePicker = [[UIView alloc]initWithFrame:CGRectMake(0, screenHeight - 200, screenWidth, 200)];
    ViewForValuePicker.backgroundColor = [UIColor grayColor];
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, screenWidth, 44)];
    numberToolbar.barStyle = UIBarStyleDefault;//UIBarStyleBlackTranslucent;
    numberToolbar.tintColor = [UIColor colorWithRed:207/255.0 green:26/255.0 blue:26/255.0 alpha:1.0];
    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleDone target:self action:@selector(cancelNumberPad)],
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
    //    [numberToolbar sizeToFit];
    [ViewForValuePicker addSubview:numberToolbar];
    
    shipmentList = [[UIPickerView alloc]initWithFrame:CGRectMake(0, 44, screenWidth, 156)];
    shipmentList.dataSource = self;
    shipmentList.delegate = self;
    shipmentList.backgroundColor = [UIColor whiteColor];
    [ViewForValuePicker addSubview:shipmentList];
    
    [self.view addSubview:ViewForValuePicker];
    
}

-(void)cancelNumberPad{
    
    [ViewForValuePicker removeFromSuperview];
}

-(void)doneWithNumberPad{
    
    [ViewForValuePicker removeFromSuperview];
    
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{

    return shipArr.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    // Component 0 should load the array1 values, Component 1 will have the array2 values
    
    if (pickerView == shipmentList) {
        ShipmentTypeObj * shpmntObj = (ShipmentTypeObj*)[shipArr objectAtIndex:row];
        
        return shpmntObj.shipmentTyp;
    }
    
    return nil;
}

/*
-(UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    
    if (!view)
    {
        view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screenWidth/2, 80)];
        
        if (pickerView == typesView) {
            
            subTypeModel * globalObj = (subTypeModel*)[listofParentTypeArr objectAtIndex:row];
            
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 150, 50)];
            label.font = [UIFont systemFontOfSize:16];
            label.backgroundColor = [UIColor clearColor];
            label.textColor = [UIColor blackColor];
            label.textAlignment = NSTextAlignmentCenter;
            label.tag = 1;
            label.text = [NSString stringWithFormat:@"%@",globalObj.trucktype];
            [view addSubview:label];
            
        }else{
            
            subTypeModel * globalObj = (subTypeModel*)[listofSubTypeArr objectAtIndex:row];
            
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 150, 50)];
            label.font = [UIFont systemFontOfSize:16];
            label.backgroundColor = [UIColor clearColor];
            label.textColor = [UIColor blackColor];
            label.textAlignment = NSTextAlignmentCenter;
            label.tag = 1;
            label.text = [NSString stringWithFormat:@"%@",globalObj.trucktype];
            [view addSubview:label];
            
            
        }
    }
    
    return view;
    
}
*/
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    ShipmentTypeObj* shipObj = (ShipmentTypeObj*)[shipArr objectAtIndex:row];
    shipmentType.text = [NSString stringWithFormat:@"%@",shipObj.shipmentTyp];
    
}


- (void)didSelectedOption:(NSIndexPath*)indexPath withTitle:(NSString*)title{
    
    [self closeMenu];
    
    if (indexPath.row == 0) {
        
        ProfileViewController* profileView = [[ProfileViewController alloc]init];
        profileView.utype = userType;
        
        [self.navigationController pushViewController:profileView animated:YES];
        
    }else if (indexPath.row == 1){
        
        [self closeMenu];
        
//        RecentBookingViewController* recentBookView = [[RecentBookingViewController alloc]init];
////        [self presentViewController:recentBookView animated:YES completion:nil];
//        [self.navigationController pushViewController:recentBookView animated:YES];
        
        [self showAlert:@"Working in it"];
        
    }else if (indexPath.row == 2) {
        
        [self showAlert:@"Will be available in Next link"];
        
    }else if (indexPath.row == 3){
        
        [self showAlert:@"Will be available in Next link"];
        
    }else if (indexPath.row == 4) {
        
        [self showAlert:@"Will be available in Next link"];
        
    }else if (indexPath.row == 5) {
        
     
            
            UIAlertController * alert=   [UIAlertController
                                          alertControllerWithTitle:@"Message"
                                          message:@"Do you really want to quit the QuikMov?"
                                          preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction
                                 actionWithTitle:@"OK"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     
                                     exit(0);
                                     
                                 }];
            
            UIAlertAction* cancel = [UIAlertAction
                                     actionWithTitle:@"Cancel"
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         
                                         [self dismissViewControllerAnimated:YES completion:^{
                                             
                                         }];
                                         
                                     }];
            
            [alert addAction:ok];
            [alert addAction:cancel];
            
            [self presentViewController:alert animated:YES completion:nil];
            
        
}
    
}


-(void)logout{
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Message"
                                  message:@"Do you really want to quit the QuikMov?"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             
                             exit(0);
                             
                         }];
    
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 
                                 [self dismissViewControllerAnimated:YES completion:^{
                                     
                                 }];
                                 
                             }];
    
    
    [alert addAction:ok];
    [alert addAction:cancel];
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(IBAction)bookNow:(id)sender{

    [popView removeFromSuperview];
    [shadowButton removeFromSuperview];
    [shadowView removeFromSuperview];
    
    autocompleteTableView = [[UITableView alloc] initWithFrame:
                             CGRectMake(0, 80, 320, 120) style:UITableViewStylePlain];
    autocompleteTableView.delegate = self;
    autocompleteTableView.dataSource = self;
    autocompleteTableView.scrollEnabled = YES;
    autocompleteTableView.hidden = YES;
    [self.view addSubview:autocompleteTableView];
    
    autocompleteUrls = [[NSMutableArray alloc]init];
    pastUrls = [[NSMutableArray alloc]init];
    
    serviceArr = @"shipment";
    
    [panGest setEnabled:NO];
    
    bookshadowView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screenWidth, screenHeight)];
    bookshadowView.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.5f];
    [self.view addSubview:bookshadowView];
    
    bookshadowButton = [DesignObj initWithButton:CGRectMake(0, 0, screenWidth, screenHeight) tittle:@"" img:@""];
    [bookshadowButton setBackgroundColor:[UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.5f]];
    [bookshadowButton addTarget:self action:@selector(closebookPopView) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:bookshadowButton];
    
    bookpopView = [[UIView alloc] initWithFrame:CGRectMake((screenWidth - (screenWidth - 40))/2, (screenHeight - (screenHeight - 150))/2,screenWidth - 40, screenHeight - 150)];
    bookpopView.layer.cornerRadius = 8;
    [bookpopView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg.png"]]];
    [self.view addSubview:bookpopView];
    
    _scrView = [[UIScrollView alloc]init];
    _scrView.frame = CGRectMake(0, 0, bookpopView.frame.size.width, bookpopView.frame.size.height);
    [bookpopView addSubview:_scrView];
    _scrView.contentSize = CGSizeMake(bookpopView.frame.size.width, bookpopView.frame.size.height);
    
    txtbgImg1 = [DesignObj initWithImage:CGRectMake((bookpopView.frame.size.width-((bookpopView.frame.size.width - 20)))/2,30,(bookpopView.frame.size.width - 20),60) img:@"text_box.png"];
    [_scrView addSubview:txtbgImg1];
    
    // Google AutoComplete
    
    deliveryArea = [DesignObj initWithTextfield:CGRectMake((bookpopView.frame.size.width-((bookpopView.frame.size.width - 20)))/2,30,(bookpopView.frame.size.width - 20),60) placeholder:@"" tittle:@"Delivery Point" delegate:self font:16];
    deliveryArea.textAlignment = NSTextAlignmentCenter;
    deliveryArea.keyboardType = UIKeyboardTypeDefault;
    deliveryArea.delegate = self;
//    [deliveryArea addTarget:self action:@selector(getlatandlong:) forControlEvents:UIControlEventEditingDidBegin];
    deliveryArea.tag = 401;
    deliveryArea.returnKeyType = UIReturnKeyDone;
    [_scrView addSubview:deliveryArea];
 
    txtbgImg2 = [DesignObj initWithImage:CGRectMake((bookpopView.frame.size.width-((screenWidth - 60)))/2,deliveryArea.frame.origin.y+60 +20,(screenWidth - 60),40) img:@"text_box.png"];
    [bookpopView addSubview:txtbgImg2];
    
    shipmentType = [DesignObj initWithTextfield:CGRectMake((bookpopView.frame.size.width-((screenWidth - 60)))/2,deliveryArea.frame.origin.y+60 +20,(screenWidth - 60),40) placeholder:@"" tittle:@"Shipment Type" delegate:self font:16];
    [shipmentType addTarget:self action:@selector(shipmentType:) forControlEvents:UIControlEventEditingDidBegin];
    shipmentType.textAlignment = NSTextAlignmentCenter;
    shipmentType.keyboardType = UIKeyboardTypeAlphabet;
//    [shipmentType setEnabled:NO];
    shipmentType.tag = 402;
    shipmentType.returnKeyType = UIReturnKeyDone;
    [_scrView addSubview:shipmentType];
    
    txtbgImg3 = [DesignObj initWithImage:CGRectMake((bookpopView.frame.size.width-((screenWidth - 60)))/2,shipmentType.frame.origin.y+ 40+20,(screenWidth - 60),60) img:@"text_box.png"];
    [bookpopView addSubview:txtbgImg3];
    
    Description = [DesignObj initWithTextfield:CGRectMake((bookpopView.frame.size.width-((screenWidth - 60)))/2,shipmentType.frame.origin.y+ 40+20,(screenWidth - 60),60) placeholder:@"" tittle:@"Description" delegate:self font:16];
    Description.textAlignment = NSTextAlignmentCenter;
    Description.keyboardType = UIKeyboardTypeDefault;
    Description.tag = 403;
    Description.returnKeyType = UIReturnKeyDone;
    [_scrView addSubview:Description];
    
    if (screenHeight <= 568) {
        
        confirmBooking = [DesignObj initWithButton:CGRectMake(5, Description.frame.origin.y+60 +30, 130, 40) tittle:@"Confirm Booking" img:@"button.png"];
        confirmBooking.titleLabel.font = [UIFont systemFontOfSize:14];
        [confirmBooking addTarget:self action:@selector(confirmBook:) forControlEvents:UIControlEventTouchUpInside];
        [confirmBooking setTitleColor:[DesignObj quikYellow] forState:UIControlStateNormal];
        [_scrView addSubview:confirmBooking];
        
        contactTruck = [DesignObj initWithButton:CGRectMake(bookpopView.frame.size.width-135, Description.frame.origin.y+60 +30, 130, 40) tittle:@"Contact trucker" img:@"button.png"];
        contactTruck.titleLabel.font = [UIFont systemFontOfSize:14];
        [contactTruck addTarget:self action:@selector(contactView:) forControlEvents:UIControlEventTouchUpInside];
        [contactTruck setTitleColor:[DesignObj quikYellow] forState:UIControlStateNormal];
        [_scrView addSubview:contactTruck];
        
    }else{
    
    confirmBooking = [DesignObj initWithButton:CGRectMake(25, Description.frame.origin.y+60 +30, 130, 40) tittle:@"Confirm Booking" img:@"button.png"];
    confirmBooking.titleLabel.font = [UIFont systemFontOfSize:14];
    [confirmBooking addTarget:self action:@selector(confirmBook:) forControlEvents:UIControlEventTouchUpInside];
    [confirmBooking setTitleColor:[DesignObj quikYellow] forState:UIControlStateNormal];
    [_scrView addSubview:confirmBooking];
    
    contactTruck = [DesignObj initWithButton:CGRectMake(bookpopView.frame.size.width-155, Description.frame.origin.y+60 +30, 130, 40) tittle:@"Contact trucker" img:@"button.png"];
    contactTruck.titleLabel.font = [UIFont systemFontOfSize:14];
    [contactTruck addTarget:self action:@selector(contactView:) forControlEvents:UIControlEventTouchUpInside];
    [contactTruck setTitleColor:[DesignObj quikYellow] forState:UIControlStateNormal];
    [_scrView addSubview:contactTruck];
    }
    
    bookcloseView = [UIButton buttonWithType:UIButtonTypeCustom];
    bookcloseView.frame = CGRectMake(bookpopView.frame.size.width-30,0,30,30);
    [bookcloseView addTarget:self action:@selector(closebookPopView) forControlEvents:UIControlEventTouchUpInside];
    [bookcloseView setImage:[UIImage imageNamed:@"close.png"] forState:UIControlStateNormal];
    [bookpopView addSubview:bookcloseView];
    

}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    if (textField.tag == 401) {
        
        GMSPlacesClient *placesClient= [[GMSPlacesClient alloc] init];
        
        [placesClient autocompleteQuery:textField.text bounds:nil
                                 filter:kGMSPlacesAutocompleteTypeFilterNoFilter
                               callback:^(NSArray *results, NSError *error) {
                                   if (error != nil) {
                                       NSLog(@"Autocomplete error %@", [error localizedDescription]);
                                       return;
                                   }
                                   
                                   for (GMSAutocompletePrediction* result in results) {
                                       
                                       NSLog(@"Result '%@' with placeID %@", result.attributedFullText.string, result.placeID);
                                   }
                               }];
    }
    
}


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField.tag == 402) {
        
        [self showshipmenttypeService];
        return NO;
    }
    return YES;
}


-(IBAction)shipmentType:(id)sender{
    [deliveryArea resignFirstResponder];
    [Description resignFirstResponder];
    [self showshipmenttypeService];
}

-(void)showshipmenttypeService{
    
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
    [SVProgressHUD show];
    
    NSURL* url = [NSURL URLWithString:@"http://quikmov.quikmovapp.com/auth/getShipmenttype"];
    NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession* session = [NSURLSession sessionWithConfiguration:config ];
    
    NSMutableURLRequest*request = [[NSMutableURLRequest alloc]initWithURL:url];
    request.HTTPMethod = @"POST";
    
    NSString* bodycntnt = [NSString stringWithFormat:@"button click=%@",shipmentType.text];
    
    NSData *data = [bodycntnt dataUsingEncoding:NSUTF8StringEncoding];
    
    NSURLSessionUploadTask *uploadtask = [session uploadTaskWithRequest:request fromData:data completionHandler:^(NSData*data, NSURLResponse*response, NSError*error){
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
            [self shipserviceSuccess:data];
        });
    }];
    
    [uploadtask resume];
}

- (void)shipserviceSuccess:(NSData *)data{
    
    if (data == nil) {
        return;
    }
    NSString* str = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
    NSError * error;
    NSDictionary* dic = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    NSLog(@"%@******%@,%@",str,[dic allValues],[dic objectForKey:@"response"]);
    
    NSString * Result = [dic objectForKey:@"status"];
    
    NSLog(@"%@",Result);
    
    if ([Result isEqualToString:@"success"]){
        NSArray * arr = [dic objectForKey:@"result"];
        
        shipArr = [[NSMutableArray alloc]init];
        
        for (int index = 0; index < [arr count]; index++) {
            
            ShipmentTypeObj * shipObj = [[ShipmentTypeObj alloc]init];
            NSDictionary * objDic = [arr objectAtIndex:index];
            
            shipObj.shipmentTyp = [objDic objectForKey:@"shipmenttype"];
//            globleObj.trucktype = [objDic objectForKey:@"trucktype"];
//            globleObj.parent_id = [[objDic objectForKey:@"parent_id"] integerValue];
//            globleObj.subcat = [objDic objectForKey:@"subcat"];
            
            [shipArr addObject:shipObj];
        }
        [self loadpickerView];
//        [self getparentTypes];
        
    }else{
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Message" message:[NSString stringWithFormat:@"%@",[dic objectForKey:@"result"]] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
}

-(void)loadshiplistView{
    
//    NSInteger height;
//    if (shipArr.count*44>screenHeight-170-70) {
//        height = screenHeight-100-40-30;
//    }else{
//        height = shipArr.count*44;
//    }
    //   (, trucklbl.frame.origin.y +40 +20, 200, 30)
    
    shipmentypeTableView = [[UITableView alloc]initWithFrame:CGRectMake((bookpopView.frame.size.width-((screenWidth - 60)))/2, shipmentType.frame.origin.y +39, screenWidth-60, 250) style:UITableViewStylePlain];
    shipmentypeTableView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg.png"]];
    shipmentypeTableView.delegate = self;
    shipmentypeTableView.dataSource = self;
    //    trucktypestblView.layer.cornerRadius = 5;
    shipmentypeTableView.separatorColor = [DesignObj quikRed];
    shipmentypeTableView.layer.borderWidth = 1.0;
    shipmentypeTableView.layer.borderColor = [DesignObj quikRed].CGColor;
    [bookpopView addSubview:shipmentypeTableView];

    
}

- (IBAction)getlatandlong:(UITextField *)sender {
    
    
    /*
     GMSAutocompleteViewController *acController = [[GMSAutocompleteViewController alloc] init];
    acController.delegate = self;
    [self presentViewController:acController animated:YES completion:nil];
    */
    
    
    /*
    
    if(forwardGeocoder == nil)
    {
        forwardGeocoder = [[CLGeocoder alloc] init];
    }
    NSString *address = deliveryArea.text;
    [forwardGeocoder geocodeAddressString:address completionHandler:^(NSArray *placemarks, NSError *error) {
        
        if(placemarks.count > 0)
        {
            CLPlacemark *placemark = [placemarks objectAtIndex:0];
            //            self.outputLabel.text = placemark.location.description;
            forwardlat = [NSString stringWithFormat:@"%f",placemark.location.coordinate.latitude];
            forwardlong = [NSString stringWithFormat:@"%f",placemark.location.coordinate.longitude];
            NSLog(@"%@",placemark.location.description);
        }
        else if (error.domain == kCLErrorDomain)
        {
            switch (error.code)
            {
                case kCLErrorDenied:
                    [self showAlert:@"checking"];
                    break;
                case kCLErrorNetwork:
                    [self showAlert:@"No Network"];
                    break;
                case kCLErrorGeocodeFoundNoResult:
                    [self showAlert:@"No Result Found"];
                    break;
                default:
                    [self showAlert:[NSString stringWithFormat:@"%@",error.localizedDescription]];
                    break;
            }
        }
        else
        {
            [self showAlert:[NSString stringWithFormat:@"%@",error.localizedDescription]];
            
        }
        
    }];
     
     */
    
//    [shipmentType becomeFirstResponder];
    
    
}

-(void)closebookPopView{
    
    [bookshadowButton removeFromSuperview];
    [bookshadowView removeFromSuperview];
    [bookpopView removeFromSuperview];
    [ViewForValuePicker removeFromSuperview];
    
}

-(void)closePopView{
    [closeView removeFromSuperview];
    [popView removeFromSuperview];
    [shadowView removeFromSuperview];
    [shadowButton removeFromSuperview];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    
//        NSInteger nextTag = textField.tag +001;
//        // Try to find next responder
//        UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
//        if (nextResponder) {
//            // Found next responder, so set it.
//            [nextResponder becomeFirstResponder];
//            
//        } else {
//            // Not found, so remove keyboard.
//            [textField resignFirstResponder];
//            [self bookService:textField.tag];
    
    if ([deliveryArea resignFirstResponder]) {
        [self getlatandlong:textField];
    }
    else if ([shipmentType resignFirstResponder]) {
        [Description becomeFirstResponder];
    }else if ([Description resignFirstResponder]){
        
        [self bookService:textField.tag];
        
    }
//        }
        return NO; // We do not want UITextField to insert line-breaks.
    }


-(IBAction)confirmBook:(id)sender{
    
    
    [deliveryArea resignFirstResponder];
    [shipmentType resignFirstResponder];
    [Description resignFirstResponder];
    
    if (deliveryArea.text.length==0) {
        [self showAlert:@"Please enter your Delivery point"];
    }else if (shipmentType.text.length==0) {
        [self showAlert:@"Please enter your Shipment Type"];
    }else if (Description.text.length==0) {
        [self showAlert:@"Please enter Description"];
    }else{
        
        [self bookService:[sender tag]];
    }
    
}

-(void)bookService:(NSInteger)tag{
    
//    NSInteger tag;
//    GlobalObjects* gblObj = (GlobalObjects*)[objectsArr objectAtIndex:tag];
//
//    trukrId = [NSString stringWithFormat:@"%@",gblObj.userId];
    
    if ([Reachability reachabilityForInternetConnection]) {
        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
        [SVProgressHUD show];
        
        NSURL* url = [NSURL URLWithString:@"http://quikmov.quikmovapp.com/auth/booking"];
        
        NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession* session = [NSURLSession sessionWithConfiguration:config ];
        
        NSMutableURLRequest*request = [[NSMutableURLRequest alloc]initWithURL:url];
        request.HTTPMethod = @"POST";
        
        
        NSUserDefaults* userStoredetails = [NSUserDefaults standardUserDefaults];
        userID = [userStoredetails objectForKey:@"user_id"];
        NSLog(@"%@",userID);
        
        // inputs : userid,truckerid, pickupfromlat,pickupfromlon,deliverytolat.deliverytolon
        
        NSString* bodycntnt = [NSString stringWithFormat:@"userid=%@&truckerid=%@&pickupfromlat=%@&pickupfromlon=%@&deliverytolat=%@&deliverytolon=%@&shipmenttypeid=%@&description=%@",userID,trukrId,lat,log,forwardlat,forwardlong,shipmentType.text,Description.text];
        
        NSData *data = [bodycntnt dataUsingEncoding:NSUTF8StringEncoding];
        
        
        NSURLSessionUploadTask *uploadtask = [session uploadTaskWithRequest:request fromData:data completionHandler:^(NSData*data, NSURLResponse*response, NSError*error){
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                
                NSString* str = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
                NSError* error;
                
                NSDictionary* dic = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                NSLog(@"%@******%@,%@",str,[dic allValues],[dic objectForKey:@"response"]);
                
                // userId = [[dic objectForKey:@"message"] stringValue];
                NSString * Result = [dic objectForKey:@"status"];
                
                NSLog(@"%@",Result);
                if ([Result isEqualToString:@"success"]){
                    
                    
                    UIAlertController * alert=   [UIAlertController
                                                  alertControllerWithTitle:@"Message"
                                                  message:@"Booking Successful\nIf you doesn't get Respose from the Trucker\nPlease contact the Trucker in the next 5 Minutes"
                                                  preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* ok = [UIAlertAction
                                         actionWithTitle:@"OK"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action)
                                         {
                                             
                                             
                                             [bookpopView removeFromSuperview];
                                             [bookshadowButton removeFromSuperview];
                                             [bookcloseView removeFromSuperview];
                                             [bookshadowView removeFromSuperview];
                                             [deliveryArea removeFromSuperview];
                                             [shipmentType removeFromSuperview];
                                             [Description removeFromSuperview];
                                             [closeView removeFromSuperview];
                                             [popView removeFromSuperview];
                                             [shadowView removeFromSuperview];
                                             [shadowButton removeFromSuperview];
                                             listtableView.hidden =YES;
                                             [bookcloseView removeFromSuperview];
                                             [bookshadowButton removeFromSuperview];
                                             [self uploadcurrentLocation];
                                             
                                             
                                         }];
                    
                    
                    [alert addAction:ok];
                    
                    [self presentViewController:alert animated:YES completion:nil];
                    
                    
                }else{
                    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Message" message:[NSString stringWithFormat:@"%@",[dic objectForKey:@"result"]] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                    [alert show];
                }
                
            });
        }];
        [uploadtask resume];
    }else{
        [self showAlert:@"No internet connectivity"];
    }
    
    
    
}

- (void)showAlert:(NSString *)message{
    
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Message"
                                 message:message
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [alert dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    
    [alert addAction:ok];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
