//
//  LoginViewController.h
//  QuikMov
//
//  Created by CSCS on 1/25/16.
//  Copyright © 2016 CSCS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreTelephony/CTCarrier.h>
#import <CoreTelephony/CTTelephonyNetworkInfo.h>

@interface LoginViewController : UIViewController<UITextFieldDelegate>{
    
    UIButton* login;
    UIButton* back;
}

@property (nonatomic, copy) NSString* userType;
@property (strong, nonatomic)NSString* usrId;
@property (strong, nonatomic)UIScrollView* scrView;

@end
