//
//  TruckRegisterViewController.h
//  QuikMov
//
//  Created by CSCS on 1/28/16.
//  Copyright © 2016 CSCS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface TruckRegisterViewController : BaseViewController<UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITableViewDataSource,UITableViewDelegate>{
}

@property(nonatomic, strong)UIImageView* truckImg1;
@property(nonatomic, strong)UIImageView* truckImg2;
@property(nonatomic, strong)UIImageView* truckImg3;
@property(nonatomic, strong)UIImageView* truckImg4;
@property(nonatomic, copy)UIImage* truckregImg;
@property(nonatomic, copy)NSString* truckfname;;
@property(nonatomic, copy)NSString* truckmname;
@property(nonatomic, copy)NSString* trucklname;
@property(nonatomic, copy)NSString* trucknumber;
@property(nonatomic, copy)NSString* truckemail;
@property(nonatomic, copy)NSString* truckpass;
@property(nonatomic, copy)NSString* truckcpass;
@property(nonatomic, copy)NSString* truckdateob;
@property(nonatomic, copy)NSString* truckerUsetyp;
@property(nonatomic, copy)NSString* truckssn;
@property(nonatomic, copy)NSString* truckaddress;
@property(nonatomic, copy)NSString* truckpostal;
@property(nonatomic, copy)NSString* truckregion;
@property(nonatomic, copy)NSString* truckLocation;
@property(nonatomic, copy)NSString* truckstreetaddr;

@end
