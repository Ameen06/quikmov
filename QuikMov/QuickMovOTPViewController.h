//
//  QuickMovOTPViewController.h
//  QuikMov
//
//  Created by Ajmal Khan on 3/4/16.
//  Copyright © 2016 CSCS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuickMovOTPViewController : UIViewController{
    
//    CGFloat screenWidth;
//    CGFloat screenHeight;
}
@property(nonatomic,copy)UIImage* profImag;
@property(nonatomic, copy)NSString * fname;
@property(nonatomic, copy)NSString * mname;
@property(nonatomic, copy)NSString * lname;
@property(nonatomic, copy)NSString * email;
@property(nonatomic, copy)NSString * mobNum;
@property(nonatomic, copy)NSString * pass;
@property(nonatomic, copy)NSString * conpass;
@property(nonatomic, copy)NSString * dob;
@property(nonatomic, copy)NSString * regType;
@property(nonatomic, copy)UIImage * truckregImg;
@property(nonatomic, copy)NSString* truckfname1;
@property(nonatomic, copy)NSString* truckmname1;
@property(nonatomic, copy)NSString* trucklname1;
@property(nonatomic, copy)NSString* trucknumber1;
@property(nonatomic, copy)NSString* truckemail1;
@property(nonatomic, copy)NSString* truckpass1;
@property(nonatomic, copy)NSString* truckcpass1;
@property(nonatomic, copy)NSString* truckdateob1;
@property(nonatomic, copy)NSString* truckerUsetyp1;
@property(nonatomic, copy)NSString* truckssn1;
@property(nonatomic, copy)NSString* truckaddress1;
@property(nonatomic, copy)NSString* truckpostal1;
@property(nonatomic, copy)NSString* truckregion1;
@property(nonatomic, copy)NSString* truckLocation1;
@property(nonatomic, copy)NSString* truckstreetaddr1;
@property(nonatomic, copy)NSString*truckType;
@property(nonatomic, copy)NSString* heightofTruck;
@property(nonatomic, copy)NSString*widthofTruck;
@property(nonatomic, copy)NSString*lengthofTruck;
@property(nonatomic, copy)NSString*regno;
@property(nonatomic, copy)NSString*insurenceValidity;
@property(nonatomic, copy)NSString*insurenceno;
@property(nonatomic, copy)NSString*serviceCategories;
@property(nonatomic, copy)NSString*Equipments;
@property(nonatomic, copy)NSString*accountNo;
@property(nonatomic, copy)NSString*routingNo;
@property(nonatomic, copy)UIImage* truckregImgs1;
@property(nonatomic, copy)UIImage* truckregImgs2;
@property(nonatomic, copy)UIImage* truckregImgs3;
@property(nonatomic, copy)UIImage* truckregImgs4;
@property(nonatomic, copy)NSString * copiOTP;
@property(nonatomic, copy)NSString * mobilenumbr;
@property (strong, nonatomic)UIScrollView * scrView;

@end
