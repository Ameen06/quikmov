//
//  QuickMenuViewController.m
//  QuikMov
//
//  Created by CSCS on 2/9/16.
//  Copyright © 2016 CSCS. All rights reserved.
//

#import "QuickMenuViewController.h"
#import "DesignObj.h"
#import "GlobalObjects.h"
#import "SVProgressHUD.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "MHGallery.h"
#import "SettingViewController.h"
#import "JTSImageViewController.h"
#import "RecentBookingViewController.h"

@interface QuickMenuViewController ()<UITableViewDataSource, UITableViewDelegate>{
    
    CGFloat screenWidth;
    CGFloat screenHeight;
    NSArray * menuItems;
    NSArray * menuIcons;
    NSMutableArray * objectsArr;
    NSString* userID;
    NSString* username;
    UIImageView* profimg;
    GlobalObjects* globalObj;
    UIButton* taptoSee;
}

@property (strong, nonatomic) UITableView * menuTableView;

@end


@implementation QuickMenuViewController
@synthesize delagte;
@synthesize userTyp;

- (void)viewDidLoad {
    [super viewDidLoad];
    screenWidth = [UIScreen mainScreen].bounds.size.width;
    screenHeight = [UIScreen mainScreen].bounds.size.height;
    self.view.backgroundColor = [UIColor clearColor];
    
        [self loadService];
    
//    menuIcons = @[@"home",@"profile",@"logBook",@"faq",@"contact",@"exit"];
    
}


-(void)loadService{
    
    NSURL* url = [NSURL URLWithString:@"http://quikmov.quikmovapp.com/auth/getUserDetails"];
    NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession* session = [NSURLSession sessionWithConfiguration:config ];
    
    NSMutableURLRequest*request = [[NSMutableURLRequest alloc]initWithURL:url];
    request.HTTPMethod = @"POST";
    
    NSUserDefaults* userStoredetails = [NSUserDefaults standardUserDefaults];
    userID = [userStoredetails objectForKey:@"user_id"];
    NSLog(@"%@",userID);
    
    
    NSString* bodycntnt = [NSString stringWithFormat:@"userid=%@",userID];
    
    NSData *data = [bodycntnt dataUsingEncoding:NSUTF8StringEncoding];
    
    NSURLSessionUploadTask *uploadtask = [session uploadTaskWithRequest:request fromData:data completionHandler:^(NSData*data, NSURLResponse*response, NSError*error){
        
        dispatch_async(dispatch_get_main_queue(), ^{

            [self serviceSuccess:data];
            
        });
    }];
    
    [uploadtask resume];
}

- (void)serviceSuccess:(NSData *)data{
    
    if (data == nil) {
        return;
    }
    
    NSString* str = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
    NSError * error;
    NSDictionary* dic = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    NSLog(@"%@******%@,%@",str,[dic allValues],[dic objectForKey:@"response"]);
    
    NSString * Result = [dic objectForKey:@"status"];
    
    NSLog(@"%@",Result);
    
    if ([Result isEqualToString:@"success"]){
        
        NSDictionary * objDic = [dic objectForKey:@"result"];
        
        globalObj = [[GlobalObjects alloc]init];
        
        globalObj.username = [objDic objectForKey:@"user_name"];
        globalObj.userId = [objDic objectForKey:@"usertype"];
        userTyp = globalObj.userId;
        globalObj.userPhotoUrl = [NSString stringWithFormat:@"http://quikmov.quikmovapp.com%@",[objDic objectForKey:@"userfilepath"]];
        [self callArr];
        
    }else{
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Message" message:[NSString stringWithFormat:@"%@",[dic objectForKey:@"result"]] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    
}

-(void)callArr{
    
    if ([userTyp isEqualToString:@"1"]) {
        
        menuItems = @[@"Profile",@"Recent Bookings",@"Payment Details",@"About-Us",];
        [self showMenuView];
    }else{
        
        menuItems = @[@"Profile",@"Recent Bookings",@"Payment Details",@"About-Us",@"Exit"];
        [self showMenuView];
    }

}

- (void)showMenuView{
    
    UIView * guestView = [[UIView alloc]initWithFrame:CGRectMake(0, 0,screenWidth-50, 250)];
    guestView.backgroundColor = [UIColor colorWithRed:249/255.0 green:177/255.0 blue:39/255.0 alpha:0.5];
    [self.view addSubview:guestView];
    
    taptoSee =[DesignObj initWithButton:CGRectMake(0, 0, screenWidth-50, guestView.frame.size.height) tittle:@"" img:@""];
    [taptoSee addTarget:self action:@selector(showProfileImage:) forControlEvents:UIControlEventTouchUpInside];
    [guestView addSubview:taptoSee];
    
    profimg = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, screenWidth-50, guestView.frame.size.height)];
    dispatch_async(dispatch_get_main_queue(), ^{
    [profimg sd_setImageWithURL:[NSURL URLWithString:globalObj.userPhotoUrl] placeholderImage:[UIImage imageNamed:@"user.png"]];
    });
    
    [guestView addSubview:profimg];
    
    UILabel * namelbl = [DesignObj initWithLabel:CGRectMake(50, guestView.frame.size.height - 40, 180, 30) title:[NSString stringWithFormat:@"%@",globalObj.username] font:16 txtcolor:[UIColor whiteColor]];
    namelbl.font = [UIFont systemFontOfSize:18];
    namelbl.textAlignment = NSTextAlignmentLeft;
    [guestView addSubview:namelbl];
    
    UIButton * settingsBtn = [DesignObj initWithButton:CGRectMake(screenWidth - 100, 20, 40, 40) tittle:@"" img:@"setting.png"];
    [settingsBtn addTarget:self action:@selector(openSetting) forControlEvents:UIControlEventTouchUpInside];
    [guestView addSubview:settingsBtn];
    
    self.menuTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 250, screenWidth-50, screenHeight) style:UITableViewStylePlain];
    [self.menuTableView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg.png"]]];
    self.menuTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.menuTableView.delegate=self;
    self.menuTableView.dataSource=self;
    self.menuTableView.rowHeight = 60.0f;
    [self.view addSubview:self.menuTableView];
}

-(void)openSetting{
    
    SettingViewController* settngs = [[SettingViewController alloc]init];
    settngs.usrtyp = userTyp;
    
    UINavigationController *setingsNav =
    [[UINavigationController alloc] initWithRootViewController:settngs];
    
    [self presentViewController:setingsNav animated:YES completion:nil];
}

-(IBAction)showProfileImage:(id)sender{
    
 /*
    JTSImageInfo *imageInfo = [[JTSImageInfo alloc] init];
    imageInfo.image = profimg.image;
//    imageInfo.referenceRect = CGRectMake((screenWidth - (screenWidth - 40))/2, (screenHeight - (screenHeight - 150))/2,screenWidth - 40, screenHeight - 150);
//    imageInfo.referenceView = UIViewContentModeCenter;
    
    // Setup view controller
    JTSImageViewController *imageViewer = [[JTSImageViewController alloc]
                                           initWithImageInfo:imageInfo
                                           mode:JTSImageViewControllerMode_Image
                                           backgroundStyle:JTSImageViewControllerBackgroundOption_Scaled];
    imageViewer.view.frame = CGRectMake((screenWidth - (screenWidth - 40))/2, (screenHeight - (screenHeight - 150))/2,screenWidth - 40, screenHeight - 150);
    
    // Present the view controller.
    [imageViewer showFromViewController:self transition:JTSImageViewControllerTransition_FromOriginalPosition];
    
    */
    
    MHGalleryItem *landschaft1 = [[MHGalleryItem alloc]initWithImage:profimg.image];
    
    NSArray *galleryItem = @[landschaft1];
    
    MHGalleryController *gallery = [MHGalleryController galleryWithPresentationStyle:MHGalleryViewModeImageViewerNavigationBarHidden];
    gallery.galleryItems = galleryItem;
    gallery.presentingFromImageView = nil;
    gallery.presentationIndex = 0;
    gallery.UICustomization.showOverView =NO;
    gallery.UICustomization.barStyle = UIBarStyleBlackTranslucent;
    gallery.UICustomization.hideShare = YES;
    gallery.UICustomization.barTintColor = [DesignObj quikRed];
    gallery.UICustomization.barButtonsTintColor = [DesignObj quikYellow];
    
    __weak MHGalleryController *blockGallery = gallery;
    
    gallery.finishedCallback = ^(NSInteger currentIndex,UIImage *image,MHTransitionDismissMHGallery *interactiveTransition,MHGalleryViewMode viewMode){
        if (viewMode == MHGalleryViewModeOverView) {
            [blockGallery dismissViewControllerAnimated:YES completion:^{
                [self setNeedsStatusBarAppearanceUpdate];
            }];
        }else{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [blockGallery dismissViewControllerAnimated:YES dismissImageView:nil completion:^{
                    
                    [self setNeedsStatusBarAppearanceUpdate];
                }];
            });
        }
    };
    [self presentMHGalleryController:gallery animated:YES completion:nil];
    

}



#pragma mark - tableView Functions.
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [menuItems count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    // This will create a "invisible" footer
    return 0.001;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *MyIdentifier = @"MyIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:MyIdentifier];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor clearColor];
    
    UILabel * titlelbl = [DesignObj initWithLabel:CGRectMake(50, 10, 150, 40) title:[menuItems objectAtIndex:indexPath.row] font:16 txtcolor:[DesignObj quikRed]];
    titlelbl.font = [UIFont systemFontOfSize:16];
    titlelbl.textAlignment = NSTextAlignmentLeft;
    [cell.contentView addSubview:titlelbl];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [delagte didSelectedOption:indexPath withTitle:[menuItems objectAtIndex:indexPath.row]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
