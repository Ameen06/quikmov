//
//  SettingViewController.m
//  QuikMov
//
//  Created by CSCS on 23/03/16.
//  Copyright © 2016 CSCS. All rights reserved.
//

#import "SettingViewController.h"
#import "DesignObj.h"
#import "Reachability.h"
#import "SVProgressHUD.h"
#import "FeedBackViewController.h"


@interface SettingViewController ()<UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource>{
    
    UILabel* topIc;
    UILabel* sideLbl;
    UITextField * rangeField;
    NSString* userID;
    UILabel* noteLbl;
    UISwitch* switchStatus;
    UIPickerView * milesView;
    UIPickerView * milesView2;
    NSArray* ranges;
    UIView * rangeShadow;
    UIButton * rangecancel;
    NSArray * milesrange;
    UIButton * feedbackView;
}

@property(nonatomic, strong)UIScrollView * scrView;

@end

@implementation SettingViewController
@synthesize usrtyp;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"QuikMov";
    
    self.navigationController.navigationBarHidden = NO;
    
      self.navigationController.navigationBar.tintColor = [DesignObj quikYellow];
    
    screenWidth = [UIScreen mainScreen].bounds.size.width;
    screenHeight = [UIScreen mainScreen].bounds.size.height;
    
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [DesignObj quikYellow]}];
    self.navigationController.navigationBar.barTintColor = [DesignObj quikRed];
    
    self.navigationController.navigationBar.tintColor = [DesignObj quikYellow];
    
    UIImageView*bgView = [DesignObj initWithImage:CGRectMake(0, 0, screenWidth, screenHeight) img:@"bg"];
    [self.view addSubview:bgView];
    
    
    _scrView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, screenHeight )];
    _scrView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:_scrView];
    
    _scrView.contentSize = CGSizeMake(screenWidth, screenHeight);
    
    UIButton* cancelBtn = [DesignObj initWithButton:CGRectMake(10, 10, 70, 20) tittle:@"Cancel" img:@""];
    [cancelBtn addTarget:self action:@selector(cancelSetting) forControlEvents:UIControlEventTouchUpInside];
    [cancelBtn setTitleColor:[DesignObj quikYellow] forState:UIControlStateNormal];
    [_scrView addSubview:cancelBtn];
    
    UIBarButtonItem* leftbarbtn = [[UIBarButtonItem alloc]initWithCustomView:cancelBtn];
    self.navigationItem.leftBarButtonItem = leftbarbtn;

    UIImageView * settingsIcon = [DesignObj initWithImage:CGRectMake(((screenWidth - 120)/2)-15 -5, 100, 25, 25) img:@"setting.png"];
    [_scrView addSubview:settingsIcon];
    
    topIc = [DesignObj initWithLabel:CGRectMake((screenWidth - 120)/2, 100, 120, 30) title:@"Settings" font:18 txtcolor:[DesignObj quikRed]];
    topIc.textAlignment = NSTextAlignmentCenter;
    [_scrView addSubview:topIc];
    
    if ([usrtyp isEqualToString:@"1"]) {
        
        sideLbl = [DesignObj initWithLabel:CGRectMake(30, topIc.frame.origin.y+50 + 20, 100, 30) title:@"Truck Range" font:16 txtcolor:[DesignObj quikRed]];
        [_scrView addSubview:sideLbl];
        
        UIButton *logoutBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [logoutBtn addTarget:self action:@selector(logout) forControlEvents:UIControlEventTouchUpInside]; //adding action
        [logoutBtn setTitleColor:[DesignObj quikYellow] forState:UIControlStateNormal];
        [logoutBtn setImage:[UIImage imageNamed:@"logout.png"] forState:UIControlStateNormal];
        logoutBtn.frame = CGRectMake(0 ,0,34,34);
        
        UIBarButtonItem* rytbarbtn = [[UIBarButtonItem alloc]initWithCustomView:logoutBtn];
        self.navigationItem.rightBarButtonItem = rytbarbtn;

        
    }else{
        
        sideLbl = [DesignObj initWithLabel:CGRectMake(30, topIc.frame.origin.y+50 + 20, 100, 30) title:@"User Range" font:16 txtcolor:[DesignObj quikRed]];
        [_scrView addSubview:sideLbl];
        
        NSUserDefaults * user = [NSUserDefaults standardUserDefaults];

        
        switchStatus= [[UISwitch alloc]initWithFrame:CGRectMake(screenWidth-120, 84, 120, 30)];
        [switchStatus setOn:[user boolForKey:@"toggle"]];
        [switchStatus setTintColor:[DesignObj quikYellow]];
        [switchStatus setOnTintColor:[DesignObj quikYellow]];
        [switchStatus addTarget:self action:@selector(switchToggled:) forControlEvents:UIControlEventValueChanged];
        [switchStatus setOnImage:[UIImage imageNamed:@"close.png"]];
        [self.view addSubview:switchStatus];
        
       UIBarButtonItem* rightbarbtn = [[UIBarButtonItem alloc]initWithCustomView:switchStatus];
        self.navigationItem.rightBarButtonItem = rightbarbtn;

        
    }

    
     rangeField = [DesignObj initWithTextfield:CGRectMake(sideLbl.frame.origin.x+ 120 + 30, topIc.frame.origin.y+50 + 20, 120, 30)  placeholder:@"" tittle:@"Range" delegate:self font:16];
    rangeField.textAlignment = NSTextAlignmentCenter;
    [rangeField addTarget:self action:@selector(checkLimit) forControlEvents:UIControlEventEditingDidBegin];
    rangeField.tag = 102;
    [rangeField setBackground:[UIImage imageNamed:@"text_box.png"]];
    [_scrView addSubview:rangeField];
    
//    UIImageView * txtbg_Img = [[UIImageView alloc]initWithFrame:CGRectMake(sideLbl.frame.origin.x+ 120 + 30, topIc.frame.origin.y+30 + 20, 120, 30)];
//    [txtbg_Img setImage:[UIImage imageNamed:@"text_box.png"]];
//    [self.view addSubview:txtbg_Img];
    
    
    if ([usrtyp isEqualToString:@"1"]) {
        
        noteLbl = [DesignObj initWithLabel:CGRectMake(sideLbl.frame.origin.x+ 90, rangeField.frame.origin.y + 30 + 20, 200, 30) title:@"(Search Trucks within this range(miles))" font:10 txtcolor:[DesignObj quikRed]];
        [noteLbl setTextAlignment:NSTextAlignmentLeft];
        [_scrView addSubview:noteLbl];
        
    }else{
        
        noteLbl = [DesignObj initWithLabel:CGRectMake(sideLbl.frame.origin.x+ 70, rangeField.frame.origin.y + 30 + 20, 200, 30) title:@"(Available to customer within this range(miles) only)" font:10 txtcolor:[DesignObj quikRed]];
        [noteLbl setTextAlignment:NSTextAlignmentLeft];
        [_scrView addSubview:noteLbl];
    }
    
    
    UIView* lineView = [[UIView alloc]initWithFrame:CGRectMake((screenWidth- (screenWidth - 30))/2, noteLbl.frame.origin.y+ 30 +30, (screenWidth - 30), 1)];
    lineView.backgroundColor = [DesignObj quikRed];
    [_scrView addSubview:lineView];
    
    feedbackView = [DesignObj initWithButton:CGRectMake(30, lineView.frame.origin.y+ 20, 100, 30) tittle:@"FeedBack" img:@""];
    [feedbackView setTitleColor:[DesignObj quikRed] forState:UIControlStateNormal];
    [feedbackView addTarget:self action:@selector(feedbackScreen) forControlEvents:UIControlEventTouchUpInside];
    [_scrView addSubview:feedbackView];
    
    
    // Do any additional setup after loading the view.
}

- (void) switchToggled:(id)sender {
    
    NSUserDefaults * user = [NSUserDefaults standardUserDefaults];
    
    switchStatus = (UISwitch *)sender;
    
    if ([switchStatus isOn]) {
        
        [user setBool:YES forKey:@"toggle"];
//        statusCheck =@"1";
//        hideMapview.hidden = YES;
//        [self uploadcurrentLocation];
        [self showAlert:@"You are Active now\n(Now you are visible to your Customers)"];
        
    } else {
        
        [user setBool:NO forKey:@"toggle"];
//        statusCheck =@"0";
//        hideMapview.hidden = NO;
//        [self uploadcurrentLocation];
        [self showAlert:@"You are InActive now\n(The Customers can't see and Book you)"];
        
    }
    [user synchronize];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField.tag == 102) {
        
        [self checkLimit];
        return NO;
    }
    return YES;
}

-(void)feedbackScreen{
    
    FeedBackViewController * feedBack = [[FeedBackViewController alloc]init];
    
    UINavigationController * feedBackNav =
    [[UINavigationController alloc] initWithRootViewController:feedBack];
    
    [self presentViewController:feedBackNav animated:YES completion:nil];
}


-(void)checkLimit{
    
//    rangeShadow = [[UIView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, screenHeight)];
//    rangeShadow.backgroundColor = [UIColor blackColor];
//    rangeShadow.alpha = 0.5;
//    [self.view addSubview:rangeShadow];
    
//    rangecancel = [DesignObj initWithButton:CGRectMake(0, 0, screenWidth, screenHeight) tittle:@"" img:@""];
//    [rangecancel addTarget:self action:@selector(closerangeview) forControlEvents:UIControlEventTouchUpInside];
//    [self.view addSubview:rangecancel];
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, screenWidth, 44)];
    numberToolbar.barStyle = UIBarStyleDefault;//UIBarStyleBlackTranslucent;
    numberToolbar.tintColor = [UIColor colorWithRed:207/255.0 green:26/255.0 blue:26/255.0 alpha:1.0];
    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleDone target:self action:@selector(cancelNumberPad)],
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad:)]];
    [numberToolbar sizeToFit];

    
    ranges = [[NSArray alloc]initWithObjects:@"5",@"10",@"15",@"20 ",@"25 ",@"30",@"35",@"40",@"45",@"50", nil];
    
    milesView = [[UIPickerView alloc]init];
    milesView.frame = CGRectMake(0, screenHeight - 200, screenWidth/2, 200);
    milesView.backgroundColor = [UIColor whiteColor];
    milesView.delegate = self;
    milesView.dataSource = self;
    [self.view addSubview:milesView];
    
    milesView2 = [[UIPickerView alloc]init];
    milesView2.frame = CGRectMake((screenWidth - (screenWidth/2)), screenHeight - 200, screenWidth/2, 200);
    milesView2.backgroundColor = [UIColor whiteColor];
    milesView2.delegate = self;
    milesView2.dataSource = self;
    [self.view addSubview:milesView2];

    
//    NSInteger inte = [rangeField.text intValue];
//    
//    if (inte > 50) {
//        [self showAlert:@"You have Exceeded Maximum Range\n(Maximum range 50 Miles)"];
//    }else{
//        
////        [self rangeoftrucks];
//    }
}

-(void)cancelNumberPad{
    
    rangeField.text = @"";
}

-(IBAction)doneWithNumberPad:(id)sender{

    rangeField.text = [NSString stringWithFormat:@"%@",[ranges objectAtIndex:[sender tag]]];
    [self closerangeview];

}

-(void)closerangeview{
    
    [milesView removeFromSuperview];
    [milesView2 removeFromSuperview];
//    [rangecancel removeFromSuperview];
    
    [self rangeoftrucks];
    
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    
    if (pickerView == milesView) {
        
        return ranges.count;
        
    }else{
        
        return 1;
    }
}

-(UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    
    if (!view)
    {
        view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screenWidth/2, 80)];
        
        if (pickerView == milesView) {
            
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 150, 50)];
            label.font = [UIFont systemFontOfSize:16];
            label.backgroundColor = [UIColor clearColor];
            label.textColor = [UIColor blackColor];
            label.textAlignment = NSTextAlignmentLeft;
            label.tag = 1;
            label.text = [NSString stringWithFormat:@"%@",[ranges objectAtIndex:row]];
            [view addSubview:label];
            
        }else if(pickerView == milesView2){
            
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 150, 50)];
            label.font = [UIFont systemFontOfSize:16];
            label.backgroundColor = [UIColor clearColor];
            label.textColor = [UIColor blackColor];
            label.textAlignment = NSTextAlignmentLeft;
            label.tag = 1;
            label.text = [NSString stringWithFormat:@"Miles"];
            [view addSubview:label];
            
            
        }
    }
    
    
    return view;

}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
    rangeField.text = [NSString stringWithFormat:@"%@",[ranges objectAtIndex:row]];
    [self closerangeview];
}

-(void)rangeoftrucks{
    
    [rangeField resignFirstResponder];
    
//    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
//    [SVProgressHUD show];
    // 1
    
    //    inputs : userid,distance
//    url : http://quikmov.quikmovapp.com/auth/Settings
    

    NSURL *url = [NSURL URLWithString:@"http://quikmov.quikmovapp.com/auth/Settings"];
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    // 2
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    request.HTTPMethod = @"POST";
    
    // 3
    
    NSUserDefaults* userStoredetails = [NSUserDefaults standardUserDefaults];
    userID = [userStoredetails objectForKey:@"user_id"];
    NSLog(@"%@",userID);
    
    NSString * bodyMsg = [NSString stringWithFormat:@"userid=%@&distance=%@",userID,rangeField.text];
    
    
    
    NSData * data = [bodyMsg dataUsingEncoding:NSUTF8StringEncoding];
    
    // 4
    NSURLSessionUploadTask *uploadTask = [session uploadTaskWithRequest:request
                                                               fromData:data completionHandler:^(NSData *data,NSURLResponse *response,NSError *error) {
                                                                   // Handle response here
                                                                   dispatch_async(dispatch_get_main_queue(), ^{
                                                                       [SVProgressHUD dismiss];
                                                                       [self loginSuccess:data];
                                                                   });
                                                                   
                                                               }];
    
    // 5
    [uploadTask resume];
    
}
- (void)loginSuccess:(NSData *)data{
    
    NSString * newStr = [[NSString alloc] initWithData:data                                                                                                                 encoding:NSUTF8StringEncoding];
    
    NSLog(@"%@",newStr);
    
    // NSError* error;
    
    NSString* str = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
    
    if (str.length == 0) {
        [self showAlert:@"Server error"];
        return;
    }
    NSError * error;
    NSDictionary* dic = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    
    NSLog(@"%@******%@,%@",str,[dic allValues],[dic objectForKey:@"status"]);
    
    // userId = [[dic objectForKey:@"message"] stringValue];
    NSString * Result = [dic objectForKey:@"status"];
    
    NSLog(@"%@",Result);
    
    if ([Result isEqualToString:@"success"]){
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@"Message"
                                      message:@"Range Updated Successfully"
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                                 
                             }];
        
        
        [alert addAction:ok];
        
        [self presentViewController:alert animated:YES completion:nil];


        
    }else{
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Message" message:[NSString stringWithFormat:@"%@",[dic objectForKey:@"result"]] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        
    }
    
    
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    {
        NSInteger nextTag = textField.tag +1;
        // Try to find next responder
        UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
        if (nextResponder) {
            // Found next responder, so set it.
            [nextResponder becomeFirstResponder];
            
        } else {
            // Not found, so remove keyboard.
            [textField resignFirstResponder];
            
        }
        return NO; // We do not want UITextField to insert line-breaks.
    }
}

-(void)cancelSetting{
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)showAlert:(NSString *)message{
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Message"
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [alert dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    
    
    [alert addAction:ok];
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)logout{
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Message"
                                  message:@"Do you really want to quit the QuikMov?"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             
                             exit(0);
                             
                         }];
    
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 
                                 [self dismissViewControllerAnimated:YES completion:^{
                                     
                                 }];
                                 
                             }];
    
    
    [alert addAction:ok];
    [alert addAction:cancel];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    //
    
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
