//
//  ViewController.m
//  QuikMov
//
//  Created by CSCS on 1/21/16.
//  Copyright © 2016 CSCS. All rights reserved.
//

#import "ViewController.h"
#import "DesignObj.h"
#import "LoginViewController.h"
#import "RegisterViewController.h"
#import "UserTypeViewController.h"
#import "AppDelegate.h"

@interface ViewController ()<UIScrollViewDelegate>{
    
    UIScrollView * introScrollView;
    UIPageControl * pageControl;
    
    NSArray * infoArr;
    UILabel * infolbl;
    
    UIButton * signIn;
    UIButton * registerIn;
}

@end

@implementation ViewController

@synthesize copyuType;


- (void)viewDidLoad {
    [super viewDidLoad];
    
//    screenWidth = [UIScreen mainScreen].bounds.size.width;
//    screenHeight = [UIScreen mainScreen].bounds.size.height;
//    
    self.view.backgroundColor = [UIColor clearColor];
    
    self.navigationItem.hidesBackButton = YES;
    
    UIImageView * bgView = [DesignObj initWithImage:CGRectMake(0, 0, screenWidth, screenHeight) img:@"bg.png"];
    [self.view addSubview:bgView];
    
    UILabel * getStarted = [DesignObj initWithLabel:CGRectMake((screenWidth-200)/2, 50, 200, 50) title:@"GET STARTED" font:18 txtcolor:[DesignObj quikRed]];
    [self.view addSubview:getStarted];
    
    UIButton * backbtn = [DesignObj initWithButton:CGRectMake(20, 20, 40, 20) tittle:@"Back" img:@""];
    backbtn.backgroundColor = [UIColor clearColor];
    [backbtn setTitleColor:[DesignObj quikRed] forState:UIControlStateNormal];
    [backbtn addTarget:self action:@selector(backAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backbtn];
    
    introScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(getStarted.frame) + 60, screenWidth, 200)];
    introScrollView.backgroundColor = [UIColor clearColor];
    introScrollView.pagingEnabled = YES;
    introScrollView.delegate = self;
    introScrollView.showsHorizontalScrollIndicator = NO;
    [introScrollView setBounces:NO];
    [self.view addSubview:introScrollView];
    
    infoArr = [[NSArray alloc]initWithObjects:@"Lorem Ipsum is a Dummy word. Use for all samples",@"Next Lorem Ipsum is a Dummy word. Use for all samplesLorem Ipsum is a Dummy word. Use for all samples",@"Next Lorem Ipsum is a Dummy word. Use for all samples.Next Lorem Ipsum is a Dummy word. Use for all samples",@"Great App,Next Lorem Ipsum is a Dummy word. Use for all samples ,Lorem Ipsum is a Dummy word. Use for all samples",nil] ;
    
    [introScrollView setContentSize:CGSizeMake(screenWidth * 4, 200)];
    
    for (int i = 0; i < infoArr.count; i++) {
        
        infolbl = [DesignObj initWithLabel:CGRectMake(screenWidth * i + 50, 0, screenWidth - 100,200) title:[infoArr objectAtIndex:i] font:18 txtcolor:[DesignObj quikRed]];
        infolbl.numberOfLines = 0;
        infolbl.textAlignment = NSTextAlignmentJustified;
        [introScrollView addSubview:infolbl];
    }
    
    pageControl = [[UIPageControl alloc] init];
    pageControl.frame = CGRectMake((screenWidth - 150)/2,CGRectGetMaxY(introScrollView.frame), 150, 50);
    pageControl.numberOfPages = 4;
    pageControl.currentPage = 0;
    pageControl.pageIndicatorTintColor = [DesignObj quikRed];
    pageControl.currentPageIndicatorTintColor = [DesignObj quikYellow];
    [self.view addSubview:pageControl];

    
    signIn = [DesignObj initWithButton:CGRectMake(10, screenHeight - 60, (screenWidth/2) - 15, 40) tittle:@"Sign-In" img:@"button.png"];
    [signIn setTitleColor:[DesignObj quikYellow] forState:UIControlStateNormal];
    [signIn addTarget:self action:@selector(signinBtn:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:signIn];
    
    registerIn = [DesignObj initWithButton:CGRectMake((screenWidth - ((screenWidth/2)-5)), screenHeight - 60, (screenWidth/2) - 15, 40) tittle:@"Register" img:@"button.png"];
    [registerIn setTitleColor:[DesignObj quikYellow] forState:UIControlStateNormal];
    [registerIn addTarget:self action:@selector(regBtn:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:registerIn];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    self.navigationController.navigationBarHidden =YES;

}

-(IBAction)backAction:(id)sender{
    //    UserTypeViewController*usrtyp = [[UserTypeViewController alloc]init];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    CGFloat pageWidth = scrollView.frame.size.width;
    float fractionalPage = scrollView.contentOffset.x / pageWidth;
    
    NSInteger page = lround(fractionalPage);
    pageControl.currentPage = page;
    //infolbl.text = [infoArr objectAtIndex:pageControl.currentPage];
}

-(IBAction)signinBtn:(id)sender{
    
    LoginViewController *login = [[LoginViewController alloc]init];
    login.userType = copyuType;
    [self.navigationController pushViewController:login animated:YES];
}

-(IBAction)regBtn:(id)sender{
        
    RegisterViewController *reg = [[RegisterViewController alloc]init];
    reg.utype = copyuType;
    [self.navigationController pushViewController:reg animated:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
