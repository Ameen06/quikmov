//
//  FeedBackViewController.m
//  QuikMov
//
//  Created by CSCS on 19/04/16.
//  Copyright © 2016 CSCS. All rights reserved.
//

#import "FeedBackViewController.h"
#import "AppDelegate.h"
#import "DesignObj.h"


@interface FeedBackViewController (){
    
    UILabel * topIc;
    UITextField * subjectField;
    UITextField * messageField;
    NSString * userID;
    UIButton * submitBtn;
    
}

@property (strong, nonatomic)UIScrollView* scrView;


@end

@implementation FeedBackViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = @"QuikMov";
    
    self.navigationController.navigationBarHidden = NO;
    
    self.navigationController.navigationBar.tintColor = [DesignObj quikYellow];
    
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [DesignObj quikYellow]}];
    self.navigationController.navigationBar.barTintColor = [DesignObj quikRed];
    
    self.navigationController.navigationBar.tintColor = [DesignObj quikYellow];
    
    UIImageView*bgView = [DesignObj initWithImage:CGRectMake(0, 0, screenWidth, screenHeight) img:@"bg.png"];
    [self.view addSubview:bgView];
    
    
    _scrView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, screenHeight )];
    _scrView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:_scrView];
    
    _scrView.contentSize = CGSizeMake(screenWidth, screenHeight + 100);
    
    UIButton* cancelBtn = [DesignObj initWithButton:CGRectMake(10, 10, 70, 20) tittle:@"Cancel" img:@""];
    [cancelBtn addTarget:self action:@selector(cancelFeedBack) forControlEvents:UIControlEventTouchUpInside];
    [cancelBtn setTitleColor:[DesignObj quikYellow] forState:UIControlStateNormal];
    [_scrView addSubview:cancelBtn];
    
    UIBarButtonItem* leftbarbtn = [[UIBarButtonItem alloc]initWithCustomView:cancelBtn];
    self.navigationItem.leftBarButtonItem = leftbarbtn;
    
    UIImageView * settingsIcon = [DesignObj initWithImage:CGRectMake(((screenWidth - 120)/2)-15 -5, 100, 25, 25) img:@"feeback.png"];
    [_scrView addSubview:settingsIcon];
    
    topIc = [DesignObj initWithLabel:CGRectMake((screenWidth - 120)/2, 100, 120, 30) title:@"FeedBack" font:18 txtcolor:[DesignObj quikRed]];
    topIc.textAlignment = NSTextAlignmentCenter;
    [_scrView addSubview:topIc];
    
    subjectField = [DesignObj initWithTextfield:CGRectMake((screenWidth - (screenWidth - 44))/2, CGRectGetMaxY(topIc.frame)+ 20, (screenWidth - 44), 40)  placeholder:@"" tittle:@"Subject" delegate:self font:16];
    subjectField.textColor = [DesignObj quikRed];
    subjectField.leftView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 10, 40)];
    subjectField.leftViewMode = UITextFieldViewModeAlways;
    subjectField.textAlignment = NSTextAlignmentLeft;
    subjectField.tag = 102;
    [subjectField setBackground:[UIImage imageNamed:@"textbox_small.png"]];
    [_scrView addSubview:subjectField];
    
    messageField = [DesignObj initWithTextfield:CGRectMake((screenWidth - (screenWidth - 44))/2,CGRectGetMaxY(subjectField.frame)+20, (screenWidth - 44), 150)  placeholder:@"" tittle:@"Message" delegate:self font:16];
    messageField.textAlignment = NSTextAlignmentLeft;
    messageField.textColor = [DesignObj quikRed];
    messageField.leftView = [[UIView alloc]initWithFrame:CGRectMake(0, -100, 10, 40)];
    messageField.leftViewMode = UITextFieldViewModeAlways;
    messageField.tag = 103;
    [messageField setBackground:[UIImage imageNamed:@"textbox_big.png"]];
    [_scrView addSubview:messageField];
  
    submitBtn = [DesignObj initWithButton:CGRectMake((screenWidth - (screenWidth - 44))/2, CGRectGetMaxY(messageField.frame)+ 20, (screenWidth - 44), 45) tittle:@"Submit" img:@"button.png"];
    [submitBtn setTitleColor:[DesignObj quikYellow] forState:UIControlStateNormal];
    [submitBtn addTarget:self action:@selector(submitAction) forControlEvents:UIControlEventTouchUpInside];
    [_scrView addSubview:submitBtn];
    
    UITapGestureRecognizer* taptohideKeyboard = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hideKeyboard)];
    [_scrView addGestureRecognizer:taptohideKeyboard];
    
    
    // Do any additional setup after loading the view.
}

-(void)hideKeyboard{
    
    [self.view endEditing:YES];
}

-(void)submitAction{
    
    [messageField resignFirstResponder];
    
    NSURL *url = [NSURL URLWithString:@"http://quikmov.quikmovapp.com/auth/insertFeedback"];
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    // 2
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    request.HTTPMethod = @"POST";
    
    // 3
    
    NSUserDefaults* userStoredetails = [NSUserDefaults standardUserDefaults];
    userID = [userStoredetails objectForKey:@"user_id"];
    NSLog(@"%@",userID);
    
    NSString * bodyMsg = [NSString stringWithFormat:@"userid=%@&title=%@&message=%@",userID,subjectField.text,messageField.text];
    
    
    
    NSData * data = [bodyMsg dataUsingEncoding:NSUTF8StringEncoding];
    
    // 4
    NSURLSessionUploadTask *uploadTask = [session uploadTaskWithRequest:request
                                                               fromData:data completionHandler:^(NSData *data,NSURLResponse *response,NSError *error) {
                                                                   // Handle response here
                                                                   dispatch_async(dispatch_get_main_queue(), ^{
                                                                       
                                                            [self feedbackSuccess:data];
                                                                   });
                                                                   
                                                               }];
    
    // 5
    [uploadTask resume];
    
}
- (void)feedbackSuccess:(NSData *)data{
    
    NSString * newStr = [[NSString alloc] initWithData:data                                                                                                                 encoding:NSUTF8StringEncoding];
    
    NSLog(@"%@",newStr);
    
    // NSError* error;
    
    NSString* str = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
    
    if (str.length == 0) {
        [self showAlert:@"Server error"];
        return;
    }
    NSError * error;
    NSDictionary* dic = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    
    NSLog(@"%@******%@,%@",str,[dic allValues],[dic objectForKey:@"status"]);
    
    // userId = [[dic objectForKey:@"message"] stringValue];
    NSString * Result = [dic objectForKey:@"message"];
    
    NSLog(@"%@",Result);
    
    if ([Result isEqualToString:@"success"]){
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@"Message"
                                      message:@"Thanks for Your FeedBack"
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                            [alert dismissViewControllerAnimated:YES completion:nil];
                            [self dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
        
        
        [alert addAction:ok];
        
        [self presentViewController:alert animated:YES completion:nil];
        
        
        
    }else{
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Message" message:[NSString stringWithFormat:@"%@",[dic objectForKey:@"result"]] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        
    }
    
    
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    {
        NSInteger nextTag = textField.tag +001;
        // Try to find next responder
        UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
        if (nextResponder) {
            // Found next responder, so set it.
            [nextResponder becomeFirstResponder];
            
        } else {
            // Not found, so remove keyboard.
            [textField resignFirstResponder];
            
        }
        return NO; // We do not want UITextField to insert line-breaks.
    }
}

-(void)cancelFeedBack{
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)showAlert:(NSString *)message{
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Message"
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [alert dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    
    
    [alert addAction:ok];
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)logout{
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Message"
                                  message:@"Do you really want to quit the QuikMov?"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             
                             exit(0);
                             
                         }];
    
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 
                                 [self dismissViewControllerAnimated:YES completion:^{
                                     
                                 }];
                                 
                             }];
    
    
    [alert addAction:ok];
    [alert addAction:cancel];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    //
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
