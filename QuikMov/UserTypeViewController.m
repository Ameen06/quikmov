//
//  UserTypeViewController.m
//  QuikMov
//
//  Created by CSCS on 1/25/16.
//  Copyright © 2016 CSCS. All rights reserved.
//

#import "UserTypeViewController.h"
#import "GlobalObjects.h"
#import "LoginViewController.h"
#import "RegisterViewController.h"
#import "DesignObj.h"
#import "ViewController.h"
#import "AppDelegate.h"

@interface UserTypeViewController (){
    
    UIButton * customer;
    UIButton * trucker;
    UILabel * user;
    UILabel * truck;
}

@end

@implementation UserTypeViewController
@synthesize userType;
@synthesize usertyp;

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    self.navigationController.navigationBarHidden = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.navigationBarHidden =  YES;
    
    self.navigationItem.hidesBackButton = YES;
    
    UIImageView * bgView = [DesignObj initWithImage:CGRectMake(0, 0, screenWidth, screenHeight) img:@"bg.png"];
    [self.view addSubview:bgView];
    
    UIImageView * logoView = [DesignObj initWithImage:CGRectMake((screenWidth-250)/2, (screenHeight-180)/2-50, 250, 85) img:@"QuikMov_Logo"];
    [self.view addSubview:logoView];
    
    customer = [DesignObj initWithButton:CGRectMake(((screenWidth-50)/2)-100, logoView.frame.origin.y+ 85+60, 50, 50) tittle:@"" img:@"user.png"];
    [customer addTarget:self action:@selector(goLogin:) forControlEvents:UIControlEventTouchUpInside];
    customer.tag = 1;
    [self.view addSubview:customer];
    
    trucker = [DesignObj initWithButton:CGRectMake(customer.frame.origin.x+180, logoView.frame.origin.y+ 85+60, 50, 50) tittle:@"" img:@"truck_login.png"];
    [trucker addTarget:self action:@selector(goLogin:) forControlEvents:UIControlEventTouchUpInside];
    trucker.tag = 2;
    [self.view addSubview:trucker];
    
    user = [DesignObj initWithLabel:CGRectMake(((screenWidth-100)/2)-100, customer.frame.origin.y+ 40+ 10, 100, 30) title:@"Customer" font:16 txtcolor:[DesignObj quikRed]];
    user.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:user];
    
    truck = [DesignObj initWithLabel:CGRectMake(trucker.frame.origin.x, trucker.frame.origin.y+ 40+ 10, 120, 30) title:@"Trucker" font:16 txtcolor:[DesignObj quikRed]];
    truck.textAlignment = NSTextAlignmentLeft;
    [self.view addSubview:truck];
}

-(IBAction)goLogin:(id)sender{
    
    if ([sender tag] == 1) {
        userType = @"1";
        
    }else if ([sender tag] == 2){
        
        userType = @"2";
    }
    
    ViewController * selectType = [[ViewController alloc]init];
    selectType.copyuType = userType;
    [self.navigationController pushViewController:selectType animated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
