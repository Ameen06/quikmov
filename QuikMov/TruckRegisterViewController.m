//
//  TruckRegisterViewController.m
//  QuikMov
//
//  Created by CSCS on 1/28/16.
//  Copyright © 2016 CSCS. All rights reserved.
//

#import "TruckRegisterViewController.h"
#import "QuickMovOTPViewController.h"
#import "MapViewController.h"
#import "DesignObj.h"
#import "Reachability.h"
#import "SVProgressHUD.h"
#import "GlobalObjects.h"
#import "DisplayTableViewCell.h"
#import "subTypeModel.h"
#import "MBProgressHUD.h"
#import "DBCameraContainerViewController.h"
#import "AppDelegate.h"


#define MIN_UPLOAD_RESOLUTION 320 * 480
#define MAX_UPLOAD_SIZE 1124000

@interface TruckRegisterViewController ()<DBCameraViewControllerDelegate,UIPickerViewDataSource, UIPickerViewDelegate>{
    
    UIDatePicker * datePicker;
    NSMutableArray * txtfieldarr;
    UITextField * Txt;
    UITextField * truckType;
    UITextField * heightofTruck;
    UITextField * widthofTruck;
    UITextField * lengthofTruck;
    UITextField * regno;
    UITextField * insurenceValidity;
    UITextField * insurenceno;
    UITextField * serviceCategories;
    UITextField * Equipments;
    UITextField * accountNo;
    UITextField * routingNo;
    
    UIButton * selectimage1;
    UIButton * selectimage2;
    UIButton * selectimage3;
    UIButton * selectimage4;
    NSArray  * titlearr;
    NSArray  * titlelblarr;
    NSData * imageData;
    NSString * userid;
    UILabel * trucklbl;
    UILabel* termsandcondi;
    
    int imageTag;
    
    UILabel * truckTypelbl;
    UILabel * truckSizelbl;
    UILabel * truckregnolbl;
    UILabel * truckinsurenceLbl;
    UILabel * truckRatelbl;
    UILabel * truckServiceCatlbl;
    UILabel * truckPaymentlbl;
    NSString* regUserid;
    
    UITableView * trucktypestblView;
    UITableView * expandableView;
    
    NSMutableArray * listofSubTypeArr;
    NSMutableArray * listofParentTypeArr;
    NSMutableArray * listofTypesArr;
    
    UIView * closeView;
    UIButton * closebtn;
    DisplayTableViewCell* cellsendingData;
    int count;
    NSInteger selectedRow;
    UIPickerView * typesView;
    UIPickerView * typesView2;
    NSUserDefaults * typesave;
    NSString * otp;
    
    UIView * barView;
    UIPickerView * truckerORmover;
    NSArray * truckerORmoverarr;
    UITapGestureRecognizer * taptoKeyboardHide;
    NSMutableDictionary * dict;
    NSMutableData * _downloadedData;
    UIView * ViewForValuePicker;
    UIView * ViewforPicker;


}

@property (strong, nonatomic)UIScrollView* scrView;


@end

@implementation TruckRegisterViewController
@synthesize truckImg1;
@synthesize truckImg2;
@synthesize truckImg3;
@synthesize truckImg4;
@synthesize truckfname;
@synthesize truckmname;
@synthesize trucklname;
@synthesize truckemail;
@synthesize trucknumber;
@synthesize truckpass;
@synthesize truckcpass;
@synthesize truckdateob;
@synthesize truckregImg;
@synthesize truckerUsetyp;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    selectedRow = -1;
    
    self.title = @"Truck Registration";
    
    self.navigationController.navigationBarHidden = NO;
    
    self.navigationController.navigationBar.tintColor = [DesignObj quikYellow];
    
//    screenWidth = [UIScreen mainScreen].bounds.size.width;
//    screenHeight = [UIScreen mainScreen].bounds.size.height;
//                        
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [DesignObj quikYellow]}];
    self.navigationController.navigationBar.barTintColor = [DesignObj quikRed];
    
    listofParentTypeArr = [[NSMutableArray alloc]init];
    listofSubTypeArr = [[NSMutableArray alloc]init];
    truckerORmoverarr = [[NSArray alloc]initWithObjects:@"Trucker",@"Mover", nil];
    
    UIImageView*bgView = [DesignObj initWithImage:CGRectMake(0, 0, screenWidth, screenHeight) img:@"bg.png"];
    [self.view addSubview:bgView];
        
    _scrView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, screenHeight)];
    [self.view addSubview:_scrView];
    
    _scrView.contentSize = CGSizeMake(screenWidth, screenHeight+ 410);
    
    datePicker = [[UIDatePicker alloc]init];
    [datePicker addTarget:self action:@selector(datePickerChanged) forControlEvents:UIControlEventValueChanged];
    datePicker.datePickerMode = UIDatePickerModeDate;
    
//    int yaxis = 90;
//    int y1axis = 90;
    
    trucklbl = [DesignObj initWithLabel:CGRectMake((screenWidth-100)/2, 64+20, 100, 40) title:@"Truck Image" font:16 txtcolor:[DesignObj quikRed]];
    [_scrView addSubview:trucklbl];
    
    truckImg1 = [DesignObj initWithImage:CGRectMake((screenWidth - 220)/2, trucklbl.frame.origin.y +40 +10, 40, 40) img:@"addtruckimg.png"];
    [_scrView addSubview:truckImg1];
    
    truckImg2 = [DesignObj initWithImage:CGRectMake(truckImg1.frame.origin.x + 40 +20, trucklbl.frame.origin.y +40 +10, 40, 40) img:@"addtruckimg.png"];
    [_scrView addSubview:truckImg2];
    
    truckImg3 = [DesignObj initWithImage:CGRectMake(truckImg2.frame.origin.x + 40 +20, trucklbl.frame.origin.y +40 +10, 40, 40) img:@"addtruckimg.png"];
    [_scrView addSubview:truckImg3];
    
    truckImg4 = [DesignObj initWithImage:CGRectMake(truckImg3.frame.origin.x + 40 +20, trucklbl.frame.origin.y +40 +10, 40, 40) img:@"addtruckimg.png"];
    [_scrView addSubview:truckImg4];
    
    selectimage1 = [DesignObj initWithButton:CGRectMake((screenWidth - 220)/2,trucklbl.frame.origin.y +40 +10, 40, 40) tittle:nil img:nil];
    [selectimage1 addTarget:self action:@selector(uploadPic:) forControlEvents:UIControlEventTouchUpInside];
    selectimage1.tag = 1;
    [_scrView addSubview:selectimage1];
    
    selectimage2 = [DesignObj initWithButton:CGRectMake(selectimage1.frame.origin.x + 40 +20, trucklbl.frame.origin.y +40 +10, 40, 40) tittle:nil img:nil];
    [selectimage2 addTarget:self action:@selector(uploadPic:) forControlEvents:UIControlEventTouchUpInside];
    selectimage2.tag = 2;
    [_scrView addSubview:selectimage2];
    
    selectimage3 = [DesignObj initWithButton:CGRectMake(selectimage2.frame.origin.x + 40 +20, trucklbl.frame.origin.y +40 +10, 40, 40) tittle:nil img:nil];
    [selectimage3 addTarget:self action:@selector(uploadPic:) forControlEvents:UIControlEventTouchUpInside];
    selectimage3.tag = 3;
    [_scrView addSubview:selectimage3];
    
    selectimage4 = [DesignObj initWithButton:CGRectMake(selectimage3.frame.origin.x + 40 +20, trucklbl.frame.origin.y +40 +10, 40, 40) tittle:nil img:nil];
    [selectimage4 addTarget:self action:@selector(uploadPic:) forControlEvents:UIControlEventTouchUpInside];
    selectimage4.tag = 4;
    [_scrView addSubview:selectimage4];
    
//    truckTypelbl = [DesignObj initWithLabel:CGRectMake(20, trucklbl.frame.origin.y +40 +20, 100, 40) title:@"Truck Type: " font:16 txtcolor:[DesignObj quikRed]];
//    truckTypelbl.textAlignment = NSTextAlignmentLeft;
//    [_scrView addSubview:truckTypelbl];
    
    truckSizelbl = [DesignObj initWithLabel:CGRectMake((screenWidth-100)/2, selectimage4.frame.origin.y +40 +20, 100, 40) title:@"Truck Size " font:16 txtcolor:[DesignObj quikRed]];
//    truckSizelbl.textAlignment = NSTextAlignmentLeft;
    [_scrView addSubview:truckSizelbl];
    
    heightofTruck = [DesignObj initWithTextfield:CGRectMake(60, truckSizelbl.frame.origin.y +40 +10, 60, 30) placeholder:@"" tittle:@"Height" delegate:self font:16];
    heightofTruck.tag = 201;
    [heightofTruck setBackground:[UIImage imageNamed:@"text_box.png"]];
    [heightofTruck setReturnKeyType:UIReturnKeyNext];
    heightofTruck.textAlignment = NSTextAlignmentCenter;
    [_scrView addSubview:heightofTruck];
    
    
    lengthofTruck = [DesignObj initWithTextfield:CGRectMake((screenWidth-60)/2, truckSizelbl.frame.origin.y +40 +10, 60, 30) placeholder:@"" tittle:@"Length" delegate:self font:16];
    [lengthofTruck setBackground:[UIImage imageNamed:@"text_box.png"]];
    lengthofTruck.textAlignment = NSTextAlignmentCenter;
    lengthofTruck.tag = 202;
    [lengthofTruck setReturnKeyType:UIReturnKeyNext];
    [_scrView addSubview:lengthofTruck];

//    
//    lengthofTruck = [DesignObj initWithTextfield:CGRectMake(heightofTruck.frame.origin.x +60 +20, truckSizelbl.frame.origin.y +40 +10, 60, 30) placeholder:@"" tittle:@"Length" delegate:self font:16];
//    [lengthofTruck setBackground:[UIImage imageNamed:@"text_box.png"]];
//    lengthofTruck.textAlignment = NSTextAlignmentCenter;
//    lengthofTruck.tag = 203;
//    [lengthofTruck setReturnKeyType:UIReturnKeyNext];
//    [_scrView addSubview:lengthofTruck];
    
//    widthofTruck = [DesignObj initWithTextfield:CGRectMake(lengthofTruck.frame.origin.x +60 +20, truckSizelbl.frame.origin.y +40 +10, 60, 30) placeholder:@"" tittle:@"Width" delegate:self font:16];
//    [widthofTruck setBackground:[UIImage imageNamed:@"text_box.png"]];
//    [widthofTruck setReturnKeyType:UIReturnKeyNext];
//    widthofTruck.tag = 204;
//    widthofTruck.textAlignment = NSTextAlignmentCenter;
//    [_scrView addSubview:widthofTruck];
    
    
        widthofTruck = [DesignObj initWithTextfield:CGRectMake(screenWidth - 120, truckSizelbl.frame.origin.y +40 +10, 60, 30) placeholder:@"" tittle:@"Width" delegate:self font:16];
        [widthofTruck setBackground:[UIImage imageNamed:@"text_box.png"]];
        [widthofTruck setReturnKeyType:UIReturnKeyNext];
        widthofTruck.tag = 203;
        widthofTruck.textAlignment = NSTextAlignmentCenter;
        [_scrView addSubview:widthofTruck];

    truckType = [DesignObj initWithTextfield:CGRectMake((screenWidth-(screenWidth-44))/2, lengthofTruck.frame.origin.y +40 +20, screenWidth-44, 30) placeholder:@"" tittle:@"Select Truck Type" delegate:self font:16];
    [truckType setBackground:[UIImage imageNamed:@"text_box.png"]];
    [truckType setReturnKeyType:UIReturnKeyNext];
    [truckType addTarget:self action:@selector(showTypes:) forControlEvents:UIControlEventEditingDidBegin];
    truckType.tag = 204;
    truckType.textAlignment = NSTextAlignmentCenter;
    [_scrView addSubview:truckType];
    
//    truckregnolbl = [DesignObj initWithLabel:CGRectMake(20, truckSizelbl.frame.origin.y +40 +20, 120, 40) title:@"Registration No: " font:16 txtcolor:[DesignObj quikRed]];
//    truckregnolbl.textAlignment = NSTextAlignmentLeft;
//    [_scrView addSubview:truckregnolbl];
    
    regno = [DesignObj initWithTextfield:CGRectMake((screenWidth-(screenWidth-44))/2, truckType.frame.origin.y +40 +20, screenWidth-44, 30) placeholder:@"" tittle:@"Registration No" delegate:self font:16];
    [regno setBackground:[UIImage imageNamed:@"text_box.png"]];
    [regno setReturnKeyType:UIReturnKeyNext];
    regno.textAlignment = NSTextAlignmentCenter;
    regno.tag = 205;
    [_scrView addSubview:regno];
//
//    truckinsurenceLbl = [DesignObj initWithLabel:CGRectMake(20, truckregnolbl.frame.origin.y +40 +20, 140, 40) title:@"Insurence Validity: " font:16 txtcolor:[DesignObj quikRed]];
//    truckinsurenceLbl.textAlignment = NSTextAlignmentLeft;
//    [_scrView addSubview:truckinsurenceLbl];
    
    insurenceno = [DesignObj initWithTextfield:CGRectMake((screenWidth-(screenWidth-44))/2, regno.frame.origin.y +40 +20, screenWidth-44, 30) placeholder:@"" tittle:@"Insurance No" delegate:self font:16];
    [insurenceno setBackground:[UIImage imageNamed:@"text_box.png"]];
    insurenceno.textAlignment = NSTextAlignmentCenter;
    [insurenceno setReturnKeyType:UIReturnKeyNext];
    insurenceno.tag = 206;
    [_scrView addSubview:insurenceno];

    
    insurenceValidity = [DesignObj initWithTextfield:CGRectMake((screenWidth-(screenWidth-44))/2, insurenceno.frame.origin.y +40 +20, screenWidth-44, 30) placeholder:@"" tittle:@"Insurance Validity" delegate:self font:16];
    [insurenceValidity setBackground:[UIImage imageNamed:@"text_box.png"]];
    insurenceValidity.textAlignment = NSTextAlignmentCenter;
    [insurenceValidity setReturnKeyType:UIReturnKeyNext];
    insurenceValidity.tag = 207;
    [_scrView addSubview:insurenceValidity];

//    truckRatelbl = [DesignObj initWithLabel:CGRectMake(20, truckinsurenceLbl.frame.origin.y +40 +20, 120, 40) title:@"Truck Rate: " font:16 txtcolor:[DesignObj quikRed]];
//    truckRatelbl.textAlignment = NSTextAlignmentLeft;
//    [_scrView addSubview:truckRatelbl];

//    truckServiceCatlbl = [DesignObj initWithLabel:CGRectMake(20, truckRatelbl.frame.origin.y +40 +20, 150, 40) title:@"Service Categories: " font:16 txtcolor:[DesignObj quikRed]];
//    truckServiceCatlbl.textAlignment = NSTextAlignmentLeft;
//    [_scrView addSubview:truckServiceCatlbl];
    
    serviceCategories = [DesignObj initWithTextfield:CGRectMake((screenWidth-(screenWidth-44))/2, insurenceValidity.frame.origin.y +40 +20, screenWidth-44, 30) placeholder:@"" tittle:@"Categories" delegate:self font:16];
    [serviceCategories setBackground:[UIImage imageNamed:@"text_box.png"]];
    [serviceCategories addTarget:self action:@selector(loadlistView) forControlEvents:UIControlEventTouchUpInside];
    serviceCategories.textAlignment = NSTextAlignmentCenter;
    [serviceCategories setReturnKeyType:UIReturnKeyNext];
    serviceCategories.tag = 208;
    [_scrView addSubview:serviceCategories];

//    truckEquiplbl = [DesignObj initWithLabel:CGRectMake(20, truckServiceCatlbl.frame.origin.y +40 +20, 120, 40) title:@"Equipments: " font:16 txtcolor:[DesignObj quikRed]];
//    truckEquiplbl.textAlignment = NSTextAlignmentLeft;;
//    [_scrView addSubview:truckEquiplbl];
    
    Equipments = [DesignObj initWithTextfield:CGRectMake((screenWidth- (screenWidth-44))/2, serviceCategories.frame.origin.y +40 +20, screenWidth-44, 30) placeholder:@"" tittle:@"Equipments" delegate:self font:16];
    [Equipments setBackground:[UIImage imageNamed:@"text_box.png"]];
    [Equipments setReturnKeyType:UIReturnKeyNext];
    Equipments.textAlignment = NSTextAlignmentCenter;
    Equipments.tag = 209;
    [_scrView addSubview:Equipments];
    
    truckPaymentlbl = [DesignObj initWithLabel:CGRectMake((screenWidth - 120)/2, Equipments.frame.origin.y +40 +20, 120, 40) title:@"Payment" font:16 txtcolor:[DesignObj quikRed]];
    truckPaymentlbl.textAlignment = NSTextAlignmentCenter;;
    [_scrView addSubview:truckPaymentlbl];
    
    accountNo = [DesignObj initWithTextfield:CGRectMake((screenWidth- (screenWidth-44))/2, truckPaymentlbl.frame.origin.y +40 +20, screenWidth-44, 30) placeholder:@"" tittle:@"Account No" delegate:self font:16];
    [accountNo setBackground:[UIImage imageNamed:@"text_box.png"]];
    [accountNo setReturnKeyType:UIReturnKeyNext];
    accountNo.keyboardType = UIKeyboardTypeNumberPad;
    accountNo.textAlignment = NSTextAlignmentCenter;
    accountNo.tag = 210;
    [_scrView addSubview:accountNo];
    
    routingNo = [DesignObj initWithTextfield:CGRectMake((screenWidth- (screenWidth-44))/2, accountNo.frame.origin.y +40 +20, screenWidth-44, 30) placeholder:@"" tittle:@"Routing No" delegate:self font:16];
    [routingNo setBackground:[UIImage imageNamed:@"text_box.png"]];
    [routingNo setReturnKeyType:UIReturnKeyNext];
    routingNo.textAlignment = NSTextAlignmentCenter;
    routingNo.keyboardType = UIKeyboardTypeNumberPad;
    routingNo.tag = 211;
    [_scrView addSubview:routingNo];
    
//    NSValue* keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameEndUserInfoKey];
    
//    CGRect keyboardFrame = [keyboardFrameBegin CGRectValue];
    
    heightofTruck.keyboardType = UIKeyboardTypeNumberPad;
    widthofTruck.keyboardType = UIKeyboardTypeNumberPad;
    lengthofTruck.keyboardType = UIKeyboardTypeNumberPad;
    insurenceValidity.inputView = datePicker;
    regno.keyboardType = UIKeyboardTypeNumberPad;
    insurenceno.keyboardType = UIKeyboardTypeNumberPad;
    
    
    UIButton* regbtn =[DesignObj initWithButton:CGRectMake((screenWidth - 200)/2, routingNo.frame.origin.y + 38 +30, 200, 40)  tittle:@"Join" img:@"button.png"];
    [regbtn addTarget:self action:@selector(regtruckAction) forControlEvents:UIControlEventTouchUpInside];
    [regbtn setTitleColor:[DesignObj quikYellow] forState:UIControlStateNormal];
    [_scrView addSubview:regbtn];
    
    termsandcondi = [DesignObj initWithLabel:CGRectMake((screenWidth - 300)/2, regbtn.frame.origin.y+ 40 +20, 300, 60) title:@"By clicking Join you are agreeing to the\n Terms of use and Privacy Policy" font:16 txtcolor:[DesignObj quikRed]];
    termsandcondi.numberOfLines = 2;
    termsandcondi.textAlignment = NSTextAlignmentCenter;
    [_scrView addSubview:termsandcondi];
    
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, screenWidth, 44)];
    numberToolbar.barStyle = UIBarStyleDefault;//UIBarStyleBlackTranslucent;
    numberToolbar.tintColor = [UIColor colorWithRed:207/255.0 green:26/255.0 blue:26/255.0 alpha:1.0];
    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleDone target:self action:@selector(cancelNumberPad)],
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
    [numberToolbar sizeToFit];
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDate *currentDate = [NSDate date];
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    [comps setYear:0];
    NSDate *maxDate = [calendar dateByAddingComponents:comps toDate:currentDate options:0];
    [comps setYear:10];
    NSDate *minDate = [calendar dateByAddingComponents:comps toDate:currentDate options:0];
    
    [datePicker setMaximumDate:maxDate];
    [datePicker setMinimumDate:minDate];
    
    heightofTruck.inputAccessoryView = numberToolbar;
    widthofTruck.inputAccessoryView = numberToolbar;
    lengthofTruck.inputAccessoryView = numberToolbar;
    regno.inputAccessoryView = numberToolbar;
    insurenceno.inputAccessoryView = numberToolbar;
   insurenceValidity.inputAccessoryView = numberToolbar;
    accountNo.inputAccessoryView = numberToolbar;
    routingNo.inputAccessoryView = numberToolbar;
    
/*    taptoKeyboardHide = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hideKeyboard:)];
    taptoKeyboardHide.numberOfTouchesRequired = 1;
    taptoKeyboardHide.numberOfTapsRequired = 1;
    [_scrView addGestureRecognizer:taptoKeyboardHide];


    // Do any additional setup after loading the view.
}
-(IBAction)hideKeyboard:(id)sender{
    
    [self.view endEditing:YES];
}
*/
    
}

- (BOOL)textFieldShouldReturn:(UITextField*)textField
{
    NSInteger nextTag = textField.tag + 001;
    // Try to find next responder
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else {
        // Not found, so remove keyboard.
        [textField resignFirstResponder];
    }
    return NO; // We do not want UITextField to insert line-breaks.
}

-(IBAction)showTypes:(id)sender{
    
    [self truckTypeService];
    
}

-(void)loadlistView{
    
    ViewForValuePicker = [[UIView alloc]initWithFrame:CGRectMake(0, screenHeight - 200, screenWidth, 200)];
    ViewForValuePicker.backgroundColor = [UIColor grayColor];
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, screenWidth, 44)];
    numberToolbar.barStyle = UIBarStyleDefault;//UIBarStyleBlackTranslucent;
    numberToolbar.tintColor = [UIColor colorWithRed:207/255.0 green:26/255.0 blue:26/255.0 alpha:1.0];
    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleDone target:self action:@selector(cancelNumberPad)],
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
//    [numberToolbar sizeToFit];
    [ViewForValuePicker addSubview:numberToolbar];
    
    truckerORmover = [[UIPickerView alloc]initWithFrame:CGRectMake(0, 44, screenWidth, 156)];
    truckerORmover.backgroundColor = [UIColor whiteColor];
    truckerORmover.delegate = self;
    truckerORmover.dataSource = self;
    truckerORmover.showsSelectionIndicator=YES;
    [ViewForValuePicker addSubview:truckerORmover];
    
    [self.view addSubview:ViewForValuePicker];
    
    
    
//    serviceCategories.inputView = truckerORmover;
//    serviceCategories.inputAccessoryView = numberToolbar;
    
    }



-(void)closeTypeView{
    
    [closeView removeFromSuperview];
    [closebtn removeFromSuperview];
    [trucktypestblView removeFromSuperview];
    
}



- (void)datePickerChanged{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMM-dd-yyyy"];
    NSString *strDate = [dateFormatter stringFromDate:datePicker.date];
    insurenceValidity.text = strDate;
}

-(void)cancelNumberPad{
    [heightofTruck resignFirstResponder];
    [widthofTruck resignFirstResponder];
    [lengthofTruck resignFirstResponder];
    [insurenceno resignFirstResponder];
    [regno resignFirstResponder];
    [insurenceValidity resignFirstResponder];
    [ViewforPicker removeFromSuperview];
    [ViewForValuePicker removeFromSuperview];
    
//    heightofTruck.text = @"";
//    widthofTruck.text = @"";
//    lengthofTruck.text = @"";
//    insurenceno.text = @"";
//    regno.text = @"";
//    insurenceValidity.text = @"";

}

-(void)doneWithNumberPad{
    
//    UITextField* textField = [[UITextField alloc]init];
//    
//    NSInteger nextTag = textField.tag +001;
//    // Try to find next responder
//    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
//    if (nextResponder) {
//        // Found next responder, so set it.
//        [nextResponder becomeFirstResponder];
//    } else {
//        // Not found, so remove keyboard.
//        [textField resignFirstResponder];
//    }
//    
    if ([heightofTruck resignFirstResponder]) {
        
        [lengthofTruck becomeFirstResponder];
    }else if ([lengthofTruck resignFirstResponder]) {
        
        [widthofTruck becomeFirstResponder];
    }else if ([widthofTruck resignFirstResponder]) {
        
        [self truckTypeService];
    }else if ([regno resignFirstResponder]) {
        
        [ViewforPicker removeFromSuperview];
        [insurenceno becomeFirstResponder];
    }else if ([insurenceno resignFirstResponder]) {
        
        [insurenceValidity becomeFirstResponder];
        
    }else if([insurenceValidity resignFirstResponder]){
        
        [ViewForValuePicker becomeFirstResponder];
        
    }else if([accountNo resignFirstResponder]){
        
        [routingNo becomeFirstResponder];
    }else if([routingNo resignFirstResponder]){
        
        [self regtruckAction];
    }
    
    [ViewForValuePicker removeFromSuperview];
    [ViewforPicker removeFromSuperview];

    
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    
    if (textField == regno) {

        [_scrView setContentOffset:CGPointMake(_scrView.frame.origin.x, _scrView.frame.origin.y+144) animated:YES];

    }
    else if (textField == insurenceno) {
        
        [_scrView setContentOffset:CGPointMake(_scrView.frame.origin.x, _scrView.frame.origin.y+180) animated:YES];

    }
    else if (textField == insurenceValidity) {
        [_scrView setContentOffset:CGPointMake(_scrView.frame.origin.x, _scrView.frame.origin.y+230) animated:YES];

    }else if (textField == serviceCategories) {
        [_scrView setContentOffset:CGPointMake(_scrView.frame.origin.x, _scrView.frame.origin.y+270) animated:YES];

    }else if (textField == Equipments) {
        
        [_scrView setContentOffset:CGPointMake(_scrView.frame.origin.x, _scrView.frame.origin.y+288) animated:YES];

    }else if (textField == accountNo) {

        [_scrView setContentOffset:CGPointMake(_scrView.frame.origin.x, _scrView.frame.origin.y+520) animated:YES];
    }else if (textField == routingNo) {

        [_scrView setContentOffset:CGPointMake(_scrView.frame.origin.x, _scrView.frame.origin.y+570) animated:YES];
    }
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField.tag == 204) {
        
        [self truckTypeService];
        [widthofTruck resignFirstResponder];

        return NO;
    }else if (textField.tag == 208){
        
        [self loadlistView];
        return NO;
    }
//    if (textField == regno) {
//        typesView.hidden = YES;
//        typesView2.hidden = YES;
//    }else if (textField == widthofTruck) {
//        typesView.hidden = YES;
//        typesView2.hidden = YES;
//    }
    
    if (!(textField.tag == 208)) {
        
        typesView.hidden = YES;
        typesView2.hidden = YES;
    }
    return YES;
}



-(IBAction)uploadPic:(id)sender{
    
    if ([sender tag]==1) {
        imageTag = 101;
    }else if ([sender tag]==2) {
        imageTag = 102;
    }else if ([sender tag]==3) {
        imageTag = 103;
    }else if ([sender tag]==4) {
        imageTag = 104;
    }
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Message"
                                  message:@"Choose options to Upload your Truck image"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* library = [UIAlertAction
                              actionWithTitle:@"From Photo Library"
                              style:UIAlertActionStyleDefault
                              handler:^(UIAlertAction * action)
                              {
                                  [alert dismissViewControllerAnimated:YES completion:nil];
                                  [self selectPhoto];
                                  
                              }];
    
    UIAlertAction* Camera = [UIAlertAction
                             actionWithTitle:@"Take Photo"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 [self takePhoto];
                             }];
    
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    
    [alert addAction:library];
    [alert addAction:Camera];
    [alert addAction:cancel];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (void)selectPhoto{
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:NULL];
    
    
}

- (void)takePhoto{
    
    DBCameraContainerViewController *cameraContainer = [[DBCameraContainerViewController alloc] initWithDelegate:self];
    [cameraContainer setFullScreenMode];
    
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:cameraContainer];
    [nav setNavigationBarHidden:YES];
    [self presentViewController:nav animated:YES completion:nil];
    
    
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    
        UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    //    profileImg.image = image;
    //    imageData = UIImagePNGRepresentation(profileImg.image);

    
    if (imageTag == 101) {
        truckImg1.image = chosenImage;
    }else if (imageTag == 102) {
        truckImg2.image = chosenImage;
    }else if (imageTag == 103) {
        truckImg3.image = chosenImage;
    }else if (imageTag == 104) {
        truckImg4.image = chosenImage;
    }

    [picker dismissViewControllerAnimated:YES completion:NULL];

    
}

- (void) camera:(id)cameraViewController didFinishWithImage:(UIImage *)image withMetadata:(NSDictionary *)metadata
{
//        UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
//    profileImg.image = image;
//    imageData = UIImagePNGRepresentation(profileImg.image);
    
    if (imageTag == 101) {
        truckImg1.image = image;
    }else if (imageTag == 102) {
        truckImg2.image = image;
    }else if (imageTag == 103) {
        truckImg3.image = image;
    }else if (imageTag == 104) {
        truckImg4.image = image;
    }
    
    

    
    //    DetailViewController *detail = [[DetailViewController alloc] init];
    //    [detail setDetailImage:image];
    //    [self.navigationController pushViewController:detail animated:NO];
    //    [cameraViewController restoreFullScreenMode];
    [self.presentedViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void) dismissCamera:(id)cameraViewController{

    [self dismissViewControllerAnimated:YES completion:nil];
    [cameraViewController restoreFullScreenMode];
    
}



- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

-(IBAction)regtruckAction{
    
    [Equipments resignFirstResponder];
    if (truckType.text.length==0) {
        [self showAlert:@"Please enter your Truck Type"];
    }else if (heightofTruck.text.length==0) {
        [self showAlert:@"Please enter your Truck Height"];
    }else if (widthofTruck.text.length==0) {
        [self showAlert:@"Please enter your Truck Width"];
    }else if (lengthofTruck.text.length==0) {
        [self showAlert:@"Please enter your Truck Length"];
    }else if (insurenceValidity.text.length==0){
        [self showAlert:@"Please enter your Truck Insurence validity date"];
    }else if (insurenceno.text.length==0){
        [self showAlert:@"Please enter the Rate of your Truck"];
    }else if (serviceCategories.text.length==0){
        [self showAlert:@"Please enter Truck Service Categories"];
    }else if (Equipments.text.length==0){
        [self showAlert:@"Please enter the Equipments of your Truck"];
    }else{
       /*
        QuickMovOTPViewController* otptxtView = [[QuickMovOTPViewController alloc]init];
        otptxtView.truckfname1 = truckfname;
        otptxtView.truckmname1 = truckmname;
        otptxtView.trucklname1 = trucklname;
        otptxtView.truckemail1 = truckemail;
        otptxtView.trucknumber1 = trucknumber;
        otptxtView.truckpass1 = truckpass;
        otptxtView.truckcpass1 = truckcpass;
        otptxtView.truckdateob1 = truckdateob;
        otptxtView.truckregImg= truckImg1.image;
        otptxtView.truckerUsetyp1 = truckerUsetyp;
        otptxtView.truckssn1 = _truckssn;
        otptxtView.truckLocation1 = _truckLocation;
        otptxtView.truckregion1 = regno.text;
        otptxtView.truckstreetaddr1 = _truckstreetaddr;
        otptxtView.truckType = truckType.text;
        otptxtView.widthofTruck = widthofTruck.text;
        otptxtView.lengthofTruck = lengthofTruck.text;
        otptxtView.heightofTruck = heightofTruck.text;
        otptxtView.regno = regno.text;
        otptxtView.insurenceValidity = insurenceValidity.text;
        otptxtView.insurenceno = insurenceno.text;
        otptxtView.serviceCategories = serviceCategories.text;
        otptxtView.Equipments = Equipments.text;
        otptxtView.accountNo = accountNo.text;
        otptxtView.routingNo = routingNo.text;
        otptxtView.truckregImgs1 = truckImg1.image;
        otptxtView.truckregImgs2 = truckImg2.image;
        otptxtView.truckregImgs3 = truckImg3.image;
        otptxtView.truckregImgs4 = truckImg4.image;
        otptxtView.truckregImg = truckregImg;
        
        [self.navigationController pushViewController:otptxtView animated:YES];

        */
//        [self AnothertruckupdateUserProfilePic];
        [self truckupdateUserProfilePic];
    }
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (void)getparentTypes{
    
    NSLog(@"%@",listofTypesArr);
    
    [regno resignFirstResponder];
    
    listofParentTypeArr = [NSMutableArray arrayWithArray:[listofTypesArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"parent_id == 0"]]];
}

- (void)getsubTypesWithTruckId:(NSInteger)truckid{
    
    NSLog(@"%@",listofTypesArr);
    
    NSArray * arr = [listofTypesArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"parent_id == %d",truckid]];
    NSLog(@"%@",arr);
    listofSubTypeArr = [NSMutableArray arrayWithArray:arr];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [truckerORmoverarr count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    // This will create a "invisible" footer
    return 0.001;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString * MyIdentifier = @"MyIdentifier";
    
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:MyIdentifier];
    }
   
    for (UIView * view1 in cell.contentView.subviews)
    {
        [view1 removeFromSuperview];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.backgroundColor = [UIColor clearColor];
    cell.textLabel.text = [NSString stringWithFormat:@"%@",[truckerORmoverarr objectAtIndex:indexPath.row]];
    NSLog(@"%@",[truckerORmoverarr objectAtIndex:indexPath.row]);
    cell.textLabel.textColor = [DesignObj quikRed];
    cell.textLabel.textAlignment = NSTextAlignmentLeft;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    serviceCategories.text = [NSString stringWithFormat:@"%@",[truckerORmoverarr objectAtIndex:indexPath.row]];
    [ViewForValuePicker removeFromSuperview];
    [Equipments becomeFirstResponder];
    
}

- (UITableView *)showExpandableView{
    expandableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 44, trucktypestblView.frame.size.width, 200) style:UITableViewStylePlain];
    expandableView.delegate = self;
    expandableView.dataSource = self;
    return expandableView;
}

- (void)subTruckTypeService:(GlobalObjects*)globalObj{
    
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
    [SVProgressHUD show];
    
    NSURL* url = [NSURL URLWithString:@"http://quikmov.quikmovapp.com/auth/getSubTrucktype"];
    NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession* session = [NSURLSession sessionWithConfiguration:config ];
    
    NSMutableURLRequest*request = [[NSMutableURLRequest alloc]initWithURL:url];
    request.HTTPMethod = @"POST";
    
    NSString* bodycntnt = [NSString stringWithFormat:@"truckid=%@",globalObj.truckTypesparentid];
    
    NSData *data = [bodycntnt dataUsingEncoding:NSUTF8StringEncoding];
    
    NSURLSessionUploadTask *uploadtask = [session uploadTaskWithRequest:request fromData:data completionHandler:^(NSData*data, NSURLResponse*response, NSError*error){
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
            [self subTypeserviceSuccess:data];
        });
    }];
    
    [uploadtask resume];
    
}

-(void)subTypeserviceSuccess:(NSData *)data{
    
    if (data != nil) {
        
        NSError * error;
        NSDictionary* dic = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        
        NSLog(@"%@******,%@",[dic allValues],[dic objectForKey:@"response"]);
        
        NSString * Result = [dic objectForKey:@"status"];
        
        NSLog(@"%@",Result);
        
        if ([Result isEqualToString:@"success"]){
            
            NSArray * arr = [dic objectForKey:@"result"];
            
            for (int i = 0; i < [arr count]; i++) {
                
                //
                subTypeModel * globleObj = [[subTypeModel alloc]init];
                NSDictionary * objDic = [arr objectAtIndex:i];
                
                globleObj.trucktypeid = [objDic objectForKey:@"trucktypeid"];
                globleObj.trucktype = [objDic objectForKey:@"trucktype"];
                globleObj.parent_id = [[objDic objectForKey:@"parent_id"] integerValue];
                globleObj.subcat = [objDic objectForKey:@"subcat"];
                //[listofSubTypeArr addObject:globleObj];
            }
            [self getparentTypes];
            [trucktypestblView reloadData];
        }else{
            
            GlobalObjects * globalObj = (GlobalObjects*)[listofTypesArr objectAtIndex:selectedRow];
            truckType.text = [NSString stringWithFormat:@"%@",globalObj.truckTypes];
            [trucktypestblView removeFromSuperview];
            [closebtn removeFromSuperview];
            [regno becomeFirstResponder];
        }
    }
}


-(void)truckupdateUserProfilePic{
    
    if ([Reachability reachabilityForInternetConnection]) {
        
        MBProgressHUD* hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.mode = MBProgressHUDModeIndeterminate;
        hud.labelText = @"Loading...";
        hud.labelColor = [DesignObj quikYellow];
        hud.animationType = MBProgressHUDAnimationZoomIn;
        hud.color = [DesignObj quikRed];
        hud.alpha = 0.8;
        
        NSArray *keys = [[NSArray alloc]initWithObjects:@"name",@"mname",@"lname",@"email",@"mobile",@"password",@"cpassword",@"dob",@"usertype",nil];
        
        //
        NSArray *values =[[NSArray alloc]initWithObjects:truckfname, truckmname, trucklname, truckemail, trucknumber,truckpass,truckcpass,truckdateob,truckerUsetyp, nil];
        
        NSURL *url =[NSURL URLWithString:[NSString stringWithFormat:@"http://quikmov.quikmovapp.com/auth/signup"]];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        [request setHTTPMethod:@"POST"];
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        NSMutableData *body = [NSMutableData data];
        
        if(!(truckregImg.imageOrientation == UIImageOrientationUp ||
             truckregImg.imageOrientation == UIImageOrientationUpMirrored))
        {
            CGSize imgsize = truckregImg.size;
            UIGraphicsBeginImageContext(imgsize);
            [truckregImg drawInRect:CGRectMake(0.0, 0.0, imgsize.width, imgsize.height)];
            truckregImg = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
        }
        
        NSData * data = [NSData dataWithData:UIImagePNGRepresentation(truckregImg)];
        //        11700.6005859
        //        float factor;
        //        float resol = image.size.height*image.size.width;
        //        if (resol >MIN_UPLOAD_RESOLUTION){
        //            factor = sqrt(resol/MIN_UPLOAD_RESOLUTION)*2;
        //            image = [self scaleDown:image withSize:CGSizeMake(image.size.width/factor, image.size.height/factor)];
        //        }
        
        //Compress the image
        CGFloat compression = 0.9f;
        CGFloat maxCompression = 0.1f;
        
        while ([data length] > MAX_UPLOAD_SIZE && compression > maxCompression)
        {
            compression -= 0.10;
            data = UIImageJPEGRepresentation(truckregImg, compression);
            NSLog(@"Compress : %lu",(unsigned long)data.length);
        }

        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"imagefile\"; filename=\"%@userprofile.png\"\r\n",truckfname] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: image/png\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[NSData dataWithData:data]];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        for (int i=0;i<keys.count;i++) {
            
            NSLog(@"%@",[keys objectAtIndex:i]);
            NSString *key = [keys objectAtIndex:i];
            NSString *value = [values objectAtIndex:i];
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n%@",key, value] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [request setHTTPBody:body];
        
        NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession* session = [NSURLSession sessionWithConfiguration:config ];
        
        
        NSURLSessionUploadTask *uploadtask = [session uploadTaskWithRequest:request fromData:body completionHandler:^(NSData*data, NSURLResponse*response, NSError*error){
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                
                NSString* str = [[NSString alloc]initWithData:data
                                                     encoding:NSUTF8StringEncoding];
                
                NSError* error;
                
                if (str.length == 0) {
                    
                    [self showAlert:@"Server Error"];
                    
                }
                
                NSDictionary* dic = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                
                NSLog(@"%@******%@,%@",str,[dic allValues],[dic objectForKey:@"response"]);
                
                // userId = [[dic objectForKey:@"message"] stringValue];
                NSString * Result = [dic objectForKey:@"status"];
                
                NSDictionary * result = [dic objectForKey:@"result"];
                
                if ([[dic objectForKey:@"result"] isKindOfClass:[NSDictionary class]]){
                    
                    regUserid = [result objectForKey:@"user_id"];
                    NSLog(@"%@",regUserid);
                    
                    NSUserDefaults* userStoredetails = [NSUserDefaults standardUserDefaults];
                    [userStoredetails setObject:regUserid forKey:@"user_id"];
                    [userStoredetails synchronize];
                    
                    NSLog(@"%@",Result);
                    
                    if ([Result isEqualToString:@"success"]){
                        
                        otp = [result objectForKey:@"otp"];
                        
//                        UIAlertController * alert=   [UIAlertController alertControllerWithTitle:@"Message"
//                                                                                         message:@"Registration Successful" preferredStyle:UIAlertControllerStyleAlert];
//                        
//                        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK"
//                                                                     style:UIAlertActionStyleDefault
//                                                                   handler:^(UIAlertAction * action)
//                                             {
//                                                 [alert dismissViewControllerAnimated:YES completion:nil];
                        
                                                 
                                                 [self truckregService];
                                                 
                                            // }];
                        
                        
                      //  [alert addAction:ok];
                        
                     //   [self presentViewController:alert animated:YES completion:nil];
                    }else{
                        
                        [self showAlert:[NSString stringWithFormat:@"%@",[dic objectForKey:@"result"]]];
                    }
                }
                
            });
            
            
        }];
        [uploadtask resume];
        
        
        //        NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        //        if( theConnection )
        //        {
        //            _downloadedData = [NSMutableData data] ;
        //            NSLog(@"request uploading successful");
        //        }
        //        else
        //        {
        //            NSLog(@"theConnection is NULL");
        //        }
        
    }else{
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"No internet connectivity" message:@"Please try later" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];


    }
}
 
/*

-(void)truckupdateUserProfilePic{
    
    if ([Reachability reachabilityForInternetConnection]) {
        
        MBProgressHUD* hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.mode = MBProgressHUDModeIndeterminate;
        hud.labelText = @"Loading...";
        hud.labelColor = [DesignObj quikYellow];
        hud.animationType = MBProgressHUDAnimationZoomIn;
        hud.color = [DesignObj quikRed];
        hud.alpha = 0.8;
        
//        NSString* bodycntnt = [NSString stringWithFormat:@"email=%@&password=%@&fname=%@&lname=%@&phone=%@&dob=%@&ssn=%@&address=%@&locality=%@&region=%@&zipcode=%@&accountNumber=%@&routingNumber=%@&companyName=%@&appKeyToken=%@",truckemail, truckpass,truckfname,trucklname,trucknumber,truckdateob,_truckssn,_truckstreetaddr,_truckLocation,_truckregion,_truckpostal,accountNo.text,routingNo.text,widthofTruck.text,APP_KEY];
        
        NSArray *keys = [[NSArray alloc]initWithObjects:@"email",@"password",@"fname",@"lname",@"phone",@"dob",@"ssn",@"address",@"locality",@"region",@"zipcode",@"accountNumber",@"routingNumber",@"companyName",@"appKeyToken",nil];
        
        //
        NSArray *values =[[NSArray alloc]initWithObjects:truckemail, truckpass,truckfname,trucklname,trucknumber,truckdateob,_truckssn,_truckstreetaddr,_truckLocation,_truckregion,_truckpostal,accountNo.text,routingNo.text,widthofTruck.text,APP_KEY, nil];
        
        NSURL *url =[NSURL URLWithString:[NSString stringWithFormat:@"http://prod.quikmovapp.com/member/truckerRegister"]];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        [request setHTTPMethod:@"POST"];
       NSString *boundary = @"---------------------------14737809831466499882746641449";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        NSMutableData *body = [NSMutableData data];
        
        if(!(truckregImg.imageOrientation == UIImageOrientationUp ||
             truckregImg.imageOrientation == UIImageOrientationUpMirrored))
        {
            CGSize imgsize = truckregImg.size;
            UIGraphicsBeginImageContext(imgsize);
            [truckregImg drawInRect:CGRectMake(0.0, 0.0, imgsize.width, imgsize.height)];
            truckregImg = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
        }
        
        NSData * data = [NSData dataWithData:UIImagePNGRepresentation(truckregImg)];
        //        11700.6005859
        //        float factor;
        //        float resol = image.size.height*image.size.width;
        //        if (resol >MIN_UPLOAD_RESOLUTION){
        //            factor = sqrt(resol/MIN_UPLOAD_RESOLUTION)*2;
        //            image = [self scaleDown:image withSize:CGSizeMake(image.size.width/factor, image.size.height/factor)];
        //        }
        
        //Compress the image
        CGFloat compression = 0.9f;
        CGFloat maxCompression = 0.1f;
        
        while ([data length] > MAX_UPLOAD_SIZE && compression > maxCompression)
        {
            compression -= 0.10;
            data = UIImageJPEGRepresentation(truckregImg, compression);
            NSLog(@"Compress : %lu",(unsigned long)data.length);
        }
        
        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"imagefile\"; filename=\"%@userprofile.png\"\r\n",truckfname] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: image/png\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[NSData dataWithData:data]];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        for (int i=0;i<keys.count;i++) {
            
            NSLog(@"%@",[keys objectAtIndex:i]);
            NSString *key = [keys objectAtIndex:i];
            NSString *value = [values objectAtIndex:i];
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n%@",key, value] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [request setHTTPBody:body];
        
        NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession* session = [NSURLSession sessionWithConfiguration:config ];
        
        
        NSURLSessionUploadTask *uploadtask = [session uploadTaskWithRequest:request fromData:body completionHandler:^(NSData*data, NSURLResponse*response, NSError*error){
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                
                NSString* str = [[NSString alloc]initWithData:data
                                                     encoding:NSUTF8StringEncoding];
                
                NSError* error;
                
                if (str.length == 0) {
                    
                    [self showAlert:@"Server Error"];
                    
                }
                
                NSDictionary* dic = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                
                NSLog(@"%@******%@,%@",str,[dic allValues],[dic objectForKey:@"response"]);
                
                // userId = [[dic objectForKey:@"message"] stringValue];
                NSString * Result = [dic objectForKey:@"status"];
                
                NSDictionary * result = [dic objectForKey:@"result"];
                
                    regUserid = [result objectForKey:@"user_id"];
                    NSLog(@"%@",regUserid);
                    
                    NSUserDefaults* userStoredetails = [NSUserDefaults standardUserDefaults];
                    [userStoredetails setObject:regUserid forKey:@"user_id"];
                    [userStoredetails synchronize];
                    
                    NSLog(@"%@",Result);
                    
                    if ([Result isEqualToString:@"success"]){
                        
                        otp = [result objectForKey:@"otp"];
                        
                        //                        UIAlertController * alert=   [UIAlertController alertControllerWithTitle:@"Message"
                        //                                                                                         message:@"Registration Successful" preferredStyle:UIAlertControllerStyleAlert];
                        //
                        //                        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK"
                        //                                                                     style:UIAlertActionStyleDefault
                        //                                                                   handler:^(UIAlertAction * action)
                        //                                             {
                        //                                                 [alert dismissViewControllerAnimated:YES completion:nil];
                        
                        
                        [self truckregService];
                        
                        // }];
                        
                        
                        //  [alert addAction:ok];
                        
                        //   [self presentViewController:alert animated:YES completion:nil];
                    }else{
                        
                        [self showAlert:[NSString stringWithFormat:@"%@",[dic objectForKey:@"result"]]];
                    }

            });
            
            
        }];
        [uploadtask resume];
        
        
        //        NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        //        if( theConnection )
        //        {
        //            _downloadedData = [NSMutableData data] ;
        //            NSLog(@"request uploading successful");
        //        }
        //        else
        //        {
        //            NSLog(@"theConnection is NULL");
        //        }
        
    }else{
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"No internet connectivity" message:@"Please try later" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        
    }

}*/


-(void)truckregService{
    
        if ([Reachability reachabilityForInternetConnection]) {
            
            MBProgressHUD* hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            hud.mode = MBProgressHUDModeIndeterminate;
            hud.labelText = @"Loading...";
            hud.labelColor = [DesignObj quikYellow];
            hud.animationType = MBProgressHUDAnimationZoomIn;
            hud.color = [DesignObj quikRed];
            hud.alpha = 0.8;
            
//            inputs :trucktype,truckheight,truckwidth,trucklen,imagefile, userid , regno,insurance,rate,,servicecategories,equipment
//            url : http://truckcsoft.mapcee.com/auth/truckerProfile
            
            NSUserDefaults* userStoredetails = [NSUserDefaults standardUserDefaults];
            NSString * userID = [userStoredetails objectForKey:@"user_id"];
            NSLog(@"%@",userID);
            
            NSArray *keys = [[NSArray alloc]initWithObjects:@"trucktype",@"truckheight",@"truckwidth",@"trucklen",@"userid",@"regno",@"insurance",@"rate",@"servicecategories",@"equipment",nil];
            //
            NSArray *values =[[NSArray alloc]initWithObjects:truckType.text, heightofTruck.text, widthofTruck.text,lengthofTruck.text,userID,regno.text,insurenceValidity.text,insurenceno.text,serviceCategories.text,Equipments.text, nil];
            

            NSURL *url =[NSURL URLWithString:[NSString stringWithFormat:@"http://quikmov.quikmovapp.com/auth/truckerProfile"]];
            
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            [request setHTTPMethod:@"POST"];
            NSString *boundary = @"---------------------------14737809831466499882746641449";
            NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
            [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
            
            NSMutableData *body = [NSMutableData data];
            NSData * data = [NSData dataWithData:UIImagePNGRepresentation(truckImg1.image)];
//            NSData * data2 = [NSData dataWithData:UIImagePNGRepresentation(truckImg2.image)];
//            NSData * data3 = [NSData dataWithData:UIImagePNGRepresentation(truckImg3.image)];
//            NSData * data4 = [NSData dataWithData:UIImagePNGRepresentation(truckImg4.image)];
            
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"imagefile\"; filename=\"%@userprofile.png\"\r\n",userID] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[@"Content-Type: image/png\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            
            [body appendData:[NSData dataWithData:data]];
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            
            for (int i=0;i<keys.count;i++) {
                
                NSLog(@"%@",[keys objectAtIndex:i]);
                NSString *key = [keys objectAtIndex:i];
                NSString *value = [values objectAtIndex:i];
                [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n%@",key, value] dataUsingEncoding:NSUTF8StringEncoding]];
            }
            
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            
            [request setHTTPBody:body];
            
            NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
            NSURLSession* session = [NSURLSession sessionWithConfiguration:config ];
            
            NSURLSessionUploadTask *uploadtask = [session uploadTaskWithRequest:request fromData:body completionHandler:^(NSData*data, NSURLResponse*response, NSError*error){
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                    
                    NSString* str = [[NSString alloc]initWithData:data
                                                         encoding:NSUTF8StringEncoding];
                    
                    if(str.length == 0){
                        
                        [self showAlert:@"Server Error\n Please click JOIN again"];
                    }
                    NSError* error;
                    
                    
                    
                    NSDictionary* dic = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                    
                    NSLog(@"%@******%@,%@",str,[dic allValues],[dic objectForKey:@"response"]);
                    
        // userId = [[dic objectForKey:@"message"] stringValue];
       // BOOL Result = [dic objectForKey:[NSString stringWithFormat: @"message"]];
        
                    //             NSLog(@"%hhd",Result);
                    
                    if ([[dic objectForKey:@"iserror"]boolValue] == NO){
                        
//                        UIAlertController * alert=   [UIAlertController alertControllerWithTitle:@"Message"
//                                                                                         message:@"Truck Registration Successful" preferredStyle:UIAlertControllerStyleAlert];
//                        
//                        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK"
//                                                                     style:UIAlertActionStyleDefault
//                                                                   handler:^(UIAlertAction * action)
//                                             {
//                                                 [alert dismissViewControllerAnimated:YES completion:nil];
//                                                 
//                                                 MapViewController* mapview = [[MapViewController alloc]init];
//                                                 [self.navigationController pushViewController:mapview animated:YES];
//                                                 
//                                             }];
//                        
//                        
//                        [alert addAction:ok];
//                        
//                        [self presentViewController:alert animated:YES completion:nil];
                        
                        [self uploadService];
                    }else{
                        
                        [self showAlert:[NSString stringWithFormat:@"%@",[dic objectForKey:@"result"]]];
                    }
                    
                });
                
                
            }];
            [uploadtask resume];
            
            
            //        NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
            //        if( theConnection )
            //        {
            //            _downloadedData = [NSMutableData data] ;
            //            NSLog(@"request uploading successful");
            //        }
            //        else
            //        {
            //            NSLog(@"theConnection is NULL");
            //        }
            
        }else{
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"No internet connectivity" message:@"Please try later" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        }
    }

-(UIImage*)scaleDown:(UIImage*)img withSize:(CGSize)newSize{
    
    
    UIGraphicsBeginImageContextWithOptions(newSize, YES, 0.0);
    [img drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return scaledImage;
}


- (void)uploadService{
    count = 0;
    [self imageService:@"1" withImage:truckImg1.image];
}

-(void)imageService:(NSString*)imagename withImage:(UIImage*)image{
    
    if ([Reachability reachabilityForInternetConnection]) {
        
            MBProgressHUD* hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            hud.mode = MBProgressHUDModeDeterminateHorizontalBar;
            hud.labelText = @"Uploading Truck Images...";
            hud.labelColor = [DesignObj quikYellow];
            hud.animationType = MBProgressHUDAnimationZoomIn;
            hud.color = [DesignObj quikRed];
            hud.alpha = 0.8;
        
        NSUserDefaults* userStoredetails = [NSUserDefaults standardUserDefaults];
        NSString * userID = [userStoredetails objectForKey:@"user_id"];
        NSLog(@"%@",userID);
        
        NSArray *keys = [[NSArray alloc]initWithObjects:@"userid",nil];
        //
        NSArray *values =[[NSArray alloc]initWithObjects:userID, nil];
        
        
        NSURL *url =[NSURL URLWithString:[NSString stringWithFormat:@"http://quikmov.quikmovapp.com/auth/truckImages"]];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        [request setHTTPMethod:@"POST"];
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        NSMutableData *body = [NSMutableData data];
        
        if(!(image.imageOrientation == UIImageOrientationUp ||
             image.imageOrientation == UIImageOrientationUpMirrored))
        {
            CGSize imgsize = image.size;
            UIGraphicsBeginImageContext(imgsize);
            [image drawInRect:CGRectMake(0.0, 0.0, imgsize.width, imgsize.height)];
            image = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
        }
        
        NSData * data = [NSData dataWithData:UIImagePNGRepresentation(image)];
        //        11700.6005859
        //        float factor;
        //        float resol = image.size.height*image.size.width;
        //        if (resol >MIN_UPLOAD_RESOLUTION){
        //            factor = sqrt(resol/MIN_UPLOAD_RESOLUTION)*2;
        //            image = [self scaleDown:image withSize:CGSizeMake(image.size.width/factor, image.size.height/factor)];
        //        }
        
        //Compress the image
        CGFloat compression = 0.9f;
        CGFloat maxCompression = 0.1f;
        
        while ([data length] > MAX_UPLOAD_SIZE && compression > maxCompression)
        {
            compression -= 0.10;
            data = UIImageJPEGRepresentation(image, compression);
            NSLog(@"Compress : %lu",(unsigned long)data.length);
        }


        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"imagefile\"; filename=\"%@truk%@.png\"\r\n",userID,imagename] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: image/png\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[NSData dataWithData:data]];
        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        for (int i=0;i<keys.count;i++) {
            
            NSLog(@"%@",[keys objectAtIndex:i]);
            NSString *key = [keys objectAtIndex:i];
            NSString *value = [values objectAtIndex:i];
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n%@",key, value] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [request setHTTPBody:body];
        
        NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession* session = [NSURLSession sessionWithConfiguration:config ];
        
        NSURLSessionUploadTask *uploadtask = [session uploadTaskWithRequest:request fromData:body completionHandler:^(NSData*data, NSURLResponse*response, NSError*error){
            dispatch_async(dispatch_get_main_queue(), ^{

                [MBProgressHUD hideHUDForView:self.view animated:YES];
                
                NSString* str = [[NSString alloc]initWithData:data
                                                     encoding:NSUTF8StringEncoding];
                
                if (str.length == 0) {
                    [self showAlert:@"Server Error\n Please Click JOIN again"];
                }
                
                NSError* error;
                
                
                
                NSDictionary* dic = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                
                NSLog(@"%@******%@,%@",str,[dic allValues],[dic objectForKey:@"response"]);
                
                // userId = [[dic objectForKey:@"message"] stringValue];
                // BOOL Result = [dic objectForKey:[NSString stringWithFormat: @"message"]];
                
                //             NSLog(@"%hhd",Result);
                
                if ([[dic objectForKey:@"iserror"]boolValue] == NO){
                    
                    count = count + 1;
                    
                    if (count  < 4) {
                        
                        if (count == 1) {
                            [self imageService:@"2" withImage:truckImg2.image];
                        }else if (count == 2){
                            [self imageService:@"3" withImage:truckImg3.image];
                        }else if (count == 3){
                            [self imageService:@"4" withImage:truckImg4.image];
                        }
                        
                    }else{
                        UIAlertController * alert=   [UIAlertController alertControllerWithTitle:@"Message" message:[NSString stringWithFormat:@"Please enter the OTP\n%@",otp] preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                             {
                                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                                 
                                                 QuickMovOTPViewController* otpView = [[QuickMovOTPViewController alloc]init];
                                                 otpView.copiOTP = otp;
                                                 otpView.mobilenumbr = trucknumber;
                                                 [self.navigationController pushViewController:otpView animated:YES];
                                                 
                                             }];
                        
                        
                        [alert addAction:ok];
                        
                        [self presentViewController:alert animated:YES completion:nil];
                    }

                }else{
                    
                    [self showAlert:[NSString stringWithFormat:@"%@",[dic objectForKey:@"result"]]];
                }
                
            });
            
            
        }];
        [uploadtask resume];
        
        
        //        NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        //        if( theConnection )
        //        {
        //            _downloadedData = [NSMutableData data] ;
        //            NSLog(@"request uploading successful");
        //        }
        //        else
        //        {
        //            NSLog(@"theConnection is NULL");
        //        }
        
    }else{
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"No internet connectivity" message:@"Please try later" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    
}


-(void)truckTypeService{
    
//    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
//    [SVProgressHUD show];
    
    MBProgressHUD* hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = @"Loading...";
    hud.labelColor = [DesignObj quikYellow];
    hud.animationType = MBProgressHUDAnimationZoomIn;
    hud.color = [DesignObj quikRed];
    hud.alpha = 0.8;
    
    
    NSURL* url = [NSURL URLWithString:@"http://quikmov.quikmovapp.com/auth/getTrucktype"];
    NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession* session = [NSURLSession sessionWithConfiguration:config ];
    
    NSMutableURLRequest*request = [[NSMutableURLRequest alloc]initWithURL:url];
    request.HTTPMethod = @"POST";
    
    NSString* bodycntnt = [NSString stringWithFormat:@"button click=%@",truckType];
    
    NSData *data = [bodycntnt dataUsingEncoding:NSUTF8StringEncoding];
    
    NSURLSessionUploadTask *uploadtask = [session uploadTaskWithRequest:request fromData:data completionHandler:^(NSData*data, NSURLResponse*response, NSError*error){
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
            
            if (data == nil) {
                return;
            }
            NSString* str = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
            NSError * error;
            NSDictionary* dic = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            NSLog(@"%@******%@,%@",str,[dic allValues],[dic objectForKey:@"response"]);
            
            NSString * Result = [dic objectForKey:@"status"];
            
            NSLog(@"%@",Result);
            
            if ([Result isEqualToString:@"success"]){
                NSArray * arr = [dic objectForKey:@"result"];
                
                listofTypesArr = [[NSMutableArray alloc]init];
                
                for (int i = 0; i < [arr count]; i++) {
                    
                    
                    subTypeModel * globleObj = [[subTypeModel alloc]init];
                    NSDictionary * objDic = [arr objectAtIndex:i];
                    
                    globleObj.trucktypeid = [objDic objectForKey:@"trucktypeid"];
                    globleObj.trucktype = [objDic objectForKey:@"trucktype"];
                    globleObj.parent_id = [[objDic objectForKey:@"parent_id"] integerValue];
                    globleObj.subcat = [objDic objectForKey:@"subcat"];
                    
                    [listofTypesArr addObject:globleObj];
                }
                [self getparentTypes];
                [self loadpickerView];
                
                
            }else{
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Message" message:[NSString stringWithFormat:@"%@",[dic objectForKey:@"result"]] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alert show];
            }

            
            
        });
    }];
    
    [uploadtask resume];
    
    
}

- (void)serviceSuccess:(NSData *)data{
    if (data == nil) {
        return;
    }
    NSString* str = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
    NSError * error;
    NSDictionary* dic = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    NSLog(@"%@******%@,%@",str,[dic allValues],[dic objectForKey:@"response"]);
    
    NSString * Result = [dic objectForKey:@"status"];
    
    NSLog(@"%@",Result);
    
    if ([Result isEqualToString:@"success"]){
        NSArray * arr = [dic objectForKey:@"result"];
        
        listofTypesArr = [[NSMutableArray alloc]init];
        
        for (int i = 0; i < [arr count]; i++) {
            
            
            subTypeModel * globleObj = [[subTypeModel alloc]init];
            NSDictionary * objDic = [arr objectAtIndex:i];
            
            globleObj.trucktypeid = [objDic objectForKey:@"trucktypeid"];
            globleObj.trucktype = [objDic objectForKey:@"trucktype"];
            globleObj.parent_id = [[objDic objectForKey:@"parent_id"] integerValue];
            globleObj.subcat = [objDic objectForKey:@"subcat"];
            
            [listofTypesArr addObject:globleObj];
        }
        [self getparentTypes];
        [self loadpickerView];


    }else{
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Message" message:[NSString stringWithFormat:@"%@",[dic objectForKey:@"result"]] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }

}

-(void)loadpickerView{
    
//    [typesView reloadAllComponents];
    
    ViewforPicker = [[UIView alloc]initWithFrame:CGRectMake(0, screenHeight - 200, screenWidth, 200)];
    ViewforPicker.backgroundColor = [UIColor grayColor];
    
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, screenWidth, 44)];
    numberToolbar.barStyle = UIBarStyleDefault;//UIBarStyleBlackTranslucent;
    numberToolbar.tintColor = [UIColor colorWithRed:207/255.0 green:26/255.0 blue:26/255.0 alpha:1.0];
    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleDone target:self action:@selector(cancelNumberPad)],
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];

    [ViewforPicker addSubview:numberToolbar];
    
//    UIBarButtonItem* barButn=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)];
    
    typesView = [[UIPickerView alloc]init];
    typesView.frame = CGRectMake(0, 44, screenWidth/2, 156);
    typesView.backgroundColor = [UIColor whiteColor];
    typesView.delegate = self;
    typesView.dataSource = self;
    [ViewforPicker addSubview:typesView];
    
    /*
    barView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, typesView.frame.size.width, 50)];
    barView.backgroundColor = [UIColor blackColor];
    [typesView addSubview:barView];
    
    UIButton* buttondone = [UIButton buttonWithType:UIButtonTypeCustom];
    buttondone.frame = CGRectMake(10, 10 , 60, 30);
    [buttondone addTarget:self action:@selector(doneWithNumberPad) forControlEvents:UIControlEventTouchUpInside];
    [buttondone setTitle:@"Done" forState:UIControlStateNormal];
    [buttondone setTitleColor:[DesignObj quikRed] forState:UIControlStateNormal];
    [buttondone setUserInteractionEnabled:YES];
    [barView addSubview:buttondone];
     */
    
    typesView2 = [[UIPickerView alloc]init];
    typesView2.frame = CGRectMake(screenWidth/2, 44, screenWidth/2, 156);
    typesView2.backgroundColor = [UIColor whiteColor];
    typesView2.delegate = self;
    typesView2.dataSource = self;
    [ViewforPicker addSubview:typesView2];
    
    [self.view addSubview:ViewforPicker];
       
    
    
}

-(void)hello{
    
    
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    
    if (pickerView == typesView) {
        return [listofParentTypeArr count];
    }else if (pickerView == truckerORmover){
        
        return [truckerORmoverarr count];
    }
    else{
        return [listofSubTypeArr count];
    }
    
    return 0;
}
/*
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    // Component 0 should load the array1 values, Component 1 will have the array2 values
    
    if (pickerView == typesView) {
        subTypeModel * globalObj = (subTypeModel*)[listofParentTypeArr objectAtIndex:row];
        
        return globalObj.trucktype;
    }
    else if (pickerView == typesView2) {
        subTypeModel * globalObj = (subTypeModel*)[listofSubTypeArr objectAtIndex:row];
        return globalObj.trucktype;
    }
    return nil;
} */

-(UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    
    if (!view)
    {
        view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screenWidth/2, 80)];
        
        if (pickerView == typesView) {
            
            subTypeModel * globalObj = (subTypeModel*)[listofParentTypeArr objectAtIndex:row];
            
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 150, 50)];
            label.font = [UIFont systemFontOfSize:16];
            label.backgroundColor = [UIColor clearColor];
            label.textColor = [UIColor blackColor];
            label.textAlignment = NSTextAlignmentLeft;
            label.tag = 1;
            label.text = [NSString stringWithFormat:@"%@",globalObj.trucktype];
            [view addSubview:label];

        }else if (pickerView == truckerORmover){
            
            view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screenWidth, 80)];
            
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake((screenWidth - 150)/2, 0, 150, 80)];
            label.font = [UIFont systemFontOfSize:18];
            label.backgroundColor = [UIColor clearColor];
            label.textColor = [UIColor blackColor];
            label.textAlignment = NSTextAlignmentCenter;
            label.tag = 1;
            label.text = [NSString stringWithFormat:@"%@",[truckerORmoverarr objectAtIndex:row]];
            [view addSubview:label];
        }
        
        else{
            
            subTypeModel * globalObj = (subTypeModel*)[listofSubTypeArr objectAtIndex:row];
            
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 150, 50)];
            label.font = [UIFont systemFontOfSize:16];
            label.backgroundColor = [UIColor clearColor];
            label.textColor = [UIColor blackColor];
            label.textAlignment = NSTextAlignmentLeft;
            label.tag = 1;
            label.text = [NSString stringWithFormat:@"%@",globalObj.trucktype];
            [view addSubview:label];
            
        }
    }
    
    return view;
    
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    if (pickerView == typesView2) {
        
        if (listofSubTypeArr.count > 0 ) {
         
        
        subTypeModel * globalObj = (subTypeModel*)[listofSubTypeArr objectAtIndex:row];
        truckType.text = [NSString stringWithFormat:@"%@",globalObj.trucktype];
        }
        
    }else if(pickerView == truckerORmover){
        
        serviceCategories.text = [NSString stringWithFormat:@"%@",[truckerORmoverarr objectAtIndex:row]];

    }
        
        else{
          subTypeModel * globalObj = (subTypeModel*)[listofParentTypeArr objectAtIndex:row];
        
        [self getsubTypesWithTruckId:[globalObj.trucktypeid integerValue]];
        
        if (listofSubTypeArr.count == 0) {
            truckType.text = [NSString stringWithFormat:@"%@",globalObj.trucktype];
//            typesave = [NSUserDefaults standardUserDefaults];
//            [typesave setObject:truckType.text forKey:@"typeview"];
//            [typesave synchronize];
                    }
        [typesView2 reloadAllComponents];
    }
    
   
}

-(void)AnothertruckupdateUserProfilePic{
    
    if ([Reachability reachabilityForInternetConnection]) {
        
        MBProgressHUD* hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.mode = MBProgressHUDModeIndeterminate;
        hud.labelText = @"Loading...";
        hud.labelColor = [DesignObj quikYellow];
        hud.animationType = MBProgressHUDAnimationZoomIn;
        hud.color = [DesignObj quikRed];
        hud.alpha = 0.8;
        
        //        NSString* bodycntnt = [NSString stringWithFormat:@"email=%@&password=%@&fname=%@&lname=%@&phone=%@&dob=%@&ssn=%@&address=%@&locality=%@&region=%@&zipcode=%@&accountNumber=%@&routingNumber=%@&companyName=%@&appKeyToken=%@",truckemail, truckpass,truckfname,trucklname,trucknumber,truckdateob,_truckssn,_truckstreetaddr,_truckLocation,_truckregion,_truckpostal,accountNo.text,routingNo.text,widthofTruck.text,APP_KEY];
        
        /*  NSArray *keys = [[NSArray alloc]initWithObjects:@"email",@"password",@"fname",@"lname",@"phone",@"dob",@"ssn",@"address",@"locality",@"region",@"zipcode",@"accountNumber",@"routingNumber",@"companyName",@"appKeyToken",nil];
         
         //
         NSArray *values =[[NSArray alloc]initWithObjects:truckemail, truckpass,truckfname,trucklname,trucknumber,truckdateob,_truckssn,_truckstreetaddr,_truckLocation,_truckregion,_truckpostal,accountNo.text,routingNo.text,widthofTruck.text,APP_KEY, nil];
         */
        
        //        NSURL *url =[NSURL URLWithString:[NSString stringWithFormat:@"http://prod.quikmovapp.com/member/truckerRegister"]];
        
        dict = [[NSMutableDictionary alloc]init];
        
        NSString* companyName = @"quickmoveTrucker";
        
        [dict setValue:truckemail forKey:@"email"];
        [dict setValue:truckpass forKey:@"password"];
        [dict setValue:truckfname forKey:@"fname"];
        [dict setValue:trucklname forKey:@"lname"];
        [dict setValue:trucknumber forKey:@"phone"];
        [dict setValue:truckdateob forKey:@"dob"];
        [dict setValue:_truckssn forKey:@"ssn"];
        [dict setValue:_truckstreetaddr forKey:@"address"];
        [dict setValue:_truckLocation forKey:@"locality"];
        [dict setValue:_truckregion forKey:@"region"];
        [dict setValue:_truckpostal forKey:@"zipcode"];
        [dict setValue:accountNo.text forKey:@"accountNumber"];
        [dict setValue:routingNo.text forKey:@"routingNumber"];
        [dict setValue:companyName forKey:@"companyName"];
        [dict setValue:APP_KEY forKey:@"appKeyToken"];
        
        NSLog(@"%@",dict);
        NSError * error;
        NSData *data = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:&error];
        
        NSString* newStr = [[NSString alloc] initWithData:data
                                                 encoding:NSUTF8StringEncoding];
        
        NSLog(@"%@",newStr);
        
        [self startWith:data];
        
    }
}

- (void)startWith:(NSData*)bodyStr{
    
    NSURLConnection * Globalconnection;
    [Globalconnection cancel];
    Globalconnection = nil;
    
    NSURL *url =[NSURL URLWithString:[NSString stringWithFormat:@"http://prod.quikmovapp.com/member/truckerRegister"]];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:30.0];
    [request setHTTPMethod:@"POST"];
    //Pass some default parameter(like content-type etc.)
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setHTTPBody:bodyStr];
    Globalconnection = [NSURLConnection connectionWithRequest:request delegate:self];
    [Globalconnection start];
    
    if(Globalconnection)
        _downloadedData = [NSMutableData data];
    
}

#pragma mark -
#pragma mark NSURLConnectionDelegate

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    //This function is called when the download begins.
    //You can get all the response headers
    if (_downloadedData!=nil) {
        _downloadedData = nil;
    }
    _downloadedData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    //This function is called whenever there is downloaded data available
    //It will be called multiple times and each time you will get part of downloaded data
    [_downloadedData appendData:data];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection{
    
    [self DownloadfileCompleted:_downloadedData];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    
//    [self.delegate DownloadfilHasError:error];
}

-(void)DownloadfileCompleted:(NSData*)responsedata{
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    NSString* newStr = [[NSString alloc] initWithData:responsedata
                                             encoding:NSUTF8StringEncoding];
    
    NSError* error;
    NSDictionary* dic = [NSJSONSerialization JSONObjectWithData:responsedata options:kNilOptions
                                                          error:&error];
    
    NSLog(@"%@*****,%@",newStr,[dic objectForKey:@"result"]);
    
    NSString * result = [dic objectForKey:@"result"];
    
        if ([result isEqualToString:@"Please enter valid inputs"]) {
            
            UIAlertController * alert=   [UIAlertController alertControllerWithTitle:@"Message"
                                                                                                                                   message:@"Please Enter Valid Details" preferredStyle:UIAlertControllerStyleAlert];
                                          
                                                                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK"
                                                                                                               style:UIAlertActionStyleDefault
                                                                                                             handler:^(UIAlertAction * action)
                                                                                       {
                                                                                           [alert dismissViewControllerAnimated:YES completion:nil];
                                          
                                           }];
                                          
                                          
                                            [alert addAction:ok];
                                          
                                             [self presentViewController:alert animated:YES completion:nil];

                    
        }else if ([result isEqualToString:@"Trucker Registered successfully"]){
//
            regUserid = @"21";
            
            NSUserDefaults* userStoredetails = [NSUserDefaults standardUserDefaults];
            [userStoredetails setObject:regUserid forKey:@"user_id"];
            [userStoredetails synchronize];
            
            [self truckregService];
        }
        
        else{
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Message" message:[NSString stringWithFormat:@"%@",[dic objectForKey:@"result"]] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
