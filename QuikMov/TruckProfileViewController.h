//
//  TruckProfileViewController.h
//  QuikMov
//
//  Created by Ajmal Khan on 3/1/16.
//  Copyright © 2016 CSCS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "UIImageView+WebCache.h"

@interface TruckProfileViewController : UIViewController<UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITableViewDataSource,UITableViewDelegate>{
    
    CGFloat screenWidth;
    CGFloat screenHeight;
}

@property(nonatomic, strong)UIImageView* truckImg1;
@property(nonatomic, strong)UIImageView* truckImg2;
@property(nonatomic, strong)UIImageView* truckImg3;
@property(nonatomic, strong)UIImageView* truckImg4;
@property(nonatomic, copy)UIImage* truckregImg;
@property(nonatomic, copy)NSString* truckfname;;
@property(nonatomic, copy)NSString* truckmname;
@property(nonatomic, copy)NSString* trucklname;
@property(nonatomic, copy)NSString* trucknumber;
@property(nonatomic, copy)NSString* truckemail;
@property(nonatomic, copy)NSString* truckpass;
@property(nonatomic, copy)NSString* truckcpass;
@property(nonatomic, copy)NSString* truckdateob;
@property(nonatomic, copy)NSString* truckerUsetyp;

@end
