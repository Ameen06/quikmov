//
//  CSMarker.m
//  QuikMov
//
//  Created by Ajmal Khan on 3/1/16.
//  Copyright © 2016 CSCS. All rights reserved.
//

#import "CSMarker.h"

@implementation CSMarker

-(BOOL)isEqual:(id)object{
    
    CSMarker* othermarker = (CSMarker*)object;
    if ([self.objectID isEqualToString:othermarker.objectID]) {
        
        return YES;
    }
        
        return NO;
}

-(NSUInteger)hash{
    
    return [self.objectID hash];
}

@end
