//
//  RegisterViewController.m
//  QuikMov
//
//  Created by CSCS on 1/25/16.
//  Copyright © 2016 CSCS. All rights reserved.
//

#import "RegisterViewController.h"
#import "UserTypeViewController.h"
#import "MapViewController.h"
#import "TruckRegisterViewController.h"
#import "QuikMovGoogleViewController.h"
#import "QuickMovOTPViewController.h"
#import "SVProgressHUD.h"
#import "DesignObj.h"
#import "GlobalObjects.h"
#import "Reachability.h"
#import "DBCameraViewController.h"
#import "DBCameraContainerViewController.h"
#import "AppDelegate.h"

#define kOFFSET_FOR_KEYBOARD 80.0
#define MIN_UPLOAD_RESOLUTION 320 * 480
#define MAX_UPLOAD_SIZE 1124000

@interface RegisterViewController ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate,DBCameraViewControllerDelegate>{
    
    int iphone6Yaxis;
    UIDatePicker * datePicker;
    NSMutableArray * txtfieldarr;
    UITextField * Txt;
    UITextField * fnameTxt;
    UITextField * mnameTxt;
    UITextField * lnameTxt;
    UITextField * emailTxt;
    UITextField * mobNum;
    UITextField * passTxt;
    UITextField * conpassTxt;
    UITextField * commontxt;
    UITextField * dobTxt;
    UITextField * SSN;
    UITextField * streetaddr;
    UITextField * region;
    UITextField * location;
    UITextField * postalcode;
    UILabel* termsandcondi;
    UIButton* regbtn;
    
    NSArray* titlearr;
    UIButton* selectimage;
    NSData *imageData;
    UIActivityIndicatorView* indicator;
    NSMutableURLRequest *requeest;
    NSMutableData *_responseData;
    NSString*userId;
    int imageTag;
    UITapGestureRecognizer* taptoKeyboardHide;
    
    int height;
    
    UIImageView* profileImg;
    NSDictionary *dictDialingCodes;
    NSString * countryCode;
    NSString * codeValue;
    NSString * otp;
   
}

@property (strong, nonatomic)UIScrollView* scrView;


@end


@implementation RegisterViewController
@synthesize utype;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Register";
    
    self.navigationController.navigationBarHidden = NO;
    
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [DesignObj quikYellow]}];
    self.navigationController.navigationBar.barTintColor = [DesignObj quikRed];
    
    self.navigationController.navigationBar.tintColor = [DesignObj quikYellow];
    
    CTTelephonyNetworkInfo * networkInfo = [[CTTelephonyNetworkInfo alloc]init];
    CTCarrier * carrier = [networkInfo subscriberCellularProvider];
    
    countryCode = [carrier.isoCountryCode uppercaseString];
    
    NSLog(@"%@",countryCode);
    
    dictDialingCodes = [[NSDictionary alloc]initWithObjectsAndKeys:
                        @"972", @"IL",
                        @"93", @"AF",
                        @"355", @"AL",
                        @"213", @"DZ",
                        @"1", @"AS",
                        @"376", @"AD",
                        @"244", @"AO",
                        @"1", @"AI",
                        @"1", @"AG",
                        @"54", @"AR",
                        @"374", @"AM",
                        @"297", @"AW",
                        @"61", @"AU",
                        @"43", @"AT",
                        @"994", @"AZ",
                        @"1", @"BS",
                        @"973", @"BH",
                        @"880", @"BD",
                        @"1", @"BB",
                        @"375", @"BY",
                        @"32", @"BE",
                        @"501", @"BZ",
                        @"229", @"BJ",
                        @"1", @"BM", @"975", @"BT",
                        @"387", @"BA", @"267", @"BW", @"55", @"BR", @"246", @"IO",
                        @"359", @"BG", @"226", @"BF", @"257", @"BI", @"855", @"KH",
                        @"237", @"CM", @"1", @"CA", @"238", @"CV", @"345", @"KY",
                        @"236", @"CF", @"235", @"TD", @"56", @"CL", @"86", @"CN",
                        @"61", @"CX", @"57", @"CO", @"269", @"KM", @"242", @"CG",
                        @"682", @"CK", @"506", @"CR", @"385", @"HR", @"53", @"CU",
                        @"537", @"CY", @"420", @"CZ", @"45", @"DK", @"253", @"DJ",
                        @"1", @"DM", @"1", @"DO", @"593", @"EC", @"20", @"EG",
                        @"503", @"SV", @"240", @"GQ", @"291", @"ER", @"372", @"EE",
                        @"251", @"ET", @"298", @"FO", @"679", @"FJ", @"358", @"FI",
                        @"33", @"FR", @"594", @"GF", @"689", @"PF", @"241", @"GA",
                        @"220", @"GM", @"995", @"GE", @"49", @"DE", @"233", @"GH",
                        @"350", @"GI", @"30", @"GR", @"299", @"GL", @"1", @"GD",
                        @"590", @"GP", @"1", @"GU", @"502", @"GT", @"224", @"GN",
                        @"245", @"GW", @"595", @"GY", @"509", @"HT", @"504", @"HN",
                        @"36", @"HU", @"354", @"IS", @"91", @"IN", @"62", @"ID",
                        @"964", @"IQ", @"353", @"IE", @"972", @"IL", @"39", @"IT",
                        @"1", @"JM", @"81", @"JP", @"962", @"JO", @"77", @"KZ",
                        @"254", @"KE", @"686", @"KI", @"965", @"KW", @"996", @"KG",
                        @"371", @"LV", @"961", @"LB", @"266", @"LS", @"231", @"LR",
                        @"423", @"LI", @"370", @"LT", @"352", @"LU", @"261", @"MG",
                        @"265", @"MW", @"60", @"MY", @"960", @"MV", @"223", @"ML",
                        @"356", @"MT", @"692", @"MH", @"596", @"MQ", @"222", @"MR",
                        @"230", @"MU", @"262", @"YT", @"52", @"MX", @"377", @"MC",
                        @"976", @"MN", @"382", @"ME", @"1", @"MS", @"212", @"MA",
                        @"95", @"MM", @"264", @"NA", @"674", @"NR", @"977", @"NP",
                        @"31", @"NL", @"599", @"AN", @"687", @"NC", @"64", @"NZ",
                        @"505", @"NI", @"227", @"NE", @"234", @"NG", @"683", @"NU",
                        @"672", @"NF", @"1", @"MP", @"47", @"NO", @"968", @"OM",
                        @"92", @"PK", @"680", @"PW", @"507", @"PA", @"675", @"PG",
                        @"595", @"PY", @"51", @"PE", @"63", @"PH", @"48", @"PL",
                        @"351", @"PT", @"1", @"PR", @"974", @"QA", @"40", @"RO",
                        @"250", @"RW", @"685", @"WS", @"378", @"SM", @"966", @"SA",
                        @"221", @"SN", @"381", @"RS", @"248", @"SC", @"232", @"SL",
                        @"65", @"SG", @"421", @"SK", @"386", @"SI", @"677", @"SB",
                        @"27", @"ZA", @"500", @"GS", @"34", @"ES", @"94", @"LK",
                        @"249", @"SD", @"597", @"SR", @"268", @"SZ", @"46", @"SE",
                        @"41", @"CH", @"992", @"TJ", @"66", @"TH", @"228", @"TG",
                        @"690", @"TK", @"676", @"TO", @"1", @"TT", @"216", @"TN",
                        @"90", @"TR", @"993", @"TM", @"1", @"TC", @"688", @"TV",
                        @"256", @"UG", @"380", @"UA", @"971", @"AE", @"44", @"GB",
                        @"1", @"US", @"598", @"UY", @"998", @"UZ", @"678", @"VU",
                        @"681", @"WF", @"967", @"YE", @"260", @"ZM", @"263", @"ZW",
                        @"591", @"BO", @"673", @"BN", @"61", @"CC", @"243", @"CD",
                        @"225", @"CI", @"500", @"FK", @"44", @"GG", @"379", @"VA",
                        @"852", @"HK", @"98", @"IR", @"44", @"IM", @"44", @"JE",
                        @"850", @"KP", @"82", @"KR", @"856", @"LA", @"218", @"LY",
                        @"853", @"MO", @"389", @"MK", @"691", @"FM", @"373", @"MD",
                        @"258", @"MZ", @"970", @"PS", @"872", @"PN", @"262", @"RE",
                        @"7", @"RU", @"590", @"BL", @"290", @"SH", @"1", @"KN",
                        @"1", @"LC", @"590", @"MF", @"508", @"PM", @"1", @"VC",
                        @"239", @"ST", @"252", @"SO", @"47", @"SJ", @"963",
                        @"SY",@"886",
                        @"TW", @"255",
                        @"TZ", @"670",
                        @"TL",@"58",
                        @"VE",@"84",
                        @"VN",
                        @"284", @"VG",
                        @"340", @"VI",
                        @"678",@"VU",
                        @"681",@"WF",
                        @"685",@"WS",
                        @"967",@"YE",
                        @"262",@"YT",
                        @"27",@"ZA",
                        @"260",@"ZM",
                        @"263",@"ZW",
                        nil];
    
    NSLog(@"%@",[dictDialingCodes objectForKey:countryCode]);
    codeValue = [NSString stringWithFormat:@"%@",[dictDialingCodes objectForKey:countryCode]];

    
    UIImageView * bgView = [DesignObj initWithImage:CGRectMake(0, 0, screenWidth, screenHeight) img:@"bg"];
    [self.view addSubview:bgView];
    
    _scrView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, screenHeight )];
    _scrView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:_scrView];
    
    if ([utype isEqualToString:@"1"]) {
        _scrView.contentSize = CGSizeMake(screenWidth, screenHeight+ termsandcondi.frame.origin.y + 250);
    }else{
        _scrView.contentSize = CGSizeMake(screenWidth, screenHeight+ 500);
    }
    
    [self initView];
    
}

- (void)initView{
    
    datePicker = [[UIDatePicker alloc]init];
    [datePicker addTarget:self action:@selector(datePickerChanged) forControlEvents:UIControlEventValueChanged];
    datePicker.datePickerMode = UIDatePickerModeDate;
    
    
    int yaxis = 90;
    int y1axis = 90;
    
    profileImg = [DesignObj initWithImage:CGRectMake((screenWidth-80)/2, 70, 80, 80) img:@"adduser.png"];
    [_scrView addSubview:profileImg];
    
    selectimage = [DesignObj initWithButton:CGRectMake((screenWidth-80)/2, 70, 80, 80) tittle:nil img:nil];
    [selectimage addTarget:self action:@selector(uploadPic:) forControlEvents:UIControlEventTouchUpInside];
    [_scrView addSubview:selectimage];
    

    imageData = nil;
    txtfieldarr = [[NSMutableArray alloc]init];

    if ([utype isEqualToString:@"1"]) {

        titlearr = [[NSArray alloc]initWithObjects:@"First Name",@"Middle Name",@"Last Name",@"E-mail",@"Mobile Number", @"Password", @"Confirm Password",@"Date of Birth", nil];
        
    }else{
        
        titlearr = [[NSArray alloc]initWithObjects:@"First Name",@"Middle Name",@"Last Name",@"E-mail",@"Mobile Number",@"Date of Birth",@"SSN", @"Password", @"Confirm Password",@"Street Address",@"Location",@"Region",@"Postal Code", nil];
    }
    
    for (int i = 0; i < [titlearr count]; i++) {
        
        UIImageView * txtbgImg = [[UIImageView alloc]initWithFrame:CGRectMake(20, yaxis+ 100, screenWidth-44, 38)];
        [txtbgImg setImage:[UIImage imageNamed:@"text_box"]];
        [_scrView addSubview:txtbgImg];
        
        Txt= [[UITextField alloc]initWithFrame:CGRectMake((screenWidth-(screenWidth - 44))/2,  yaxis +100, screenWidth-44, 38) ];
        Txt.placeholder = [titlearr objectAtIndex:i];;
        //Txt.layer.cornerRadius =9;
        Txt.delegate = self;
        Txt.textAlignment= NSTextAlignmentCenter;
        Txt.tag = i;
        [Txt setReturnKeyType:UIReturnKeyNext];
        Txt.keyboardAppearance = UIKeyboardAppearanceDark;
        Txt.autocorrectionType = UITextAutocorrectionTypeNo;
        [_scrView addSubview:Txt];
        [txtfieldarr addObject:Txt];
        
        if (i==0||i==1||i==2) {
            
            Txt.autocapitalizationType = UITextAutocapitalizationTypeWords;
            Txt.autocorrectionType = UITextAutocorrectionTypeNo;
            
            
        }else if (i==3) {
            Txt.keyboardType = UIKeyboardTypeEmailAddress;
            Txt.autocapitalizationType = UITextAutocapitalizationTypeNone;
            
        }else if (i==4){
            
            Txt.placeholder = [NSString stringWithFormat:@"+91"];//[dictDialingCodes objectForKey:countryCode]];
            // Txt.inputAccessoryView = numberToolbar;
            Txt.keyboardType = UIKeyboardTypeNumberPad;
        }
        else if ([utype isEqualToString:@"1"]){
            
            if(i==5||i==6){
            
            Txt.secureTextEntry = YES;
                
            }
            
        }else if ([utype isEqualToString:@"2"]){
            
            if (i==7||i==8) {
                 Txt.secureTextEntry = YES;
            }
        
        }else if (i==9||i==10){
            
            Txt.keyboardType = UIKeyboardTypeNamePhonePad;
            
        }else if (i==11){
            
            Txt.keyboardType = UIKeyboardTypeNamePhonePad;
        }else if (i==12){
            
            Txt.keyboardType = UIKeyboardTypeNumberPad;
            
        }
        y1axis = y1axis + 63;
        yaxis = yaxis + 63;

    }
    
    if ([utype isEqualToString:@"1"]) {
        
        fnameTxt = [txtfieldarr objectAtIndex:0];
        mnameTxt = [txtfieldarr objectAtIndex:1];
        lnameTxt = [txtfieldarr objectAtIndex:2];
        emailTxt= [txtfieldarr objectAtIndex:3];
        mobNum = [txtfieldarr objectAtIndex:4];
        passTxt = [txtfieldarr objectAtIndex:5];
        conpassTxt = [txtfieldarr objectAtIndex:6];
        dobTxt = [txtfieldarr objectAtIndex:7];
    }else{
        
        fnameTxt = [txtfieldarr objectAtIndex:0];
        mnameTxt = [txtfieldarr objectAtIndex:1];
        lnameTxt = [txtfieldarr objectAtIndex:2];
        emailTxt= [txtfieldarr objectAtIndex:3];
        mobNum = [txtfieldarr objectAtIndex:4];
        dobTxt = [txtfieldarr objectAtIndex:5];
        SSN = [txtfieldarr objectAtIndex:6];
        passTxt= [txtfieldarr objectAtIndex:7];
        conpassTxt = [txtfieldarr objectAtIndex:8];
        streetaddr = [txtfieldarr objectAtIndex:9];
        location = [txtfieldarr objectAtIndex:10];
        region = [txtfieldarr objectAtIndex:11];
        postalcode = [txtfieldarr objectAtIndex:12];
        
    }

    
    if ([ utype isEqualToString:@"1"]) {
        
        
        regbtn =[DesignObj initWithButton:CGRectMake((screenWidth - 200)/2, dobTxt.frame.origin.y + 38 +30, 200, 40)  tittle:@"Join" img:@"button"];
        [regbtn addTarget:self action:@selector(regAction) forControlEvents:UIControlEventTouchUpInside];
        [regbtn setTitleColor:[DesignObj quikYellow] forState:UIControlStateNormal];
        [_scrView addSubview:regbtn];

        termsandcondi = [DesignObj initWithLabel:CGRectMake((screenWidth - 300)/2, regbtn.frame.origin.y+ 40 +20, 300, 60) title:@"By clicking Join you are agreeing to the\n Terms of use and Privacy Policy" font:16 txtcolor:[DesignObj quikRed]];
        termsandcondi.textAlignment = NSTextAlignmentCenter;
        termsandcondi.numberOfLines= 2;
        
        [_scrView addSubview:termsandcondi];

    }else{
        
        regbtn =[DesignObj initWithButton:CGRectMake((screenWidth - 300)/2, postalcode.frame.origin.y + 38 +30, 300, 40)  tittle:@"Next" img:@"button"];
        [regbtn addTarget:self action:@selector(truckAction) forControlEvents:UIControlEventTouchUpInside];
        [regbtn setTitleColor:[DesignObj quikYellow] forState:UIControlStateNormal];
        [_scrView addSubview:regbtn];
        
    }
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDate *currentDate = [NSDate date];
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    [comps setYear:-10];
    NSDate *maxDate = [calendar dateByAddingComponents:comps toDate:currentDate options:0];
    [comps setYear:-116];
    NSDate *minDate = [calendar dateByAddingComponents:comps toDate:currentDate options:0];
    
    [datePicker setMaximumDate:maxDate];
    [datePicker setMinimumDate:minDate];
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, screenWidth, 44)];
    numberToolbar.barStyle = UIBarStyleDefault;//UIBarStyleBlackTranslucent;
    numberToolbar.tintColor = [UIColor colorWithRed:207/255.0 green:26/255.0 blue:26/255.0 alpha:1.0];
    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleDone target:self action:@selector(cancelNumberPad)],
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
    [numberToolbar sizeToFit];
    mobNum.inputAccessoryView = numberToolbar;
    dobTxt.inputView = datePicker;
    dobTxt.inputAccessoryView = numberToolbar;
    postalcode.keyboardType = UIKeyboardTypeNumberPad;
    postalcode.inputAccessoryView = numberToolbar;
    
    taptoKeyboardHide = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hideKeyboard:)];
    taptoKeyboardHide.numberOfTouchesRequired = 1;
    taptoKeyboardHide.numberOfTapsRequired = 1;
    [_scrView addGestureRecognizer:taptoKeyboardHide];


}

-(IBAction)hideKeyboard:(id)sender{
    
    [self.view endEditing:YES];
}


-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:YES];
}



-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    {
        NSInteger nextTag = textField.tag +1;
        // Try to find next responder
        UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
        if (nextResponder) {
            // Found next responder, so set it.
            [nextResponder becomeFirstResponder];
            
        } else {
            // Not found, so remove keyboard.
            [textField resignFirstResponder];
            
        }
        return NO; // We do not want UITextField to insert line-breaks.
    }
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{

    [UIView animateWithDuration:0.5 animations:^{
        
        if ([utype isEqualToString:@"1"]) {
            
            [self customerKeyboard:textField];
        }else{
            
            [self truckerKeyboard:textField];
        }
        
    }];
    
}


-(void)animateTextField:(UITextField*)textField up:(BOOL)up withOffset:(CGFloat)offset
{
    
    const int movementDistance = -offset;
    const float movementDuration = 0.4f;
    int movement = (up ? movementDistance : -movementDistance);
    [UIView beginAnimations: @"animateTextField" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    _scrView.frame = CGRectOffset(_scrView.frame, 0, movement);
//    _scrView.frame = CGRectMake(0, movement, screenWidth, screenHeight);
    [UIView commitAnimations];
}

-(void)customerKeyboard:(UITextField *)textField{
    
    if (textField == fnameTxt) {
        _scrView.frame = CGRectMake(0, 0, _scrView.frame.size.width, _scrView.frame.size.height);
    }else if (textField == mnameTxt) {
        _scrView.frame = CGRectMake(0, 0, _scrView.frame.size.width, _scrView.frame.size.height);
        
    }else if (textField == lnameTxt) {
        _scrView.frame = CGRectMake(0, 0, _scrView.frame.size.width, _scrView.frame.size.height);
    }else if (textField == emailTxt) {
//        _scrView.frame = CGRectMake(0, -76, _scrView.frame.size.width, _scrView.frame.size.height+76);
        [_scrView setContentOffset:CGPointMake(_scrView.frame.origin.x, _scrView.frame.origin.y+76) animated:YES];
    }else if (textField == mobNum) {
//        _scrView.frame = CGRectMake(0, -200, _scrView.frame.size.width, _scrView.frame.size.height+200);
        
    [_scrView setContentOffset:CGPointMake(_scrView.frame.origin.x, _scrView.frame.origin.y+162) animated:YES];
        
    }else if (textField == passTxt) {
//        _scrView.frame = CGRectMake(0, -276, _scrView.frame.size.width, _scrView.frame.size.height+276);
        [_scrView setContentOffset:CGPointMake(_scrView.frame.origin.x, _scrView.frame.origin.y+222) animated:YES];
    }else if (textField == conpassTxt) {
//        _scrView.frame = CGRectMake(0, -350, _scrView.frame.size.width, _scrView.frame.size.height+350);
        [_scrView setContentOffset:CGPointMake(_scrView.frame.origin.x, _scrView.frame.origin.y+300) animated:YES];
    }else if (textField == dobTxt) {
//        _scrView.frame = CGRectMake(0, -390, _scrView.frame.size.width, _scrView.frame.size.height+390);
        [_scrView setContentOffset:CGPointMake(_scrView.frame.origin.x, _scrView.frame.origin.y+376) animated:YES];
   
}

}

-(void)truckerKeyboard:(UITextField*)textField{
    
 if (textField == fnameTxt) {
        _scrView.frame = CGRectMake(0, 0, _scrView.frame.size.width, _scrView.frame.size.height);
    }else if (textField == mnameTxt) {
        _scrView.frame = CGRectMake(0, 0, _scrView.frame.size.width, _scrView.frame.size.height);
        
    }else if (textField == lnameTxt) {
        _scrView.frame = CGRectMake(0, 0, _scrView.frame.size.width, _scrView.frame.size.height);
    }else if (textField == emailTxt) {

    
        [_scrView setContentOffset:CGPointMake(_scrView.frame.origin.x, _scrView.frame.origin.y+76) animated:YES];
        
    }
    else if (textField == mobNum) {
        [_scrView setContentOffset:CGPointMake(_scrView.frame.origin.x, _scrView.frame.origin.y+222) animated:YES];

    }
        else if (textField == dobTxt) {
            
            [_scrView setContentOffset:CGPointMake(_scrView.frame.origin.x, _scrView.frame.origin.y+290) animated:YES];

    }
    else if (textField == SSN) {
        [_scrView setContentOffset:CGPointMake(_scrView.frame.origin.x, _scrView.frame.origin.y+368) animated:YES];

    }else if (textField == passTxt) {
        [_scrView setContentOffset:CGPointMake(_scrView.frame.origin.x, _scrView.frame.origin.y+446) animated:YES];

    }else if (textField == conpassTxt) {
        [_scrView setContentOffset:CGPointMake(_scrView.frame.origin.x, _scrView.frame.origin.y+515) animated:YES];

    }else if (textField == streetaddr) {

        [_scrView setContentOffset:CGPointMake(_scrView.frame.origin.x, _scrView.frame.origin.y+580) animated:YES];
    }else if (textField == location) {

        [_scrView setContentOffset:CGPointMake(_scrView.frame.origin.x, _scrView.frame.origin.y+648) animated:YES];
    }else if (textField == region) {

        if (screenHeight >= 750) {
            
            [_scrView setContentOffset:CGPointMake(_scrView.frame.origin.x, _scrView.frame.origin.y+650)];
        }
        else{
            [_scrView setContentOffset:CGPointMake(_scrView.frame.origin.x, _scrView.frame.origin.y+700)];
        }

    }else if (textField == postalcode) {
        
        if (screenHeight >= 667) {
            
            [_scrView setContentOffset:CGPointMake(_scrView.frame.origin.x, _scrView.frame.origin.y+680)];
        }
        else{
        [_scrView setContentOffset:CGPointMake(_scrView.frame.origin.x, _scrView.frame.origin.y+748)];
        
        }

    }
 
}

- (void)datePickerChanged{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMM-dd-yyyy"];
    NSString *strDate = [dateFormatter stringFromDate:datePicker.date];
    dobTxt.text = strDate;

}

-(void)showAlert:(NSString*)message{
    UIAlertView * alt = [[UIAlertView alloc]initWithTitle:@"Message" message:message delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alt show];
}

-(void)cancelNumberPad{
    
    [mobNum resignFirstResponder];
    [datePicker resignFirstResponder];
    [dobTxt resignFirstResponder];
    [postalcode resignFirstResponder];
    [region resignFirstResponder];
}

-(void)doneWithNumberPad{
    
    if ([utype isEqualToString:@"1"]) {
        
        if ([mobNum resignFirstResponder]) {
            
            [passTxt becomeFirstResponder];
        }else if ([dobTxt resignFirstResponder]){
            
            //            _scrView.contentSize = CGSizeMake(screenWidth, screenHeight+ 500);
            [self regAction];
        }
    }else{
        
        if ([mobNum resignFirstResponder]) {
            
            [dobTxt becomeFirstResponder];
            
        }else if ([dobTxt resignFirstResponder]){
            
            [SSN becomeFirstResponder];
        }else if ([postalcode resignFirstResponder]){
            
            [self truckAction];
            
        }
    }
}

-(IBAction)uploadPic:(id)sender{
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Message"
                                  message:@"Choose options to Upload your Profile image"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* library = [UIAlertAction
                              actionWithTitle:@"From Photo Library"
                              style:UIAlertActionStyleDefault
                              handler:^(UIAlertAction * action)
                              {
                                  [alert dismissViewControllerAnimated:YES completion:nil];
                                  [self selectPhoto];
                                  
                              }];
    
    UIAlertAction* Camera = [UIAlertAction
                             actionWithTitle:@"Take Photo"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 [self takePhoto];
                             }];
    
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    
    [alert addAction:library];
    [alert addAction:Camera];
    [alert addAction:cancel];
    
    [self presentViewController:alert animated:YES completion:nil];
    

    
}

- (void)selectPhoto{
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:NULL];
    
    
}

- (void)takePhoto{
    
    //    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    //    picker.delegate = self;
    //    picker.allowsEditing = YES;
    //    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    //
    //    [self presentViewController:picker animated:YES completion:NULL];
    
    DBCameraContainerViewController *cameraContainer = [[DBCameraContainerViewController alloc] initWithDelegate:self];
    [cameraContainer setFullScreenMode];
    
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:cameraContainer];
    [nav setNavigationBarHidden:YES];
    [self presentViewController:nav animated:YES completion:nil];
    
    
}

- (void) camera:(id)cameraViewController didFinishWithImage:(UIImage *)image withMetadata:(NSDictionary *)metadata
{
    //    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    profileImg.image = image;
    imageData = UIImagePNGRepresentation(profileImg.image);
    
    //    DetailViewController *detail = [[DetailViewController alloc] init];
    //    [detail setDetailImage:image];
    //    [self.navigationController pushViewController:detail animated:NO];
    //    [cameraViewController restoreFullScreenMode];
    [self.presentedViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void) dismissCamera:(id)cameraViewController{
    [self dismissViewControllerAnimated:YES completion:nil];
    [cameraViewController restoreFullScreenMode];
}



- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    profileImg.image = chosenImage;
    imageData = UIImagePNGRepresentation(profileImg.image);
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}


-(IBAction)regAction{
    
    if (fnameTxt.text.length==0) {
        [self showAlert:@"Please enter your First Name"];
    }else if (lnameTxt.text.length==0) {
        [self showAlert:@"Please enter your Last Name"];
    }else if (emailTxt.text.length==0) {
        [self showAlert:@"Please enter email"];
    }else if (![self validateEmail:emailTxt.text]) {
        [self showAlert:@"Please enter valid email"];
    }else if (mobNum.text.length==0){
        [self showAlert:@"Please enter the Mobile number"];
    }else if (mobNum.text.length<10){
        [self showAlert:@"Please enter the correct mobile number"];
    }else if (passTxt.text.length==0){
        [self showAlert:@"Please enter password"];
    }else if (passTxt.text.length<6){
        [self showAlert:@"Password atleast 6 characters"];
    }else if (conpassTxt.text.length==0){
        [self showAlert:@"Please enter Confirm password"];
    }else if (![passTxt.text isEqualToString:conpassTxt.text]){
        [self showAlert:@"Your password does not match"];
    }else if (dobTxt.text.length==0){
        [self showAlert:@"Please enter your Date of Birth"];
    }
    else{
        
        [self CustomerupdateUserProfilePic];
        /*
        
//        [self otpscreen];
        
        QuickMovOTPViewController* takethistoNext = [[QuickMovOTPViewController alloc]init];

        
        takethistoNext.fname = fnameTxt.text;
        takethistoNext.mname = mnameTxt.text;
        takethistoNext.lname = lnameTxt.text;
        takethistoNext.email = emailTxt.text;
        takethistoNext.mobNum = mobNum.text;
        takethistoNext.pass = passTxt.text;
        takethistoNext.conpass = conpassTxt.text;
        takethistoNext.dob = dobTxt.text;
        takethistoNext.regType = utype;
        takethistoNext.profImag = profileImg.image;
        
        [self.navigationController pushViewController:takethistoNext animated:YES];

        */
    }
}
/*
-(void)otpscreen{
    
    UserDetailsObj* userdetlObj = [[UserDetailsObj alloc]init];
    userdetlObj.fname = fnameTxt.text;
    userdetlObj.mname = mnameTxt.text;
    userdetlObj.lname = lnameTxt.text;
    userdetlObj.user_mail = emailTxt.text;
    userdetlObj.phno = mobNum.text;
    userdetlObj.passrd = passTxt.text;
    userdetlObj.cpassrd = conpassTxt.text;
    userdetlObj.dob = dobTxt.text;
    userdetlObj.userTyp = utype;
    userdetlObj.userPrfleImg = profileImg.image;
    
    QuickMovOTPViewController* takethistoNext = [[QuickMovOTPViewController alloc]init];
    [self.navigationController pushViewController:takethistoNext animated:YES];

}
 */

-(IBAction)truckAction{
    
    if (fnameTxt.text.length==0) {
        [self showAlert:@"Please enter your First Name"];
    }else if (lnameTxt.text.length==0) {
        [self showAlert:@"Please enter your Last Name"];
    }else if (emailTxt.text.length==0) {
        [self showAlert:@"Please enter email"];
    }else if (![self validateEmail:emailTxt.text]) {
        [self showAlert:@"Please enter valid email"];
    }else if (mobNum.text.length==0){
        [self showAlert:@"Please enter the Mobile number"];
    }else if (mobNum.text.length<10){
        [self showAlert:@"Please enter the correct mobile number"];
    }else if (passTxt.text.length==0){
        [self showAlert:@"Please enter password"];
    }else if (passTxt.text.length<6){
        [self showAlert:@"Password atleast 6 characters"];
    }else if (conpassTxt.text.length==0){
        [self showAlert:@"Please enter Confirm password"];
    }else if (![passTxt.text isEqualToString:conpassTxt.text]){
        [self showAlert:@"Your password does not match"];
    }else if (dobTxt.text.length==0){
        [self showAlert:@"Please enter your Date of Birth"];
    }else{
        
        
        TruckRegisterViewController* nextTruckView = [[TruckRegisterViewController alloc]init];
        
        nextTruckView.truckfname = fnameTxt.text;
        nextTruckView.truckmname = mnameTxt.text;
        nextTruckView.trucklname = lnameTxt.text;
        nextTruckView.truckemail = emailTxt.text;
        nextTruckView.trucknumber = [NSString stringWithFormat:@"%@",mobNum.text];
//        [NSString stringWithFormat:@"+%@%@",codeValue,mobNum.text];
        nextTruckView.truckpass = passTxt.text;
        nextTruckView.truckcpass = conpassTxt.text;
        nextTruckView.truckdateob = dobTxt.text;
        nextTruckView.truckregImg = profileImg.image;
        nextTruckView.truckerUsetyp = utype;
        nextTruckView.truckssn = SSN.text;
        nextTruckView.truckLocation = location.text;
        nextTruckView.truckregion = region.text;
        nextTruckView.truckstreetaddr = streetaddr.text;
        nextTruckView.truckpostal = postalcode.text;
        nextTruckView.truckregImg = profileImg.image;
        
        [self.navigationController pushViewController:nextTruckView animated:YES];


        
//        [self truckregScreen];
        
    }
}
/*
-(void)truckregScreen{
    
    UserDetailsObj* userdetlObj = [[UserDetailsObj alloc]init];
    userdetlObj.fname = fnameTxt.text;
    userdetlObj.mname = mnameTxt.text;
    userdetlObj.lname = lnameTxt.text;
    userdetlObj.user_mail = emailTxt.text;
    userdetlObj.phno = mobNum.text;
    userdetlObj.passrd = passTxt.text;
    userdetlObj.cpassrd = conpassTxt.text;
    userdetlObj.dob = dobTxt.text;
    userdetlObj.street = conpassTxt.text;
    userdetlObj.location = dobTxt.text;
    userdetlObj.region = region.text;
    userdetlObj.postalcode = postalcode.text;
    userdetlObj.userPrfleImg = profileImg.image;
    userdetlObj.userTyp = utype;
    
    
}
 
*/

-(BOOL)validateEmail:(NSString *)emailStr {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:emailStr];
}

-(void)truckregService{
//    [self truckupdateUserProfilePic];

}

/*
-(void)truckupdateUserProfilePic{
    
    if ([Reachability reachabilityForInternetConnection]) {
        
        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
        [SVProgressHUD show];
        
        NSArray *keys = [[NSArray alloc]initWithObjects:@"name",@"mname",@"lname",@"email",@"mobile",@"password",@"cpassword",@"dob",@"usertype",nil];
        //
        NSArray *values =[[NSArray alloc]initWithObjects:fnameTxt.text, mnameTxt.text, lnameTxt.text, emailTxt.text, mobNum.text,passTxt.text,conpassTxt.text,dobTxt.text,utype, nil];
        
        NSURL *url =[NSURL URLWithString:[NSString stringWithFormat:@"http://quikmov.quikmovapp.com/auth/signup"]];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        [request setHTTPMethod:@"POST"];
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        NSMutableData *body = [NSMutableData data];
        NSData * data = [NSData dataWithData:UIImagePNGRepresentation(profileImg.image)];
        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"imagefile\"; filename=\"%@userprofile.png\"\r\n",fnameTxt.text] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: image/png\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[NSData dataWithData:data]];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        for (int i=0;i<keys.count;i++) {

            NSLog(@"%@",[keys objectAtIndex:i]);
            NSString *key = [keys objectAtIndex:i];
            NSString *value = [values objectAtIndex:i];
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n%@",key, value] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [request setHTTPBody:body];
        
        NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession* session = [NSURLSession sessionWithConfiguration:config ];

        
        NSURLSessionUploadTask *uploadtask = [session uploadTaskWithRequest:request fromData:body completionHandler:^(NSData*data, NSURLResponse*response, NSError*error){
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                
                NSString* str = [[NSString alloc]initWithData:data
                                                     encoding:NSUTF8StringEncoding];
                
                NSError* error;
                
                
                
                NSDictionary* dic = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                
                NSLog(@"%@******%@,%@",str,[dic allValues],[dic objectForKey:@"response"]);
                
                // userId = [[dic objectForKey:@"message"] stringValue];
                NSString * Result = [dic objectForKey:@"status"];
                
                NSDictionary * result = [dic objectForKey:@"result"];
                
                if ([[dic objectForKey:@"result"] isKindOfClass:[NSDictionary class]]){
                    
                    userId = [result objectForKey:@"user_id"];
                    NSLog(@"%@",userId);
                    
                        NSUserDefaults* userStoredetails = [NSUserDefaults standardUserDefaults];
                        [userStoredetails setObject:userId forKey:@"user_id"];
                        [userStoredetails synchronize];
                
                NSLog(@"%@",Result);
                
                if ([Result isEqualToString:@"success"]){
                    
                    UIAlertController * alert=   [UIAlertController alertControllerWithTitle:@"Message"
                                                                                     message:@"Registration Successful" preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK"
                                                                 style:UIAlertActionStyleDefault
                                                               handler:^(UIAlertAction * action)
                                         {
                                             [alert dismissViewControllerAnimated:YES completion:nil];
                                             
                                             TruckRegisterViewController* truckregView = [[TruckRegisterViewController alloc]init];
                                             [self.navigationController pushViewController:truckregView animated:YES];
                                             
                                         }];
                    
                    
                    [alert addAction:ok];
                    
                    [self presentViewController:alert animated:YES completion:nil];
                }else{
                    
                    [self showAlert:[NSString stringWithFormat:@"%@",[dic objectForKey:@"result"]]];
                }
                }
                
            });
            
            
        }];
        [uploadtask resume];

        
//        NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
//        if( theConnection )
//        {
//            _downloadedData = [NSMutableData data] ;
//            NSLog(@"request uploading successful");
//        }
//        else
//        {
//            NSLog(@"theConnection is NULL");
//        }
        
    }else{
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"No internet connectivity" message:@"Please try later" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
}
 */

-(UIImage*)scaleDown:(UIImage*)img withSize:(CGSize)newSize{
    
    
    UIGraphicsBeginImageContextWithOptions(newSize, YES, 0.0);
    [img drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return scaledImage;
}

-(void)CustomerupdateUserProfilePic{
    
    if ([Reachability reachabilityForInternetConnection]) {
        
        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
        [SVProgressHUD show];
        
        NSArray *keys = [[NSArray alloc]initWithObjects:@"name",@"mname",@"lname",@"email",@"mobile",@"password",@"cpassword",@"dob",@"usertype",nil];
        //
        NSArray *values =[[NSArray alloc]initWithObjects:fnameTxt.text, mnameTxt.text, lnameTxt.text, emailTxt.text,[NSString stringWithFormat:@"+%@%@",codeValue, mobNum.text],passTxt.text,conpassTxt.text,dobTxt.text,utype, nil];
        
        NSURL *url =[NSURL URLWithString:[NSString stringWithFormat:@"http://quikmov.quikmovapp.com/auth/signup"]];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        [request setHTTPMethod:@"POST"];
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        NSMutableData *body = [NSMutableData data];
        
        UIImage * image = profileImg.image;
        
        
        if(!(image.imageOrientation == UIImageOrientationUp ||
             image.imageOrientation == UIImageOrientationUpMirrored))
        {
            CGSize imgsize = image.size;
            UIGraphicsBeginImageContext(imgsize);
            [image drawInRect:CGRectMake(0.0, 0.0, imgsize.width, imgsize.height)];
            image = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
        }
        
        NSData * data = [NSData dataWithData:UIImagePNGRepresentation(image)];
        //        11700.6005859
        //        float factor;
        //        float resol = image.size.height*image.size.width;
        //        if (resol >MIN_UPLOAD_RESOLUTION){
        //            factor = sqrt(resol/MIN_UPLOAD_RESOLUTION)*2;
        //            image = [self scaleDown:image withSize:CGSizeMake(image.size.width/factor, image.size.height/factor)];
        //        }
        
        //Compress the image
        CGFloat compression = 0.9f;
        CGFloat maxCompression = 0.1f;
        
        while ([data length] > MAX_UPLOAD_SIZE && compression > maxCompression)
        {
            compression -= 0.10;
            data = UIImageJPEGRepresentation(image, compression);
            NSLog(@"Compress : %lu",(unsigned long)data.length);
        }

        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"imagefile\"; filename=\"%@userprofile.png\"\r\n",fnameTxt.text] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: image/png\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[NSData dataWithData:data]];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        for (int i=0;i<keys.count;i++) {
            
            NSLog(@"%@",[keys objectAtIndex:i]);
            NSString *key = [keys objectAtIndex:i];
            NSString *value = [values objectAtIndex:i];
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n%@",key, value] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [request setHTTPBody:body];
        
        NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession* session = [NSURLSession sessionWithConfiguration:config ];
        
        
        NSURLSessionUploadTask *uploadtask = [session uploadTaskWithRequest:request fromData:body completionHandler:^(NSData*data, NSURLResponse*response, NSError*error){
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                
                NSString* str = [[NSString alloc]initWithData:data
                                                     encoding:NSUTF8StringEncoding];
                
                NSError* error;
                
                
                
                NSDictionary* dic = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                
                NSLog(@"%@******%@,%@",str,[dic allValues],[dic objectForKey:@"response"]);
                
                // userId = [[dic objectForKey:@"message"] stringValue];
                NSString * Result = [dic objectForKey:@"status"];
                
                NSDictionary * result = [dic objectForKey:@"result"];
                
                if ([[dic objectForKey:@"result"] isKindOfClass:[NSDictionary class]]){
                    
                    userId = [result objectForKey:@"user_id"];
                    otp = [result objectForKey:@"otp"];
                    NSLog(@"%@",userId);
                    
                    NSUserDefaults* userStoredetails = [NSUserDefaults standardUserDefaults];
                    [userStoredetails setObject:userId forKey:@"user_id"];
                    [userStoredetails synchronize];
                    
                    NSLog(@"%@",Result);
                    
                    if ([Result isEqualToString:@"success"]){
                        
                        UIAlertController * alert=   [UIAlertController alertControllerWithTitle:@"Message"
                                                                                         message:[NSString stringWithFormat:@"Please Enter this OTP Verification\n%@",otp] preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK"
                                                                     style:UIAlertActionStyleDefault
                                                                   handler:^(UIAlertAction * action)
                                             {
                                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                                 
                                                 QuickMovOTPViewController* otpView = [[QuickMovOTPViewController alloc]init];
                                                 otpView.copiOTP = otp;
                                                 otpView.mobilenumbr = [NSString stringWithFormat:@"+%@%@",codeValue,mobNum.text];
                                                 otpView.regType = utype;
                                                 [self.navigationController pushViewController:otpView animated:YES];
                                                 
                                             }];
                        
                        
                        [alert addAction:ok];
                        
                        [self presentViewController:alert animated:YES completion:nil];
                    }else{
                        
                        [self showAlert:[NSString stringWithFormat:@"%@",[dic objectForKey:@"result"]]];
                    }
                }
                
            });
            
            
        }];
        [uploadtask resume];
        
    }else{
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"No internet connectivity" message:@"Please try later" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }

    
    
}

-(void)regService{
    
    [self CustomerupdateUserProfilePic];
    return;
    
//    if ([Reachability reachabilityForInternetConnection]) {
//        
//        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
//        [SVProgressHUD show];
//        
//        imageData = UIImagePNGRepresentation(profileImg.image);
//
//        
//        NSURL* url = [NSURL URLWithString:@"http://truckcsoft.mapcee.com/auth/signup" ];
//        NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
//        NSURLSession* session = [NSURLSession sessionWithConfiguration:config ];
//        
//        NSMutableURLRequest*request = [[NSMutableURLRequest alloc]initWithURL:url];
//        request.HTTPMethod = @"POST";
//        
//        //inputs : imagefile,name,mname,lname, email ,mobile, password,cpassword,dob,usertype />
////        url : http://truckcsoft.mapcee.com/auth/signup
//        
//        NSString* bodycntnt = [NSString stringWithFormat:@"imagefile=%@&name=%@&mname=%@&lname=%@&email=%@&mobile=%@&password=%@&cpassword=%@&dob=%@&usertype=%@",imageData, fnameTxt.text, mnameTxt.text, lnameTxt.text, emailTxt.text, mobNum.text,passTxt.text,conpassTxt.text,dobTxt.text,utype];
//        
//        NSLog(@"%@",bodycntnt);
//        
//        NSData *data = [bodycntnt dataUsingEncoding:NSUTF8StringEncoding];
//        
//        NSURLSessionUploadTask *uploadtask = [session uploadTaskWithRequest:request fromData:data completionHandler:^(NSData*data, NSURLResponse*response, NSError*error){
//            dispatch_async(dispatch_get_main_queue(), ^{
//                [SVProgressHUD dismiss];
//                
//                NSString* str = [[NSString alloc]initWithData:data
//                                                     encoding:NSUTF8StringEncoding];
//                
//                NSError* error;
//                
//                
//                
//                NSDictionary* dic = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
//                
//                NSLog(@"%@******%@,%@",str,[dic allValues],[dic objectForKey:@"response"]);
//                
//                // userId = [[dic objectForKey:@"message"] stringValue];
//                NSString * Result = [dic objectForKey:@"status"];
//                
//                NSLog(@"%@",Result);
//                
//                if ([Result isEqualToString:@"success"]){
//                    
//                    
//                    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Message" message:@"Registration Successful" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
//                    [alert setTag:1];
//                    [alert show];
//                    
//                }else{
//                    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Message" message:[NSString stringWithFormat:@"%@",[dic objectForKey:@"result"]] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
//                    [alert show];       }
//                
//            });
//            
//            
//        }];
//        [uploadtask resume];
//    }else{
//        [self showAlert:@"No Internet Connectivity"];
//    }
//
//    
//    MapViewController* mapView = [[MapViewController alloc]init];
//    [self.navigationController pushViewController:mapView animated:YES];
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    // A response has been received, this is where we initialize the instance var you created
    // so that we can append data to it in the didReceiveData method
    // Furthermore, this method is called each time there is a redirect so reinitializing it
    // also serves to clear it
    _responseData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    // Append the new data to the instance variable you declared
    [_responseData appendData:data];
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*)cachedResponse {
    // Return nil to indicate not necessary to store a cached response for this connection
    return nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    // The request is complete and data has been received
    // You can parse the stuff in your instance variable now
    
    NSString*response;
    
    response = [[NSString alloc] initWithData:_responseData encoding:NSUTF8StringEncoding];
    NSLog(@"_responseData %@", response);
    
    [indicator stopAnimating];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = FALSE;
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    // The request has failed for some reason!
    // Check the error var
    
    NSLog(@"didFailWithError %@", error);
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
