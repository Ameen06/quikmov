//
//  QMForgotPassViewController.m
//  QuikMov
//
//  Created by CSCS on 2/11/16.
//  Copyright © 2016 CSCS. All rights reserved.
//

#import "QMForgotPassViewController.h"
#import "DesignObj.h"
#import "Reachability.h"
#import "SVProgressHUD.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"

@interface QMForgotPassViewController (){
    
    UILabel * title;
    UIButton * sendMail;
    UIButton * back;
    UITextField * email;
}

@end

@implementation QMForgotPassViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.navigationController.navigationBarHidden = YES;
    
    UIImageView*bgView = [DesignObj initWithImage:CGRectMake(0, 0, screenWidth, screenHeight) img:@"bg"];
    [self.view addSubview:bgView];
    
    //    self.title = @"Login";
    
    title = [DesignObj initWithLabel:CGRectMake((screenWidth-200)/2, 200, 200, 40) title:@"Forgot Password" font:18 txtcolor:[DesignObj quikRed]];
    [self.view addSubview:title];
    
    
    UIImageView * txtbgImg = [[UIImageView alloc]initWithFrame:CGRectMake((screenWidth-(screenWidth - 80))/2, title.frame.origin.y+ 40+ 40, screenWidth - 80, 40)];
    [txtbgImg setImage:[UIImage imageNamed:@"text_box"]];
    [self.view addSubview:txtbgImg];
    
    email = [DesignObj initWithTextfield:CGRectMake((screenWidth-(screenWidth - 80))/2, title.frame.origin.y+ 40+ 40, screenWidth - 80, 40) placeholder:@"" tittle:@"Registered Email-id" delegate:self font:14];
    email.keyboardType = UIKeyboardTypeEmailAddress;
    email.tintColor = [DesignObj quikRed];
    email.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:email];

    sendMail = [DesignObj initWithButton:CGRectMake((screenWidth - (screenWidth - 100))/2, email.frame.origin.y+40+ 30, screenWidth - 100, 40) tittle:@"Send Password" img:@"button"];
    [sendMail setTitleColor:[DesignObj quikYellow] forState:UIControlStateNormal];
    [sendMail addTarget:self action:@selector(nextAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:sendMail];
    
    back = [DesignObj initWithButton:CGRectMake((screenWidth - (screenWidth - 100))/2, sendMail.frame.origin.y+30+ 30, (screenWidth - 100), 40) tittle:@"Back" img:@"button"];
    [back setTitleColor:[DesignObj quikYellow] forState:UIControlStateNormal];
    [back addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:back];
}

-(BOOL)textFieldShouldReturn:(UITextField*)textField
{
    
    [textField resignFirstResponder];
    [self nextAction];
    
    return YES;
}


-(void)nextAction{
    
    if (email.text.length == 0) {
        [self showAlert:@"Please enter your Registered E-mail"];
    }else{
        [self forgotpassService];
    }
    
}

-(void)backAction{
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

-(void)forgotpassService{
    
    [email resignFirstResponder];
    
    
    if ([Reachability reachabilityForInternetConnection]) {
//        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
//        [SVProgressHUD show];
        
        
        MBProgressHUD* hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.mode = MBProgressHUDModeIndeterminate;
        hud.labelText = @"Sending...";
        hud.labelColor = [DesignObj quikYellow];
        hud.animationType = MBProgressHUDAnimationZoomIn;
        hud.color = [DesignObj quikRed];
        hud.alpha = 0.8;
        
        NSURL* url = [NSURL URLWithString:@"http://quikmov.quikmovapp.com/auth/ForgotPassword"];
        
        NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession* session = [NSURLSession sessionWithConfiguration:config ];
        
        NSMutableURLRequest*request = [[NSMutableURLRequest alloc]initWithURL:url];
        request.HTTPMethod = @"POST";
        
        
        //    NSUserDefaults* userStoredetails = [NSUserDefaults standardUserDefaults];
        //    NSString * userID = [userStoredetails objectForKey:@"userid"];
        //    NSLog(@"%@",userID);
        
        NSString* bodycntnt = [NSString stringWithFormat:@"email=%@",email.text];
        
        NSData *data = [bodycntnt dataUsingEncoding:NSUTF8StringEncoding];
        
        
        NSURLSessionUploadTask *uploadtask = [session uploadTaskWithRequest:request fromData:data completionHandler:^(NSData*data, NSURLResponse*response, NSError*error){
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                
                NSString* str = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
                NSError* error;
                
                NSDictionary* dic = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                NSLog(@"%@******%@,%@",str,[dic allValues],[dic objectForKey:@"response"]);
                
                // userId = [[dic objectForKey:@"message"] stringValue];
                NSString * Result = [dic objectForKey:@"status"];
                
                NSLog(@"%@",Result);
                if ([Result isEqualToString:@"success"]){
                    
                    
                    UIAlertController * alert=   [UIAlertController
                                                  alertControllerWithTitle:@"Message"
                                                  message:@"Password sent to your email"
                                                  preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* ok = [UIAlertAction
                                         actionWithTitle:@"OK"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action)
                                         {
                                             [self.navigationController popViewControllerAnimated:YES];
                                             
                                         }];
                    
                    
                    [alert addAction:ok];
                    
                    [self presentViewController:alert animated:YES completion:nil];

                    
                }else{
                    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Message" message:[NSString stringWithFormat:@"%@",[dic objectForKey:@"result"]] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                    [alert show];
                }
                
            });
        }];
        [uploadtask resume];
    }else{
        [self showAlert:@"No internet connectivity"];
    }


}

- (void)showAlert:(NSString *)message{
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Message"
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [alert dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    
    
    [alert addAction:ok];
    
    [self presentViewController:alert animated:YES completion:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
