//
//  TruckProfileViewController.m
//  QuikMov
//
//  Created by Ajmal Khan on 3/1/16.
//  Copyright © 2016 CSCS. All rights reserved.
//

#import "TruckProfileViewController.h"
#import "DesignObj.h"
#import "GlobalObjects.h"
#import "SVProgressHUD.h"
#import "Reachability.h"
#import "DBCameraContainerViewController.h"

#define MIN_UPLOAD_RESOLUTION 320*480
#define MAX_UPLOAD_SIZE 1124000

@interface TruckProfileViewController ()<DBCameraViewControllerDelegate>{
    
    int iphone6Yaxis;
    UIDatePicker * datePicker;
    NSMutableArray * txtfieldarr;
    UITextField * Txt;
    UITextField * truckType;
    UITextField * heightofTruck;
    UITextField * widthofTruck;
    UITextField * lengthofTruck;
    UITextField * regno;
    UITextField * insurenceValidity;
    UITextField * rate;
    UITextField * serviceCategories;
    UITextField * Equipments;
    UITextField * insurenceno;
    UITextField * routingNo;
    UITextField * accountNo;
    UIButton* selectimage1;
    UIButton* selectimage2;
    UIButton* selectimage3;
    UIButton* selectimage4;
    NSArray * titlearr;
    NSArray * titlelblarr;
    NSData  * imageData;
    NSString* userid;
    UILabel * trucklbl;
    
    int imageTag;
    
    UILabel * truckTypelbl;
    UILabel * truckSizelbl;
    UILabel * truckregnolbl;
    UILabel * truckinsurenceLbl;
    UILabel * truckRatelbl;
    UILabel * truckServiceCatlbl;
    UILabel * truckEquiplbl;
    UILabel * truckPaymentlbl;
    
    NSString* regUserid;
    
    UITableView* trucktypestblView;
    NSMutableArray* listofTypesArr;
    UIView * closeView;
    UIButton * closebtn;
    NSString* userID;
    NSArray * imagesArr;
    NSString * trukrId;
    
    int count;
    
}

@property (strong, nonatomic)UIScrollView* scrView;


@end

@implementation TruckProfileViewController
@synthesize truckImg1;
@synthesize truckImg2;
@synthesize truckImg3;
@synthesize truckImg4;
@synthesize truckfname;
@synthesize truckmname;
@synthesize trucklname;
@synthesize truckemail;
@synthesize trucknumber;
@synthesize truckpass;
@synthesize truckcpass;
@synthesize truckdateob;
@synthesize truckregImg;
@synthesize truckerUsetyp;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadTruckProfileService];
    
    self.title = @"Truck Registration";
    
    self.navigationController.navigationBarHidden = NO;
    
    self.navigationController.navigationBar.tintColor = [DesignObj quikYellow];
    
    screenWidth = [UIScreen mainScreen].bounds.size.width;
    screenHeight = [UIScreen mainScreen].bounds.size.height;
    
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [DesignObj quikYellow]}];
    self.navigationController.navigationBar.barTintColor = [DesignObj quikRed];
    
    UIImageView*bgView = [DesignObj initWithImage:CGRectMake(0, 0, screenWidth, screenHeight) img:@"bg"];
    [self.view addSubview:bgView];
    
    _scrView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, screenHeight)];
    [self.view addSubview:_scrView];
    
    _scrView.contentSize = CGSizeMake(screenWidth, screenHeight+ 150);
    
    datePicker = [[UIDatePicker alloc]init];
    [datePicker addTarget:self action:@selector(datePickerChanged) forControlEvents:UIControlEventValueChanged];
    datePicker.datePickerMode = UIDatePickerModeDate;
    
    
    //    int yaxis = 90;
    //    int y1axis = 90;
    
    trucklbl = [DesignObj initWithLabel:CGRectMake((screenWidth-100)/2, 64+20, 100, 40) title:@"Truck Image" font:16 txtcolor:[DesignObj quikRed]];
    [_scrView addSubview:trucklbl];
    
    truckImg1 = [DesignObj initWithImage:CGRectMake((screenWidth - 220)/2, trucklbl.frame.origin.y +40 +10, 40, 40) img:[NSString stringWithFormat:@"%@",[imagesArr objectAtIndex:0]]];
    [_scrView addSubview:truckImg1];
    
    //    truckImg1.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[imagesArr objectAtIndex:0]]];
    //    truckImg2.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[imagesArr objectAtIndex:1]]];
    //    truckImg3.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[imagesArr objectAtIndex:2]]];
    //    truckImg4.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[imagesArr objectAtIndex:3]]];
    
    truckImg2 = [DesignObj initWithImage:CGRectMake(truckImg1.frame.origin.x + 40 +20, trucklbl.frame.origin.y +40 +10, 40, 40) img:[NSString stringWithFormat:@"%@",[imagesArr objectAtIndex:1]]];
    [_scrView addSubview:truckImg2];
    
    truckImg3 = [DesignObj initWithImage:CGRectMake(truckImg2.frame.origin.x + 40 +20, trucklbl.frame.origin.y +40 +10, 40, 40) img:[NSString stringWithFormat:@"%@",[imagesArr objectAtIndex:2]]];
    [_scrView addSubview:truckImg3];
    
    truckImg4 = [DesignObj initWithImage:CGRectMake(truckImg3.frame.origin.x + 40 +20, trucklbl.frame.origin.y +40 +10, 40, 40) img:[NSString stringWithFormat:@"%@",[imagesArr objectAtIndex:3]]];
    [_scrView addSubview:truckImg4];
    
    selectimage1 = [DesignObj initWithButton:CGRectMake((screenWidth - 220)/2,trucklbl.frame.origin.y +40 +10, 40, 40) tittle:nil img:nil];
    [selectimage1 addTarget:self action:@selector(uploadPic:) forControlEvents:UIControlEventTouchUpInside];
    selectimage1.tag = 1;
    [_scrView addSubview:selectimage1];
    
    selectimage2 = [DesignObj initWithButton:CGRectMake(selectimage1.frame.origin.x + 40 +20, trucklbl.frame.origin.y +40 +10, 40, 40) tittle:nil img:nil];
    [selectimage2 addTarget:self action:@selector(uploadPic:) forControlEvents:UIControlEventTouchUpInside];
    selectimage2.tag = 2;
    [_scrView addSubview:selectimage2];
    
    selectimage3 = [DesignObj initWithButton:CGRectMake(selectimage2.frame.origin.x + 40 +20, trucklbl.frame.origin.y +40 +10, 40, 40) tittle:nil img:nil];
    [selectimage3 addTarget:self action:@selector(uploadPic:) forControlEvents:UIControlEventTouchUpInside];
    selectimage3.tag = 3;
    [_scrView addSubview:selectimage3];
    
    selectimage4 = [DesignObj initWithButton:CGRectMake(selectimage3.frame.origin.x + 40 +20, trucklbl.frame.origin.y +40 +10, 40, 40) tittle:nil img:nil];
    [selectimage4 addTarget:self action:@selector(uploadPic:) forControlEvents:UIControlEventTouchUpInside];
    selectimage4.tag = 4;
    [_scrView addSubview:selectimage4];
    
    //    truckTypelbl = [DesignObj initWithLabel:CGRectMake(20, trucklbl.frame.origin.y +40 +20, 100, 40) title:@"Truck Type: " font:16 txtcolor:[DesignObj quikRed]];
    //    truckTypelbl.textAlignment = NSTextAlignmentLeft;
    //    [_scrView addSubview:truckTypelbl];
    
    truckSizelbl = [DesignObj initWithLabel:CGRectMake((screenWidth-100)/2, selectimage4.frame.origin.y +40 +20, 100, 40) title:@"Truck Size " font:16 txtcolor:[DesignObj quikRed]];
    //    truckSizelbl.textAlignment = NSTextAlignmentLeft;
    [_scrView addSubview:truckSizelbl];
    
    heightofTruck = [DesignObj initWithTextfield:CGRectMake(60, truckSizelbl.frame.origin.y +40 +10, 60, 30) placeholder:@"" tittle:@"Height" delegate:self font:16];
    heightofTruck.tag = 201;
    [heightofTruck setBackground:[UIImage imageNamed:@"text_box.png"]];
    [heightofTruck setReturnKeyType:UIReturnKeyNext];
    heightofTruck.textAlignment = NSTextAlignmentCenter;
    [_scrView addSubview:heightofTruck];
    
    
    lengthofTruck = [DesignObj initWithTextfield:CGRectMake((screenWidth-60)/2, truckSizelbl.frame.origin.y +40 +10, 60, 30) placeholder:@"" tittle:@"Length" delegate:self font:16];
    [lengthofTruck setBackground:[UIImage imageNamed:@"text_box.png"]];
    lengthofTruck.textAlignment = NSTextAlignmentCenter;
    lengthofTruck.tag = 202;
    [lengthofTruck setReturnKeyType:UIReturnKeyNext];
    [_scrView addSubview:lengthofTruck];
    
    //
    //    lengthofTruck = [DesignObj initWithTextfield:CGRectMake(heightofTruck.frame.origin.x +60 +20, truckSizelbl.frame.origin.y +40 +10, 60, 30) placeholder:@"" tittle:@"Length" delegate:self font:16];
    //    [lengthofTruck setBackground:[UIImage imageNamed:@"text_box.png"]];
    //    lengthofTruck.textAlignment = NSTextAlignmentCenter;
    //    lengthofTruck.tag = 203;
    //    [lengthofTruck setReturnKeyType:UIReturnKeyNext];
    //    [_scrView addSubview:lengthofTruck];
    
    //    widthofTruck = [DesignObj initWithTextfield:CGRectMake(lengthofTruck.frame.origin.x +60 +20, truckSizelbl.frame.origin.y +40 +10, 60, 30) placeholder:@"" tittle:@"Width" delegate:self font:16];
    //    [widthofTruck setBackground:[UIImage imageNamed:@"text_box.png"]];
    //    [widthofTruck setReturnKeyType:UIReturnKeyNext];
    //    widthofTruck.tag = 204;
    //    widthofTruck.textAlignment = NSTextAlignmentCenter;
    //    [_scrView addSubview:widthofTruck];
    
    
    widthofTruck = [DesignObj initWithTextfield:CGRectMake(screenWidth-120, truckSizelbl.frame.origin.y +40 +10, 60, 30) placeholder:@"" tittle:@"Width" delegate:self font:16];
    [widthofTruck setBackground:[UIImage imageNamed:@"text_box.png"]];
    [widthofTruck setReturnKeyType:UIReturnKeyNext];
    widthofTruck.tag = 203;
    widthofTruck.textAlignment = NSTextAlignmentCenter;
    [_scrView addSubview:widthofTruck];
    
    
    
    truckType = [DesignObj initWithTextfield:CGRectMake((screenWidth-(screenWidth-44))/2, lengthofTruck.frame.origin.y +40 +20, screenWidth-44, 30) placeholder:@"" tittle:@"Select Truck Type" delegate:self font:16];
    [truckType setBackground:[UIImage imageNamed:@"text_box.png"]];
    [truckType setReturnKeyType:UIReturnKeyNext];
    [truckType addTarget:self action:@selector(showTypes:) forControlEvents:UIControlEventEditingDidBegin];
    truckType.tag = 204;
    truckType.textAlignment = NSTextAlignmentCenter;
    [_scrView addSubview:truckType];
    
    //    truckregnolbl = [DesignObj initWithLabel:CGRectMake(20, truckSizelbl.frame.origin.y +40 +20, 120, 40) title:@"Registration No: " font:16 txtcolor:[DesignObj quikRed]];
    //    truckregnolbl.textAlignment = NSTextAlignmentLeft;
    //    [_scrView addSubview:truckregnolbl];
    
    regno = [DesignObj initWithTextfield:CGRectMake((screenWidth-(screenWidth-44))/2, truckType.frame.origin.y +40 +20, screenWidth-44, 30) placeholder:@"" tittle:@"Registration No" delegate:self font:16];
    [regno setBackground:[UIImage imageNamed:@"text_box.png"]];
    [regno setReturnKeyType:UIReturnKeyNext];
    regno.textAlignment = NSTextAlignmentCenter;
    regno.tag = 205;
    [_scrView addSubview:regno];
    //
    //    truckinsurenceLbl = [DesignObj initWithLabel:CGRectMake(20, truckregnolbl.frame.origin.y +40 +20, 140, 40) title:@"Insurence Validity: " font:16 txtcolor:[DesignObj quikRed]];
    //    truckinsurenceLbl.textAlignment = NSTextAlignmentLeft;
    //    [_scrView addSubview:truckinsurenceLbl];
    
    insurenceno = [DesignObj initWithTextfield:CGRectMake((screenWidth-(screenWidth-44))/2, regno.frame.origin.y +40 +20, screenWidth-44, 30) placeholder:@"" tittle:@"Insurance No" delegate:self font:16];
    [insurenceno setBackground:[UIImage imageNamed:@"text_box.png"]];
    insurenceno.textAlignment = NSTextAlignmentCenter;
    [insurenceno setReturnKeyType:UIReturnKeyNext];
    insurenceno.tag = 206;
    [_scrView addSubview:insurenceno];
    
    
    insurenceValidity = [DesignObj initWithTextfield:CGRectMake((screenWidth-(screenWidth-44))/2, insurenceno.frame.origin.y +40 +20, screenWidth-44, 30) placeholder:@"" tittle:@"Insurance Validity" delegate:self font:16];
    [insurenceValidity setBackground:[UIImage imageNamed:@"text_box.png"]];
    insurenceValidity.textAlignment = NSTextAlignmentCenter;
    [insurenceValidity setReturnKeyType:UIReturnKeyNext];
    insurenceValidity.tag = 207;
    [_scrView addSubview:insurenceValidity];
    
    //    truckRatelbl = [DesignObj initWithLabel:CGRectMake(20, truckinsurenceLbl.frame.origin.y +40 +20, 120, 40) title:@"Truck Rate: " font:16 txtcolor:[DesignObj quikRed]];
    //    truckRatelbl.textAlignment = NSTextAlignmentLeft;
    //    [_scrView addSubview:truckRatelbl];
    
    //    truckServiceCatlbl = [DesignObj initWithLabel:CGRectMake(20, truckRatelbl.frame.origin.y +40 +20, 150, 40) title:@"Service Categories: " font:16 txtcolor:[DesignObj quikRed]];
    //    truckServiceCatlbl.textAlignment = NSTextAlignmentLeft;
    //    [_scrView addSubview:truckServiceCatlbl];
    
    serviceCategories = [DesignObj initWithTextfield:CGRectMake((screenWidth-(screenWidth-44))/2, insurenceValidity.frame.origin.y +40 +20, screenWidth-44, 30) placeholder:@"" tittle:@"Categories" delegate:self font:16];
    [serviceCategories setBackground:[UIImage imageNamed:@"text_box.png"]];
    serviceCategories.textAlignment = NSTextAlignmentCenter;
    [serviceCategories setReturnKeyType:UIReturnKeyNext];
    serviceCategories.tag = 208;
    [_scrView addSubview:serviceCategories];
    
    //    truckEquiplbl = [DesignObj initWithLabel:CGRectMake(20, truckServiceCatlbl.frame.origin.y +40 +20, 120, 40) title:@"Equipments: " font:16 txtcolor:[DesignObj quikRed]];
    //    truckEquiplbl.textAlignment = NSTextAlignmentLeft;;
    //    [_scrView addSubview:truckEquiplbl];
    
    Equipments = [DesignObj initWithTextfield:CGRectMake((screenWidth- (screenWidth-44))/2, serviceCategories.frame.origin.y +40 +20, screenWidth-44, 30) placeholder:@"" tittle:@"Equipments" delegate:self font:16];
    [Equipments setBackground:[UIImage imageNamed:@"text_box.png"]];
    [Equipments setReturnKeyType:UIReturnKeyNext];
    Equipments.textAlignment = NSTextAlignmentCenter;
    Equipments.tag = 209;
    [_scrView addSubview:Equipments];
    
    truckPaymentlbl = [DesignObj initWithLabel:CGRectMake((screenWidth - 120)/2, Equipments.frame.origin.y +40 +20, 120, 40) title:@"Payment" font:16 txtcolor:[DesignObj quikRed]];
    truckPaymentlbl.textAlignment = NSTextAlignmentCenter;;
    [_scrView addSubview:truckPaymentlbl];
    
    accountNo = [DesignObj initWithTextfield:CGRectMake((screenWidth- (screenWidth-44))/2, truckPaymentlbl.frame.origin.y +40 +20, screenWidth-44, 30) placeholder:@"" tittle:@"Account No" delegate:self font:16];
    [accountNo setBackground:[UIImage imageNamed:@"text_box.png"]];
    [accountNo setReturnKeyType:UIReturnKeyNext];
    accountNo.keyboardType = UIKeyboardTypeNumberPad;
    accountNo.textAlignment = NSTextAlignmentCenter;
    accountNo.tag = 210;
    [_scrView addSubview:accountNo];
    
    routingNo = [DesignObj initWithTextfield:CGRectMake((screenWidth- (screenWidth-44))/2, accountNo.frame.origin.y +40 +20, screenWidth-44, 30) placeholder:@"" tittle:@"Routing No" delegate:self font:16];
    [routingNo setBackground:[UIImage imageNamed:@"text_box.png"]];
    [routingNo setReturnKeyType:UIReturnKeyNext];
    routingNo.textAlignment = NSTextAlignmentCenter;
    routingNo.keyboardType = UIKeyboardTypeNumberPad;
    routingNo.tag = 211;
    [_scrView addSubview:routingNo];
    
    heightofTruck.keyboardType = UIKeyboardTypeNumberPad;
    widthofTruck.keyboardType = UIKeyboardTypeNumberPad;
    lengthofTruck.keyboardType = UIKeyboardTypeNumberPad;
    insurenceValidity.inputView = datePicker;
    regno.keyboardType = UIKeyboardTypeNumberPad;
    insurenceno.keyboardType = UIKeyboardTypeNumberPad;
    
    
    UIButton* regbtn =[DesignObj initWithButton:CGRectMake((screenWidth - 200)/2, routingNo.frame.origin.y + 38 +30, 200, 40)  tittle:@"Update" img:@"button.png"];
    [regbtn addTarget:self action:@selector(regtruckAction:) forControlEvents:UIControlEventTouchUpInside];
    [regbtn setTitleColor:[DesignObj quikYellow] forState:UIControlStateNormal];
    [_scrView addSubview:regbtn];
    
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, screenWidth, 44)];
    numberToolbar.barStyle = UIBarStyleDefault;//UIBarStyleBlackTranslucent;
    numberToolbar.tintColor = [UIColor colorWithRed:207/255.0 green:26/255.0 blue:26/255.0 alpha:1.0];
    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleDone target:self action:@selector(cancelNumberPad)],
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
    [numberToolbar sizeToFit];
    
    heightofTruck.inputAccessoryView = numberToolbar;
    widthofTruck.inputAccessoryView = numberToolbar;
    lengthofTruck.inputAccessoryView = numberToolbar;
    regno.inputAccessoryView = numberToolbar;
    insurenceno.inputAccessoryView = numberToolbar;
    insurenceValidity.inputAccessoryView = numberToolbar;
    accountNo.inputAccessoryView = numberToolbar;
    routingNo.inputAccessoryView = numberToolbar;
    
    
    
    
    // Do any additional setup after loading the view.
}


-(BOOL)textFieldShouldReturn:(UITextField*)textField
{
    NSInteger nextTag = textField.tag +001;
    // Try to find next responder
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else {
        // Not found, so remove keyboard.
        [textField resignFirstResponder];
    }
    return NO; // We do not want UITextField to insert line-breaks.
}

-(IBAction)showTypes:(id)sender{
    
//    [Txt resignFirstResponder];
    
//        [self truckTypeService];
    
}

-(void)loadlistView{
    
    NSInteger height;
    if (listofTypesArr.count*44>screenHeight-170-70) {
        height = screenHeight-100-40-30;
    }else{
        height = listofTypesArr.count*44;
    }
    [self closeTypeView];
    
    //   (, trucklbl.frame.origin.y +40 +20, 200, 30)
    
    trucktypestblView = [[UITableView alloc]initWithFrame:CGRectMake(truckTypelbl.frame.origin.x +100 +20, trucklbl.frame.origin.y +40 +20 +30, 200, height) style:UITableViewStylePlain];
    [trucktypestblView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg"]]];
    trucktypestblView.delegate = self;
    trucktypestblView.dataSource = self;
    [self.view addSubview:trucktypestblView];
    trucktypestblView.opaque =NO;
    trucktypestblView.layer.cornerRadius = 5;
    trucktypestblView.layer.borderWidth = 1.0;
    trucktypestblView.layer.borderColor = [UIColor blackColor].CGColor;
    
    //    closeView = [[UIView alloc]initWithFrame:CGRectMake(screenWidth-40, 127, 35, 35)];
    //    closeView.layer.cornerRadius = 35/2;
    //    closeView.backgroundColor = [UIColor whiteColor];
    //    closeView.layer.borderColor = [UIColor blackColor].CGColor;
    //    closeView.layer.borderWidth = 1.0;
    //    [self.view addSubview:closeView];
    
    //(screenWidth-40, 127, 35, 35)
    
    closebtn = [UIButton buttonWithType:UIButtonTypeCustom];
    closebtn.frame= CGRectMake(truckTypelbl.frame.origin.x +100 +20+185, trucklbl.frame.origin.y +40 +20 +20, 30, 30);
    [closebtn addTarget:self action:@selector(closeTypeView) forControlEvents:UIControlEventTouchUpInside];
    closebtn.layer.cornerRadius = 35/2;
    [closebtn setImage:[UIImage imageNamed:@"close.png"] forState:UIControlStateNormal];
    [self.view addSubview:closebtn];
}

-(void)closeTypeView{
    [closeView removeFromSuperview];
    [closebtn removeFromSuperview];
    [trucktypestblView removeFromSuperview];
}



- (void)datePickerChanged{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMM-dd-yyyy"];
    NSString *strDate = [dateFormatter stringFromDate:datePicker.date];
    insurenceValidity.text = strDate;
}

-(void)cancelNumberPad{
    [heightofTruck resignFirstResponder];
    [widthofTruck resignFirstResponder];
    [lengthofTruck resignFirstResponder];
    [rate resignFirstResponder];
    [regno resignFirstResponder];
    [insurenceValidity resignFirstResponder];
    
    heightofTruck.text = @"";
    widthofTruck.text = @"";
    lengthofTruck.text = @"";
    rate.text = @"";
    regno.text = @"";
    insurenceValidity.text = @"";
    
}

-(void)doneWithNumberPad{
    
    [heightofTruck resignFirstResponder];
    [widthofTruck resignFirstResponder];
    [lengthofTruck resignFirstResponder];
    [regno resignFirstResponder];
    [rate resignFirstResponder];
    [insurenceValidity resignFirstResponder];
    [serviceCategories resignFirstResponder];
}

-(IBAction)uploadPic:(id)sender{
    
    if ([sender tag]==1) {
        imageTag = 101;
    }else if ([sender tag]==2) {
        imageTag = 102;
    }else if ([sender tag]==3) {
        imageTag = 103;
    }else if ([sender tag]==4) {
        imageTag = 104;
    }
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Message"
                                  message:@"Choose options to Upload your Truck image"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* library = [UIAlertAction
                              actionWithTitle:@"From Photo Library"
                              style:UIAlertActionStyleDefault
                              handler:^(UIAlertAction * action)
                              {
                                  [alert dismissViewControllerAnimated:YES completion:nil];
                                  [self selectPhoto];
                                  
                              }];
    
    UIAlertAction* Camera = [UIAlertAction
                             actionWithTitle:@"Take Photo"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 [self takePhoto];
                             }];
    
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    
    [alert addAction:library];
    [alert addAction:Camera];
    [alert addAction:cancel];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    
    
}

- (void)selectPhoto{
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:NULL];
    
    
}

- (void)takePhoto{
    
    DBCameraContainerViewController *cameraContainer = [[DBCameraContainerViewController alloc] initWithDelegate:self];
    [cameraContainer setFullScreenMode];
    
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:cameraContainer];
    [nav setNavigationBarHidden:YES];
    [self presentViewController:nav animated:YES completion:nil];
    
    
}

- (void) camera:(id)cameraViewController didFinishWithImage:(UIImage *)image withMetadata:(NSDictionary *)metadata
{
    //    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    //    profileImg.image = image;
    //    imageData = UIImagePNGRepresentation(profileImg.image);
    
    if (imageTag == 101) {
        truckImg1.image = image;
    }else if (imageTag == 102) {
        truckImg2.image = image;
    }else if (imageTag == 103) {
        truckImg3.image = image;
    }else if (imageTag == 104) {
        truckImg4.image = image;
    }
    
    
    
    
    //    DetailViewController *detail = [[DetailViewController alloc] init];
    //    [detail setDetailImage:image];
    //    [self.navigationController pushViewController:detail animated:NO];
    //    [cameraViewController restoreFullScreenMode];
    [self.presentedViewController dismissViewControllerAnimated:YES completion:nil];
}


- (void) dismissCamera:(id)cameraViewController{
    [self dismissViewControllerAnimated:YES completion:nil];
    [cameraViewController restoreFullScreenMode];
}

-(void)showAlert:(NSString*)message{
    UIAlertView * alt = [[UIAlertView alloc]initWithTitle:@"Message" message:message delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alt show];
}

-(IBAction)regtruckAction:(id)sender{
    
    [Equipments resignFirstResponder];
    if (truckType.text.length==0) {
        [self showAlert:@"Please enter your Truck Type"];
    }else if (heightofTruck.text.length==0) {
        [self showAlert:@"Please enter your Truck Height"];
    }else if (widthofTruck.text.length==0) {
        [self showAlert:@"Please enter your Truck Width"];
    }else if (lengthofTruck.text.length==0) {
        [self showAlert:@"Please enter your Truck Length"];
    }else if (insurenceValidity.text.length==0){
        [self showAlert:@"Please enter your Truck Insurence validity date"];
    }else if (insurenceno.text.length==0){
        [self showAlert:@"Please enter the Rate of your Truck"];
    }else if (serviceCategories.text.length==0){
        [self showAlert:@"Please enter Truck Service Categories"];
    }else if (Equipments.text.length==0){
        [self showAlert:@"Please enter the Equipments of your Truck"];
    }else{
        
        [self truckUpdate];
    }
    
}

-(void)truckUpdate{
    
    
    if ([Reachability reachabilityForInternetConnection]) {
        
        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
        [SVProgressHUD show];
        
        NSArray *keys = [[NSArray alloc]initWithObjects:@"userid",@"name",@"mname",@"lname",@"email",@"mobile",@"password",@"cpassword",nil];
        
//        ,@"dob",@"usertype",
        
        
        //
        NSArray *values =[[NSArray alloc]initWithObjects:userID,truckfname, truckmname, trucklname, truckemail, trucknumber,truckpass,truckcpass, nil];
        
//        ,truckdateob,truckerUsetyp
        
        NSURL *url =[NSURL URLWithString:[NSString stringWithFormat:@"http://quikmov.quikmovapp.com/auth/editRegister"]];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        [request setHTTPMethod:@"POST"];
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        NSMutableData *body = [NSMutableData data];
        NSData * data = [NSData dataWithData:UIImagePNGRepresentation(truckregImg)];
        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"imagefile\"; filename=\"%@userprofile.png\"\r\n",truckfname] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: image/png\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[NSData dataWithData:data]];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        for (int i=0;i<keys.count;i++) {
            
            NSLog(@"%@",[keys objectAtIndex:i]);
            NSString *key = [keys objectAtIndex:i];
            NSString *value = [values objectAtIndex:i];
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n%@",key, value] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [request setHTTPBody:body];
        
        NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession* session = [NSURLSession sessionWithConfiguration:config ];
        
        
        NSURLSessionUploadTask *uploadtask = [session uploadTaskWithRequest:request fromData:body completionHandler:^(NSData*data, NSURLResponse*response, NSError*error){
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                
                NSString* str = [[NSString alloc]initWithData:data
                                                     encoding:NSUTF8StringEncoding];
                
                NSError* error;
                
                
                
                NSDictionary* dic = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                
                NSLog(@"%@******%@,%@",str,[dic allValues],[dic objectForKey:@"response"]);
                
                // userId = [[dic objectForKey:@"message"] stringValue];
                NSString * Result = [dic objectForKey:@"status"];
                
                NSDictionary * result = [dic objectForKey:@"result"];
                
                if ([[dic objectForKey:@"result"] isKindOfClass:[NSDictionary class]]){
                    
                    regUserid = [result objectForKey:@"user_id"];
                    NSLog(@"%@",regUserid);
                    
                    NSUserDefaults* userStoredetails = [NSUserDefaults standardUserDefaults];
                    [userStoredetails setObject:regUserid forKey:@"user_id"];
                    [userStoredetails synchronize];
                    
                    NSLog(@"%@",Result);
                    
                    if ([Result isEqualToString:@"success"]){
                        
                        //                        UIAlertController * alert=   [UIAlertController alertControllerWithTitle:@"Message"
                        //                                                                                         message:@"Registration Successful" preferredStyle:UIAlertControllerStyleAlert];
                        //
                        //                        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK"
                        //                                                                     style:UIAlertActionStyleDefault
                        //                                                                   handler:^(UIAlertAction * action)
                        //                                             {
                        //                                                 [alert dismissViewControllerAnimated:YES completion:nil];
                        
                        
                        [self truckregService];
                        
                        // }];
                        
                        
                        //  [alert addAction:ok];
                        
                        //   [self presentViewController:alert animated:YES completion:nil];
                    }else{
                        
                        [self showAlert:[NSString stringWithFormat:@"%@",[dic objectForKey:@"result"]]];
                    }
                }
                
            });
            
            
        }];
        [uploadtask resume];
        
        
        //        NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        //        if( theConnection )
        //        {
        //            _downloadedData = [NSMutableData data] ;
        //            NSLog(@"request uploading successful");
        //        }
        //        else
        //        {
        //            NSLog(@"theConnection is NULL");
        //        }
        
    }else{
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"No internet connectivity" message:@"Please try later" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    
    
    
    
}

-(void)truckregService{
    
    if ([Reachability reachabilityForInternetConnection]) {
        
        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
        [SVProgressHUD show];
        
        //            inputs :trucktype,truckheight,truckwidth,trucklen,imagefile, userid , regno,insurance,rate,,servicecategories,equipment
        //            url : http://truckcsoft.mapcee.com/auth/truckerProfile
        
        //        NSUserDefaults* userStoredetails = [NSUserDefaults standardUserDefaults];
        //        NSString * userID = [userStoredetails objectForKey:@"user_id"];
        //        NSLog(@"%@",userID);
        
        NSArray *keys = [[NSArray alloc]initWithObjects:@"trucktype",@"truckheight",@"truckwidth",@"trucklen",@"userid",@"regno",@"insurance",@"rate",@"servicecategories",@"equipment",nil];
        //
        NSArray *values =[[NSArray alloc]initWithObjects:truckType, heightofTruck, widthofTruck,lengthofTruck,userID,regno,insurenceValidity,insurenceno,serviceCategories,Equipments, nil];
        
        
        NSURL *url =[NSURL URLWithString:[NSString stringWithFormat:@"http://quikmov.quikmovapp.com/auth/truckerProfile"]];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        [request setHTTPMethod:@"POST"];
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        NSMutableData *body = [NSMutableData data];
        NSData * data = [NSData dataWithData:UIImagePNGRepresentation(truckregImg)];
        //            NSData * data2 = [NSData dataWithData:UIImagePNGRepresentation(truckImg2.image)];
        //            NSData * data3 = [NSData dataWithData:UIImagePNGRepresentation(truckImg3.image)];
        //            NSData * data4 = [NSData dataWithData:UIImagePNGRepresentation(truckImg4.image)];
        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"imagefile\"; filename=\"%@userprofile.png\"\r\n",userID] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: image/png\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[NSData dataWithData:data]];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        for (int i=0;i<keys.count;i++) {
            
            NSLog(@"%@",[keys objectAtIndex:i]);
            NSString *key = [keys objectAtIndex:i];
            NSString *value = [values objectAtIndex:i];
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n%@",key, value] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [request setHTTPBody:body];
        
        NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession* session = [NSURLSession sessionWithConfiguration:config ];
        
        NSURLSessionUploadTask *uploadtask = [session uploadTaskWithRequest:request fromData:body completionHandler:^(NSData*data, NSURLResponse*response, NSError*error){
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                
                NSString* str = [[NSString alloc]initWithData:data
                                                     encoding:NSUTF8StringEncoding];
                
                NSError* error;
                
                
                
                NSDictionary* dic = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                
                NSLog(@"%@******%@,%@",str,[dic allValues],[dic objectForKey:@"response"]);
                
                // userId = [[dic objectForKey:@"message"] stringValue];
                // BOOL Result = [dic objectForKey:[NSString stringWithFormat: @"message"]];
                
                //             NSLog(@"%hhd",Result);
                
                if ([[dic objectForKey:@"iserror"]boolValue] == NO){
                    //
                    //                                            UIAlertController * alert=   [UIAlertController alertControllerWithTitle:@"Message"
                    //                                                                                                             message:@"Truck Registration Successful" preferredStyle:UIAlertControllerStyleAlert];
                    //
                    //                                            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK"
                    //                                                                                         style:UIAlertActionStyleDefault
                    //                                                                                       handler:^(UIAlertAction * action)
                    //                                                                 {
                    //                                                                     [alert dismissViewControllerAnimated:YES completion:nil];
                    //                                                                     VIewBookingViewController* mapview = [[VIewBookingViewController alloc]init];
                    //                                                                     [self.navigationController pushViewController:mapview animated:YES];
                    
                    
                    [self uploadService];
                    
                    
                    //                                                                 }];
                    //
                    //
                    //                                            [alert addAction:ok];
                    //
                    //                                            [self presentViewController:alert animated:YES completion:nil];
                    
                    //                    [self imageService];
                }else{
                    
                    [self showAlert:[NSString stringWithFormat:@"%@",[dic objectForKey:@"result"]]];
                }
                
            });
            
            
        }];
        [uploadtask resume];
        
        
        //        NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        //        if( theConnection )
        //        {
        //            _downloadedData = [NSMutableData data] ;
        //            NSLog(@"request uploading successful");
        //        }
        //        else
        //        {
        //            NSLog(@"theConnection is NULL");
        //        }
        
    }else{
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"No internet connectivity" message:@"Please try later" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    
}

-(UIImage*)scaleDown:(UIImage*)img withSize:(CGSize)newSize{
    
    
    UIGraphicsBeginImageContextWithOptions(newSize, YES, 0.0);
    [img drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return scaledImage;
}

- (void)uploadService{
    count = 0;
    [self imageService:@"1" withImage:truckImg1.image];
}

-(void)imageService:(NSString*)imagename withImage:(UIImage*)image{
    
    
    if ([Reachability reachabilityForInternetConnection]) {
        [SVProgressHUD showWithStatus:@"Truck Images Uploading"];
        
        //        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
        //        [SVProgressHUD show];
        
        //        NSUserDefaults* userStoredetails = [NSUserDefaults standardUserDefaults];
        //        NSString * userID = [userStoredetails objectForKey:@"user_id"];
        //        NSLog(@"%@",userID);
        
        NSArray *keys = [[NSArray alloc]initWithObjects:@"userid",nil];
        //
        NSArray *values =[[NSArray alloc]initWithObjects:userID, nil];
        
        
        NSURL *url =[NSURL URLWithString:[NSString stringWithFormat:@"http://quikmov.quikmovapp.com/auth/truckImages"]];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        [request setHTTPMethod:@"POST"];
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        NSMutableData *body = [NSMutableData data];
        
        if(!(image.imageOrientation == UIImageOrientationUp ||
             image.imageOrientation == UIImageOrientationUpMirrored))
        {
            CGSize imgsize = image.size;
            UIGraphicsBeginImageContext(imgsize);
            [image drawInRect:CGRectMake(0.0, 0.0, imgsize.width, imgsize.height)];
            image = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
        }
        
        NSData * data = [NSData dataWithData:UIImagePNGRepresentation(image)];
        //        11700.6005859
        //        float factor;
        //        float resol = image.size.height*image.size.width;
        //        if (resol >MIN_UPLOAD_RESOLUTION){
        //            factor = sqrt(resol/MIN_UPLOAD_RESOLUTION)*2;
        //            image = [self scaleDown:image withSize:CGSizeMake(image.size.width/factor, image.size.height/factor)];
        //        }
        
        //Compress the image
        CGFloat compression = 0.9f;
        CGFloat maxCompression = 0.1f;
        
        while ([data length] > MAX_UPLOAD_SIZE && compression > maxCompression)
        {
            compression -= 0.10;
            data = UIImageJPEGRepresentation(image, compression);
            NSLog(@"Compress : %lu",(unsigned long)data.length);
        }
        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"imagefile\"; filename=\"%@truk%@.png\"\r\n",userID,imagename] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: image/png\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        //        Data changes as imageData
        [body appendData:[NSData dataWithData:data]];
        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        for (int i=0;i<keys.count;i++) {
            
            NSLog(@"%@",[keys objectAtIndex:i]);
            NSString *key = [keys objectAtIndex:i];
            NSString *value = [values objectAtIndex:i];
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n%@",key, value] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [request setHTTPBody:body];
        
        NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession* session = [NSURLSession sessionWithConfiguration:config ];
        
        //        Data Changed as imageData
        NSURLSessionUploadTask *uploadtask = [session uploadTaskWithRequest:request fromData:body completionHandler:^(NSData*data, NSURLResponse*response, NSError*error){
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                
                if (data == nil) {
                    return;
                }
                
                NSString* str = [[NSString alloc]initWithData:data
                                                     encoding:NSUTF8StringEncoding];
                
                NSError* error;
                
                
                
                NSDictionary* dic = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                
                NSLog(@"%@******%@,%@",str,[dic allValues],[dic objectForKey:@"response"]);
                
                // userId = [[dic objectForKey:@"message"] stringValue];
                // BOOL Result = [dic objectForKey:[NSString stringWithFormat: @"message"]];
                
                //             NSLog(@"%hhd",Result);
                
                if ([[dic objectForKey:@"iserror"]boolValue] == NO){
                    
                    
                    count = count + 1;
                    
                    if (count  < 4) {
                        
                        if (count == 1) {
                            [self imageService:@"2" withImage:truckImg2.image];
                        }else if (count == 2){
                            [self imageService:@"3" withImage:truckImg3.image];
                        }else if (count == 3){
                            [self imageService:@"4" withImage:truckImg4.image];
                        }
                        
                    }else{
                        //                        [SVProgressHUD dismiss];
                        UIAlertController * alert=   [UIAlertController alertControllerWithTitle:@"Message" message:@"Truck Updated Successful" preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                             {
                                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                                 
                                                 
                                             }];
                        
                        
                        [alert addAction:ok];
                        
                        [self presentViewController:alert animated:YES completion:nil];
                    }
                    
                }else{
                    
                    [self showAlert:[NSString stringWithFormat:@"%@",[dic objectForKey:@"result"]]];
                }
                
            });
            
            
        }];
        [uploadtask resume];
        
        
        //        NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        //        if( theConnection )
        //        {
        //            _downloadedData = [NSMutableData data] ;
        //            NSLog(@"request uploading successful");
        //        }
        //        else
        //        {
        //            NSLog(@"theConnection is NULL");
        //        }
        
    }else{
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"No internet connectivity" message:@"Please try later" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    
    
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return listofTypesArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    // This will create a "invisible" footer
    return 0.001;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *MyIdentifier = @"MyIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:MyIdentifier];
    }
    
    for (UIView * view1 in cell.contentView.subviews)
    {
        [view1 removeFromSuperview];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    GlobalObjects * globalObj = (GlobalObjects*)[listofTypesArr objectAtIndex:indexPath.row];
    if (tableView==trucktypestblView) {
        cell.textLabel.textColor = [DesignObj quikRed];
        cell.contentView.backgroundColor =[UIColor clearColor];
        
        //        [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg"]];
        cell.textLabel.text = [NSString stringWithFormat:@"%@",globalObj.truckTypes];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    GlobalObjects * globalObj = (GlobalObjects*)[listofTypesArr objectAtIndex:indexPath.row];
    truckType.text = [NSString stringWithFormat:@"%@",globalObj.truckTypes];
    [self closeTypeView];
}



-(void)loadTruckProfileService{
    
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
    [SVProgressHUD show];
    
    NSURL* url = [NSURL URLWithString:@"http://quikmov.quikmovapp.com/auth/getUserDetails"];
    NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession* session = [NSURLSession sessionWithConfiguration:config ];
    
    NSMutableURLRequest*request = [[NSMutableURLRequest alloc]initWithURL:url];
    request.HTTPMethod = @"POST";
    
    NSUserDefaults* userStoredetails = [NSUserDefaults standardUserDefaults];
    userID = [userStoredetails objectForKey:@"user_id"];
    NSLog(@"%@",userID);
    
    
    NSString* bodycntnt = [NSString stringWithFormat:@"userid=%@",userID];
    
    NSData *data = [bodycntnt dataUsingEncoding:NSUTF8StringEncoding];
    
    NSURLSessionUploadTask *uploadtask = [session uploadTaskWithRequest:request fromData:data completionHandler:^(NSData*data, NSURLResponse*response, NSError*error){
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
            [self serviceSuccess:data];
        });
    }];
    
    [uploadtask resume];
    
    
}

- (void)serviceSuccess:(NSData *)data{
    if (data == nil) {
        return;
    }
    NSString* str = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
    NSError * error;
    NSDictionary* dic = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    NSLog(@"%@******%@,%@",str,[dic allValues],[dic objectForKey:@"response"]);
    
    NSString * Result = [dic objectForKey:@"status"];
    
    NSLog(@"%@",Result);
    if ([Result isEqualToString:@"success"]){
        
    /*    GlobalObjects * globleObj = [[GlobalObjects alloc]init];
        NSDictionary * objDic = [dic objectForKey:@"result"];
        //
        
        globleObj.fname = [objDic objectForKey:@"user_name"];
        
        globleObj.mname = [objDic objectForKey:@"mname"];
        
        globleObj.lname = [objDic objectForKey:@"lname"];
        
        globleObj.user_mail = [objDic objectForKey:@"user_email"];
        
        globleObj.phno = [objDic objectForKey:@"user_phone"];
        
        globleObj.passrd = [objDic objectForKey:@"user_password"];
        
        globleObj.cpassrd = [objDic objectForKey:@"user_password"];
        
        globleObj.truckWidth = [objDic objectForKey:@"truckwidth"];
        
        globleObj.truckHeight = [objDic objectForKey:@"truckheight"];
        
        globleObj.truckLength = [objDic objectForKey:@"trucklen"];
        
        globleObj.truckType = [objDic objectForKey:@"trucktype"];
        
        globleObj.sizeOfTruck = [objDic objectForKey:@"sizeoftruck"];
        
        globleObj.raTe = [objDic objectForKey:@"rate"];
        
        globleObj.regNo = [objDic objectForKey:@"regno"];
        
        globleObj.serviceCatogories = [objDic objectForKey:@"servicecategories"];
        
        globleObj.equip = [objDic objectForKey:@"equipment"];
        
        globleObj.insurance = [objDic objectForKey:@"insurance"];
        
        globleObj.truckImag = [objDic objectForKey:[NSString stringWithFormat:@"http://quikmov.quikmovapp.com%@",[objDic objectForKey:@"filepath"]]];
        
        globleObj.dob = [objDic objectForKey:@"dob"];
        
        //        truckImg1 = [DesignObj initWithImage:CGRectMake((screenWidth-80)/2, 70, 80, 80) img:@""];
        //        dispatch_async(dispatch_get_main_queue(), ^{
        //            [truckImg1 sd_setImageWithURL:[NSURL URLWithString:globleObj.truckImag] placeholderImage:[UIImage imageNamed:@"truck.png"]];
        //
        //            //[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:object.userPhotoUrl]]];
        //        });
        //        [_scrView addSubview:truckImg1];
        
        widthofTruck.text = globleObj.truckWidth;
        heightofTruck.text = globleObj.truckHeight;
        lengthofTruck.text = globleObj.truckLength;
        truckType.text = globleObj.truckType;
        regno.text = globleObj.regNo;
        insurenceValidity.text = globleObj.insurance;
        rate.text = globleObj.raTe;
        serviceCategories.text = globleObj.serviceCatogories;
        Equipments.text = globleObj.equip;
        
        [self getTruckImages];
        
        
        //            globleObj.userId = [dic objectForKey:@"user_id"];
    }
    
    */
    }
}

-(void)getTruckImages{
    
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
    [SVProgressHUD show];
    
    NSURL* url = [NSURL URLWithString:@"http://quikmov.quikmovapp.com/auth/getTruckImages"];
    NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession* session = [NSURLSession sessionWithConfiguration:config ];
    
    NSMutableURLRequest*request = [[NSMutableURLRequest alloc]initWithURL:url];
    request.HTTPMethod = @"POST";
    
    NSString* bodycntnt = [NSString stringWithFormat:@"userid=%@",userID];
    
    NSData *data = [bodycntnt dataUsingEncoding:NSUTF8StringEncoding];
    
    NSURLSessionUploadTask *uploadtask = [session uploadTaskWithRequest:request fromData:data completionHandler:^(NSData*data, NSURLResponse*response, NSError*error){
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
            
            NSString* str = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
            NSError * error;
            NSDictionary* dic = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            NSLog(@"%@******%@,%@",str,[dic allValues],[dic objectForKey:@"response"]);
            
            NSString * Result = [dic objectForKey:@"status"];
            
            NSLog(@"%@",Result);
            
            if ([Result isEqualToString:@"success"]){
                NSArray * arr = [dic objectForKey:@"result"];
                
                GlobalObjects * globleObj = [[GlobalObjects alloc]init];
                globleObj.truckPhotos = [NSMutableArray new];
                
                for ( int i = 0; i < [arr count]; i++) {
                    
                    //
                    NSDictionary * objDic = [arr objectAtIndex:i];
                    
                    NSString * url = [NSString stringWithFormat:@"http://quikmov.quikmovapp.com%@",[objDic objectForKey:@"filepath"]];
                    if (i == 0) {
                        
//                        [truckImg1 sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:@"addtruckimg.png"]];
                      truckImg1.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:url]]];
                    }else if (i == 1){
                       
//                        [truckImg2 sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:@"addtruckimg.png"]];
                        truckImg2.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:url]]];
                    }else if (i == 2){
                        
//                        [truckImg3 sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:@"addtruckimg.png"]];
                        truckImg3.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:url]]];
                    }else{
                        
//                      [truckImg4 sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:@"addtruckimg.png"]];
                        truckImg4.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:url]]];
                    }
                    
                }
                
            }else{
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Message" message:[NSString stringWithFormat:@"%@",[dic objectForKey:@"result"]] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alert show];
            }
        });
    }];
    
    [uploadtask resume];
    
    
}
//
//-(void)truckupdateUserProfilePic{
//
//    if ([Reachability reachabilityForInternetConnection]) {
//
//        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
//        [SVProgressHUD show];
//
//        NSArray *keys = [[NSArray alloc]initWithObjects:@"name",@"mname",@"lname",@"email",@"mobile",@"password",@"cpassword",@"dob",@"usertype",nil];
//
//
//        //
//        NSArray *values =[[NSArray alloc]initWithObjects:truckfname1, truckmname1, trucklname1, truckemail1, trucknumber1,truckpass1,truckcpass1,truckdateob1,truckerUsetyp1, nil];
//
//        NSURL *url =[NSURL URLWithString:[NSString stringWithFormat:@"http://quikmov.quikmovapp.com/auth/signup"]];
//
//        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
//        [request setHTTPMethod:@"POST"];
//        NSString *boundary = @"---------------------------14737809831466499882746641449";
//        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
//        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
//
//        NSMutableData *body = [NSMutableData data];
//        NSData * data = [NSData dataWithData:UIImagePNGRepresentation(_truckregImgs)];
//
//        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"imagefile\"; filename=\"%@userprofile.png\"\r\n",truckfname1] dataUsingEncoding:NSUTF8StringEncoding]];
//        [body appendData:[@"Content-Type: image/png\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
//
//        [body appendData:[NSData dataWithData:data]];
//        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//
//        for (int i=0;i<keys.count;i++) {
//
//            NSLog(@"%@",[keys objectAtIndex:i]);
//            NSString *key = [keys objectAtIndex:i];
//            NSString *value = [values objectAtIndex:i];
//            [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n%@",key, value] dataUsingEncoding:NSUTF8StringEncoding]];
//        }
//
//        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//
//        [request setHTTPBody:body];
//
//        NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
//        NSURLSession* session = [NSURLSession sessionWithConfiguration:config ];
//
//
//        NSURLSessionUploadTask *uploadtask = [session uploadTaskWithRequest:request fromData:body completionHandler:^(NSData*data, NSURLResponse*response, NSError*error){
//            dispatch_async(dispatch_get_main_queue(), ^{
//                [SVProgressHUD dismiss];
//
//                NSString* str = [[NSString alloc]initWithData:data
//                                                     encoding:NSUTF8StringEncoding];
//
//                NSError* error;
//
//
//
//                NSDictionary* dic = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
//
//                NSLog(@"%@******%@,%@",str,[dic allValues],[dic objectForKey:@"response"]);
//
//                // userId = [[dic objectForKey:@"message"] stringValue];
//                NSString * Result = [dic objectForKey:@"status"];
//
//                NSDictionary * result = [dic objectForKey:@"result"];
//
//                if ([[dic objectForKey:@"result"] isKindOfClass:[NSDictionary class]]){
//
//                    regUserid = [result objectForKey:@"user_id"];
//                    NSLog(@"%@",regUserid);
//
//                    NSUserDefaults* userStoredetails = [NSUserDefaults standardUserDefaults];
//                    [userStoredetails setObject:regUserid forKey:@"user_id"];
//                    [userStoredetails synchronize];
//
//                    NSLog(@"%@",Result);
//
//                    if ([Result isEqualToString:@"success"]){
//
//                        //                        UIAlertController * alert=   [UIAlertController alertControllerWithTitle:@"Message"
//                        //                                                                                         message:@"Registration Successful" preferredStyle:UIAlertControllerStyleAlert];
//                        //
//                        //                        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK"
//                        //                                                                     style:UIAlertActionStyleDefault
//                        //                                                                   handler:^(UIAlertAction * action)
//                        //                                             {
//                        //                                                 [alert dismissViewControllerAnimated:YES completion:nil];
//
//
//                        [self truckregService];
//
//                        // }];
//
//
//                        //  [alert addAction:ok];
//
//                        //   [self presentViewController:alert animated:YES completion:nil];
//                    }else{
//
//                        [self showAlert:[NSString stringWithFormat:@"%@",[dic objectForKey:@"result"]]];
//                    }
//                }
//
//            });
//
//
//        }];
//        [uploadtask resume];
//
//
//        //        NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
//        //        if( theConnection )
//        //        {
//        //            _downloadedData = [NSMutableData data] ;
//        //            NSLog(@"request uploading successful");
//        //        }
//        //        else
//        //        {
//        //            NSLog(@"theConnection is NULL");
//        //        }
//
//    }else{
//        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"No internet connectivity" message:@"Please try later" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
//        [alert show];
//    }
//
//
//}
//
//-(void)truckregService{
//
//    if ([Reachability reachabilityForInternetConnection]) {
//
//        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
//        [SVProgressHUD show];
//
//        //            inputs :trucktype,truckheight,truckwidth,trucklen,imagefile, userid , regno,insurance,rate,,servicecategories,equipment
//        //            url : http://truckcsoft.mapcee.com/auth/truckerProfile
//
//        NSUserDefaults* userStoredetails = [NSUserDefaults standardUserDefaults];
//        NSString * userID = [userStoredetails objectForKey:@"user_id"];
//        NSLog(@"%@",userID);
//
//        NSArray *keys = [[NSArray alloc]initWithObjects:@"trucktype",@"truckheight",@"truckwidth",@"trucklen",@"userid",@"regno",@"insurance",@"rate",@"servicecategories",@"equipment",nil];
//        //
//        NSArray *values =[[NSArray alloc]initWithObjects:_truckType, _heightofTruck, _widthofTruck,_lengthofTruck,userID,_regno,_insurenceValidity,_insurenceno,_serviceCategories,_Equipments, nil];
//
//
//        NSURL *url =[NSURL URLWithString:[NSString stringWithFormat:@"http://quikmov.quikmovapp.com/auth/truckerProfile"]];
//
//        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
//        [request setHTTPMethod:@"POST"];
//        NSString *boundary = @"---------------------------14737809831466499882746641449";
//        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
//        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
//
//        NSMutableData *body = [NSMutableData data];
//        NSData * data = [NSData dataWithData:UIImagePNGRepresentation(_truckregImgs)];
//        //            NSData * data2 = [NSData dataWithData:UIImagePNGRepresentation(truckImg2.image)];
//        //            NSData * data3 = [NSData dataWithData:UIImagePNGRepresentation(truckImg3.image)];
//        //            NSData * data4 = [NSData dataWithData:UIImagePNGRepresentation(truckImg4.image)];
//
//        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"imagefile\"; filename=\"%@userprofile.png\"\r\n",userID] dataUsingEncoding:NSUTF8StringEncoding]];
//        [body appendData:[@"Content-Type: image/png\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
//
//        [body appendData:[NSData dataWithData:data]];
//        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//
//        for (int i=0;i<keys.count;i++) {
//
//            NSLog(@"%@",[keys objectAtIndex:i]);
//            NSString *key = [keys objectAtIndex:i];
//            NSString *value = [values objectAtIndex:i];
//            [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n%@",key, value] dataUsingEncoding:NSUTF8StringEncoding]];
//        }
//
//        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//
//        [request setHTTPBody:body];
//
//        NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
//        NSURLSession* session = [NSURLSession sessionWithConfiguration:config ];
//
//        NSURLSessionUploadTask *uploadtask = [session uploadTaskWithRequest:request fromData:body completionHandler:^(NSData*data, NSURLResponse*response, NSError*error){
//            dispatch_async(dispatch_get_main_queue(), ^{
//                [SVProgressHUD dismiss];
//
//                NSString* str = [[NSString alloc]initWithData:data
//                                                     encoding:NSUTF8StringEncoding];
//
//                NSError* error;
//
//
//
//                NSDictionary* dic = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
//
//                NSLog(@"%@******%@,%@",str,[dic allValues],[dic objectForKey:@"response"]);
//
//                // userId = [[dic objectForKey:@"message"] stringValue];
//                // BOOL Result = [dic objectForKey:[NSString stringWithFormat: @"message"]];
//
//                //             NSLog(@"%hhd",Result);
//
//                if ([[dic objectForKey:@"iserror"]boolValue] == NO){
//
//                    UIAlertController * alert=   [UIAlertController alertControllerWithTitle:@"Message"
//                                                                                     message:@"Truck Registration Successful" preferredStyle:UIAlertControllerStyleAlert];
//
//                    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK"
//                                                                 style:UIAlertActionStyleDefault
//                                                               handler:^(UIAlertAction * action)
//                                         {
//                                             [alert dismissViewControllerAnimated:YES completion:nil];
//
//                                             QuikMovGoogleViewController* mapview = [[QuikMovGoogleViewController alloc]init];
//                                             [self.navigationController pushViewController:mapview animated:YES];
//
//                                         }];
//
//
//                    [alert addAction:ok];
//
//                    [self presentViewController:alert animated:YES completion:nil];
//
//                    //                    [self imageService];
//                }else{
//
//                    [self showAlert:[NSString stringWithFormat:@"%@",[dic objectForKey:@"result"]]];
//                }
//
//            });
//
//
//        }];
//        [uploadtask resume];
//
//
//        //        NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
//        //        if( theConnection )
//        //        {
//        //            _downloadedData = [NSMutableData data] ;
//        //            NSLog(@"request uploading successful");
//        //        }
//        //        else
//        //        {
//        //            NSLog(@"theConnection is NULL");
//        //        }
//
//    }else{
//        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"No internet connectivity" message:@"Please try later" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
//        [alert show];
//    }
//}
//


/*-(void)truckTypeService{
 
 [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
 [SVProgressHUD show];
 
 NSURL* url = [NSURL URLWithString:@"http://quikmov.quikmovapp.com/auth/getTrucktype"];
 NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
 NSURLSession* session = [NSURLSession sessionWithConfiguration:config ];
 
 NSMutableURLRequest*request = [[NSMutableURLRequest alloc]initWithURL:url];
 request.HTTPMethod = @"POST";
 
 NSString* bodycntnt = [NSString stringWithFormat:@"button click=%@",truckType];
 
 NSData *data = [bodycntnt dataUsingEncoding:NSUTF8StringEncoding];
 
 NSURLSessionUploadTask *uploadtask = [session uploadTaskWithRequest:request fromData:data completionHandler:^(NSData*data, NSURLResponse*response, NSError*error){
 
 dispatch_async(dispatch_get_main_queue(), ^{
 [SVProgressHUD dismiss];
 [self serviceSuccess:data];
 });
 }];
 
 [uploadtask resume];
 
 
 }
 
 - (void)serviceSuccess:(NSData *)data{
 if (data == nil) {
 return;
 }
 NSString* str = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
 NSError * error;
 NSDictionary* dic = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
 NSLog(@"%@******%@,%@",str,[dic allValues],[dic objectForKey:@"response"]);
 
 NSString * Result = [dic objectForKey:@"status"];
 
 NSLog(@"%@",Result);
 
 if ([Result isEqualToString:@"success"]){
 NSArray * arr = [dic objectForKey:@"result"];
 
 listofTypesArr = [NSMutableArray new];
 
 for (int i = 0; i < [arr count]; i++) {
 
 //
 GlobalObjects * globleObj = [[GlobalObjects alloc]init];
 NSDictionary * objDic = [arr objectAtIndex:i];
 
 globleObj.truckTypes = [objDic objectForKey:@"trucktype"];
 [listofTypesArr addObject:globleObj];
 [self loadlistView];
 
 }
 }else{
 UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Message" message:[NSString stringWithFormat:@"%@",[dic objectForKey:@"result"]] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
 [alert show];
 }
 
 }
 */



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
