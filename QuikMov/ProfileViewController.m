//
//  ProfileViewController.m
//  QuikMov
//
//  Created by Ajmal Khan on 2/29/16.
//  Copyright © 2016 CSCS. All rights reserved.
//

#import "ProfileViewController.h"
#import "DesignObj.h"
#import "SVProgressHUD.h"
#import "Reachability.h"
#import "GlobalObjects.h"
#import "TruckProfileViewController.h"
#import "DBCameraContainerViewController.h"

@interface ProfileViewController ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate,DBCameraViewControllerDelegate>{
    
    int iphone6Yaxis;
    UIDatePicker*datePicker;
    NSMutableArray* txtfieldarr;
    UITextField * Txt;
    UITextField * fnameTxt;
    UITextField * mnameTxt;
    UITextField * lnameTxt;
    UITextField * emailTxt;
    UITextField * mobNum;
    UITextField * passTxt;
    UITextField * conpassTxt;
    UITextField * commontxt;
    UITextField * dobTxt;
    UITextField * SSN;
    UITextField * streetaddr;
    UITextField * region;
    UITextField * location;
    UITextField * postalcode;
    NSArray* titlearr;
    UIButton* selectimage;
    NSData *imageData;
    UIActivityIndicatorView* indicator;
    NSMutableURLRequest *requeest;
    NSMutableData *_responseData;
    NSString*userId;
    int imageTag;
    NSString* userID;
    
}

@property(nonatomic,strong)UIScrollView* scrView;

@end

@implementation ProfileViewController
@synthesize profileImg;
@synthesize utype;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadProfileService];
    
    
    self.title = @"Profile";
    
    self.navigationController.navigationBarHidden = NO;
    
    //  self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    screenWidth = [UIScreen mainScreen].bounds.size.width;
    screenHeight = [UIScreen mainScreen].bounds.size.height;
    
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [DesignObj quikYellow]}];
    self.navigationController.navigationBar.barTintColor = [DesignObj quikRed];
    
    self.navigationController.navigationBar.tintColor = [DesignObj quikYellow];
    
    UIImageView*bgView = [DesignObj initWithImage:CGRectMake(0, 0, screenWidth, screenHeight) img:@"bg"];
    [self.view addSubview:bgView];
    
    _scrView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, screenHeight )];
    _scrView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:_scrView];
    
    _scrView.contentSize = CGSizeMake(screenWidth, screenHeight+ 500);
    [self initView];
    
}
- (void)initView{
    
    datePicker = [[UIDatePicker alloc]init];
    [datePicker addTarget:self action:@selector(datePickerChanged) forControlEvents:UIControlEventValueChanged];
    datePicker.datePickerMode = UIDatePickerModeDate;
    
    
    int yaxis = 90;
    int y1axis = 90;
    
    selectimage = [DesignObj initWithButton:CGRectMake((screenWidth-80)/2, 70, 80, 80) tittle:nil img:nil];
    [selectimage addTarget:self action:@selector(uploadPic:) forControlEvents:UIControlEventTouchUpInside];
    [_scrView addSubview:selectimage];
    
    //    NSString *documentsPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    //    NSString *path = [documentsPath stringByAppendingPathComponent:@"image.png"];
    //    NSData * imgdata = [NSData dataWithContentsOfFile:path];
    //    if (imgdata!=nil) {
    //        profileImg.image = [UIImage imageWithData:imgdata];
    //    }
    
    imageData =nil;
    
    if ([utype isEqualToString:@"1"]) {
        
        
        txtfieldarr = [[NSMutableArray alloc]init];
        titlearr = [[NSArray alloc]initWithObjects:@"First Name",@"Middle Name",@"Last Name",@"E-mail",@"Mobile Number", @"Password", @"Confirm Password",@"Date of Birth", nil];
        [txtfieldarr removeAllObjects];
        
    }else{
        
        txtfieldarr = [[NSMutableArray alloc]init];
        titlearr = [[NSArray alloc]initWithObjects:@"First Name",@"Middle Name",@"Last Name",@"E-mail",@"Mobile Number",@"Date of Birth",@"SSN", @"Password", @"Confirm Password",@"Street Address",@"Location",@"Region",@"Postal Code", nil];
        [txtfieldarr removeAllObjects];
        
    }
    for (int i = 0; i<titlearr.count; i++) {
        
        UIImageView * txtbgImg = [[UIImageView alloc]initWithFrame:CGRectMake(20, yaxis+iphone6Yaxis+ 100, screenWidth-44, 38+iphone6Yaxis)];
        [txtbgImg setImage:[UIImage imageNamed:@"text_box"]];
        [_scrView addSubview:txtbgImg];
        
        Txt= [[UITextField alloc]initWithFrame:CGRectMake((screenWidth-(screenWidth - 44))/2,  yaxis+iphone6Yaxis +100, screenWidth-44, 38+iphone6Yaxis) ];
        Txt.placeholder = [titlearr objectAtIndex:i];;
        //Txt.layer.cornerRadius =9;
        Txt.delegate = self;
        Txt.textAlignment= NSTextAlignmentCenter;
        Txt.tag = i;
        [Txt setReturnKeyType:UIReturnKeyNext];
        [_scrView addSubview:Txt];
        [txtfieldarr addObject:Txt];
        
        if (i==0||i==1||i==2) {
            
            Txt.autocapitalizationType = UITextAutocapitalizationTypeWords;
            Txt.autocorrectionType = UITextAutocorrectionTypeNo;
            
        }else if (i==3) {
            Txt.keyboardType = UIKeyboardTypeEmailAddress;
            
        }else if (i==4){
            
            Txt.placeholder = [NSString stringWithFormat:@"+91"];//[dictDialingCodes objectForKey:countryCode]];
            // Txt.inputAccessoryView = numberToolbar;
            Txt.keyboardType = UIKeyboardTypeNumberPad;
        }
        else if ((i==5)||(i==6)){
            
            Txt.secureTextEntry = YES;
            
        }else if (i==7){
            
            
            
            //            UIButton* dateBtn = [DesignObj initWithButton:CGRectMake((screenWidth-(screenWidth - 44))/2,  yaxis+iphone6Yaxis +100, screenWidth-44, 38+iphone6Yaxis) tittle:nil img:nil];
            //            [dateBtn addTarget:self action:@selector(datePickerChanged) forControlEvents:UIControlEventTouchUpInside];
            //            [dobTxt addSubview:dateBtn];
            
        }
        y1axis = y1axis + 63 + iphone6Yaxis;
        yaxis = yaxis + 63 + iphone6Yaxis;
        
    }
    
    if ([utype isEqualToString:@"1"]) {
        
        fnameTxt = [txtfieldarr objectAtIndex:0];
        mnameTxt = [txtfieldarr objectAtIndex:1];
        lnameTxt = [txtfieldarr objectAtIndex:2];
        emailTxt= [txtfieldarr objectAtIndex:3];
        mobNum = [txtfieldarr objectAtIndex:4];
        passTxt = [txtfieldarr objectAtIndex:5];
        conpassTxt = [txtfieldarr objectAtIndex:6];
        dobTxt = [txtfieldarr objectAtIndex:7];
    }else{
        
        fnameTxt = [txtfieldarr objectAtIndex:0];
        mnameTxt = [txtfieldarr objectAtIndex:1];
        lnameTxt = [txtfieldarr objectAtIndex:2];
        emailTxt= [txtfieldarr objectAtIndex:3];
        mobNum = [txtfieldarr objectAtIndex:4];
        dobTxt = [txtfieldarr objectAtIndex:5];
        SSN = [txtfieldarr objectAtIndex:6];
        passTxt= [txtfieldarr objectAtIndex:7];
        conpassTxt = [txtfieldarr objectAtIndex:8];
        streetaddr = [txtfieldarr objectAtIndex:9];
        location = [txtfieldarr objectAtIndex:10];
        region = [txtfieldarr objectAtIndex:11];
        postalcode = [txtfieldarr objectAtIndex:12];
        
    }
    
    
    
    
    y1axis = y1axis + 63 + iphone6Yaxis;
    yaxis = yaxis + 63 + iphone6Yaxis;
    
    if ([ utype isEqualToString:@"1"]) {
        
        
        UIButton* regbtn =[DesignObj initWithButton:CGRectMake((screenWidth - 200)/2, dobTxt.frame.origin.y + 38 +30, 200, 40)  tittle:@"Update" img:@"button"];
        [regbtn addTarget:self action:@selector(updateAction:) forControlEvents:UIControlEventTouchUpInside];
        [regbtn setTitleColor:[DesignObj quikYellow] forState:UIControlStateNormal];
        [_scrView addSubview:regbtn];
        
    }else{
        
        UIButton* regbtn =[DesignObj initWithButton:CGRectMake((screenWidth - 200)/2, postalcode.frame.origin.y + 38 +40, 200, 40)  tittle:@"Next" img:@"button"];
        [regbtn addTarget:self action:@selector(nextAction) forControlEvents:UIControlEventTouchUpInside];
        [regbtn setTitleColor:[DesignObj quikYellow] forState:UIControlStateNormal];
        [_scrView addSubview:regbtn];
        
    }
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, screenWidth, 44)];
    numberToolbar.barStyle = UIBarStyleDefault;//UIBarStyleBlackTranslucent;
    numberToolbar.tintColor = [UIColor colorWithRed:207/255.0 green:26/255.0 blue:26/255.0 alpha:1.0];
    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleDone target:self action:@selector(cancelNumberPad)],
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
    [numberToolbar sizeToFit];
    mobNum.inputAccessoryView = numberToolbar;
    dobTxt.inputView = datePicker;
    dobTxt.inputAccessoryView = numberToolbar;
    
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:YES];
    
}

-(BOOL)textFieldShouldReturn:(UITextField*)textField
{
    NSInteger nextTag = textField.tag +1;
    // Try to find next responder
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
        
    } else {
        // Not found, so remove keyboard.
        [textField resignFirstResponder];
    }
    return NO; // We do not want UITextField to insert line-breaks.
}


- (void)datePickerChanged{
    
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMM-dd-yyyy"];
    NSString *strDate = [dateFormatter stringFromDate:datePicker.date];
    dobTxt.text = strDate;
    
}

-(void)showAlert:(NSString*)message{
    UIAlertView * alt = [[UIAlertView alloc]initWithTitle:@"Message" message:message delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alt show];
}

-(void)cancelNumberPad{
    [mobNum resignFirstResponder];
    [datePicker resignFirstResponder];
    [dobTxt resignFirstResponder];
    mobNum.text = @"";
    dobTxt.text = @"";
}

-(void)doneWithNumberPad{
    [mobNum resignFirstResponder];
    [datePicker resignFirstResponder];
    [dobTxt resignFirstResponder];
}

-(IBAction)uploadPic:(id)sender{
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Message"
                                  message:@"Choose options to Upload your Profile image"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* library = [UIAlertAction
                              actionWithTitle:@"From Photo Library"
                              style:UIAlertActionStyleDefault
                              handler:^(UIAlertAction * action)
                              {
                                  [alert dismissViewControllerAnimated:YES completion:nil];
                                  [self selectPhoto];
                                  
                              }];
    
    UIAlertAction* Camera = [UIAlertAction
                             actionWithTitle:@"Take Photo"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 [self takePhoto];
                             }];
    
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    
    [alert addAction:library];
    [alert addAction:Camera];
    [alert addAction:cancel];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    
    
}

- (void)selectPhoto{
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:NULL];
    
    
}

- (void)takePhoto{
    
    DBCameraContainerViewController *cameraContainer = [[DBCameraContainerViewController alloc] initWithDelegate:self];
    [cameraContainer setFullScreenMode];
    
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:cameraContainer];
    [nav setNavigationBarHidden:YES];
    [self presentViewController:nav animated:YES completion:nil];
    
    
}

- (void) camera:(id)cameraViewController didFinishWithImage:(UIImage *)image withMetadata:(NSDictionary *)metadata
{
    //    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    profileImg.image = image;
    imageData = UIImagePNGRepresentation(profileImg.image);
    
    //    DetailViewController *detail = [[DetailViewController alloc] init];
    //    [detail setDetailImage:image];
    //    [self.navigationController pushViewController:detail animated:NO];
    //    [cameraViewController restoreFullScreenMode];
    [self.presentedViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void) dismissCamera:(id)cameraViewController{
    [self dismissViewControllerAnimated:YES completion:nil];
    [cameraViewController restoreFullScreenMode];
}


-(void)loadProfileService{
    
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
    [SVProgressHUD show];
    
    NSURL* url = [NSURL URLWithString:@"http://quikmov.quikmovapp.com/auth/getUserDetails"];
    NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession* session = [NSURLSession sessionWithConfiguration:config ];
    
    NSMutableURLRequest*request = [[NSMutableURLRequest alloc]initWithURL:url];
    request.HTTPMethod = @"POST";
    
    NSUserDefaults* userStoredetails = [NSUserDefaults standardUserDefaults];
    userID = [userStoredetails objectForKey:@"user_id"];
    NSLog(@"%@",userID);
    
    
    NSString* bodycntnt = [NSString stringWithFormat:@"userid=%@",userID];
    
    NSData *data = [bodycntnt dataUsingEncoding:NSUTF8StringEncoding];
    
    NSURLSessionUploadTask *uploadtask = [session uploadTaskWithRequest:request fromData:data completionHandler:^(NSData*data, NSURLResponse*response, NSError*error){
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
            [self serviceSuccess:data];
        });
    }];
    
    [uploadtask resume];
    
    
}

- (void)serviceSuccess:(NSData *)data{
    if (data == nil) {
        return;
    }
    NSString* str = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
    NSError * error;
    NSDictionary* dic = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    NSLog(@"%@******%@,%@",str,[dic allValues],[dic objectForKey:@"response"]);
    
    NSString * Result = [dic objectForKey:@"status"];
    
    NSLog(@"%@",Result);
    if ([Result isEqualToString:@"success"]){
        
        GlobalObjects * globleObj = [[GlobalObjects alloc]init];
        NSDictionary * objDic = [dic objectForKey:@"result"];
      
        
        globleObj.fname = [objDic objectForKey:@"user_name"];
        
        globleObj.mname = [objDic objectForKey:@"mname"];
        
        globleObj.lname = [objDic objectForKey:@"lname"];
        
        globleObj.user_mail = [objDic objectForKey:@"user_email"];
        
        globleObj.phno = [objDic objectForKey:@"user_phone"];
        
        globleObj.passrd = [objDic objectForKey:@"user_password"];
        
        globleObj.cpassrd = [objDic objectForKey:@"user_password"];
        
        globleObj.truckWidth = [objDic objectForKey:@"truckwidth"];
        
        globleObj.truckHeight = [objDic objectForKey:@"truckheight"];
        
        globleObj.truckLength = [objDic objectForKey:@"trucklen"];
        
        globleObj.truckType = [objDic objectForKey:@"trucktype"];
        
        globleObj.sizeOfTruck = [objDic objectForKey:@"sizeoftruck"];
        
        globleObj.raTe = [objDic objectForKey:@"rate"];
        
        globleObj.regNo = [objDic objectForKey:@"regno"];
        
        globleObj.serviceCatogories = [objDic objectForKey:@"servicecategories"];
        
        globleObj.equip = [objDic objectForKey:@"equipment"];
        
        globleObj.insurance = [objDic objectForKey:@"insurance"];
        
        globleObj.truckImag = [objDic objectForKey:[NSString stringWithFormat:@"http://quikmov.quikmovapp.com%@",[objDic objectForKey:@"filepath"]]];
        
        globleObj.userPhotoUrl = [NSString stringWithFormat:@"http://quikmov.quikmovapp.com%@",[objDic objectForKey:@"userfilepath"]];
        
        globleObj.dob = [objDic objectForKey:@"dob"];
        
        globleObj.currentUsertype = [objDic objectForKey:@"usertype"];
        
        globleObj.currentUsertype = utype;
        
        profileImg = [DesignObj initWithImage:CGRectMake((screenWidth-80)/2, 70, 80, 80) img:@""];
        dispatch_async(dispatch_get_main_queue(), ^{
            
            //            [profileImg sd_setImageWithPreviousCachedImageWithURL:[NSURL URLWithString:globleObj.userPhotoUrl] placeholderImage:[UIImage imageNamed:@"add_profile_image.png"] options:SDWebImageAvoidAutoSetImage progress:0 completed:nil];
            [[SDImageCache sharedImageCache] removeImageForKey:globleObj.userPhotoUrl fromDisk:YES];
            [profileImg sd_setImageWithURL:[NSURL URLWithString:globleObj.userPhotoUrl] placeholderImage:[UIImage imageNamed:@"add_profile_image.png"]];
            
            //            //[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:object.userPhotoUrl]]];
        });
        [_scrView addSubview:profileImg];
        
        fnameTxt.text = globleObj.fname;
        mnameTxt.text = globleObj.mname;
        lnameTxt.text = globleObj.lname;
        emailTxt.text = globleObj.user_mail;
        mobNum.text = globleObj.phno;
        passTxt.text = globleObj.passrd;
        conpassTxt.text = globleObj.cpassrd;
        dobTxt.text = globleObj.dob;
        
        
        //            globleObj.userId = [dic objectForKey:@"user_id"];
    }
    
    
}

-(void)nextAction{
    
    if (fnameTxt.text.length==0) {
        [self showAlert:@"Please enter your First Name"];
    }else if (lnameTxt.text.length==0) {
        [self showAlert:@"Please enter your Last Name"];
    }else if (emailTxt.text.length==0) {
        [self showAlert:@"Please enter email"];
    }else if (![self validateEmail:emailTxt.text]) {
        [self showAlert:@"Please enter valid email"];
    }else if (mobNum.text.length==0){
        [self showAlert:@"Please enter the Mobile number"];
    }else if (mobNum.text.length<10){
        [self showAlert:@"Please enter the correct mobile number"];
    }else if (passTxt.text.length==0){
        [self showAlert:@"Please enter password"];
    }else if (passTxt.text.length<6){
        [self showAlert:@"Password atleast 6 characters"];
    }else if (conpassTxt.text.length==0){
        [self showAlert:@"Please enter Confirm password"];
    }else if (![passTxt.text isEqualToString:conpassTxt.text]){
        [self showAlert:@"Your password does not match"];
    }else if (dobTxt.text.length==0){
        [self showAlert:@"Please enter your Date of Birth"];
    }else{
        
        //        TruckProfileViewController * nextTruckView = [[TruckProfileViewController alloc]init];
        //        nextTruckView.truckfname = fnameTxt.text;
        //        nextTruckView.truckmname = mnameTxt.text;
        //        nextTruckView.trucklname = lnameTxt.text;
        //        nextTruckView.truckemail = emailTxt.text;
        //        nextTruckView.trucknumber = mobNum.text;
        //        nextTruckView.truckpass = passTxt.text;
        //        nextTruckView.truckcpass = conpassTxt.text;
        //        nextTruckView.truckdateob = dobTxt.text;
        //        nextTruckView.truckregImg = profileImg.image;
        //        nextTruckView.truckerUsetyp = utype;
        //
        //        [self.navigationController pushViewController:nextTruckView animated:YES];
        
        [self updateProfile];
    }
    
    
    
    
    
}

-(void)updateProfile{
    
//    NSArray *values =[[NSArray alloc]initWithObjects:truckfname, truckmname, trucklname, truckemail, trucknumber,truckpass,truckcpass,truckdateob,truckerUsetyp, nil];
    
    TruckProfileViewController * nextTruckView = [[TruckProfileViewController alloc]init];
    nextTruckView.truckfname = fnameTxt.text;
    nextTruckView.truckmname = mnameTxt.text;
    nextTruckView.trucklname = lnameTxt.text;
    nextTruckView.truckemail = emailTxt.text;
    nextTruckView.trucknumber = mobNum.text;
    nextTruckView.truckpass = passTxt.text;
    nextTruckView.truckcpass = conpassTxt.text;
    nextTruckView.truckregImg = profileImg.image;
    [self.navigationController pushViewController:nextTruckView animated:YES];
    
    
}

-(IBAction)updateAction:(id)sender{
    
    if ([Reachability reachabilityForInternetConnection]) {
        
        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
        [SVProgressHUD show];
        
        //        http://quikmov.quikmovapp.com/auth/editRegister
        //userid,imagefile,name,mname,lname mobile, email , password,cpassword
        
        NSArray *keys = [[NSArray alloc]initWithObjects:@"userid",@"name",@"mname",@"lname",@"mobile",@"email",@"password",@"cpassword",nil];
        //
        NSArray *values =[[NSArray alloc]initWithObjects:userID,fnameTxt.text, mnameTxt.text, lnameTxt.text, mobNum.text, emailTxt.text, passTxt.text,conpassTxt.text, nil];
        
        NSURL *url =[NSURL URLWithString:[NSString stringWithFormat:@"http://quikmov.quikmovapp.com/auth/editRegister"]];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        [request setHTTPMethod:@"POST"];
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        NSMutableData *body = [NSMutableData data];
        NSData * data = [NSData dataWithData:UIImagePNGRepresentation(profileImg.image)];
        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"imagefile\"; filename=\"%@userprofile.png\"\r\n",fnameTxt.text] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: image/png\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[NSData dataWithData:data]];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        for (int i=0;i<keys.count;i++) {
            
            NSLog(@"%@",[keys objectAtIndex:i]);
            NSString *key = [keys objectAtIndex:i];
            NSString *value = [values objectAtIndex:i];
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n%@",key, value] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [request setHTTPBody:body];
        
        NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession* session = [NSURLSession sessionWithConfiguration:config ];
        
        
        NSURLSessionUploadTask *uploadtask = [session uploadTaskWithRequest:request fromData:body completionHandler:^(NSData*data, NSURLResponse*response, NSError*error){
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                
                NSString* str = [[NSString alloc]initWithData:data
                                                     encoding:NSUTF8StringEncoding];
                
                NSError* error;
                
                if (str.length == 0) {
                    
                    [self showAlert:@" Server Error"];
                }
                
                
                NSDictionary* dic = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                
                NSLog(@"%@******%@,%@",str,[dic allValues],[dic objectForKey:@"response"]);
                
                // userId = [[dic objectForKey:@"message"] stringValue];
                NSString * Result = [dic objectForKey:@"status"];
                
                //                NSDictionary * result = [dic objectForKey:@"result"];
                
                NSLog(@"%@",Result);
                
                if ([Result isEqualToString:@"success"]){
                    
                    UIAlertController * alert=   [UIAlertController alertControllerWithTitle:@"Message"
                                                                                     message:@"Updated Successfully" preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK"
                                                                 style:UIAlertActionStyleDefault
                                                               handler:^(UIAlertAction * action)
                                         {
                                             [alert dismissViewControllerAnimated:YES completion:nil];
                                             //                                                 [self loadProfileService];
                                             
                                         }];
                    
                    
                    [alert addAction:ok];
                    
                    [self presentViewController:alert animated:YES completion:nil];
                }else{
                    
                    [self showAlert:[NSString stringWithFormat:@"%@",[dic objectForKey:@"result"]]];
                }
                
            });
            
            
        }];
        [uploadtask resume];
        
    }else{
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"No internet connectivity" message:@"Please try later" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    
}

-(BOOL)validateEmail:(NSString *)emailStr {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:emailStr];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    //    SDImageCache* imagecache = [SDImageCache sharedImageCache];
    //    [imagecache cleanDisk];
    //    [imagecache clearMemory];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
