//
//  DesignObj.m
//  QuikMov
//
//  Created by CSCS on 1/21/16.
//  Copyright © 2016 CSCS. All rights reserved.
//

#import "DesignObj.h"

@implementation DesignObj



+(UILabel *)initWithLabel:(CGRect)Frame title:(NSString*)title font:(CGFloat)fontsize txtcolor:(UIColor*)textcolor{
    
    UILabel*lbl = [[UILabel alloc]initWithFrame:Frame];
    lbl.text = title;
    lbl.font = [UIFont systemFontOfSize:fontsize];
    lbl.textColor = textcolor;
    lbl.backgroundColor = [UIColor clearColor];
    lbl.textAlignment = NSTextAlignmentCenter;
    
    return lbl;
    
}

+(UITableView *)initWithTableView:(id)delegate frame:(CGRect)Frame{
    
    UITableView* tblview = [[UITableView alloc]initWithFrame:Frame];
    tblview.delegate = delegate;
    
    return tblview;

}

+(UIImageView*)initWithImage:(CGRect)Frame img:(NSString*)image{
    
    UIImageView*imgView = [[UIImageView alloc]initWithFrame:Frame];
    imgView.image = [UIImage imageNamed:image];
    
    return imgView;
    
    
}

+(UITextField *)initWithTextfield:(CGRect)Frame placeholder:(NSString*)placeholder tittle:(NSString*)tittle delegate:(id)delegate font:(CGFloat)fontsize
{
    UITextField* txtFeild = [[UITextField alloc]initWithFrame:Frame];
    txtFeild.placeholder=tittle;
    txtFeild.textColor = [UIColor blackColor];
    txtFeild.font = [UIFont systemFontOfSize:fontsize];
    txtFeild.backgroundColor = [UIColor clearColor];
    txtFeild.keyboardType = UIKeyboardTypeDefault;
    txtFeild.keyboardAppearance = UIKeyboardAppearanceAlert;
    txtFeild.returnKeyType = UIReturnKeyDone;
    txtFeild.clearButtonMode = UITextFieldViewModeWhileEditing;
    txtFeild.autocapitalizationType = UITextAutocapitalizationTypeNone;
    txtFeild.delegate = delegate;
    txtFeild.textAlignment = NSTextAlignmentLeft;
    
    return txtFeild;
    
}

+ (UIButton *)initWithButton:(CGRect)Frame tittle:(NSString*)tittle img:(NSString*)img{
    
    UIButton* btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = Frame;
    [btn setTitle:tittle forState:UIControlStateNormal];
    [btn setBackgroundImage:[UIImage imageNamed:img] forState:UIControlStateNormal];
  
    return btn;
 
    
}

+(UIColor*)quikRed{
    
    UIColor *colr=[UIColor colorWithRed:237/255.0 green:28/255.0 blue:36/255.0 alpha:1.0];
    return colr;
}

+(UIColor*)quikYellow{
    
    UIColor *colr=[UIColor colorWithRed:247/255.0 green:220/255.0 blue:17/255.0 alpha:1.0];
    return colr;
}


@end
