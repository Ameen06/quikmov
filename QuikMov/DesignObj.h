//
//  DesignObj.h
//  QuikMov
//
//  Created by CSCS on 1/21/16.
//  Copyright © 2016 CSCS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface DesignObj : NSObject

+(UILabel *)initWithLabel:(CGRect)Frame title:(NSString*)title font:(CGFloat)fontsize txtcolor:(UIColor*)textcolor;
+(UIButton *)initWithButton:(CGRect)Frame tittle:(NSString*)tittle img:(NSString*)img;
+(UITableView *)initWithTableView:(id)delegate frame:(CGRect)Frame;
+(UITextField *)initWithTextfield:(CGRect)Frame placeholder:(NSString*)placeholder tittle:(NSString*)tittle delegate:(id)delegate font:(CGFloat)fontsize;
+(UIImageView*)initWithImage:(CGRect)Frame img:(NSString*)image;
+(UIColor*)quikRed;
+(UIColor*)quikYellow;

@end
