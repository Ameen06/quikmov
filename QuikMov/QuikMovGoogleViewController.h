//
//  QuikMovGoogleViewController.h
//  QuikMov
//
//  Created by CSCS on 2/24/16.
//  Copyright © 2016 CSCS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <QuartzCore/QuartzCore.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "QuickMenuViewController.h"
#import "UIImageView+WebCache.h"
#import "MHPresenterImageView.h"
#import <MessageUI/MessageUI.h>
#import <GoogleMaps/GoogleMaps.h>

@import GoogleMaps;


#define SHAWDOW_ALPHA 0.5
#define MENU_DURATION 0.3
#define MENU_TRIGGER_VELOCITY 350

@interface QuikMovGoogleViewController : UIViewController<CLLocationManagerDelegate,GMSMapViewDelegate,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,UIGestureRecognizerDelegate,menuDelegate,MFMessageComposeViewControllerDelegate>{
    
}
@property(nonatomic, strong) NSString* userType;

// Async Images

@property (nonatomic, strong) NSURL * imageURL;
@property (nonatomic, assign) BOOL showActivityIndicator;
@property (nonatomic, assign) UIActivityIndicatorViewStyle activityIndicatorStyle;
@property (nonatomic, strong) NSCache *cache;

@end
