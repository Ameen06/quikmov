//
//  GlobalObjects.m
//  QuikMov
//
//  Created by CSCS on 1/25/16.
//  Copyright © 2016 CSCS. All rights reserved.
//

#import "GlobalObjects.h"

@implementation GlobalObjects
@synthesize currentUsertype;
@synthesize latitudee;
@synthesize longitudee;
@synthesize usermailid;
@synthesize username;
@synthesize userPhone;
@synthesize userDistance;
@synthesize truckTypes;
@synthesize userStatus;
@synthesize userId;
@synthesize bookName;
@synthesize bookImageurl;
@synthesize bookingStatus;
@synthesize pickupLat;
@synthesize pickupLong;
@synthesize description;
@synthesize deliveryLat;
@synthesize deliveryLong;
@synthesize shipmentType;
@synthesize truckerId;
@synthesize updatedOn;
@synthesize bookingId;
@synthesize truckTypesid;
@synthesize truckTypesparentid;
@synthesize subtruckTypes;


//Profile Users
@synthesize fname;
@synthesize lname;
@synthesize mname;
@synthesize user_mail;
@synthesize phno;
@synthesize dob;
@synthesize passrd;
@synthesize cpassrd;

//Truck Profile

@synthesize truckWidth;
@synthesize truckHeight;
@synthesize truckLength;
@synthesize truckType;
@synthesize raTe;
@synthesize insurance;
@synthesize sizeOfTruck;
@synthesize equip;
@synthesize serviceCatogories;
@synthesize regNo;
@synthesize truckImag;

@synthesize truckPhotos;


@end
