//
//  QuickMovOTPViewController.m
//  QuikMov
//
//  Created by Ajmal Khan on 3/4/16.
//  Copyright © 2016 CSCS. All rights reserved.
//

#import "QuickMovOTPViewController.h"
#import "QuikMovGoogleViewController.h"
#import "VIewBookingViewController.h"
#import "DesignObj.h"
#import "SVProgressHUD.h"
#import "Reachability.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"


#define MIN_UPLOAD_RESOLUTION 320 * 480
#define MAX_UPLOAD_SIZE 1124000


@interface QuickMovOTPViewController ()<UITextFieldDelegate>{
    
    
    UILabel* headingLbl;
    UILabel* received;
    UIButton* checkOTP;
    NSString* regUserid;
    
    UITextField* verification;
    NSString* userId;
    int count;
    NSString * reSendOtp;
}

@end

@implementation QuickMovOTPViewController
@synthesize fname;
@synthesize mname;
@synthesize lname;
@synthesize email;
@synthesize pass;
@synthesize conpass;
@synthesize dob;
@synthesize regType;
@synthesize mobNum;
@synthesize profImag;
@synthesize truckfname1;
@synthesize truckmname1;
@synthesize trucklname1;
@synthesize truckemail1;
@synthesize trucknumber1;
@synthesize truckpass1;
@synthesize truckcpass1;
@synthesize truckdateob1;
@synthesize truckregImgs1;
@synthesize truckregImgs2;
@synthesize truckregImgs3;
@synthesize truckregImgs4;
@synthesize truckerUsetyp1;
@synthesize truckregImg;
@synthesize copiOTP;
@synthesize mobilenumbr;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"QuikMov";
    
    self.navigationController.navigationBarHidden = NO;
    
    self.navigationController.navigationBar.tintColor = [DesignObj quikYellow];
    
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [DesignObj quikYellow]}];
    self.navigationController.navigationBar.barTintColor = [DesignObj quikRed];
    
    UIImageView*bgView = [DesignObj initWithImage:CGRectMake(0, 0, screenWidth, screenHeight) img:@"bg.png"];
    [self.view addSubview:bgView];
    
    _scrView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, screenHeight)];
    [self.view addSubview:_scrView];
    
    // Do any additional setup after loading the view.
    
    
    headingLbl = [DesignObj initWithLabel:CGRectMake((screenWidth-200)/2, 64+ 40, 200, 30) title:@"One Time Password" font:16 txtcolor:[DesignObj quikRed]];
    headingLbl.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:headingLbl];
    
    verification = [DesignObj initWithTextfield:CGRectMake((screenWidth -250)/2, headingLbl.frame.origin.y+30+ 30, 250, 40) placeholder:@"" tittle:@"Enter the verification code" delegate:self font:16];
    [verification setBackground:[UIImage imageNamed:@"text_box.png"]];
    verification.textAlignment = NSTextAlignmentCenter;
    verification.keyboardType = UIKeyboardTypeNumberPad;
    [self.view addSubview:verification];
    
    received = [DesignObj initWithLabel:CGRectMake((screenWidth-250)/2, verification.frame.origin.y+40+10, 250, 30) title:@"(received on your mobile)" font:16 txtcolor:[DesignObj quikRed]];
    received.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:received];
    
    checkOTP = [DesignObj initWithButton:CGRectMake((screenWidth - 200)/2,received.frame.origin.y + 30 + 40, 200, 40) tittle:@"Next" img:@"button.png"];
    [checkOTP addTarget:self
                 action:@selector(checkOtp:) forControlEvents:UIControlEventTouchUpInside];
    [checkOTP setTitleColor:[DesignObj quikYellow] forState:UIControlStateNormal];
    [self.view addSubview:checkOTP];
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, screenWidth, 44)];
    numberToolbar.barStyle = UIBarStyleDefault;//UIBarStyleBlackTranslucent;
    numberToolbar.tintColor = [UIColor colorWithRed:207/255.0 green:26/255.0 blue:26/255.0 alpha:1.0];
    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleDone target:self action:@selector(cancelNumberPad)],
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
    [numberToolbar sizeToFit];
    
    verification.inputAccessoryView = numberToolbar;
    
}

-(IBAction)checkOtp:(id)sender{
    
    [verification resignFirstResponder];

    if (verification.text.length == 0) {
        [self showAlert:@"Please enter your received OTP"];
    }else{
        
        [self otpService];
    }
    
}
-(void)cancelNumberPad{
    [verification resignFirstResponder];
 
    verification.text = @"";
}

-(void)doneWithNumberPad{
    [verification resignFirstResponder];
  }


-(void)otpService{
    
//    UserDetailsObj* object = [[UserDetailsObj alloc]init];
   
    if([regType isEqualToString:@"1"]){
        
//        [self CustomerupdateUserProfilePic];
        [self customerotpcheck];
    }
    else {
        [self truckerotpcheck];
    }
}


-(void)customerotpcheck{
    
    UIView* hudView = [[UIView alloc]initWithFrame:CGRectMake((screenWidth-100)/2, (screenHeight-350)/2, 100, 350)];
    hudView.backgroundColor = [UIColor blackColor];
    
    MBProgressHUD* hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = @"Loading...";
    hud.labelColor = [DesignObj quikYellow];
    hud.customView = hudView;
    hud.animationType = MBProgressHUDAnimationZoomIn;
    hud.color = [DesignObj quikRed];
    hud.alpha = 0.8;
    
    NSURL *url = [NSURL URLWithString:@"http://quikmov.quikmovapp.com/auth/CheckOtp"];
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    // 2
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    request.HTTPMethod = @"POST";
    
    // 3
    
    NSString * bodyMsg = [NSString stringWithFormat:@"mobile=%@&otp=%@",mobilenumbr,verification.text];
    
    
    
    NSData * data = [bodyMsg dataUsingEncoding:NSUTF8StringEncoding];
    
    // 4
    NSURLSessionUploadTask *uploadTask = [session uploadTaskWithRequest:request
                                                               fromData:data completionHandler:^(NSData *data,NSURLResponse *response,NSError *error) {
                                                                   // Handle response here
                                                                   dispatch_async(dispatch_get_main_queue(), ^{
                                                                       //                                                                       [SVProgressHUD dismiss];
             [MBProgressHUD hideHUDForView:self.view animated:YES];
             [self customerotpSuccess:data];
                                                                   });
                                                                   
                                                               }];
    
    // 5
    [uploadTask resume];
    
}
- (void)customerotpSuccess:(NSData *)data{
    
    NSString * newStr = [[NSString alloc] initWithData:data                                                                                                                 encoding:NSUTF8StringEncoding];
    
    NSLog(@"%@",newStr);
    
    // NSError* error;
    
    NSString* str = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
    
    if (str.length == 0) {
        [self showAlert:@"Server error"];
        return;
    }
    NSError * error;
    NSDictionary* dic = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    
    NSLog(@"%@******%@,%@",str,[dic allValues],[dic objectForKey:@"status"]);
    
    NSString * Result = [dic objectForKey:@"status"];
    
    NSLog(@"%@",Result);
    
    if ([Result isEqualToString:@"success"]){

        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@"Message"
                                      message:[NSString stringWithFormat:@"Registration Successful"]
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 QuikMovGoogleViewController* nextView = [[QuikMovGoogleViewController alloc]init];
                                 [self.navigationController pushViewController:nextView  animated:YES];
                                 
                                 
                             }];
        
        
        [alert addAction:ok];
        
        [self presentViewController:alert animated:YES completion:nil];
        

        
    }else{
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@"Message"
                                      message:[NSString stringWithFormat:@"Registration Successful"]
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 QuikMovGoogleViewController* nextView = [[QuikMovGoogleViewController alloc]init];
                                 [self.navigationController pushViewController:nextView  animated:YES];
//                                 [self userdetailsOTP];
                                 
                                 
                             }];
        
        
        [alert addAction:ok];
        
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    
}


-(void)truckerotpcheck{
    
    UIView* hudView = [[UIView alloc]initWithFrame:CGRectMake((screenWidth-100)/2, (screenHeight-350)/2, 100, 350)];
    hudView.backgroundColor = [UIColor blackColor];
    
    MBProgressHUD* hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = @"Loading...";
    hud.labelColor = [DesignObj quikYellow];
    hud.customView = hudView;
    hud.animationType = MBProgressHUDAnimationZoomIn;
    hud.color = [DesignObj quikRed];
    hud.alpha = 0.8;
    
    NSURL *url = [NSURL URLWithString:@"http://quikmov.quikmovapp.com/auth/CheckOtp"];
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    // 2
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    request.HTTPMethod = @"POST";
    
    // 3
    
    NSString * bodyMsg = [NSString stringWithFormat:@"mobile=%@&otp=%@",mobilenumbr,verification.text];
    
    
    
    NSData * data = [bodyMsg dataUsingEncoding:NSUTF8StringEncoding];
    
    // 4
    NSURLSessionUploadTask *uploadTask = [session uploadTaskWithRequest:request
                                                               fromData:data completionHandler:^(NSData *data,NSURLResponse *response,NSError *error) {
                                                                   // Handle response here
                                                                   dispatch_async(dispatch_get_main_queue(), ^{
                                                                       //                                                                       [SVProgressHUD dismiss];
                                                                       [MBProgressHUD hideHUDForView:self.view animated:YES];
                                                            [self truckotpSuccess:data];
                                                                   });
                                                                   
                                                               }];
    
    // 5
    [uploadTask resume];
    
}
- (void)truckotpSuccess:(NSData *)data{
    
    NSString * newStr = [[NSString alloc] initWithData:data                                                                                                                 encoding:NSUTF8StringEncoding];
    
    NSLog(@"%@",newStr);
    
    // NSError* error;
    
    NSString* str = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
    
    if (str.length == 0) {
        [self showAlert:@"Server error"];
        return;
    }
    NSError * error;
    NSDictionary* dic = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    
    NSLog(@"%@******%@,%@",str,[dic allValues],[dic objectForKey:@"status"]);
    
    NSString * Result = [dic objectForKey:@"status"];
    
    NSLog(@"%@",Result);
    
    if ([Result isEqualToString:@"success"]){
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@"Message"
                                      message:[NSString stringWithFormat:@"Registration Successful"]
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 VIewBookingViewController* nextView = [[VIewBookingViewController alloc]init];
                                 [self.navigationController pushViewController:nextView  animated:YES];
                                 
                                 
                             }];
        
        
        [alert addAction:ok];
        
        [self presentViewController:alert animated:YES completion:nil];
        
        
        
    }else{
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@"Message"
                                      message:[NSString stringWithFormat:@"Registration Successful"]
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 VIewBookingViewController* nextView = [[VIewBookingViewController alloc]init];
                                 [self.navigationController pushViewController:nextView  animated:YES];
                                 //                                 [self userdetailsOTP];
                                 
                                 
                             }];
        
        
        [alert addAction:ok];
        
        [self presentViewController:alert animated:YES completion:nil];
        
    }
}

-(void)userdetailsOTP{
    
    if ([Reachability reachabilityForInternetConnection]) {
        //        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
        //        [SVProgressHUD show];
        
        
        MBProgressHUD* hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.mode = MBProgressHUDModeIndeterminate;
        hud.labelText = @"Retrieving OTP...";
        hud.labelColor = [DesignObj quikYellow];
        hud.animationType = MBProgressHUDAnimationZoomIn;
        hud.color = [DesignObj quikRed];
        hud.alpha = 0.8;
        
        NSURL* url = [NSURL URLWithString:@"http://quikmov.quikmovapp.com/auth/getUserDetails"];
        
        NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession* session = [NSURLSession sessionWithConfiguration:config ];
        
        NSMutableURLRequest*request = [[NSMutableURLRequest alloc]initWithURL:url];
        request.HTTPMethod = @"POST";
        
        
            NSUserDefaults* userStoredetails = [NSUserDefaults standardUserDefaults];
            NSString * userID = [userStoredetails objectForKey:@"user_id"];
            NSLog(@"%@",userID);
        
        NSString* bodycntnt = [NSString stringWithFormat:@"userid=%@",userID];
        
        NSData *data = [bodycntnt dataUsingEncoding:NSUTF8StringEncoding];
        
        
        NSURLSessionUploadTask *uploadtask = [session uploadTaskWithRequest:request fromData:data completionHandler:^(NSData*data, NSURLResponse*response, NSError*error){
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                
                NSString* str = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
                NSError* error;
                
                NSString * newStr = [[NSString alloc] initWithData:data                                                                                                                 encoding:NSUTF8StringEncoding];
                
                NSLog(@"%@",newStr);
                
                // NSError* error;
                
                if (str.length == 0) {
                    [self showAlert:@"Server error"];
                    return;
                }
                NSDictionary* dic = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                
                NSLog(@"%@******%@,%@",str,[dic allValues],[dic objectForKey:@"status"]);
                
                //  NSDictionary * result = [dic objectForKey:@"result"];
                
                NSString * Result = [dic objectForKey:@"status"];
                
                NSDictionary* result = [dic objectForKey:@"result"];
                
                reSendOtp = [result objectForKey:@"otp"];
                
                NSLog(@"%@",Result);
                
                if ([Result isEqualToString:@"success"]){
                    
                    
                    UIAlertController * alert=   [UIAlertController
                                                  alertControllerWithTitle:@"Message"
                                                  message:[NSString stringWithFormat:@"Your OTP\n%@",reSendOtp]
                                                  preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* ok = [UIAlertAction
                                         actionWithTitle:@"OK"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action)
                                         {
                                             
                                             [alert dismissViewControllerAnimated:YES completion:nil];
                                             
                                         }];
                    
                    
                    [alert addAction:ok];
                    
                    [self presentViewController:alert animated:YES completion:nil];
                    
                    
                }else{
                    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Message" message:[NSString stringWithFormat:@"%@",[dic objectForKey:@"result"]] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                    [alert show];
                }
                
            });
        }];
        [uploadtask resume];
    }else{
        [self showAlert:@"No internet connectivity"];
    }
    

    
}

-(void)truckupdateUserProfilePic{
    
    if ([Reachability reachabilityForInternetConnection]) {
        
//        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
//        [SVProgressHUD show];
        
        MBProgressHUD* hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.mode = MBProgressHUDModeIndeterminate;
        hud.labelText = @"Loading...";
        hud.labelColor = [DesignObj quikYellow];
        hud.animationType = MBProgressHUDAnimationZoomIn;
        hud.color = [DesignObj quikRed];
        hud.alpha = 0.8;
        
//        UserDetailsObj* userdetailObj = [[UserDetailsObj alloc]init];
        
        
        NSArray *keys = [[NSArray alloc]initWithObjects:@"name",@"mname",@"lname",@"email",@"mobile",@"password",@"cpassword",@"dob",@"usertype",nil];

        NSArray *values = [[NSArray alloc]initWithObjects:truckfname1, truckmname1, trucklname1, truckemail1, trucknumber1,truckpass1,truckcpass1,truckdateob1,truckerUsetyp1, nil];
        
        
        NSURL *url =[NSURL URLWithString:[NSString stringWithFormat:@"http://quikmov.quikmovapp.com/auth/signup"]];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        [request setHTTPMethod:@"POST"];
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        NSMutableData *body = [NSMutableData data];
        
        UIImage* image =truckregImg;
        
        if(!(image.imageOrientation == UIImageOrientationUp ||
             image.imageOrientation == UIImageOrientationUpMirrored))
        {
            CGSize imgsize = image.size;
            UIGraphicsBeginImageContext(imgsize);
            [image drawInRect:CGRectMake(0.0, 0.0, imgsize.width, imgsize.height)];
            image = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
        }
        
        NSData * data = [NSData dataWithData:UIImagePNGRepresentation(image)];
        //        11700.6005859
        //        float factor;
        //        float resol = image.size.height*image.size.width;
        //        if (resol >MIN_UPLOAD_RESOLUTION){
        //            factor = sqrt(resol/MIN_UPLOAD_RESOLUTION)*2;
        //            image = [self scaleDown:image withSize:CGSizeMake(image.size.width/factor, image.size.height/factor)];
        //        }
        
        //Compress the image
        CGFloat compression = 0.9f;
        CGFloat maxCompression = 0.1f;
        
        while ([data length] > MAX_UPLOAD_SIZE && compression > maxCompression)
        {
            compression -= 0.10;
            data = UIImageJPEGRepresentation(image, compression);
            NSLog(@"Compress : %lu",(unsigned long)data.length);
        }
        
//        NSData * data = [NSData dataWithData:UIImagePNGRepresentation(truckregImg)];
        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"imagefile\"; filename=\"%@userprofile.png\"\r\n",truckfname1] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: image/png\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[NSData dataWithData:data]];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        for (int i=0;i<keys.count;i++) {
            
            NSLog(@"%@",[keys objectAtIndex:i]);
            NSString *key = [keys objectAtIndex:i];
            NSString *value = [values objectAtIndex:i];
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n%@",key, value] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [request setHTTPBody:body];
        
        NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession* session = [NSURLSession sessionWithConfiguration:config ];
        
        
        NSURLSessionUploadTask *uploadtask = [session uploadTaskWithRequest:request fromData:body completionHandler:^(NSData*data, NSURLResponse*response, NSError*error){
            dispatch_async(dispatch_get_main_queue(), ^{
//                [SVProgressHUD dismiss];
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                
                NSString* str = [[NSString alloc]initWithData:data
                                                     encoding:NSUTF8StringEncoding];
                
                NSError* error;
                
                
                if (str.length == 0) {
                    [self showAlert:@"Server error"];
                    return;
                }
                
                NSDictionary* dic = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                
                NSLog(@"%@******%@,%@",str,[dic allValues],[dic objectForKey:@"response"]);
                
                // userId = [[dic objectForKey:@"message"] stringValue];
                NSString * Result = [dic objectForKey:@"status"];
                
                NSDictionary * result = [dic objectForKey:@"result"];
                
                if ([Result isEqualToString:@"success"]){
                    
                    regUserid = [result objectForKey:@"user_id"];
                    NSLog(@"%@",regUserid);
                    
                    NSUserDefaults* userStoredetails = [NSUserDefaults standardUserDefaults];
                    [userStoredetails setObject:regUserid forKey:@"user_id"];
                    [userStoredetails synchronize];
                    
                        [self truckregService];

                    }else{
                        
                        [self showAlert:[NSString stringWithFormat:@"%@",result]];
                    }
                
            });
        }];
        
        [uploadtask resume];
        
    }else{
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"No internet connectivity" message:@"Please try later" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
}

-(void)truckregService{
    
    if ([Reachability reachabilityForInternetConnection]) {
        
//        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
//        [SVProgressHUD show];
        
        MBProgressHUD* hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.mode = MBProgressHUDModeIndeterminate;
        hud.labelText = @"Loading...";
        hud.labelColor = [DesignObj quikYellow];
        hud.animationType = MBProgressHUDAnimationZoomIn;
        hud.color = [DesignObj quikRed];
        hud.alpha = 0.8;
        
        //            inputs :trucktype,truckheight,truckwidth,trucklen,imagefile, userid , regno,insurance,rate,,servicecategories,equipment
        //            url : http://truckcsoft.mapcee.com/auth/truckerProfile
        
        NSUserDefaults* userStoredetails = [NSUserDefaults standardUserDefaults];
        NSString * userID = [userStoredetails objectForKey:@"user_id"];
        NSLog(@"%@",userID);
        
        NSArray *keys = [[NSArray alloc]initWithObjects:@"trucktype",@"truckheight",@"truckwidth",@"trucklen",@"userid",@"regno",@"insurance",@"rate",@"servicecategories",@"equipment",nil];
        //
        NSArray *values =[[NSArray alloc]initWithObjects:_truckType, _heightofTruck, _widthofTruck,_lengthofTruck,userID,_regno,_insurenceValidity,_insurenceno,_serviceCategories,_Equipments, nil];
        
        
        NSURL *url =[NSURL URLWithString:[NSString stringWithFormat:@"http://quikmov.quikmovapp.com/auth/truckerProfile"]];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        [request setHTTPMethod:@"POST"];
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        NSMutableData *body = [NSMutableData data];
        NSData * data = [NSData dataWithData:UIImagePNGRepresentation(truckregImg)];
        //            NSData * data2 = [NSData dataWithData:UIImagePNGRepresentation(truckImg2.image)];
        //            NSData * data3 = [NSData dataWithData:UIImagePNGRepresentation(truckImg3.image)];
        //            NSData * data4 = [NSData dataWithData:UIImagePNGRepresentation(truckImg4.image)];
        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"imagefile\"; filename=\"%@userprofile.png\"\r\n",userID] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: image/png\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[NSData dataWithData:data]];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        for (int i=0;i<keys.count;i++) {
            
            NSLog(@"%@",[keys objectAtIndex:i]);
            NSString *key = [keys objectAtIndex:i];
            NSString *value = [values objectAtIndex:i];
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n%@",key, value] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [request setHTTPBody:body];
        
        NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession* session = [NSURLSession sessionWithConfiguration:config ];
        
        NSURLSessionUploadTask *uploadtask = [session uploadTaskWithRequest:request fromData:body completionHandler:^(NSData*data, NSURLResponse*response, NSError*error){
            dispatch_async(dispatch_get_main_queue(), ^{
//                [SVProgressHUD dismiss];
                
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                
                NSString* str = [[NSString alloc]initWithData:data
                                                     encoding:NSUTF8StringEncoding];
                
                NSError* error;
                
                if (str.length == 0) {
                    [self showAlert:@"Server error"];
                    return;
                }
                
                
                NSDictionary* dic = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                
                NSDictionary * result = [dic objectForKey:@"result"];

                
                if ([[dic objectForKey:@"iserror"]boolValue] == NO){
//

                                                    [self uploadService];
                                                                     

                }else{
                    
                    [self showAlert:[NSString stringWithFormat:@"%@",result]];
                }
                
            });
            
        }];
        
        [uploadtask resume];
        
        
    }else{
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"No internet connectivity" message:@"Please try later" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
}



-(UIImage*)scaleDown:(UIImage*)img withSize:(CGSize)newSize{
    
    
    UIGraphicsBeginImageContextWithOptions(newSize, YES, 0.0);
    [img drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return scaledImage;
}

- (void)uploadService{
    count = 0;
    [self imageService:@"1" withImage:truckregImgs1];
}

-(void)imageService:(NSString*)imagename withImage:(UIImage*)image{
    
    
    if ([Reachability reachabilityForInternetConnection]) {
        //        [SVProgressHUD showWithStatus:@"Truck Images Uploading"];
        
        MBProgressHUD* hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.mode = MBProgressHUDModeDeterminateHorizontalBar;
        hud.labelText = @"Uploading Truck Images...";
        hud.labelColor = [DesignObj quikYellow];
        hud.animationType = MBProgressHUDAnimationZoomIn;
        hud.color = [DesignObj quikRed];
        hud.alpha = 0.8;
        
        //        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
        //        [SVProgressHUD show];
        
        NSUserDefaults* userStoredetails = [NSUserDefaults standardUserDefaults];
        NSString * userID = [userStoredetails objectForKey:@"user_id"];
        NSLog(@"%@",userID);
        
        NSArray *keys = [[NSArray alloc]initWithObjects:@"userid",nil];
        //
        NSArray *values =[[NSArray alloc]initWithObjects:userID, nil];
        
        
        NSURL *url =[NSURL URLWithString:[NSString stringWithFormat:@"http://quikmov.quikmovapp.com/auth/truckImages"]];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        [request setHTTPMethod:@"POST"];
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        NSMutableData *body = [NSMutableData data];
        
        if(!(image.imageOrientation == UIImageOrientationUp ||
             image.imageOrientation == UIImageOrientationUpMirrored))
        {
            CGSize imgsize = image.size;
            UIGraphicsBeginImageContext(imgsize);
            [image drawInRect:CGRectMake(0.0, 0.0, imgsize.width, imgsize.height)];
            image = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
        }
        
        NSData * data = [NSData dataWithData:UIImagePNGRepresentation(image)];

        CGFloat compression = 0.9f;
        CGFloat maxCompression = 0.1f;
        
        while ([data length] > MAX_UPLOAD_SIZE && compression > maxCompression)
        {
            compression -= 0.10;
            data = UIImageJPEGRepresentation(image, compression);
            NSLog(@"Compress : %lu",(unsigned long)data.length);
        }
        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"imagefile\"; filename=\"%@truk%@.png\"\r\n",userID,imagename] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: image/png\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        //        Data changes as imageData
        [body appendData:[NSData dataWithData:data]];
        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        for (int i=0;i<keys.count;i++) {
            
            NSLog(@"%@",[keys objectAtIndex:i]);
            NSString *key = [keys objectAtIndex:i];
            NSString *value = [values objectAtIndex:i];
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n%@",key, value] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [request setHTTPBody:body];
        
        NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession* session = [NSURLSession sessionWithConfiguration:config ];
        
        //        Data Changed as imageData
        NSURLSessionUploadTask *uploadtask = [session uploadTaskWithRequest:request fromData:body completionHandler:^(NSData*data, NSURLResponse*response, NSError*error){
            dispatch_async(dispatch_get_main_queue(), ^{
                //                [SVProgressHUD dismiss];
                
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                
                if (data == nil) {
                    return;
                }
                
                NSString* str = [[NSString alloc]initWithData:data
                                                     encoding:NSUTF8StringEncoding];
                
                NSError* error;
                
                
                
                NSDictionary* dic = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                
                NSLog(@"%@******%@,%@",str,[dic allValues],[dic objectForKey:@"response"]);
                
                NSDictionary * result = [dic objectForKey:@"result"];
                
                
                if ([[dic objectForKey:@"iserror"]boolValue] == NO){
                    
                    
                    count = count + 1;
                    
                    if (count  < 4) {
                        
                        if (count == 1) {
                            [self imageService:@"2" withImage:truckregImgs2];
                        }else if (count == 2){
                            [self imageService:@"3" withImage:truckregImgs3];
                        }else if (count == 3){
                            [self imageService:@"4" withImage:truckregImgs4];
                        }
                        
                    }else{
                        
                        UIAlertController * alert=   [UIAlertController alertControllerWithTitle:@"Message" message:@"Truck Registration Successful" preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                             {
                                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                                 
                                                 VIewBookingViewController* next = [[VIewBookingViewController alloc]init];
                                                 [self.navigationController pushViewController:next animated:YES];
                                                 
                                             }];
                        
                        
                        
                        [alert addAction:ok];
                        
                        [self presentViewController:alert animated:YES completion:nil];
                    }
                    
                }else{
                    
                    [self showAlert:[NSString stringWithFormat:@"%@",result]];
                }
                
            });
            
            
        }];
        [uploadtask resume];
        
        
        //        NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        //        if( theConnection )
        //        {
        //            _downloadedData = [NSMutableData data] ;
        //            NSLog(@"request uploading successful");
        //        }
        //        else
        //        {
        //            NSLog(@"theConnection is NULL");
        //        }
        
    }else{
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"No internet connectivity" message:@"Please try later" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    
    
    
}

-(void)showAlert:(NSString*)message{
    UIAlertView * alt = [[UIAlertView alloc]initWithTitle:@"Message" message:message delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alt show];
}


-(void)CustomerupdateUserProfilePic{
    
    if ([Reachability reachabilityForInternetConnection]) {
        
//        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
//        [SVProgressHUD show];
        
        MBProgressHUD* hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.mode = MBProgressHUDModeIndeterminate;
        hud.labelText = @"Loading...";
        hud.labelColor = [DesignObj quikYellow];
        hud.animationType = MBProgressHUDAnimationZoomIn;
        hud.color = [DesignObj quikRed];
        hud.alpha = 0.8;
        
        NSArray *keys = [[NSArray alloc]initWithObjects:@"name",@"mname",@"lname",@"email",@"mobile",@"password",@"cpassword",@"dob",@"usertype",nil];
        //
       NSArray *values =[[NSArray alloc]initWithObjects:fname, mname, lname, email, mobNum,pass,conpass,dob,regType, nil];
    
    
        NSURL *url =[NSURL URLWithString:[NSString stringWithFormat:@"http://quikmov.quikmovapp.com/auth/signup"]];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        [request setHTTPMethod:@"POST"];
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        NSMutableData *body = [NSMutableData data];
        NSData * data = [NSData dataWithData:UIImagePNGRepresentation(profImag)];
        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"imagefile\"; filename=\"%@userprofile.png\"\r\n",fname] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: image/png\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[NSData dataWithData:data]];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        for (int i=0;i<keys.count;i++) {
            
            NSLog(@"%@",[keys objectAtIndex:i]);
            NSString *key = [keys objectAtIndex:i];
            NSString *value = [values objectAtIndex:i];
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n%@",key, value] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [request setHTTPBody:body];
        
        NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession* session = [NSURLSession sessionWithConfiguration:config ];
        
        
        NSURLSessionUploadTask *uploadtask = [session uploadTaskWithRequest:request fromData:body completionHandler:^(NSData*data, NSURLResponse*response, NSError*error){
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                
                NSString* str = [[NSString alloc]initWithData:data
                                                     encoding:NSUTF8StringEncoding];
                
                NSError* error;

                NSDictionary* dic = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                
                NSLog(@"%@******%@,%@",str,[dic allValues],[dic objectForKey:@"response"]);
                
                // userId = [[dic objectForKey:@"message"] stringValue];
                NSString * Result = [dic objectForKey:@"status"];
                
                NSDictionary * result = [dic objectForKey:@"result"];
                
                if ([[dic objectForKey:@"result"] isKindOfClass:[NSDictionary class]]){
                    
                    userId = [result objectForKey:@"user_id"];
                    NSLog(@"%@",userId);
                    
                    NSUserDefaults* userStoredetails = [NSUserDefaults standardUserDefaults];
                    [userStoredetails setObject:userId forKey:@"user_id"];
                    [userStoredetails synchronize];
                    
                    NSLog(@"%@",Result);
                    
                    if ([Result isEqualToString:@"success"]){
                        
                        UIAlertController * alert=   [UIAlertController alertControllerWithTitle:@"Message"
                                                                                         message:@"Registration Successful" preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK"
                                                                     style:UIAlertActionStyleDefault
                                                                   handler:^(UIAlertAction * action)
                                             {
                                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                                 
                                                 QuikMovGoogleViewController* mapView = [[QuikMovGoogleViewController alloc]init];
                                                 [self.navigationController pushViewController:mapView animated:YES];
                                                 
                                             }];
                        
                        
                        [alert addAction:ok];
                        
                        [self presentViewController:alert animated:YES completion:nil];
                    }else{
                        
                        [self showAlert:[NSString stringWithFormat:@"%@",[dic objectForKey:@"result"]]];
                    }
                }
                
            });
            
        }];
        [uploadtask resume];
        
    }else{
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"No internet connectivity" message:@"Please try later" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
