//
//  LoginViewController.m
//  QuikMov
//
//  Created by CSCS on 1/25/16.
//  Copyright © 2016 CSCS. All rights reserved.
//

#import "LoginViewController.h"
#import "UserTypeViewController.h"
#import "RegisterViewController.h"
#import "MapViewController.h"
#import "ViewController.h"
#import "DesignObj.h"
#import "SVProgressHUD.h"
#import "QMForgotPassViewController.h"
#import "VIewBookingViewController.h"
#import "QuikMovGoogleViewController.h"
#import "QuickMenuViewController.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"

@interface LoginViewController(){
    
    UILabel* title;
    UIButton* forgotPassword;
    UILabel* forgotPasswordlbl;
    NSString* sendUserType;
    NSString* userName;
    UILabel* notMember;
    UIButton* join;
    UITapGestureRecognizer* taptoKeyboardHide;
    NSDictionary *dictDialingCodes;
    NSString * countryCode;
    NSString * codeValue;
    NSMutableDictionary * dict;
    NSMutableData * _downloadedData;
    
}
@property (nonatomic, weak) UITextField* mobNumber;
@property (nonatomic, weak) UITextField* passwrd;

@end

@implementation LoginViewController
@synthesize mobNumber;
@synthesize passwrd;
@synthesize usrId;
@synthesize userType;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.navigationBarHidden = YES;
    
    CTTelephonyNetworkInfo * networkInfo = [[CTTelephonyNetworkInfo alloc]init];
    CTCarrier * carrier = [networkInfo subscriberCellularProvider];
    
    countryCode = [carrier.isoCountryCode uppercaseString];
    
    NSLog(@"%@",countryCode);
    
    dictDialingCodes = [[NSDictionary alloc]initWithObjectsAndKeys:
                        @"972", @"IL",
                        @"93", @"AF",
                        @"355", @"AL",
                        @"213", @"DZ",
                        @"1", @"AS",
                        @"376", @"AD",
                        @"244", @"AO",
                        @"1", @"AI",
                        @"1", @"AG",
                        @"54", @"AR",
                        @"374", @"AM",
                        @"297", @"AW",
                        @"61", @"AU",
                        @"43", @"AT",
                        @"994", @"AZ",
                        @"1", @"BS",
                        @"973", @"BH",
                        @"880", @"BD",
                        @"1", @"BB",
                        @"375", @"BY",
                        @"32", @"BE",
                        @"501", @"BZ",
                        @"229", @"BJ",
                        @"1", @"BM", @"975", @"BT",
                        @"387", @"BA", @"267", @"BW", @"55", @"BR", @"246", @"IO",
                        @"359", @"BG", @"226", @"BF", @"257", @"BI", @"855", @"KH",
                        @"237", @"CM", @"1", @"CA", @"238", @"CV", @"345", @"KY",
                        @"236", @"CF", @"235", @"TD", @"56", @"CL", @"86", @"CN",
                        @"61", @"CX", @"57", @"CO", @"269", @"KM", @"242", @"CG",
                        @"682", @"CK", @"506", @"CR", @"385", @"HR", @"53", @"CU",
                        @"537", @"CY", @"420", @"CZ", @"45", @"DK", @"253", @"DJ",
                        @"1", @"DM", @"1", @"DO", @"593", @"EC", @"20", @"EG",
                        @"503", @"SV", @"240", @"GQ", @"291", @"ER", @"372", @"EE",
                        @"251", @"ET", @"298", @"FO", @"679", @"FJ", @"358", @"FI",
                        @"33", @"FR", @"594", @"GF", @"689", @"PF", @"241", @"GA",
                        @"220", @"GM", @"995", @"GE", @"49", @"DE", @"233", @"GH",
                        @"350", @"GI", @"30", @"GR", @"299", @"GL", @"1", @"GD",
                        @"590", @"GP", @"1", @"GU", @"502", @"GT", @"224", @"GN",
                        @"245", @"GW", @"595", @"GY", @"509", @"HT", @"504", @"HN",
                        @"36", @"HU", @"354", @"IS", @"91", @"IN", @"62", @"ID",
                        @"964", @"IQ", @"353", @"IE", @"972", @"IL", @"39", @"IT",
                        @"1", @"JM", @"81", @"JP", @"962", @"JO", @"77", @"KZ",
                        @"254", @"KE", @"686", @"KI", @"965", @"KW", @"996", @"KG",
                        @"371", @"LV", @"961", @"LB", @"266", @"LS", @"231", @"LR",
                        @"423", @"LI", @"370", @"LT", @"352", @"LU", @"261", @"MG",
                        @"265", @"MW", @"60", @"MY", @"960", @"MV", @"223", @"ML",
                        @"356", @"MT", @"692", @"MH", @"596", @"MQ", @"222", @"MR",
                        @"230", @"MU", @"262", @"YT", @"52", @"MX", @"377", @"MC",
                        @"976", @"MN", @"382", @"ME", @"1", @"MS", @"212", @"MA",
                        @"95", @"MM", @"264", @"NA", @"674", @"NR", @"977", @"NP",
                        @"31", @"NL", @"599", @"AN", @"687", @"NC", @"64", @"NZ",
                        @"505", @"NI", @"227", @"NE", @"234", @"NG", @"683", @"NU",
                        @"672", @"NF", @"1", @"MP", @"47", @"NO", @"968", @"OM",
                        @"92", @"PK", @"680", @"PW", @"507", @"PA", @"675", @"PG",
                        @"595", @"PY", @"51", @"PE", @"63", @"PH", @"48", @"PL",
                        @"351", @"PT", @"1", @"PR", @"974", @"QA", @"40", @"RO",
                        @"250", @"RW", @"685", @"WS", @"378", @"SM", @"966", @"SA",
                        @"221", @"SN", @"381", @"RS", @"248", @"SC", @"232", @"SL",
                        @"65", @"SG", @"421", @"SK", @"386", @"SI", @"677", @"SB",
                        @"27", @"ZA", @"500", @"GS", @"34", @"ES", @"94", @"LK",
                        @"249", @"SD", @"597", @"SR", @"268", @"SZ", @"46", @"SE",
                        @"41", @"CH", @"992", @"TJ", @"66", @"TH", @"228", @"TG",
                        @"690", @"TK", @"676", @"TO", @"1", @"TT", @"216", @"TN",
                        @"90", @"TR", @"993", @"TM", @"1", @"TC", @"688", @"TV",
                        @"256", @"UG", @"380", @"UA", @"971", @"AE", @"44", @"GB",
                        @"1", @"US", @"598", @"UY", @"998", @"UZ", @"678", @"VU",
                        @"681", @"WF", @"967", @"YE", @"260", @"ZM", @"263", @"ZW",
                        @"591", @"BO", @"673", @"BN", @"61", @"CC", @"243", @"CD",
                        @"225", @"CI", @"500", @"FK", @"44", @"GG", @"379", @"VA",
                        @"852", @"HK", @"98", @"IR", @"44", @"IM", @"44", @"JE",
                        @"850", @"KP", @"82", @"KR", @"856", @"LA", @"218", @"LY",
                        @"853", @"MO", @"389", @"MK", @"691", @"FM", @"373", @"MD",
                        @"258", @"MZ", @"970", @"PS", @"872", @"PN", @"262", @"RE",
                        @"7", @"RU", @"590", @"BL", @"290", @"SH", @"1", @"KN",
                        @"1", @"LC", @"590", @"MF", @"508", @"PM", @"1", @"VC",
                        @"239", @"ST", @"252", @"SO", @"47", @"SJ", @"963",
                        @"SY",@"886",
                        @"TW", @"255",
                        @"TZ", @"670",
                        @"TL",@"58",
                        @"VE",@"84",
                        @"VN",
                        @"284", @"VG",
                        @"340", @"VI",
                        @"678",@"VU",
                        @"681",@"WF",
                        @"685",@"WS",
                        @"967",@"YE",
                        @"262",@"YT",
                        @"27",@"ZA",
                        @"260",@"ZM",
                        @"263",@"ZW",
                        nil];
    
    NSLog(@"%@",[dictDialingCodes objectForKey:countryCode]);
    codeValue = [NSString stringWithFormat:@"%@",[dictDialingCodes objectForKey:countryCode]];

    
    UIImageView*bgView = [DesignObj initWithImage:CGRectMake(0, 0, screenWidth, screenHeight) img:@"bg.png"];
    [self.view addSubview:bgView];
    
    _scrView = [[UIScrollView alloc]init];
    _scrView.frame = CGRectMake(0, 0, screenWidth, screenHeight);
    _scrView.contentSize = CGSizeMake(screenWidth, screenHeight);
    [self.view addSubview:_scrView];

//    self.title = @"Login";
    
    title = [DesignObj initWithLabel:CGRectMake((screenWidth-200)/2, 120, 200, 40) title:@"Sign-In" font:18 txtcolor:[DesignObj quikRed]];
    [_scrView addSubview:title];
    
    
    UIImageView * txtbgImg = [[UIImageView alloc]initWithFrame:CGRectMake((screenWidth-(screenWidth - 80))/2, title.frame.origin.y+ 40+ 40, screenWidth - 80, 40)];
    [txtbgImg setImage:[UIImage imageNamed:@"text_box.png"]];
    [_scrView addSubview:txtbgImg];
    
    mobNumber = [DesignObj initWithTextfield:CGRectMake((screenWidth-(screenWidth - 80))/2, title.frame.origin.y+ 40+ 40, screenWidth - 80, 40) placeholder:@"" tittle:@"Mobile Number" delegate:self font:14];
//    mobNumber.keyboardType = UIKeyboardTypeNumberPad;
    mobNumber.keyboardType = UIKeyboardTypeNumberPad;
    mobNumber.tintColor = [DesignObj quikRed];
    mobNumber.textAlignment = NSTextAlignmentCenter;
    [_scrView addSubview:mobNumber];
    
    UIImageView * txtbg_Img = [[UIImageView alloc]initWithFrame:CGRectMake((screenWidth-(screenWidth - 80))/2, mobNumber.frame.origin.y+ 40+ 20, screenWidth - 80, 40)];
    [txtbg_Img setImage:[UIImage imageNamed:@"text_box.png"]];
    [_scrView addSubview:txtbg_Img];
    
    passwrd = [DesignObj initWithTextfield:CGRectMake((screenWidth-(screenWidth - 80))/2, mobNumber.frame.origin.y+ 40 +20, screenWidth - 80, 40) placeholder:@"" tittle:@"Password" delegate:self font:16];
    passwrd.keyboardType = UIKeyboardTypeDefault;
    passwrd.textAlignment = NSTextAlignmentCenter;
    passwrd.tintColor = [DesignObj quikRed];
    passwrd.secureTextEntry = YES;
    [_scrView addSubview:passwrd];
    
//    mobNumber.text = @"9790834026";
//    passwrd.text = @"cscscscs";
    
    forgotPassword = [DesignObj initWithButton:CGRectMake(screenWidth-200, passwrd.frame.origin.y+40+ 30,200, 40) tittle:@"" img:@""];
    [forgotPassword setTitleColor:[DesignObj quikRed] forState:UIControlStateNormal];
    [forgotPassword addTarget:self action:@selector(forgotAction) forControlEvents:UIControlEventTouchUpInside];
    [_scrView addSubview:forgotPassword];
    
    forgotPasswordlbl = [DesignObj initWithLabel:CGRectMake(screenWidth-200, passwrd.frame.origin.y+40+ 30, 200, 40) title:@"Forgot password ?" font:16 txtcolor:[DesignObj quikRed]];
    [_scrView addSubview:forgotPasswordlbl];

    login = [DesignObj initWithButton:CGRectMake((screenWidth-(screenWidth - 100))/2, forgotPasswordlbl.frame.origin.y+40+ 30, screenWidth - 100, 40) tittle:@"Enter" img:@"button.png"];
    [login setTitleColor:[DesignObj quikYellow] forState:UIControlStateNormal];
    [login addTarget:self action:@selector(nextAction) forControlEvents:UIControlEventTouchUpInside];
    [_scrView addSubview:login];
    
    if (screenHeight <= 568) {
        
        notMember = [DesignObj initWithLabel:CGRectMake((screenWidth - 150)/2, login.frame.origin.y + 40 + 15, 150, 40) title:@"Not a Member?" font:16 txtcolor:[DesignObj quikRed]];
        notMember.textAlignment = NSTextAlignmentCenter;
        [_scrView addSubview:notMember];
        
    }else{
        
        notMember = [DesignObj initWithLabel:CGRectMake((screenWidth-150)/2, screenHeight-150, 150, 40) title:@"Not a Member?" font:16 txtcolor:[DesignObj quikRed]];
        notMember.textAlignment = NSTextAlignmentCenter;
        [_scrView addSubview:notMember];
        
    }
        join = [DesignObj initWithButton:CGRectMake((screenWidth-(screenWidth - 100))/2, notMember.frame.origin.y+40+ 20, (screenWidth - 100), 40) tittle:@"Join" img:@"button.png"];
        [join setTitleColor:[DesignObj quikYellow] forState:UIControlStateNormal];
        [join addTarget:self action:@selector(joinAction) forControlEvents:UIControlEventTouchUpInside];
        [_scrView addSubview:join];

    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, screenWidth, 44)];
    numberToolbar.barStyle = UIBarStyleDefault;//UIBarStyleBlackTranslucent;
    numberToolbar.tintColor = [UIColor colorWithRed:207/255.0 green:26/255.0 blue:26/255.0 alpha:1.0];
    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleDone target:self action:@selector(cancelNumberPad)],
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
    [numberToolbar sizeToFit];

    mobNumber.inputAccessoryView = numberToolbar;
    
    taptoKeyboardHide = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hideKeyboard:)];
    taptoKeyboardHide.numberOfTouchesRequired = 1;
    taptoKeyboardHide.numberOfTapsRequired = 1;
    [_scrView addGestureRecognizer:taptoKeyboardHide];
   
    // Do any additional setup after loading the view.
}

-(IBAction)hideKeyboard:(id)sender{
    
    [self.view endEditing:YES];
    
}

-(void)joinAction{
    
    RegisterViewController* regView = [[RegisterViewController alloc]init];
    
    regView.utype =userType;
    [self.navigationController pushViewController:regView animated:YES];
   
//    [self dismissViewControllerAnimated:YES completion:nil];
    
//     [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:YES];
    
    self.navigationController.navigationBarHidden = YES;
}

-(BOOL)textFieldShouldReturn:(UITextField*)textField
{
    
    [textField resignFirstResponder];
    
    [UIView animateWithDuration:0.5 animations:^{
        if (textField == mobNumber) {
            [passwrd becomeFirstResponder];
        }else{
            _scrView.frame = CGRectMake(0, 0, _scrView.frame.size.width, _scrView.frame.size.height);
            [self nextAction];
        }
    }];
    return YES;

}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    [UIView animateWithDuration:0.5 animations:^{
        if (textField == mobNumber) {
            _scrView.frame = CGRectMake(0, - 50, _scrView.frame.size.width, _scrView.frame.size.height);
        }else{
            _scrView.frame = CGRectMake(0, - 70, _scrView.frame.size.width, _scrView.frame.size.height);
        }
        
    }];
}

-(void)cancelNumberPad{
    [mobNumber resignFirstResponder];
    mobNumber.text = @"";
}

-(void)doneWithNumberPad{
    [mobNumber resignFirstResponder];
    [passwrd becomeFirstResponder];
}

- (void)showAlert:(NSString *)message{
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Message"
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [alert dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    
    
    [alert addAction:ok];
    
    [self presentViewController:alert animated:YES completion:nil];
}


-(void)nextAction{
    
    if (mobNumber.text.length == 0) {
        [self showAlert:@"Please enter your Mobile Number"];
    }else if (passwrd.text.length==0){
        [self showAlert:@"Please enter password"];
    }else if (passwrd.text.length<6){
        [self showAlert:@"Password atleast 6 characters"];
    }else{
            [self loginService];
//        [self anotherLoginService];
        }

}

-(void)loginService{
    
    [mobNumber resignFirstResponder];
    [passwrd resignFirstResponder];
    
    UIView* hudView = [[UIView alloc]initWithFrame:CGRectMake((screenWidth-100)/2, (screenHeight-350)/2, 100, 350)];
    hudView.backgroundColor = [UIColor blackColor];
    
    MBProgressHUD* hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = @"Loading...";
    hud.labelColor = [DesignObj quikYellow];
    hud.customView = hudView;
    hud.animationType = MBProgressHUDAnimationZoomIn;
    hud.color = [DesignObj quikRed];
    hud.alpha = 0.8;

    NSURL *url = [NSURL URLWithString:@"http://quikmov.quikmovapp.com/auth/login"];
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    // 2
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    request.HTTPMethod = @"POST";
    
    // 3
    
    NSString * bodyMsg = [NSString stringWithFormat:@"mobile=%@&password=%@&usertypeid=%@",[NSString stringWithFormat:@"%@%@",[dictDialingCodes objectForKey:countryCode],mobNumber.text],passwrd.text,userType];
    
    
    
    NSData * data = [bodyMsg dataUsingEncoding:NSUTF8StringEncoding];
    
    // 4
    NSURLSessionUploadTask *uploadTask = [session uploadTaskWithRequest:request
                                                               fromData:data completionHandler:^(NSData *data,NSURLResponse *response,NSError *error) {
                                                                   // Handle response here
                                                                   dispatch_async(dispatch_get_main_queue(), ^{
//                                                                       [SVProgressHUD dismiss];
                                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                                                                       [self loginSuccess:data];
                                                                   });
                                                                   
                                                               }];
    
    // 5
    [uploadTask resume];
    
}
- (void)loginSuccess:(NSData *)data{
    
    NSString * newStr = [[NSString alloc] initWithData:data                                                                                                                 encoding:NSUTF8StringEncoding];
    
    NSLog(@"%@",newStr);
    
    // NSError* error;
    
    NSString* str = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
    
    if (str.length == 0) {
        [self showAlert:@"Server error"];
        return;
    }
    NSError * error;
    NSDictionary* dic = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    
    NSLog(@"%@******%@,%@",str,[dic allValues],[dic objectForKey:@"status"]);
    
    NSDictionary * result = [dic objectForKey:@"result"];
    
    if ([[dic objectForKey:@"result"] isKindOfClass:[NSDictionary class]]){
        
        usrId = [result objectForKey:@"user_id"];
        NSLog(@"%@",usrId);
        
        NSUserDefaults* userStoredetails = [NSUserDefaults standardUserDefaults];
        [userStoredetails setObject:usrId forKey:@"user_id"];
        [userStoredetails setObject:userName forKey:@"user_name"];
        
        [userStoredetails synchronize];
    }
    
    // userId = [[dic objectForKey:@"message"] stringValue];
    NSString * Result = [dic objectForKey:@"status"];
    
    NSLog(@"%@",Result);
    
    if ([Result isEqualToString:@"success"]){
        mobNumber.text = @"";
        passwrd.text = @"";
        [self gotoNextView];
        
    }else{
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Message" message:[NSString stringWithFormat:@"%@",[dic objectForKey:@"result"]] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        
    }

}


-(void)gotoNextView{
    
//    [self dismissViewControllerAnimated:YES completion:nil];
    
    if ([userType isEqualToString:@"1"]) {
        
    
    QuikMovGoogleViewController* mapView = [[QuikMovGoogleViewController alloc]init];
    mapView.userType = userType;
    [self.navigationController pushViewController:mapView animated:YES];

    }else{
        
        VIewBookingViewController* bookView = [[VIewBookingViewController alloc]init];
//        mapView.userType = userType;
        [self.navigationController pushViewController:bookView animated:YES];
        
 
    }
    
}

-(void)forgotAction{
    
    QMForgotPassViewController* forgotView = [[QMForgotPassViewController alloc]init];
    [self.navigationController pushViewController:forgotView animated:YES];
    
}

-(void)anotherLoginService{
    
    UIView* hudView = [[UIView alloc]initWithFrame:CGRectMake((screenWidth-100)/2, (screenHeight-350)/2, 100, 350)];
    hudView.backgroundColor = [UIColor blackColor];
    
    MBProgressHUD* hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = @"Loading...";
    hud.labelColor = [DesignObj quikYellow];
    hud.customView = hudView;
    hud.animationType = MBProgressHUDAnimationZoomIn;
    hud.color = [DesignObj quikRed];
    hud.alpha = 0.8;

    
        NSMutableDictionary * dic = [[NSMutableDictionary alloc]init];
        
        [dic setValue:mobNumber.text forKey:@"username"];
        [dic setValue:passwrd.text forKey:@"password"];
        [dic setValue:APP_KEY forKey:@"appKeyToken"];
    
        NSLog(@"%@",dic);
        NSError * error;
        NSData *data = [NSJSONSerialization dataWithJSONObject:dic options:NSJSONWritingPrettyPrinted error:&error];
        
        NSString* newStr = [[NSString alloc] initWithData:data
                                                 encoding:NSUTF8StringEncoding];
        NSLog(@"%@",newStr);

        [self startWith:data];
    
    }


-(void)startWith:(NSData*)bodyStr{
    
    NSURLConnection * Globalconnection;
    [Globalconnection cancel];
    Globalconnection = nil;
   
    NSURL *url =[NSURL URLWithString:[NSString stringWithFormat:@"http://prod.quikmovapp.com/member/login"]];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:30.0];
    [request setHTTPMethod:@"POST"];
    //Pass some default parameter(like content-type etc.)
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setHTTPBody:bodyStr];
    Globalconnection = [NSURLConnection connectionWithRequest:request delegate:self];
    [Globalconnection start];
    
    if(Globalconnection)
        _downloadedData = [NSMutableData data];
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    //This function is called when the download begins.
    //You can get all the response headers
    if (_downloadedData!=nil) {
        _downloadedData = nil;
    }
    _downloadedData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    //This function is called whenever there is downloaded data available
    //It will be called multiple times and each time you will get part of downloaded data
    [_downloadedData appendData:data];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection{
    
    [self DownloadfileCompleted:_downloadedData];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    
    //    [self.delegate DownloadfilHasError:error];
}

-(void)DownloadfileCompleted:(NSData*)responsedata{
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    NSString* newStr = [[NSString alloc] initWithData:responsedata
                                             encoding:NSUTF8StringEncoding];
    
    NSError* error;
    NSDictionary* dic = [NSJSONSerialization JSONObjectWithData:responsedata options:kNilOptions
                                                          error:&error];
    
    NSLog(@"%@*****,%@",newStr,[dic objectForKey:@"result"]);
    
     NSDictionary * result = [dic objectForKey:@"result"];
    
    
    if ([[dic objectForKey:@"result"] isKindOfClass:[NSDictionary class]]){
        
        usrId = [result objectForKey:@"user_id"];
                NSLog(@"%@",usrId);
        
        NSUserDefaults* userStoredetails = [NSUserDefaults standardUserDefaults];
        [userStoredetails setObject:usrId forKey:@"user_id"];
        
        [userStoredetails synchronize];
        
        [self gotoNextView];
        
    }else{
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Message" message:[NSString stringWithFormat:@"%@",[dic objectForKey:@"result"]] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
