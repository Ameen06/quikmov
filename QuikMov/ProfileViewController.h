//
//  ProfileViewController.h
//  QuikMov
//
//  Created by Ajmal Khan on 2/29/16.
//  Copyright © 2016 CSCS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "UIImageView+WebCache.h"

@interface ProfileViewController : UIViewController<UITextFieldDelegate>


{
    
    CGFloat screenWidth;
    CGFloat screenHeight;
    
}

@property(nonatomic,strong)UIImageView* profileImg;
@property(nonatomic,retain)NSString * utype;

@end
