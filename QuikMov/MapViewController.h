//
//  MapViewController.h
//  QuikMov
//
//  Created by CSCS on 1/27/16.
//  Copyright © 2016 CSCS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import <QuartzCore/QuartzCore.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "QuickMenuViewController.h"
#import "UIImageView+WebCache.h"
#import "MHPresenterImageView.h"


#define SHAWDOW_ALPHA 0.5
#define MENU_DURATION 0.3
#define MENU_TRIGGER_VELOCITY 350

@interface MapViewController : UIViewController<CLLocationManagerDelegate,MKMapViewDelegate,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,UIGestureRecognizerDelegate,menuDelegate>{
    
    CGFloat screenWidth;
    CGFloat screenHeight;
}

@property(nonatomic,strong)CLLocationManager* locationManager;
@property(nonatomic, strong)NSString* userType;
@property (nonatomic, strong) UIPanGestureRecognizer *panGest;
@property (nonatomic, strong)UITapGestureRecognizer *imageViewtap;
@property(nonatomic,strong)MHPresenterImageView *iv;


@end
