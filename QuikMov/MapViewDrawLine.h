//
//  MapViewDrawLine.h
//  MapViewDirection
//
//  Copyright © 2015 Savin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MapViewDrawLine : NSObject
+ (NSMutableArray *)parseResponse:(NSDictionary *)response;
@end
