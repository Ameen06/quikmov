//
//  VIewBookingViewController.m
//  QuikMov
//
//  Created by Ajmal Khan on 2/20/16.
//  Copyright © 2016 CSCS. All rights reserved.
//

#import "VIewBookingViewController.h"
#import "DesignObj.h"
#import "SVProgressHUD.h"
#import "GlobalObjects.h"
#import "Reachability.h"
#import "ProfileViewController.h"
#import <AddressBookUI/AddressBookUI.h>
#import "MapViewDrawLine.h"
#import "UICustomSwitch.h"

#define SHAWDOW_ALPHA 0.5
#define MENU_DURATION 0.3
#define MENU_TRIGGER_VELOCITY 350
#define RADIUS 100


@interface VIewBookingViewController (){
    
    CGFloat screenWidth;
    CGFloat screenHeight;
    UILabel* nodata;
    NSArray* bookingpickdetlsarr;
    NSArray* bookingdelrydetlsarr;
    NSMutableArray* detailsarr;
    UITableView * bookingView;
    NSString* convertpicStr;
    NSString* convertdelStr;
    
//    Location and MapView
    
    CLLocationCoordinate2D* coordinates;
    CLGeocoder* reverseGeocoder;
    NSString* lat;
    NSString* log;
    MKMapView* mapView;
    NSString* statusCheck;
    MKPointAnnotation* myAnnotation;
    NSMutableArray * annotations;
    NSString* forwardlat;
    NSString* forwardlong;
    UIButton* setCurrentLocation;
    NSString*  pickLat;
    NSString* pickLong;
    NSString* delvryLat;
    NSString* delvryLong;
    CLLocation *locate;
    NSString* getPickAddr;
    NSString* getDelAddr;
    GMSCameraPosition * camera;
    GMSMarker* marker;
    GMSMapView* mapView_;
    GMSPolyline * polyLine;
    
//    CGFloat  pickLat;
//    CGFloat pickLong;
//    CGFloat delvryLat;
//    CGFloat delvryLong;
    
    
 //TableViewCell
    
    UILabel* bookingName;
    UILabel* bookingStatus;
    UILabel* pickupPoint;
    UILabel* deliveryPlace;
    UILabel* updateOn;
    UIImageView* userprofimag;
    
// didselect
    
    UIView* popView;
    UIImageView* profimg;
    UIView* shadowView;
    UIButton* shadowButton;
    UIButton* closeView;
    UILabel* name;
    UILabel* phoneNo;
    UILabel* emailid;
    UILabel* pickUplocation;
    UILabel* deliveryLocation;
    UILabel* shipmentType;
    UIButton* confirmBtn;
    UIButton* cancelBtn;
    
    //Serivce Convert
    
    NSString* bookId;
    NSString* useriid;
    UIView* hideMapview;
    UISwitch* switchStatus;
    
    // Menu View
    
    QuickMenuViewController * menuView;
    UISwipeGestureRecognizer* swipetoRight;
    UIView* panshadowView;
    BOOL isOPen;
    UIButton* closingMenu;
    
//Status COnfirm, cancel or Picked
    NSString* bukStatus;

    NSString* foundlat;
    NSString* foundLong;
    UIButton* contactBtn;
    
//    COntact
    
    
    
    UIView* popContactView;
    UIView* popContactshadowView;
    UIButton* popContactshadowbtn;
    UIButton* popcontactClose;
    UIButton* call;
    UIButton* callImg;
    UIButton* sms;
    UIButton* smsImg;
    NSString* phno;
    NSString* checkingAction;
    
//    DrawLine
    
    NSString* serviceFlag;
    NSMutableArray *_path;
    NSInteger serviceCount;
    NSString* place_id;
    NSString* distanceiD;
    NSString* minutees;
    NSString* placePoints;
    NSMutableArray*stepsArr;
    NSInteger* loadingSeqnce;
    
//    Triggering view
    
    UIView* trigView;
    UILabel* milesLbl;
    UILabel* minuteLbl;
    UILabel* pickUpLocationLbl;
    UILabel* areaLbl;
    UIImageView* iconCustmr;
    
    UICustomSwitch* switchcontrolling;
    
    

}

@end

@implementation VIewBookingViewController
@synthesize locationManager;
@synthesize userType;

static NSString * googleMapApiKey = @"AIzaSyCeH7CNO75o3nr9Afl7_D4WJ4HHytIt4Ls";


- (void)viewDidLoad {
    [super viewDidLoad];
    
    checkingAction = @"Yes";
    
    statusCheck = @"1";
    
    self.title = @"QuikMov";
    
    self.navigationController.navigationBarHidden = NO;
    
    self.navigationItem.hidesBackButton = YES;
    
    self.navigationController.navigationBar.tintColor = [DesignObj quikYellow];
    
    screenWidth = [UIScreen mainScreen].bounds.size.width;
    screenHeight = [UIScreen mainScreen].bounds.size.height;
    
    detailsarr = [[NSMutableArray alloc]init];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [DesignObj quikYellow]}];
    self.navigationController.navigationBar.barTintColor = [DesignObj quikRed];
    
    //    getcurrentLocation
    
    locationManager = [[CLLocationManager alloc]init];
    locationManager.delegate = self;
    locationManager.distanceFilter = 5;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >=8) {
        [self.locationManager requestWhenInUseAuthorization];
    }
    [locationManager startUpdatingLocation];
    
    annotations = [[NSMutableArray alloc]init];
    
    NSLog(@"%@",[NSString stringWithFormat:@"latitude: %f longitude: %f", locationManager.location.coordinate.latitude, locationManager.location.coordinate.longitude]);
    
    lat = [NSString stringWithFormat:@"%f",locationManager.location.coordinate.latitude];
    log = [NSString stringWithFormat:@"%f",locationManager.location.coordinate.longitude];
    


    
//    if (locationManager.distanceFilter == 1) {
//        [self uploadcurrentLocation];
//    }
    
//    CLLocation *location1 =[[CLLocation alloc] initWithLatitude:[lat doubleValue] longitude:[log doubleValue]];
//    CLLocation *loc2 = [[CLLocation alloc] initWithLatitude:[lat doubleValue] longitude:[log doubleValue]];
//    CLLocationDistance distanceMeter = [location1 distanceFromLocation:loc2];
//    
//    NSLog(@"%f", distanceMeter);
//    
//    if (distanceMeter == 0.2) {
//        [self uploadcurrentLocation];
//    }
    
    //  Google Map View
    
    camera = [GMSCameraPosition cameraWithLatitude:[lat floatValue] longitude:[log floatValue] zoom:18];
    //    CGRectZero
    mapView_ = [GMSMapView mapWithFrame:CGRectMake(0, 0, screenWidth, screenHeight) camera:camera];
    mapView_.myLocationEnabled = YES;
    mapView_.settings.myLocationButton = YES;
    mapView_.mapType = kGMSTypeNormal;
    mapView_.delegate = self;
    //    [mapView_ setMinZoom: maxZoom:0];
    self.view = mapView_;
    
//    CLLocationCoordinate2D latlongcoords;
//    
//    latlongcoords = CLLocationCoordinate2DMake([lat floatValue], [log floatValue]);
//    
//    GMSMarker* currentlocation = [GMSMarker markerWithPosition:latlongcoords];
//    //        marker.title =  [NSString stringWithFormat:@"%d",i];
//    currentlocation.icon = [UIImage imageNamed:@"truck_login.png"];
//    currentlocation.appearAnimation = kGMSMarkerAnimationPop;
//    currentlocation.map = mapView_;
    
    // MapView Ends

    
    hideMapview = [[UIView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, screenHeight)];
    hideMapview.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.5f];
    
    [self.view addSubview:hideMapview];
    hideMapview.hidden = YES;
    
    NSUserDefaults * user = [NSUserDefaults standardUserDefaults];
    
        
        switchStatus= [[UISwitch alloc]initWithFrame:CGRectMake(screenWidth-120, 84, 120, 30)];
        [switchStatus setOn:[user boolForKey:@"toggle"]];
        [switchStatus setTintColor:[DesignObj quikYellow]];
        [switchStatus setOnTintColor:[DesignObj quikYellow]];
        [switchStatus addTarget:self action:@selector(switchToggled:) forControlEvents:UIControlEventValueChanged];
        [self.view addSubview:switchStatus];
    
  /*  switchcontrolling = [[UICustomSwitch alloc]initWithFrame:CGRectMake(screenWidth - 150, 20, 84, 31)];
    [switchcontrolling setTintColor:[DesignObj quikYellow]];
    [switchcontrolling setOn:[user boolForKey:@"toggle"]];
    switchcontrolling.leftLabel.text = @"Active";
    switchcontrolling.rightLabel.text = @"InActive";
    switchcontrolling.leftLabel.textColor = [DesignObj quikRed];
    switchcontrolling.rightLabel.textColor = [DesignObj quikRed];

    [switchcontrolling addTarget:self action:@selector(switchToggled:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:switchcontrolling];
    
    switchControl= [[DCRoundSwitch alloc]initWithFrame:CGRectMake(screenWidth-120, 84, 80, 30)];
    [switchControl setOn:[user boolForKey:@"toggle"]];
    [switchControl setTintColor:[DesignObj quikYellow]];
    [switchControl setOnTintColor:[DesignObj quikYellow]];
    [switchControl addTarget:self action:@selector(switchToggled:) forControlEvents:UIControlEventValueChanged];
    [switchControl setOnText:@"Active"];
    [switchControl setOffText:@"InActive"];
    [switchControl setOnTintColor:[DesignObj quikYellow]];
    [self.view addSubview:switchControl];
   
   */
    
    UIBarButtonItem * rightbarbtn = [[UIBarButtonItem alloc]initWithCustomView:switchStatus];
    self.navigationItem.rightBarButtonItem = rightbarbtn;
    
    
    if ([statusCheck isEqualToString:@"1"]) {
        
        [switchcontrolling setOn:YES];
    }else{
        [switchcontrolling setOn:NO];
    }

    
    UISegmentedControl *segmentControl = [[UISegmentedControl alloc]initWithItems:@[@"Terrain",@"Satelite"]];
    
    [segmentControl setBackgroundColor:[UIColor whiteColor]];
    segmentControl.frame = CGRectMake((screenWidth-200)/2, screenHeight-40, 200, 25);
    [segmentControl setTintColor:[DesignObj quikRed]];
    segmentControl.backgroundColor= [DesignObj quikYellow];
    [segmentControl addTarget:self action:@selector(segmentedControlValueDidChange:) forControlEvents:UIControlEventValueChanged];
    [segmentControl setSelectedSegmentIndex:0];
    [self.view addSubview:segmentControl];

    panshadowView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screenWidth, screenHeight)];
    panshadowView.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.0];
    panshadowView.hidden = YES;
    UITapGestureRecognizer *tapIt = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                            action:@selector(tapOnShawdow:)];
    [panshadowView addGestureRecognizer:tapIt];
    
    panshadowView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:panshadowView];
    
    menuView = [[QuickMenuViewController alloc]init];
    menuView.view.frame = CGRectMake(- screenWidth, 0, screenWidth, screenHeight);
    menuView.view.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.0];
    menuView.delagte = self;
    [self.view addSubview:menuView.view];
    
    UIView * pangestureView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 50, screenHeight)];
    pangestureView.backgroundColor =[UIColor clearColor];
    [self.view addSubview:pangestureView];
    
    self.panGest = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(moveDrawer:)];
    self.panGest.maximumNumberOfTouches = 1;
    self.panGest.minimumNumberOfTouches = 1;
    self.panGest.delegate = self;
    [pangestureView addGestureRecognizer:self.panGest];
    
//    NSTimeInterval time;
    
//    if (time == 20.0){
//        
//        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(updateBooking) name:@"Booking Check" object:nil];
//        
//    }
    
    // Do any additional setup after loading the view.
}


-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:YES];
    
//    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:207/255.0 green:26/255.0 blue:26/255.0 alpha:1.0];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self action:@selector(menuOpen) forControlEvents:UIControlEventTouchUpInside]; //adding action
    [button setTitleColor:[DesignObj quikYellow] forState:UIControlStateNormal];
    [button setImage:[UIImage imageNamed:@"menu.png"] forState:UIControlStateNormal];
    button.frame = CGRectMake(0 ,0,15,35);
    
    UIBarButtonItem* barbtn = [[UIBarButtonItem alloc]initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = barbtn;
    
    
}


- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation{
    
    CGFloat distance = [newLocation distanceFromLocation:oldLocation];
    NSLog(@"%f",distance);
    
    lat = [NSString stringWithFormat:@"%f",newLocation.coordinate.latitude];
    log = [NSString stringWithFormat:@"%f",newLocation.coordinate.longitude];
    
    
    [self uploadcurrentLocation];
//    [self loadAnnotations];

    
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:YES];
    
    if ([checkingAction isEqualToString:@"Yes"]) {

    [self loadBookingService];
    }else{
        
        [self closePopcontactView];
    }

}

// Map Segment

-(void)segmentedControlValueDidChange:(UISegmentedControl *)segment
{
    switch (segment.selectedSegmentIndex) {
        case 0:{
            
            mapView_.mapType = kGMSTypeNormal;
            break;
        }
        case 1:{
            mapView_.mapType = kGMSTypeSatellite;
            break;
        }
    }
}


// Menu View starts

-(void)menuOpen{

    [self.navigationController.view addSubview:menuView.view];
    float duration = MENU_DURATION/menuView.view.frame.size.width*fabs(menuView.view.center.x)+MENU_DURATION/2; // y=mx+c
    // shawdow
    
    [UIView animateWithDuration:duration
                          delay:0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         closingMenu = [DesignObj initWithButton:CGRectMake(screenWidth-50, 0, 50, screenHeight) tittle:@"" img:@""];
                         [closingMenu addTarget:self action:@selector(closeMenu) forControlEvents:UIControlEventTouchUpInside];
                         [menuView.view addSubview:closingMenu];
                         panshadowView.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:SHAWDOW_ALPHA];
                     }
                     completion:nil];
    panshadowView.hidden = NO;
    // drawer
    [UIView animateWithDuration:duration
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         menuView.view.frame = CGRectMake(0, 0, menuView.view.frame.size.width, menuView.view.frame.size.height);
                     }
                     completion:nil];
    
    isOPen= YES;
    
}

- (void)tapOnShawdow:(UITapGestureRecognizer *)recognizer {
    [self closeMenu];
}

-(void)moveDrawer:(UIPanGestureRecognizer *)recognizer{
    
    CGPoint translation = [recognizer translationInView:self.view];
    CGPoint velocity = [(UIPanGestureRecognizer*)recognizer velocityInView:self.view];
    //    NSLog(@"velocity x=%f",velocity.x);
    
    if([(UIPanGestureRecognizer*)recognizer state] == UIGestureRecognizerStateBegan) {
        //        NSLog(@"start");
        if ( velocity.x > MENU_TRIGGER_VELOCITY && isOPen) {
            [self menuOpen];
        }else if (velocity.x < -MENU_TRIGGER_VELOCITY && isOPen) {
            [self closeMenu];
        }
    }
    
    if([(UIPanGestureRecognizer*)recognizer state] == UIGestureRecognizerStateChanged) {
        //        NSLog(@"changing");
        float movingx = menuView.view.center.x + translation.x;
        if ( movingx > -menuView.view.frame.size.width/2 && movingx < menuView.view.frame.size.width/2){
            
            menuView.view.center = CGPointMake(movingx, menuView.view.center.y);
            [recognizer setTranslation:CGPointMake(0,0) inView:self.view];
            
            float changingAlpha = SHAWDOW_ALPHA/menuView.view.frame.size.width*movingx+SHAWDOW_ALPHA/2; // y=mx+c
            panshadowView.hidden = NO;
            panshadowView.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:changingAlpha];
        }
    }
    
    if([(UIPanGestureRecognizer*)recognizer state] == UIGestureRecognizerStateEnded) {
        //        NSLog(@"end");
        if (menuView.view.center.x>0){
            [self menuOpen];
        }else if (menuView.view.center.x<0){
            [self closeMenu];
        }
    }
    
    
    
}

-(void)closeMenu{
    
    float duration = MENU_DURATION/menuView.view.frame.size.width*fabs(menuView.view.center.x)+MENU_DURATION/2; // y=mx+c
    
    // shawdow
    [UIView animateWithDuration:duration
                          delay:0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         panshadowView.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.0f];
                     }
                     completion:^(BOOL finished){
                         panshadowView.hidden = YES;
                     }];
    
    // drawer
    [UIView animateWithDuration:duration
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         menuView.view.frame = CGRectMake(- menuView.view.frame.size.width, 0, menuView.view.frame.size.width, menuView.view.frame.size.height);;
                     }
                     completion:nil];
    isOPen= NO;
    
    
    
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}



// Menu View Ends

// Switch Toggle

- (void) switchToggled:(id)sender {
    
    NSUserDefaults * user = [NSUserDefaults standardUserDefaults];
    
    switchStatus = (UISwitch *)sender;
    
    if ([switchStatus isOn]) {
        
        [user setBool:YES forKey:@"toggle"];
        statusCheck =@"1";
        hideMapview.hidden = YES;
        [self uploadcurrentLocation];
        [self showAlert:@"You are Active now\n(Now you are visible to your Customers)"];
        marker.opacity = 1.0f;
        
    } else {
        
        [user setBool:NO forKey:@"toggle"];
        statusCheck =@"0";
        hideMapview.hidden = NO;
        [self uploadcurrentLocation];
        [self showAlert:@"You are InActive now\n(The Customers can't see and Book you)"];
        marker.opacity = 0.0f;
    
    }
    [user synchronize];
}

// mapview location update starts


- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    NSLog(@"Error:%@",error);
    switch([error code])
    {
        case kCLErrorNetwork: // general, network-related error
        {
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please check your network connection or that you are not in airplane mode" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        }
            break;
        case kCLErrorDenied:{
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Location Service not enabeld" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];        }
            break;
            
        case kCLErrorLocationUnknown:
            break;
        default:
            
            break;
    }
    
}


// MapView location update end


-(void)loadBookingService{
    
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
    [SVProgressHUD show];
    
    
    NSURL* url = [NSURL URLWithString:@"http://quikmov.quikmovapp.com/auth/viewBooking"];
    NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession* session = [NSURLSession sessionWithConfiguration:config ];
    
    NSMutableURLRequest*request = [[NSMutableURLRequest alloc]initWithURL:url];
    request.HTTPMethod = @"POST";
    
    //    LoginViewController* log = [[LoginViewController alloc]init];
    //    log.usrId;
    
    NSUserDefaults* userStoredetails = [NSUserDefaults standardUserDefaults];
    NSString * userID = [userStoredetails objectForKey:@"user_id"];
    NSLog(@"%@",userID);
    NSString* bodycntnt = [NSString stringWithFormat:@"userid=%@",userID];
    
    NSData *data = [bodycntnt dataUsingEncoding:NSUTF8StringEncoding];
    
    NSURLSessionUploadTask *uploadtask = [session uploadTaskWithRequest:request fromData:data completionHandler:^(NSData*data, NSURLResponse*response, NSError*error){
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
            [self serviceSuccess:data];
        });
    }];
    
    [uploadtask resume];
}

- (void)serviceSuccess:(NSData *)data{
    if (data == nil) {
        return;
    }
    NSString* str = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
    NSError * error;
    NSDictionary* dic = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    NSLog(@"%@******%@,%@",str,[dic allValues],[dic objectForKey:@"response"]);
    
    NSString * Result = [dic objectForKey:@"status"];
    
    NSLog(@"%@",Result);
    if ([Result isEqualToString:@"success"]){
        
        
        
        bookingView = [DesignObj initWithTableView:self
                                             frame:CGRectMake((screenWidth -(screenWidth-60))/2, ((screenHeight -(screenHeight-120))/2)+30, screenWidth-60, screenHeight-120)];
        //self.bookingView.backgroundColor = [UIColor ];
        bookingView.rowHeight = 120.0f;
        bookingView.delegate = self;
        bookingView.dataSource = self;
        bookingView.separatorColor = [DesignObj quikRed];
        bookingView.layer.borderWidth = 0.5f;
        bookingView.layer.cornerRadius = 9;
        bookingView.layer.borderColor =[[DesignObj quikRed]CGColor];
        [self.view addSubview:bookingView];

        NSArray * arr = [dic objectForKey:@"result"];
        
        [detailsarr removeAllObjects];
        
        for (int i = 0; i < [arr count]; i++) {
            
            //
            GlobalObjects * globleObj = [[GlobalObjects alloc]init];
            NSDictionary * objDic = [arr objectAtIndex:i];
            
            globleObj.bookingStatus = [objDic objectForKey:@"bookingstatus"];
            
            globleObj.pickupLat = [objDic objectForKey:@"pickupfromlat"];
            
            globleObj.pickupLong = [objDic objectForKey:@"pickupfromlon"];
            
            globleObj.deliveryLat = [objDic objectForKey:@"deliverytolat"];
            
            globleObj.deliveryLong = [objDic objectForKey:@"deliverytolon"];
            
            globleObj.updatedOn = [objDic objectForKey:@"updatedon"];
            
            globleObj.shipmentType = [objDic objectForKey:@"shipmenttypeid"];
            
            globleObj.truckerId = [objDic objectForKey:@"truckerid"];
            
            globleObj.decription = [objDic objectForKey:@"description"];
            
            globleObj.userId = [objDic objectForKey:@"userid"];
            
            globleObj.bookingId = [objDic objectForKey:@"booking_id"];
            
            globleObj.bookName = [objDic objectForKey:@"user_name"];
            
            globleObj.userPhotoUrl = [NSString stringWithFormat:@"http://quikmov.quikmovapp.com%@",[objDic objectForKey:@"userfilepath"]];
            
            globleObj.userPhone = [objDic objectForKey:@"user_phone"];
            
            globleObj.usermailid = [objDic objectForKey:@"user_email"];
            
            [detailsarr addObject:globleObj];
            
            bookId = globleObj.bookingId;
            useriid = globleObj.truckerId;
            pickLat = globleObj.pickupLat;
            pickLong = globleObj.pickupLong;
            delvryLat = globleObj.deliveryLat;
            delvryLong = globleObj.deliveryLong;
            
        }
        
        [bookingView reloadData];
        [self callPickRevesecode];
        [self calldelryRevesecode];

        
    }else if ([Result isEqualToString:@"failure"]){
        
        
        
        
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@"Message"
                                      message:@"No Current Booking Available"
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
        
        
        [alert addAction:ok];
        
        [self presentViewController:alert animated:YES completion:nil];
        
        
    }
    
    else{
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Message" message:[NSString stringWithFormat:@"%@",[dic objectForKey:@"result"]] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
}

#pragma mark - tableView Functions.

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return detailsarr.count;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    // This will create a "invisible" footer
    return 0.001;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *MyIdentifier = @"MyIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:MyIdentifier];
    }
    
    for (UIView * view in cell.contentView.subviews)
    {
        [view removeFromSuperview];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    GlobalObjects * globalObj = (GlobalObjects*)[detailsarr objectAtIndex:indexPath.row];

//    cell.imageView.frame = CGRectMake(cell.contentView.frame.origin.x+ 5, cell.contentView.frame.origin.y+5, 50, 50);
//    [cell.imageView sd_setImageWithURL:[NSURL URLWithString:globalObj.userPhotoUrl] placeholderImage:[UIImage imageNamed:@"user.png"]];
//    [cell.contentView addSubview:cell.imageView];
    
    userprofimag = [DesignObj initWithImage:CGRectMake(20, 20, 50, 50) img:@""];
    [userprofimag sd_setImageWithURL:[NSURL URLWithString:globalObj.userPhotoUrl] placeholderImage:[UIImage imageNamed:@"user.png"]];
    [cell.contentView addSubview:userprofimag];
    
                                                                                                                                 
//                                                                                                                                        sd_setImageWithURL:[NSURL URLWithString:globalObj.userPhotoUrl] placeholderImage:[UIImage imageNamed:@"user.png"]]];
    
    bookingName = [DesignObj initWithLabel:CGRectMake(cell.imageView.frame.origin.x+ 50+30, 20, 100, 30) title:[NSString stringWithFormat:@"Name: %@",globalObj.bookName] font:12 txtcolor:[DesignObj quikRed]];
    bookingName.textAlignment = NSTextAlignmentLeft;
    [cell.contentView addSubview:bookingName];
    
    bookingStatus = [DesignObj initWithLabel:CGRectMake(bookingView.frame.size.width-120, 20, 100, 30) title:[NSString stringWithFormat:@"Status: %@",globalObj.bookingStatus] font:12 txtcolor:[DesignObj quikRed]];
    [cell.contentView addSubview:bookingStatus];
    
    phno = [NSString stringWithFormat:@"%@",globalObj.userPhone];
    
//    updateOn = [DesignObj initWithLabel:CGRectMake(cell.imageView.frame.origin.x+ 50+ 30, bookingName.frame.origin.y+30 +20,210, 30) title:[NSString stringWithFormat:@"Updated on: %@",globalObj.updatedOn] font:12 txtcolor:[DesignObj quikRed]];
//    updateOn.textAlignment = NSTextAlignmentLeft;
//    [cell.contentView addSubview:updateOn];
    
    return cell;
}

- (void)callPickRevesecode{
    reverseGeocoder = [[CLGeocoder alloc] init];
    
    CGFloat picksLat = (CGFloat)[pickLat floatValue];
    CGFloat picksLong = (CGFloat)[pickLong floatValue];
    
    
    locate = [[CLLocation alloc]initWithLatitude:picksLat longitude:picksLong];
    
    NSLog(@"%@",locate);
    
    [reverseGeocoder reverseGeocodeLocation:locate completionHandler:^(NSArray *placemarks, NSError *error) {
        NSLog(@"Finding address");
        if (error) {
            NSLog(@"Error %@", error.description);
        } else {
//            CLPlacemark *placemark = [placemarks lastObject];
            CLPlacemark *placemark = [placemarks objectAtIndex:0];
//            NSString* locations = [NSString stringWithFormat:@"%@",placemark.addressDictionary];
  
// Converting string to a normal string
            
            getPickAddr = [placemark.addressDictionary objectForKey:@"FormattedAddressLines"];
            
            bookingpickdetlsarr  = [[NSArray alloc]initWithObjects:getPickAddr, nil];
            convertpicStr = [bookingpickdetlsarr componentsJoinedByString:@"."];
            convertpicStr = [convertpicStr stringByReplacingOccurrencesOfString:@"\n" withString:@""];
            convertpicStr = [convertpicStr stringByReplacingOccurrencesOfString:@"\"" withString:@""];
            convertpicStr = [convertpicStr stringByReplacingOccurrencesOfString:@"    " withString:@""];
            convertpicStr = [convertpicStr stringByReplacingOccurrencesOfString:@"(" withString:@""];
            convertpicStr = [convertpicStr stringByReplacingOccurrencesOfString:@")" withString:@""];
            convertpicStr = [convertpicStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];

        }
    }];


}

-(void)calldelryRevesecode{
    
    reverseGeocoder = [[CLGeocoder alloc] init];
    
    CGFloat delryLat = (CGFloat)[delvryLat floatValue];
    CGFloat delryLong = (CGFloat)[delvryLong floatValue];
    
    CLLocation*lcate;
    lcate = [[CLLocation alloc]initWithLatitude:delryLat longitude:delryLong];
    
    NSLog(@"%@",lcate);
    
    [reverseGeocoder reverseGeocodeLocation:lcate completionHandler:^(NSArray *placemarks, NSError *error) {
        NSLog(@"Finding address");
        if (error) {
            NSLog(@"Error %@", error.description);
        } else {
            
            CLPlacemark *placemark = [placemarks objectAtIndex:0];
            
            // Converting string to a normal string
            
            getDelAddr = [placemark.addressDictionary objectForKey:@"FormattedAddressLines"];
            
            bookingdelrydetlsarr  = [[NSArray alloc]initWithObjects:getDelAddr, nil];
            convertdelStr = [bookingdelrydetlsarr componentsJoinedByString:@"."];
            convertdelStr = [convertdelStr stringByReplacingOccurrencesOfString:@"\n" withString:@""];
            convertdelStr = [convertdelStr stringByReplacingOccurrencesOfString:@"\"" withString:@""];
            convertdelStr = [convertdelStr stringByReplacingOccurrencesOfString:@"    " withString:@""];
            convertdelStr = [convertdelStr stringByReplacingOccurrencesOfString:@"(" withString:@""];
            convertdelStr = [convertdelStr stringByReplacingOccurrencesOfString:@")" withString:@""];
            convertdelStr = [convertdelStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            
        }
    }];

    
}

- (void)ShowDetailView:(NSInteger)tag{
    
    GlobalObjects * object = (GlobalObjects*)[detailsarr objectAtIndex:tag];
    
    shadowView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screenWidth, screenHeight)];
    shadowView.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.5f];
    [self.view addSubview:shadowView];
    
    shadowButton = [DesignObj initWithButton:CGRectMake(0, 0, screenWidth, screenHeight) tittle:@"" img:@""];
    [shadowButton setBackgroundColor:[UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.5f]];
    [shadowButton addTarget:self action:@selector(closePopView) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:shadowButton];
    
    popView = [[UIView alloc] initWithFrame:CGRectMake((screenWidth -(screenWidth-60))/2, ((screenHeight -(screenHeight-120))/2)+30, screenWidth-60, screenHeight-120)];
    popView.layer.cornerRadius = 9;
    [popView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg.png"]]];
    [self.view addSubview:popView];
    
    UIScrollView* scrView = [[UIScrollView alloc]init];
    scrView.frame = CGRectMake(0, 0, popView.frame.size.width, popView.frame.size.height);
    [popView addSubview:scrView];
    
    if (screenHeight > 480) {
        scrView.contentSize = CGSizeMake(popView.frame.size.width, popView.frame.size.height + 120);
    }
    
    profimg = [[UIImageView alloc]initWithFrame:CGRectMake((popView.frame.size.width-120)/2, 50, 120, 120)];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [profimg sd_setImageWithURL:[NSURL URLWithString:object.userPhotoUrl] placeholderImage:[UIImage imageNamed:@"user.png"]];
            });
    
    [scrView addSubview:profimg];
    
    name = [DesignObj initWithLabel:CGRectMake(((popView.frame.size.width -(popView.frame.size.width - 60))/2), profimg.frame.origin.y +120, popView.frame.size.width - 60, 30) title:[NSString stringWithFormat:@"Name: %@",object.bookName] font:16 txtcolor:[DesignObj quikRed]];
    name.textAlignment = NSTextAlignmentCenter;
    [scrView addSubview:name];
    
    phoneNo = [DesignObj initWithLabel:CGRectMake(((popView.frame.size.width -(popView.frame.size.width - 60))/2), name.frame.origin.y+30 +20, popView.frame.size.width - 60, 30) title:[NSString stringWithFormat:@"Phone No: %@",object.userPhone] font:18 txtcolor:[DesignObj quikRed]];
    phoneNo.textAlignment = NSTextAlignmentCenter;
    [scrView addSubview:phoneNo];
    
    emailid = [DesignObj initWithLabel:CGRectMake(((popView.frame.size.width -(popView.frame.size.width - 60))/2), phoneNo.frame.origin.y+30 +20, popView.frame.size.width - 60, 30) title:[NSString stringWithFormat:@"E-mail: %@",object.usermailid] font:18 txtcolor:[DesignObj quikRed]];
    emailid.textAlignment = NSTextAlignmentCenter;
    [scrView addSubview:emailid];
    
    pickUplocation = [DesignObj initWithLabel:CGRectMake(((popView.frame.size.width -(popView.frame.size.width - 130))/2), emailid.frame.origin.y+30 +20,popView.frame.size.width - 120, 90) title:[NSString stringWithFormat:@"%@",convertpicStr] font:18 txtcolor:[DesignObj quikRed]];
    NSLog(@"%@",pickUplocation.text);
    pickUplocation.textAlignment = NSTextAlignmentJustified;
    pickUplocation.lineBreakMode = NSLineBreakByWordWrapping;
    pickUplocation.numberOfLines = 4;
    [scrView addSubview:pickUplocation];
    
    deliveryLocation = [DesignObj initWithLabel:CGRectMake(((popView.frame.size.width -(popView.frame.size.width - 120))/2), pickUplocation.frame.origin.y+90 +20,popView.frame.size.width - 120, 90) title:[NSString stringWithFormat:@"Delivey Location: %@",[NSString stringWithFormat:@"%@",convertdelStr]] font:18 txtcolor:[DesignObj quikRed]];
    deliveryLocation.textAlignment = NSTextAlignmentJustified;
    deliveryLocation.lineBreakMode = NSLineBreakByWordWrapping;
    deliveryLocation.numberOfLines = 3;
    [scrView addSubview:deliveryLocation];
    
    confirmBtn = [DesignObj initWithButton:CGRectMake((popView.frame.size.width - 200)/2, deliveryLocation.frame.origin.y+90 +20, 200, 30) tittle:@"Confirm Booking" img:@"button.png"];
    confirmBtn.titleLabel.tintColor = [DesignObj quikYellow];
    confirmBtn.tag = 201;
    [confirmBtn addTarget:self action:@selector(confirmBook:) forControlEvents:UIControlEventTouchUpInside];
    [confirmBtn setTitleColor:[DesignObj quikYellow] forState:UIControlStateNormal];
    confirmBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [scrView addSubview:confirmBtn];
    
    cancelBtn = [DesignObj initWithButton:CGRectMake((popView.frame.size.width - 200)/2,confirmBtn.frame.origin.y+30 +20, 200, 30) tittle:@"Cancel Booking" img:@"button.png"];
    [cancelBtn setTitleColor:[DesignObj quikYellow] forState:UIControlStateNormal];
    cancelBtn.tag = 202;
    [cancelBtn addTarget:self action:@selector(confirmBook:) forControlEvents:UIControlEventTouchUpInside];
    cancelBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [scrView addSubview:cancelBtn];
    
    contactBtn = [DesignObj initWithButton:CGRectMake((popView.frame.size.width - 200)/2,cancelBtn.frame.origin.y+30 +20, 200, 30) tittle:@"Contact" img:@"button.png"];
    [contactBtn setTitleColor:[DesignObj quikYellow] forState:UIControlStateNormal];
    contactBtn.tag = 203;
    [contactBtn addTarget:self action:@selector(confirmBook:) forControlEvents:UIControlEventTouchUpInside];
    contactBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [scrView addSubview:contactBtn];
    
    closeView = [UIButton buttonWithType:UIButtonTypeCustom];
    closeView.frame = CGRectMake(popView.frame.size.width-30,0,30,30);
    [closeView addTarget:self action:@selector(closePopView) forControlEvents:UIControlEventTouchUpInside];
    [closeView setImage:[UIImage imageNamed:@"close.png"] forState:UIControlStateNormal];
    [popView addSubview:closeView];
    
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
//    [bookingView removeFromSuperview];
    bookingView.hidden = YES;
    [self ShowDetailView:indexPath.row];
    
}

-(IBAction)confirmBook:(id)sender{
    
    if ([sender tag]==201){
        
        bukStatus = @"confirmed";
        
        [self bookService];
    }else if([sender tag]==202){
        bukStatus = @"cancelled";
        
        [self bookService];
    }else{
        
        [self contactCustomer];
    }
}

-(void)contactCustomer{
    
    
    [UIView animateWithDuration:1.0 delay:2.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        
        
        
    } completion:^(BOOL finished) {
        
        //        popContactView = nil;
        popContactshadowView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screenWidth, screenHeight)];
        popContactshadowView.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.5f];
        [self.view addSubview:popContactshadowView];
        
        popContactshadowbtn = [DesignObj initWithButton:CGRectMake(0, 0, screenWidth, screenHeight) tittle:@"" img:@""];
        [popContactshadowbtn setBackgroundColor:[UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.5f]];
        [popContactshadowbtn addTarget:self action:@selector(closePopcontactView) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:popContactshadowbtn];
        
        popContactView = [[UIView alloc]initWithFrame:CGRectMake((screenWidth-300)/2, (screenHeight - 150)/2, 300, 150)];
        [popContactView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg.png"]]];
        popContactView.layer.cornerRadius = 9;
        [self.view addSubview:popContactView];
        
        callImg = [DesignObj initWithButton:CGRectMake(10, (75-40)/2, 40, 40) tittle:@"" img:@"call.png"];
        [callImg addTarget:self action:@selector(callAction:) forControlEvents:UIControlEventTouchUpInside];
        [popContactView addSubview:callImg];
        
        call = [DesignObj initWithButton:CGRectMake(0,0 , 300, 75) tittle:@"Call Trucker" img:@""];
        [call setTitleColor:[DesignObj quikRed] forState:UIControlStateNormal];
        //        [call setBackgroundColor:[UIColor greenColor]];
        [call addTarget:self action:@selector(callAction:) forControlEvents:UIControlEventTouchUpInside ];
        [popContactView addSubview:call];
        
        smsImg = [DesignObj initWithButton:CGRectMake(10, 150-60, 40, 40) tittle:@"" img:@"sms.png"];
        [smsImg addTarget:self action:@selector(smsAction) forControlEvents:UIControlEventTouchUpInside];
        [popContactView addSubview:smsImg];
        
        sms = [DesignObj initWithButton:CGRectMake(0, 75, 300, 75) tittle:@"SMS Trucker" img:@""];
        [sms setTitleColor:[DesignObj quikRed] forState:UIControlStateNormal];
        //        [sms setBackgroundColor:[DesignObj quikRed]];
        [sms addTarget:self action:@selector(smsAction) forControlEvents:UIControlEventTouchUpInside ];
        [popContactView addSubview:sms];
        
        popcontactClose = [UIButton buttonWithType:UIButtonTypeCustom];
        popcontactClose.frame = CGRectMake(popContactView.frame.size.width-20,0,20,20);
        [popcontactClose addTarget:self action:@selector(closePopcontactView) forControlEvents:UIControlEventTouchUpInside];
        [popcontactClose setImage:[UIImage imageNamed:@"close.png"] forState:UIControlStateNormal];
        [popContactView addSubview:popcontactClose];
        
    }];
    
    
}

-(IBAction)callAction:(id)sender{
    
    
    NSString * phoneNumber = [@"tel://" stringByAppendingString:[NSString stringWithFormat:@"%@",phno]];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
//    [self closePopcontactView];
//    [self confirmBook:sender];
}

-(void)smsAction{
    
    checkingAction = @"No";
    
    if(![MFMessageComposeViewController canSendText]) {
        UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Your device doesn't support SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [warningAlert show];
        return;
    }
    
    NSArray *recipents = @[phno];
    NSString *message = [NSString stringWithFormat:@" Hey Trucker am waiting for your Pick-Up"];
    
    MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
    messageController.messageComposeDelegate = self;
    [messageController setRecipients:recipents];
    [messageController setBody:message];
    
    // Present message view controller on screen
    [self presentViewController:messageController animated:YES completion:nil];
//    [self closePopcontactView];
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result
{
    switch (result) {
        case MessageComposeResultCancelled:
            break;
            
        case MessageComposeResultFailed:
        {
//            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Failed to send SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//            [warningAlert show];
            [self showAlert:@"Failed to send SMS!"];
            break;
        }
            
        case MessageComposeResultSent:
        {
            
//            UIAlertView *successAlert = [[UIAlertView alloc] initWithTitle:@"Success" message:@"Message Sent to Trucker!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//            [successAlert show];
            
            [self showAlert:@"Message Sent to Trucker!"];
        }
            break;
            
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
//    [self ]
}



-(void)closePopcontactView{
    
    [popContactshadowbtn removeFromSuperview];
    [popContactshadowView removeFromSuperview];
    [popContactView removeFromSuperview];
    [popcontactClose removeFromSuperview];
    
}


-(void)closePopView{
    
    bookingView.hidden = NO;
    
    [closeView removeFromSuperview];
    [popView removeFromSuperview];
    [shadowView removeFromSuperview];
    [shadowButton removeFromSuperview];
    
}


-(void)bookService{
    
    
        if ([Reachability reachabilityForInternetConnection]) {
    
            [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
            [SVProgressHUD show];
    
            NSURL* url = [NSURL URLWithString:@"http://quikmov.quikmovapp.com/auth/changeBooking" ];
            NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
            NSURLSession* session = [NSURLSession sessionWithConfiguration:config ];
    
            NSMutableURLRequest*request = [[NSMutableURLRequest alloc]initWithURL:url];
            request.HTTPMethod = @"POST";
    
            
            NSUserDefaults* userStoredetails = [NSUserDefaults standardUserDefaults];
            NSString * userID = [userStoredetails objectForKey:@"user_id"];
            NSLog(@"%@",userID);
            
            NSString* bodycntnt = [NSString stringWithFormat:@"userid=%@&bookingid=%@&bookingistatus=%@",userID,bookId,bukStatus];
    
            NSLog(@"%@",bodycntnt);
    
            NSData *data = [bodycntnt dataUsingEncoding:NSUTF8StringEncoding];
            
            NSURLSessionUploadTask *uploadtask = [session uploadTaskWithRequest:request fromData:data completionHandler:^(NSData*data, NSURLResponse*response, NSError*error){
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [SVProgressHUD dismiss];
    
                    NSString* str = [[NSString alloc]initWithData:data
                                                         encoding:NSUTF8StringEncoding];
    
                    NSError* error;
    
    
    
                    NSDictionary* dic = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    
                    NSLog(@"%@******%@,%@",str,[dic allValues],[dic objectForKey:@"response"]);
    
                    // userId = [[dic objectForKey:@"message"] stringValue];
                    NSString * Result = [dic objectForKey:@"status"];
    
                    NSLog(@"%@",Result);
    
                    if ([Result isEqualToString:@"success"]){
                        
                        
                        if ([bukStatus isEqualToString:@"confirmed"]) {
                            
                            UIAlertController * alert=   [UIAlertController
                                                          alertControllerWithTitle:@"Message"
                                                          message:@"Booked Successfully"
                                                          preferredStyle:UIAlertControllerStyleAlert];
                            
                            UIAlertAction* ok = [UIAlertAction
                                                 actionWithTitle:@"OK"
                                                 style:UIAlertActionStyleDefault
                                                 handler:^(UIAlertAction * action)
                                                 {
                                                     [self closePopView];
                                                     bookingView.hidden = YES;
                                                     //
                                                     [self loadAnnotations];
                                                     
                                                 }];
                            
                            
                            [alert addAction:ok];
                            
                            [self presentViewController:alert animated:YES completion:nil];
                            
                        }else if ([bukStatus isEqualToString:@"cancelled"]){
                        
                        UIAlertController * alert=   [UIAlertController
                                                      alertControllerWithTitle:@"Message"
                                                      message:@"Booking cancelled Successfully"
                                                      preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction* ok = [UIAlertAction
                                             actionWithTitle:@"OK"
                                             style:UIAlertActionStyleDefault
                                             handler:^(UIAlertAction * action)
                                             {
                                                 [self closePopView];
                                                 [bookingView removeFromSuperview];
                                                 [self loadBookingService];
                                                 
                                                
//                                                 if (bookingView == nil) {
//                                                     [self showAlert:@"No Current Booking Available"];
//                                                     bookingView.hidden = YES;
//                                                 }else{
                                                 
                                                     bookingView.hidden = YES;// it was NO
//                                                 }
                                                 
                                                 
                                             }];
                        
                        
                        [alert addAction:ok];
                        
                        [self presentViewController:alert animated:YES completion:nil];
    
                    }else{
                        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Message" message:[NSString stringWithFormat:@"%@",[dic objectForKey:@"result"]] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                        
                        [popView removeFromSuperview];
                        [closeView removeFromSuperview];
                        [shadowView removeFromSuperview];
                        [shadowButton removeFromSuperview];
                        [alert show];
                    
                    }
                    }
                    
                });
                
                
            }];
            [uploadtask resume];
        }else{
            [self showAlert:@"No Internet Connectivity"];
        }
    
}


// Annotation View


-(void)loadAnnotations{
    
//    [pickLat floatValue];
    
    // if bust latitude is zero we need to load only stops
    
    
        CLLocationCoordinate2D theCoordinate1;
    

    
    theCoordinate1 = CLLocationCoordinate2DMake([pickLat floatValue],[pickLong floatValue]);
    
        marker = [GMSMarker markerWithPosition:theCoordinate1];
        //        marker.title =  [NSString stringWithFormat:@"%d",i];
        marker.icon = [UIImage imageNamed:@"user.png"];
        marker.appearAnimation = kGMSMarkerAnimationPop;
        marker.title =@"Pick-Up Location";
        marker.snippet = [NSString stringWithFormat:@"%@",convertpicStr];
        marker.map = mapView_;
    
    [self getMapPointsfromService:[lat floatValue] originlon:[log floatValue] withDestintionlat:[pickLat floatValue] destlon:[pickLong floatValue]];
    
//    [self mapService];
    
//    GMSMutablePath *path = [GMSMutablePath path];
//    [path addCoordinate:CLLocationCoordinate2DMake([pickLat floatValue], [pickLong floatValue])];
//    
//    polyLine = [GMSPolyline polylineWithPath:path];
//    polyLine.map = mapView_;
//    polyLine.strokeColor = [UIColor blackColor];
//    polyLine.map = mapView_;
    
//    GMSCameraUpdate* cameraUpdate = [GMSCameraUpdate setTarget:theCoordinate1];
//    [mapView_ animateWithCameraUpdate:cameraUpdate];
    
        
    
}

- (void)showAlert:(NSString *)message{
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Message"
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [alert dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    
    
    [alert addAction:ok];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)getMapPointsfromService:(CGFloat)originLat originlon:(CGFloat)originLon withDestintionlat:(CGFloat)destLat destlon:(CGFloat)destLon{
    
    
    serviceFlag = @"drawline";
    NSString * urlStr = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/directions/json?sensor=true&destination=%f%@%f&origin=%f%@%f",destLat,@"%2C",destLon,originLat,@"%2C",originLon];
    
    
//    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
//    [SVProgressHUD show];
    // 1
    
    //    mobile,password
    //    url : http://truckcsoft.mapcee.com/auth/login
    NSURL *url = [NSURL URLWithString:urlStr];
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    // 2
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    request.HTTPMethod = @"POST";
    
    // 3
    
    NSString * bodyMsg = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/directions/json?sensor=true&destination=%f%@%f&origin=%f%@%f",destLat,@"%2C",destLon,originLat,@"%2C",originLon];
    
    
    
    NSData * data = [bodyMsg dataUsingEncoding:NSUTF8StringEncoding];
    
    // 4
    NSURLSessionUploadTask *uploadTask = [session uploadTaskWithRequest:request
                                                               fromData:data completionHandler:^(NSData *data,NSURLResponse *response,NSError *error) {
                                                                   // Handle response here
                                                                   dispatch_async(dispatch_get_main_queue(), ^{
//                                                                       [SVProgressHUD dismiss];
                                                                       [self DownloadfileCompleted:data];
                                                                   });
                                                                   
                                                               }];
    
    // 5
    [uploadTask resume];
}

#pragma Mark - parser delegate
-(void)DownloadfileCompleted:(NSData*)responseData{
    
//    [SVProgressHUD dismiss];
    
    NSString* newStr = [[NSString alloc] initWithData:responseData
                                             encoding:NSUTF8StringEncoding];
    if (responseData == nil) {
        return;
    }
    
    NSError* error;
    NSDictionary* dic = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions
                                                          error:&error];
    NSLog(@"%@******%@",newStr,[dic allValues]);
    
    NSArray * arr = [dic objectForKey:@"routes"];
    [stepsArr removeAllObjects];
    stepsArr = [NSMutableArray new];
    
    for (int i = 0; i < [arr count]; i++) {
        
        //
//        GlobalObjects * globleObj = [[GlobalObjects alloc]init];
        NSDictionary * objDic = [arr objectAtIndex:i];

        place_id = objDic[@"overview_polyline"][@"points"];
        
        distanceiD = objDic[@"legs"][0][@"distance"][@"text"];

        minutees = objDic[@"legs"][0][@"duration"][@"text"];
        
        NSLog(@"%@",distanceiD);
        NSLog(@"%@",minutees);
        
    }

    if ([serviceFlag isEqualToString:@"drawline"]) {
        _path = [MapViewDrawLine parseResponse:dic];
        
        serviceCount = serviceCount + 1;
//        [self loadAnotherMappoints:serviceCount];
        
        if (_path.count > 0) {
            [self drawLine];
            [self showTriggeringView];
        }
    }
}

-(void)showTriggeringView{
    
    trigView = [[UIView alloc]initWithFrame:CGRectMake(0, 64, screenWidth, 150)];
    trigView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:trigView];
    
    milesLbl = [DesignObj initWithLabel:CGRectMake(30, 10, 100, 40) title:[NSString stringWithFormat:@"%@",distanceiD] font:18 txtcolor:[UIColor grayColor]];
    [milesLbl setTextAlignment:NSTextAlignmentLeft];
    [trigView addSubview:milesLbl];
    
    minuteLbl = [DesignObj initWithLabel:CGRectMake(screenWidth - 180, 10, 150, 40) title:[NSString stringWithFormat:@"%@",minutees] font:18 txtcolor:[UIColor grayColor]];
    [minuteLbl setTextAlignment:NSTextAlignmentRight];
    [trigView addSubview:minuteLbl];
    
    iconCustmr = [DesignObj initWithImage:CGRectMake(30, trigView.frame.size.height - 95, 35, 35) img:@"user.png"];
    [trigView addSubview:iconCustmr];
    
    pickUpLocationLbl = [DesignObj initWithLabel:CGRectMake((screenWidth - 125)/2, 35, 125, 40) title:@"Pick-Up Location" font:16 txtcolor:[DesignObj quikRed]];
    [pickUpLocationLbl setTextAlignment:NSTextAlignmentCenter];
    [trigView addSubview:pickUpLocationLbl];
    
    areaLbl = [DesignObj initWithLabel:CGRectMake((screenWidth - (screenWidth - 10))/2, trigView.frame.size.height - 70, screenWidth-10, 60) title:[NSString stringWithFormat:@"%@",convertpicStr] font:16 txtcolor:[UIColor grayColor]];
    areaLbl.numberOfLines = 3;
    [areaLbl setTextAlignment:NSTextAlignmentCenter];
    [trigView addSubview:areaLbl];
    
    
}

-(void)drawLine{
    
//    [mapView_ clear];
    
    CLLocationCoordinate2D theCoordinate1;
   
    theCoordinate1 = CLLocationCoordinate2DMake([pickLat floatValue],[pickLong floatValue]);
    
//    CLLocation *location1 =[[CLLocation alloc] initWithLatitude:[pickLat floatValue] longitude:[pickLong floatValue]];
//    
//    CLLocation *loc2 = [[CLLocation alloc] initWithLatitude:[lat doubleValue] longitude:[log doubleValue]];
//    
//    CLLocationDistance distanceMeter = [location1 distanceFromLocation:loc2];
//    
    GMSPath *path = [GMSPath pathFromEncodedPath:place_id];
//    [path addCoordinate:CLLocationCoordinate2DMake([lat floatValue], [log floatValue])];
//    [path addCoordinate:CLLocationCoordinate2DMake([pickLat floatValue], [pickLong floatValue])];
    
        polyLine = [GMSPolyline polylineWithPath:path];
        polyLine.map = mapView_;
        polyLine.strokeColor = [DesignObj quikRed];
        polyLine.strokeWidth = 4.0f;
        polyLine.map = mapView_;
    
    
    
    GMSCameraUpdate* cameraUpdate = [GMSCameraUpdate setTarget:theCoordinate1 zoom:12];
    [mapView_ animateWithCameraUpdate:cameraUpdate];
    
}

-(void)uploadcurrentLocation{
    
    if ([Reachability reachabilityForInternetConnection]) {
        
//        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
        //        [SVProgressHUD show];
        
        NSURL* url = [NSURL URLWithString:@"http://quikmov.quikmovapp.com/auth/userLocation" ];
        NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession* session = [NSURLSession sessionWithConfiguration:config ];
        
        NSMutableURLRequest*request = [[NSMutableURLRequest alloc]initWithURL:url];
        request.HTTPMethod = @"POST";
        
        //        inputs : userid,lat,lon,status(1 or 0)
        //        url : http://truckcsoft.mapcee.com/auth/userLocation
        
        NSUserDefaults* userStoredetails = [NSUserDefaults standardUserDefaults];
        useriid = [userStoredetails objectForKey:@"user_id"];
        NSLog(@"%@",useriid);
        
        NSString* bodycntnt = [NSString stringWithFormat:@"userid=%@&lat=%@&lon=%@&status=%@",useriid,lat,log,statusCheck];
        
        NSLog(@"%@",bodycntnt);
        
        NSData *data = [bodycntnt dataUsingEncoding:NSUTF8StringEncoding];
        
        NSURLSessionUploadTask *uploadtask = [session uploadTaskWithRequest:request fromData:data completionHandler:^(NSData*data, NSURLResponse*response, NSError*error){
            dispatch_async(dispatch_get_main_queue(), ^{
//                [SVProgressHUD dismiss];
                
                NSString* str = [[NSString alloc]initWithData:data
                                                     encoding:NSUTF8StringEncoding];
                
                NSError* error;
                
                if (str.length == 0) {
                    [self showAlert:@"Server error\nPlease Quit the app and Try again "];
                    return;
                }
                
                NSDictionary* dic = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                
                NSLog(@"%@******%@,%@",str,[dic allValues],[dic objectForKey:@"response"]);
                
                // userId = [[dic objectForKey:@"message"] stringValue];
                NSString * Result = [dic objectForKey:@"status"];
                
                NSLog(@"%@",Result);
                
            });
            
            
        }];
        [uploadtask resume];
    }
    
    
}

- (void)didSelectedOption:(NSIndexPath*)indexPath withTitle:(NSString*)title{
    
    [self closeMenu];
    
    if (indexPath.row == 0) {
        
        ProfileViewController* profileView = [[ProfileViewController alloc]init];
    
        [self.navigationController pushViewController:profileView animated:YES];
        
    }else if (indexPath.row == 1){
        [self showAlert:@"Will be available in Next link"];
        
    }else if (indexPath.row == 2) {
        
        [self showAlert:@"Will be available in Next link"];
        
    }else if (indexPath.row == 3){
        
        [self showAlert:@"Will be available in Next link"];
        
    }else if (indexPath.row == 5) {
        
        [self showAlert:@"Will be available in Next link"];
        
    }else if (indexPath.row == 4) {
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@"Message"
                                      message:@"Do you really want to quit the QuikMov?"
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 
                                 exit(0);
                                 
                             }];
        
        UIAlertAction* cancel = [UIAlertAction
                                 actionWithTitle:@"Cancel"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     
                                     [self dismissViewControllerAnimated:YES completion:^{
                                         
                                     }];
                                     
                                 }];
        
        
        [alert addAction:ok];
        [alert addAction:cancel];
        
        [self presentViewController:alert animated:YES completion:nil];
        
        
    }
}


-(void)updateBooking{
    
    NSURL* url = [NSURL URLWithString:@"http://quikmov.quikmovapp.com/auth/viewBooking"];
    NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession* session = [NSURLSession sessionWithConfiguration:config ];
    
    NSMutableURLRequest*request = [[NSMutableURLRequest alloc]initWithURL:url];
    request.HTTPMethod = @"POST";
    
    //    LoginViewController* log = [[LoginViewController alloc]init];
    //    log.usrId;
    
    NSUserDefaults* userStoredetails = [NSUserDefaults standardUserDefaults];
    NSString * userID = [userStoredetails objectForKey:@"user_id"];
    NSLog(@"%@",userID);
    NSString* bodycntnt = [NSString stringWithFormat:@"userid=%@",userID];
    
    NSData *data = [bodycntnt dataUsingEncoding:NSUTF8StringEncoding];
    
    NSURLSessionUploadTask *uploadtask = [session uploadTaskWithRequest:request fromData:data completionHandler:^(NSData*data, NSURLResponse*response, NSError*error){
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if (data == nil) {
                return;
            }
            NSString* str = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
            NSError * error;
            NSDictionary* dic = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            NSLog(@"%@******%@,%@",str,[dic allValues],[dic objectForKey:@"response"]);
            
            NSString * Result = [dic objectForKey:@"status"];
            
            NSLog(@"%@",Result);
            if ([Result isEqualToString:@"success"]){
                
                
                
                bookingView = [DesignObj initWithTableView:self
                                                     frame:CGRectMake((screenWidth -(screenWidth-60))/2, ((screenHeight -(screenHeight-120))/2)+30, screenWidth-60, screenHeight-120)];
                //self.bookingView.backgroundColor = [UIColor ];
                bookingView.rowHeight = 120.0f;
                bookingView.delegate = self;
                bookingView.dataSource = self;
                bookingView.separatorColor = [DesignObj quikRed];
                bookingView.layer.borderWidth = 0.5f;
                bookingView.layer.cornerRadius = 9;
                bookingView.layer.borderColor =[[DesignObj quikRed]CGColor];
                [self.view addSubview:bookingView];
                
                NSArray * arr = [dic objectForKey:@"result"];
                
                [detailsarr removeAllObjects];
                
                for (int i = 0; i < [arr count]; i++) {
                    
                    //
                    GlobalObjects * globleObj = [[GlobalObjects alloc]init];
                    NSDictionary * objDic = [arr objectAtIndex:i];
                    
                    globleObj.bookingStatus = [objDic objectForKey:@"bookingstatus"];
                    
                    globleObj.pickupLat = [objDic objectForKey:@"pickupfromlat"];
                    
                    globleObj.pickupLong = [objDic objectForKey:@"pickupfromlon"];
                    
                    globleObj.deliveryLat = [objDic objectForKey:@"deliverytolat"];
                    
                    globleObj.deliveryLong = [objDic objectForKey:@"deliverytolon"];
                    
                    globleObj.updatedOn = [objDic objectForKey:@"updatedon"];
                    
                    globleObj.shipmentType = [objDic objectForKey:@"shipmenttypeid"];
                    
                    globleObj.truckerId = [objDic objectForKey:@"truckerid"];
                    
                    globleObj.decription = [objDic objectForKey:@"description"];
                    
                    globleObj.userId = [objDic objectForKey:@"userid"];
                    
                    globleObj.bookingId = [objDic objectForKey:@"booking_id"];
                    
                    globleObj.bookName = [objDic objectForKey:@"user_name"];
                    
                    globleObj.userPhotoUrl = [NSString stringWithFormat:@"http://quikmov.quikmovapp.com%@",[objDic objectForKey:@"userfilepath"]];
                    
                    globleObj.userPhone = [objDic objectForKey:@"user_phone"];
                    
                    globleObj.usermailid = [objDic objectForKey:@"user_email"];
                    
                    [detailsarr addObject:globleObj];
                    
                    bookId = globleObj.bookingId;
                    useriid = globleObj.truckerId;
                    pickLat = globleObj.pickupLat;
                    pickLong = globleObj.pickupLong;
                    delvryLat = globleObj.deliveryLat;
                    delvryLong = globleObj.deliveryLong;
                    
                }
                
                [bookingView reloadData];
                [self callPickRevesecode];
                [self calldelryRevesecode];
                
                
            }else if ([Result isEqualToString:@"failure"]){
                
                
                
                
                
                UIAlertController * alert=   [UIAlertController
                                              alertControllerWithTitle:@"Message"
                                              message:@"No Current Booking Available"
                                              preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* ok = [UIAlertAction
                                     actionWithTitle:@"OK"
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         [alert dismissViewControllerAnimated:YES completion:nil];
                                         
                                     }];
                
                
                [alert addAction:ok];
                
                [self presentViewController:alert animated:YES completion:nil];
                
                
            }
            
            else{
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Message" message:[NSString stringWithFormat:@"%@",[dic objectForKey:@"result"]] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alert show];
            }


        });
    }];
    
    [uploadtask resume];
    
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
