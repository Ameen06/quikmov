//
//  CSMarker.h
//  QuikMov
//
//  Created by Ajmal Khan on 3/1/16.
//  Copyright © 2016 CSCS. All rights reserved.
//

#import <GoogleMaps/GoogleMaps.h>

@interface CSMarker : GMSMarker

@property(nonatomic,copy)NSString* objectID;

@end
