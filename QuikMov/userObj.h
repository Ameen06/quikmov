//
//  userObj.h
//  QuikMov
//
//  Created by CSCS on 11/03/16.
//  Copyright © 2016 CSCS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface userObj : NSObject

@property(nonatomic,copy)NSString* currentUsertype;
@property(nonatomic,copy)NSString* latitudee;
@property(nonatomic,copy)NSString* longitudee;
@property(nonatomic,copy)NSString* username;
@property(nonatomic,copy)NSString* usermailid;
@property(nonatomic,copy)NSString* userPhone;
@property(nonatomic,copy)NSString* userDistance;
@property(nonatomic,copy)NSString* userPhotoUrl;

@end
