//
//  MapViewController.m
//  QuikMov
//
//  Created by CSCS on 1/27/16.
//  Copyright © 2016 CSCS. All rights reserved.
//

#import "MapViewController.h"
#import "SVProgressHUD.h"
#import "Reachability.h"
#import "GlobalObjects.h"
#import "LoginViewController.h"
#import "DesignObj.h"
#import "WildcardGestureRecognizer.h"
#import "MHGallery.h"

#define SHAWDOW_ALPHA 0.5
#define MENU_DURATION 0.3
#define MENU_TRIGGER_VELOCITY 350

@interface MapViewController ()<UINavigationControllerDelegate>{
    
    CLLocationCoordinate2D* coordinates;
    CLGeocoder* forwardGeocoder;
    NSString* lat;
    NSString* log;
    MKMapView* mapView;
    NSString* statusCheck;
    MKPointAnnotation* myAnnotation;
    NSMutableArray * annotations;
    NSString* forwardlat;
    NSString* forwardlong;
    UIButton* setCurrentLocation;
    
    
    NSMutableArray * objectsArr;
    NSMutableArray * latPosition;
    NSMutableArray * longPosition;
    NSMutableArray * name;
    NSMutableArray * mailid;
    NSMutableArray * phoneno;
    
    UIView* shadowView;
    UIView* popView;
    UIButton* closeView;
    UIButton* shadowButton;
    
    NSString * userID;
    NSString*userdistance;
    NSString* selectType;
    
    UIButton* viewimage1;
    UIButton* viewimage2;
    UIButton* viewimage3;
    UIButton* viewimage4;
    
    UIImageView * profimg;
    UIImageView * profimg2;
    UIImageView * profimg3;
    UIImageView * profimg4;
    
    UISwitch* switchStatus;
    UIView* hideMapview;
    
    QuickMenuViewController * menuView;
    UISwipeGestureRecognizer* swipetoRight;
    UIButton *listButton;
    
    UITableView* listtableView;
    UIButton* tblcloseView;
    UIButton* tableShaddowview;
    
    UIView* panshadowView;
    BOOL isOPen;
    
    UIView* bookshadowView;
    UIView* bookpopView;
    UIButton* bookcloseView;
    UIButton* bookshadowButton;
    UITextField* deliveryArea;
    UITextField* shipmentType;
    UITextField* Description;
    UIButton* confirmBooking;
    UIImageView * txtbgImg1;
    UIImageView * txtbgImg2;
    UIImageView * txtbgImg3;
    
    UIButton* closingMenu;
    NSString* trukrId;


    
}
@property (strong, nonatomic)UIScrollView* scrView;
@property (nonatomic) CGRect outFrame;
@property (nonatomic) CGRect inFrame;


@end

@implementation MapViewController
@synthesize locationManager;
@synthesize userType;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    screenWidth = [UIScreen mainScreen].bounds.size.width;
    screenHeight = [UIScreen mainScreen].bounds.size.height;

    self.navigationController.navigationBarHidden = NO;
    
    self.navigationItem.hidesBackButton = YES;
    
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [DesignObj quikYellow]}];
    self.navigationController.navigationBar.barTintColor = [DesignObj quikRed];

    
    self.title = @"QuikMov";
    
    mapView = [[MKMapView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, screenHeight)];
    [mapView setShowsUserLocation:YES];
    mapView.zoomEnabled = YES;
    [mapView setDelegate:self];
    mapView.mapType = MKMapTypeStandard;
    mapView.showsCompass = NO;
    [self.view addSubview:mapView];
    
    
//    [self getcurrentLocation];
    locationManager = [[CLLocationManager alloc]init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >=8) {
        [self.locationManager requestWhenInUseAuthorization];
    }
    [locationManager startUpdatingLocation];
    
    annotations = [[NSMutableArray alloc]init];
    
    NSLog(@"%@",[NSString stringWithFormat:@"latitude: %f longitude: %f", locationManager.location.coordinate.latitude, locationManager.location.coordinate.longitude]);
    
    lat = [NSString stringWithFormat:@"%f",locationManager.location.coordinate.latitude];
    log = [NSString stringWithFormat:@"%f",locationManager.location.coordinate.longitude];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self action:@selector(menuOpen) forControlEvents:UIControlEventTouchUpInside]; //adding action
    [button setTitleColor:[DesignObj quikYellow] forState:UIControlStateNormal];
    [button setImage:[UIImage imageNamed:@"menu.png"] forState:UIControlStateNormal];
    button.frame = CGRectMake(0 ,0,15,35);
    
    UIBarButtonItem* barbtn = [[UIBarButtonItem alloc]initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = barbtn;

    if ([userType isEqualToString:@"1"]) {
        
        listButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [listButton addTarget:self action:@selector(listOpen:) forControlEvents:UIControlEventTouchUpInside]; //adding action
        [listButton setTitleColor:[DesignObj quikYellow] forState:UIControlStateNormal];
        [listButton setImage:[UIImage imageNamed:@"list.png"] forState:UIControlStateNormal];
        listButton.frame = CGRectMake(screenWidth-60 ,80,45,45);
        [self.view addSubview:listButton];
    }else{
        
        
    }
    
    NSUserDefaults * user = [NSUserDefaults standardUserDefaults];

    if ([userType isEqualToString:@"2"]) {
        
    switchStatus= [[UISwitch alloc]initWithFrame:CGRectMake(screenWidth-70, 84, 70, 30)];
    [switchStatus setOn:[user boolForKey:@"toggle"]];
    [switchStatus setTintColor:[DesignObj quikYellow]];
    [switchStatus setOnTintColor:[DesignObj quikYellow]];
    [switchStatus addTarget:self action:@selector(switchToggled:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:switchStatus];
        
        
    }else{
        switchStatus.hidden =YES;
        [hideMapview removeFromSuperview ];
        
        }
    
    
    UIBarButtonItem* rightbarbtn = [[UIBarButtonItem alloc]initWithCustomView:switchStatus];
    self.navigationItem.rightBarButtonItem = rightbarbtn;
    

    UISegmentedControl *segmentControl = [[UISegmentedControl alloc]initWithItems:@[@"Terrain",@"Satelite"]];
    
    [segmentControl setBackgroundColor:[UIColor whiteColor]];
    segmentControl.frame = CGRectMake((screenWidth-200)/2, screenHeight-40, 200, 25);
    [segmentControl setTintColor:[DesignObj quikRed]];
    segmentControl.backgroundColor= [DesignObj quikYellow];
    [segmentControl addTarget:self action:@selector(segmentedControlValueDidChange:) forControlEvents:UIControlEventValueChanged];
    [segmentControl setSelectedSegmentIndex:0];
    [self.view addSubview:segmentControl];
    


    [self uploadcurrentLocation];
    
    swipetoRight = [[UISwipeGestureRecognizer alloc]init];
    swipetoRight.direction = UISwipeGestureRecognizerDirectionRight;
    [swipetoRight addTarget:self action:@selector(swipemenuOpen:)];
    [self.view addGestureRecognizer:swipetoRight];
    
//    [NSTimer scheduledTimerWithTimeInterval:20.0
//                                     target:self
//                                   selector:@selector(uploadcurrentLocation)
//                                   userInfo:nil
//                                    repeats:YES];
    
    panshadowView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screenWidth, screenHeight)];
    panshadowView.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.0];
    panshadowView.hidden = YES;
    UITapGestureRecognizer *tapIt = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                            action:@selector(tapOnShawdow:)];
    [panshadowView addGestureRecognizer:tapIt];
    
    panshadowView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:panshadowView];
    
    menuView = [[QuickMenuViewController alloc]init];
    menuView.view.frame = CGRectMake(- screenWidth, 0, screenWidth, screenHeight);
    menuView.view.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.0];
    menuView.delagte = self;
    [self.view addSubview:menuView.view];
    
    UIView * pangestureView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 20, screenHeight)];
    pangestureView.backgroundColor =[UIColor clearColor];
    [self.view addSubview:pangestureView];
    
    self.panGest = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(moveDrawer:)];
    self.panGest.maximumNumberOfTouches = 1;
    self.panGest.minimumNumberOfTouches = 1;
    //self.pan_gr.delegate = self;
    [pangestureView addGestureRecognizer:self.panGest];
    
//    WildcardGestureRecognizer * tapInterceptor = [[WildcardGestureRecognizer alloc] init];
//    tapInterceptor.touchesBeganCallback = ^(NSSet * touches, UIEvent * event) {
    
//        NSArray * touchArr = [touches allObjects];
//        NSLog(@"%@",touches);
//
//        for (UITouch * touch in touchArr) {
//             CGPoint point = [touch locationInView:mapView];
//              NSLog(@"%f",point.x);
//            if (point.x < 20) {
//                //[self menuOpen];
//                [self moveDrawer:nil];
//            }
//        }
//

       // NSLog(@"%@",touches);
        
//    };
//    [mapView addGestureRecognizer:tapInterceptor];
    

    setCurrentLocation= [DesignObj initWithButton:CGRectMake(screenWidth-50, screenHeight-40, 30, 30) tittle:@"" img:@"mark30.png"];
    [setCurrentLocation addTarget:self action:@selector(showCurrentLocation:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:setCurrentLocation];

       }

-(void)getcurrentLocation{
    
    locationManager = [[CLLocationManager alloc]init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >=8) {
        [self.locationManager requestWhenInUseAuthorization];
    }
    [locationManager startUpdatingLocation];
    
    annotations = [[NSMutableArray alloc]init];
    
    NSLog(@"%@",[NSString stringWithFormat:@"latitude: %f longitude: %f", locationManager.location.coordinate.latitude, locationManager.location.coordinate.longitude]);
    
    lat = [NSString stringWithFormat:@"%f",locationManager.location.coordinate.latitude];
    log = [NSString stringWithFormat:@"%f",locationManager.location.coordinate.longitude];
    
    [self showSearchLocation:locationManager.location.coordinate.latitude lon:locationManager.location.coordinate.longitude];
}

-(void)swipemenuOpen:(UISwipeGestureRecognizer*)rightgesture{
    
    
    [self.navigationController.view addSubview:menuView.view];
    float duration = MENU_DURATION/menuView.view.frame.size.width*fabs(menuView.view.center.x)+MENU_DURATION/2; // y=mx+c
    // shawdow
    
    [UIView animateWithDuration:duration
                          delay:0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         closingMenu = [DesignObj initWithButton:CGRectMake(screenWidth-50, 0, 50, screenHeight) tittle:@"" img:@""];
                         [closingMenu addTarget:self action:@selector(closeMenu) forControlEvents:UIControlEventTouchUpInside];
                         [menuView.view addSubview:closingMenu];
                         panshadowView.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:SHAWDOW_ALPHA];
                     }
                     completion:nil];
    panshadowView.hidden = NO;
    // drawer
    [UIView animateWithDuration:duration
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         menuView.view.frame = CGRectMake(0, 0, menuView.view.frame.size.width, menuView.view.frame.size.height);
                     }
                     completion:nil];
    
    isOPen= YES;

    
    
    
}

- (void) switchToggled:(id)sender {
    
    NSUserDefaults * user = [NSUserDefaults standardUserDefaults];
    
    switchStatus = (UISwitch *)sender;
    
    if ([switchStatus isOn]) {
        
        [user setBool:YES forKey:@"toggle"];
        statusCheck =@"1";
        hideMapview.hidden = YES;
        [self uploadcurrentLocation];
        [self showAlert:@"You are Active now\n(Now you are visible to your Customers)"];
        
    } else {
        
        [user setBool:NO forKey:@"toggle"];
        statusCheck =@"0";
        hideMapview.hidden = NO;
        [self uploadcurrentLocation];
        [self showAlert:@"You are InActive now\n(The Customers can't see and Book you)"];
        
    }
    [user synchronize];
}



-(void)segmentedControlValueDidChange:(UISegmentedControl *)segment
{
    switch (segment.selectedSegmentIndex) {
        case 0:{
            
            mapView.mapType = MKMapTypeStandard;
            break;
        }
        case 1:{
            mapView.mapType = MKMapTypeSatelliteFlyover;
            break;
        }
    }
}

-(IBAction)listOpen:(id)sender{
        
    tableShaddowview = [UIButton buttonWithType:UIButtonTypeCustom];
    tableShaddowview.frame = CGRectMake(0,0,screenWidth,screenHeight);
    [tableShaddowview addTarget:self action:@selector(closetblView) forControlEvents:UIControlEventTouchUpInside];
    tableShaddowview.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.3];
    [self.view addSubview:tableShaddowview];
    
    _scrView = [[UIScrollView alloc]init];
    _scrView.frame = CGRectMake(0, 0, listtableView.frame.size.width, listtableView.frame.size.height);
    [listtableView addSubview:_scrView];
    
    
    listtableView = [DesignObj initWithTableView:self frame:CGRectMake((screenWidth - (screenWidth - 40))/2, (screenHeight - (screenHeight - 150))/2,screenWidth - 40, screenHeight - 150)];
    [listtableView setBackgroundColor:[UIColor whiteColor]];
    listtableView.dataSource =self;
    listtableView.delegate = self;
    listtableView.rowHeight = 70.0f;
    [self.view addSubview:listtableView];
    
    
    tblcloseView = [UIButton buttonWithType:UIButtonTypeCustom];
    tblcloseView.frame = CGRectMake(screenWidth-40,65,40,40);
    [tblcloseView addTarget:self action:@selector(closetblView) forControlEvents:UIControlEventTouchUpInside];
    [tblcloseView setImage:[UIImage imageNamed:@"close.png"] forState:UIControlStateNormal];
    [self.view addSubview:tblcloseView];
    
//    [self.panGest setEnabled:NO];
    
    
    
}

-(void)closetblView{
    
    [listtableView removeFromSuperview];
    [tblcloseView removeFromSuperview];
    [tableShaddowview removeFromSuperview];
}

-(void)showCurrentLocation:(id)sender{
    

    [self getcurrentLocation];
    
    
}

-(void)menuOpen{

/*   if (menuView == nil) {
        menuView = [[QuickMenuViewController alloc]init];
        menuView.view.frame = CGRectMake(- screenWidth, 0, screenWidth, screenHeight);
        menuView.view.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.1];
        menuView.delagte = self;
        
        closingMenu = [DesignObj initWithButton:CGRectMake(screenWidth-50, 0, 50, screenHeight) tittle:@"" img:@""];
        [closingMenu addTarget:self action:@selector(closeMenu) forControlEvents:UIControlEventTouchUpInside];
        [menuView.view addSubview:closingMenu];
        
        [self.navigationController.view addSubview:menuView.view];

        [UIView animateWithDuration:0.5 animations:^{
            
            menuView.view.frame = CGRectMake(0, 0, screenWidth, screenHeight);
            menuView.view.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.5];
            
        }];
    }else{
        
        [UIView animateWithDuration:0.5 animations:^{
            menuView.view.frame = CGRectMake(- screenWidth, 0, screenWidth, screenHeight);
        
        } completion:^(BOOL finished) {
            [menuView.view removeFromSuperview];
            menuView = nil;
        }];*/
    
//}
    [self.navigationController.view addSubview:menuView.view];
    float duration = MENU_DURATION/menuView.view.frame.size.width*fabs(menuView.view.center.x)+MENU_DURATION/2; // y=mx+c
    // shawdow
    
    [UIView animateWithDuration:duration
                          delay:0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         closingMenu = [DesignObj initWithButton:CGRectMake(screenWidth-50, 0, 50, screenHeight) tittle:@"" img:@""];
                         [closingMenu addTarget:self action:@selector(closeMenu) forControlEvents:UIControlEventTouchUpInside];
                         [menuView.view addSubview:closingMenu];
                         panshadowView.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:SHAWDOW_ALPHA];
                     }
                     completion:nil];
    panshadowView.hidden = NO;
    // drawer
    [UIView animateWithDuration:duration
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         menuView.view.frame = CGRectMake(0, 0, menuView.view.frame.size.width, menuView.view.frame.size.height);
                     }
                     completion:nil];
    
    isOPen= YES;

}

- (void)tapOnShawdow:(UITapGestureRecognizer *)recognizer {
    [self closeMenu];
}

-(void)moveDrawer:(UIPanGestureRecognizer *)recognizer{
    
    CGPoint translation = [recognizer translationInView:self.view];
    CGPoint velocity = [(UIPanGestureRecognizer*)recognizer velocityInView:self.view];
    //    NSLog(@"velocity x=%f",velocity.x);
    
    if([(UIPanGestureRecognizer*)recognizer state] == UIGestureRecognizerStateBegan) {
        //        NSLog(@"start");
        if ( velocity.x > MENU_TRIGGER_VELOCITY && isOPen) {
            [self menuOpen];
        }else if (velocity.x < -MENU_TRIGGER_VELOCITY && isOPen) {
            [self closeMenu];
        }
    }
    
    if([(UIPanGestureRecognizer*)recognizer state] == UIGestureRecognizerStateChanged) {
        //        NSLog(@"changing");
        float movingx = menuView.view.center.x + translation.x;
        if ( movingx > -menuView.view.frame.size.width/2 && movingx < menuView.view.frame.size.width/2){
            
            menuView.view.center = CGPointMake(movingx, menuView.view.center.y);
            [recognizer setTranslation:CGPointMake(0,0) inView:self.view];
            
            float changingAlpha = SHAWDOW_ALPHA/menuView.view.frame.size.width*movingx+SHAWDOW_ALPHA/2; // y=mx+c
            panshadowView.hidden = NO;
            panshadowView.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:changingAlpha];
        }
    }
    
    if([(UIPanGestureRecognizer*)recognizer state] == UIGestureRecognizerStateEnded) {
        //        NSLog(@"end");
        if (menuView.view.center.x>0){
            [self menuOpen];
        }else if (menuView.view.center.x<0){
            [self closeMenu];
        }
    }
    

    
}

-(void)closeMenu{
    
    float duration = MENU_DURATION/menuView.view.frame.size.width*fabs(menuView.view.center.x)+MENU_DURATION/2; // y=mx+c
    
    // shawdow
    [UIView animateWithDuration:duration
                          delay:0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         panshadowView.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.0f];
                     }
                     completion:^(BOOL finished){
                         panshadowView.hidden = YES;
                     }];
    
    // drawer
    [UIView animateWithDuration:duration
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                    menuView.view.frame = CGRectMake(- menuView.view.frame.size.width, 0, menuView.view.frame.size.width, menuView.view.frame.size.height);;
                     }
                     completion:nil];
    isOPen= NO;


    
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}



-(void)mapView:(MKMapView *)mapview didUpdateUserLocation:(MKUserLocation *)userLocation

{
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(userLocation.coordinate, 800, 800);
    [mapView setRegion:[mapview regionThatFits:region] animated:YES];
}

-(void)showSearchLocation:(CGFloat)latitude lon:(CGFloat)longitude

{
    CLLocationCoordinate2D location1;
    
    MKCoordinateRegion region;
    MKCoordinateSpan span;
    span.latitudeDelta=0.5;
    span.longitudeDelta=0.5;
    location1.latitude = latitude;
    location1.longitude = longitude;
    mapView.centerCoordinate = location1;
    region.span=span;
    region.center=location1;
    [mapView setRegion:region animated:TRUE];
    [mapView regionThatFits:region];
    mapView.centerCoordinate = location1;
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    NSLog(@"Error:%@",error);
    switch([error code])
    {
        case kCLErrorNetwork: // general, network-related error
        {
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please check your network connection or that you are not in airplane mode" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        }
            break;
        case kCLErrorDenied:{
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Location Service not enabeld" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];        }
            break;
            
        case kCLErrorLocationUnknown:
            break;
        default:
            
            break;
    }
    
}


-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:YES];
    
    [self getNearbyTrucks];
    
    
}

-(void)getNearbyTrucks{
    
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
    [SVProgressHUD show];
    
    NSURL* url = [NSURL URLWithString:@"http://quikmov.quikmovapp.com/auth/getList"];
    NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession* session = [NSURLSession sessionWithConfiguration:config ];
    
    NSMutableURLRequest*request = [[NSMutableURLRequest alloc]initWithURL:url];
    request.HTTPMethod = @"POST";
    
    NSUserDefaults* userStoredetails = [NSUserDefaults standardUserDefaults];
    userID = [userStoredetails objectForKey:@"user_id"];
    NSLog(@"%@",userID);

    
    NSString* bodycntnt = [NSString stringWithFormat:@"userid=%@",userID];
    
    NSData *data = [bodycntnt dataUsingEncoding:NSUTF8StringEncoding];
    
    NSURLSessionUploadTask *uploadtask = [session uploadTaskWithRequest:request fromData:data completionHandler:^(NSData*data, NSURLResponse*response, NSError*error){
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
            [self serviceSuccess:data];
        });
    }];
    
    [uploadtask resume];

    
}

- (void)serviceSuccess:(NSData *)data{
    if (data == nil) {
        return;
    }
    NSString* str = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
    NSError * error;
    NSDictionary* dic = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    NSLog(@"%@******%@,%@",str,[dic allValues],[dic objectForKey:@"response"]);
    
    NSString * Result = [dic objectForKey:@"status"];
    
    NSLog(@"%@",Result);
    
    if ([Result isEqualToString:@"success"]){
        NSArray * arr = [dic objectForKey:@"result"];
        
        objectsArr = [NSMutableArray new];
        
        for (int i = 0; i < [arr count]; i++) {
            
            //
            GlobalObjects * globleObj = [[GlobalObjects alloc]init];
            NSDictionary * objDic = [arr objectAtIndex:i];
            globleObj.latitudee = [objDic objectForKey:@"latitude"];
            
            globleObj.longitudee = [objDic objectForKey:@"longitude"];
            
            globleObj.username = [objDic objectForKey:@"user_name"];
            
            globleObj.userPhone = [objDic objectForKey:@"user_phone"];
            
            globleObj.usermailid = [objDic objectForKey:@"user_email"];
            
            globleObj.currentUsertype = [objDic objectForKey:@"usertype"];
            
            globleObj.userDistance = [objDic objectForKey:@"distance"];
            
            globleObj.userPhotoUrl = [NSString stringWithFormat:@"http://quikmov.quikmovapp.com%@",[objDic objectForKey:@"userfilepath"]];
            
            globleObj.userStatus = [objDic objectForKey:@"status"];
            
            globleObj.userId = [objDic objectForKey:@"user_id"];
            
            trukrId = globleObj.userId;

            if(![userID isEqualToString:[objDic objectForKey:@"user_id"]]){

                if ([userType isEqualToString:@"1"]) {
                    if (![globleObj.userStatus isEqualToString:@"0"]){
                        [objectsArr addObject:globleObj];
                    }
                }else{
                    [objectsArr addObject:globleObj];
                }
                
            }
        }
        
        [self loadAnnotations];
        
        
    }else{
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Message" message:[NSString stringWithFormat:@"%@",[dic objectForKey:@"result"]] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
}

-(void)uploadcurrentLocation{
    
    if ([Reachability reachabilityForInternetConnection]) {
        
        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
//        [SVProgressHUD show];
        
        NSURL* url = [NSURL URLWithString:@"http://quikmov.quikmovapp.com/auth/userLocation" ];
        NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession* session = [NSURLSession sessionWithConfiguration:config ];
        
        NSMutableURLRequest*request = [[NSMutableURLRequest alloc]initWithURL:url];
        request.HTTPMethod = @"POST";
        
        //        inputs : userid,lat,lon,status(1 or 0)
        //        url : http://truckcsoft.mapcee.com/auth/userLocation
        
        NSUserDefaults* userStoredetails = [NSUserDefaults standardUserDefaults];
        userID = [userStoredetails objectForKey:@"user_id"];
        NSLog(@"%@",userID);
        
        NSString* bodycntnt = [NSString stringWithFormat:@"userid=%@&lat=%@&lon=%@&status=%@",userID,lat,log,statusCheck];
        
        NSLog(@"%@",bodycntnt);
        
        NSData *data = [bodycntnt dataUsingEncoding:NSUTF8StringEncoding];
        
        NSURLSessionUploadTask *uploadtask = [session uploadTaskWithRequest:request fromData:data completionHandler:^(NSData*data, NSURLResponse*response, NSError*error){
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                
                NSString* str = [[NSString alloc]initWithData:data
                                                     encoding:NSUTF8StringEncoding];
                
                NSError* error;
                
                if (str.length == 0) {
                    [self showAlert:@"Server error"];
                    return;
                }
                
                NSDictionary* dic = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                
                NSLog(@"%@******%@,%@",str,[dic allValues],[dic objectForKey:@"response"]);
                
                // userId = [[dic objectForKey:@"message"] stringValue];
                NSString * Result = [dic objectForKey:@"status"];
                
                NSLog(@"%@",Result);
                
            });
            
            
        }];
        [uploadtask resume];
    }
    
    
}

-(void)loadAnnotations{
    
    if (annotations.count>0) {
        [annotations removeAllObjects];
        [mapView removeAnnotations:[mapView annotations]];
    }
    // if bust latitude is zero we need to load only stops
    
    for(int i = 0; i < objectsArr.count; i++)
    {
        GlobalObjects * object = (GlobalObjects*)[objectsArr objectAtIndex:i];
        
        CLLocationCoordinate2D theCoordinate1;
        
        if (![userType isEqualToString:object.currentUsertype]) {
            NSLog(@"Hello world");
        }else{
            
            NSLog(@"loaded");
        }
        
        myAnnotation =[[MKPointAnnotation alloc] init];
        
        theCoordinate1.latitude = [object.latitudee floatValue];
        theCoordinate1.longitude = [object.longitudee floatValue];
        myAnnotation.coordinate = theCoordinate1;
        myAnnotation.title = [NSString stringWithFormat:@"%d",i];
        
        [mapView addAnnotation:myAnnotation];
        [annotations addObject:myAnnotation];

    }
    
    MKMapRect flyTo = MKMapRectNull;
    for (id <MKAnnotation> annotation in annotations) {
        //	NSLog(@"fly to on");
        MKMapPoint annotationPoint = MKMapPointForCoordinate(annotation.coordinate);
        MKMapRect pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0, 0);
        if (MKMapRectIsNull(flyTo)) {
            flyTo = pointRect;
        } else {
            flyTo = MKMapRectUnion(flyTo, pointRect);
        }
    }

    mapView.visibleMapRect = flyTo;
    //[self drawRoute];
    
}

-(MKAnnotationView *)mapView:(MKMapView *)mV viewForAnnotation:(id <MKAnnotation>)annotation
{

//    GlobalObjects*globalObj = [[GlobalObjects alloc]init];
    
    
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    
    long int pinCount = [annotations indexOfObject:annotation];
    
    NSLog(@"%lu",(unsigned long)[annotations indexOfObject:annotation]);
    
    static NSString *defaultPinID = @"annotationViewID";
    
    MKAnnotationView * pinView = (MKAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:defaultPinID];
    
    if ( pinView == nil ){
        pinView = [[MKAnnotationView alloc]
                   initWithAnnotation:annotation reuseIdentifier:defaultPinID];
    }
    
    [pinView setCanShowCallout:NO];
    pinView.tag = pinCount;
    NSLog(@"PinCount:%ld",pinCount);
    
    if (pinCount == NSNotFound || NSIntegerMax == 2147483647) {
        [SVProgressHUD showInfoWithStatus:@"Searching your Location"];
        
    }else{
        [SVProgressHUD dismiss];
        [self uploadcurrentLocation];
    }
    
    
    UIView *vw = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 60, 30)];
    vw.backgroundColor = [UIColor clearColor];
    
    UIImageView * img = [[UIImageView alloc]initWithFrame:vw.frame];
    img.image = [UIImage imageNamed:@"pin.png"];
    [vw addSubview:img];
    
//    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(1, 0, 50, 30)];
//    label.numberOfLines = 2;
//    label.backgroundColor = [UIColor clearColor];
//    label.textAlignment = NSTextAlignmentLeft;
//    
//    label.font = [UIFont systemFontOfSize:10.0];
//    label.textColor = [UIColor whiteColor];
//    pinView.tag = pinCount;
//    label.text = [NSString stringWithFormat:@"%.1fM",[self getDistancefromCurrentLocation:[[lat objectAtIndex:pinCount] floatValue] with:[[lon objectAtIndex:pinCount] floatValue]]];
//    [vw addSubview:label];
    
    pinView.image = [self imageWithView:vw];
    pinView.annotation = annotation;
    
//    if ([globalObj.userStatus isEqualToString:@"0"]) {
//        pinView.hidden = YES;
//    }
    
    return pinView;
}

- (UIImage *) imageWithView:(UIView *)view
{
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, NO, 0.0);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return img;
}

- (void)mapView:(MKMapView *)mapView1 didSelectAnnotationView:(MKAnnotationView *)view
{
    //[mapView deselectAnnotation:view.annotation animated:YES];
    NSLog(@"selectedTag:%ld",(long)view.tag);
    [self ShowDetailView:view.tag];
}

- (void)ShowDetailView:(NSInteger)tag{
    
//    [self.panGest setEnabled:NO];
    
    GlobalObjects * object = (GlobalObjects*)[objectsArr objectAtIndex:tag];
    shadowView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screenWidth, screenHeight)];
    shadowView.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.5f];
    [self.view addSubview:shadowView];
    
    shadowButton = [DesignObj initWithButton:CGRectMake(0, 0, screenWidth, screenHeight) tittle:@"" img:@""];
    [shadowButton setBackgroundColor:[UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.5f]];
    [shadowButton addTarget:self action:@selector(closePopView) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:shadowButton];
    
    popView = [[UIView alloc] initWithFrame:CGRectMake((screenWidth - (screenWidth - 40))/2, (screenHeight - (screenHeight - 150))/2,screenWidth - 40, screenHeight - 150)];
    popView.layer.cornerRadius = 8;
    [popView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg.png"]]];
    [self.view addSubview:popView];
    
    profimg = [[UIImageView alloc]initWithFrame:CGRectMake(50, 20, 50, 50)];
    
//    ((popView.frame.size.width - (screenWidth - 200))/2
    dispatch_async(dispatch_get_main_queue(), ^{
        [profimg sd_setImageWithURL:[NSURL URLWithString:object.userPhotoUrl] placeholderImage:[UIImage imageNamed:@"truck_login.png"]];
        
        //[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:object.userPhotoUrl]]];
    });
    
    [popView addSubview:profimg];
    
    viewimage1 = [DesignObj initWithButton:CGRectMake(50, 20, 50, 50) tittle:nil img:nil];
    [viewimage1 addTarget:self action:@selector(showPic:) forControlEvents:UIControlEventTouchUpInside];
    viewimage1.tag = 110;
    [popView addSubview:viewimage1];
    
    profimg2 = [[UIImageView alloc]initWithFrame:CGRectMake(profimg.frame.origin.x + 50 + 20, 20, 50, 50)];
    dispatch_async(dispatch_get_main_queue(), ^{
        [profimg2 sd_setImageWithURL:[NSURL URLWithString:object.userPhotoUrl] placeholderImage:[UIImage imageNamed:@"truck_login.png"]];
    });
    
    [popView addSubview:profimg2];
    
    viewimage2 = [DesignObj initWithButton:CGRectMake(profimg.frame.origin.x + 50 + 20, 20, 50, 50) tittle:nil img:nil];
    [viewimage2 addTarget:self action:@selector(showPic:) forControlEvents:UIControlEventTouchUpInside];
    viewimage2.tag = 120;
    [popView addSubview:viewimage2];
    
    profimg3 = [[UIImageView alloc]initWithFrame:CGRectMake(profimg2.frame.origin.x + 50 + 20, 20, 50, 50)];
    dispatch_async(dispatch_get_main_queue(), ^{
        [profimg3 sd_setImageWithURL:[NSURL URLWithString:object.userPhotoUrl] placeholderImage:[UIImage imageNamed:@"truck_login.png"]];
    });
    
    [popView addSubview:profimg3];
    
    viewimage3 = [DesignObj initWithButton:CGRectMake(profimg2.frame.origin.x + 50 + 20, 20, 50, 50) tittle:nil img:nil];
    [viewimage3 addTarget:self action:@selector(showPic:) forControlEvents:UIControlEventTouchUpInside];
    viewimage3.tag = 130;
    [popView addSubview:viewimage3];
    
    profimg4 = [[UIImageView alloc]initWithFrame:CGRectMake(profimg3.frame.origin.x + 50 + 20, 20, 50, 50)];
    dispatch_async(dispatch_get_main_queue(), ^{
        [profimg4 sd_setImageWithURL:[NSURL URLWithString:object.userPhotoUrl] placeholderImage:[UIImage imageNamed:@"truck_login.png"]];
    });
    [popView addSubview:profimg4];
    
    viewimage4 = [DesignObj initWithButton:CGRectMake(profimg3.frame.origin.x + 50 + 20, 20, 50, 50) tittle:nil img:nil];
    [viewimage4 addTarget:self action:@selector(showPic:) forControlEvents:UIControlEventTouchUpInside];
    viewimage4.tag = 140;
    [popView addSubview:viewimage4];
    
    UILabel *namelabel = [[UILabel alloc] initWithFrame:CGRectMake(30, profimg.frame.origin.y+ 50 +30, 200, 40)];
//    CGRectMake((popView.frame.size.width-100)/2, profimg.frame.origin.y+ 70 +30, 200, 40)];
    namelabel.numberOfLines = 2;
    namelabel.textAlignment = NSTextAlignmentLeft;
    namelabel.textColor = [DesignObj quikRed];
    namelabel.font = [UIFont systemFontOfSize:16.0];
    namelabel.text = [NSString stringWithFormat:@"Name: %@",object.username];
    NSLog(@"%@",object.username);
    [popView addSubview:namelabel];
    
    UILabel *phonelabel = [[UILabel alloc] initWithFrame:CGRectMake(30, namelabel.frame.origin.y+ 40+ 20, 300, 40)];
//    CGRectMake((popView.frame.size.width - 100)/2, namelabel.frame.origin.y+ 40+ 20, 200, 40)];
    phonelabel.numberOfLines = 2;
    phonelabel.textAlignment = NSTextAlignmentLeft;
    phonelabel.textColor = [DesignObj quikRed];
    phonelabel.font = [UIFont systemFontOfSize:16.0];
    phonelabel.text = [NSString stringWithFormat:@"Phone Number: %@",object.userPhone];
    NSLog(@"%@",object.userPhone);
    [popView addSubview:phonelabel];
    
    UILabel *emaillabel = [[UILabel alloc] initWithFrame:CGRectMake(30, phonelabel.frame.origin.y+ 40+ 20, 200, 40)];
//    CGRectMake((popView.frame.size.width - 100)/2, phonelabel.frame.origin.y+ 40+ 20, 200, 40)]
    emaillabel.numberOfLines = 2;
    emaillabel.textAlignment = NSTextAlignmentLeft;
    emaillabel.textColor = [DesignObj quikRed];
    emaillabel.font = [UIFont systemFontOfSize:16.0];
    emaillabel.text = [NSString stringWithFormat:@"E-mail-id: %@",object.usermailid];
    NSLog(@"%@",object.usermailid);
    [popView addSubview:emaillabel];
    
    UIButton *bookNow = [DesignObj initWithButton:CGRectMake((popView.frame.size.width - 200)/2, phonelabel.frame.origin.y+ 40+ 20+70, 200, 40) tittle:@"Book Now" img:@"button"];
    [bookNow addTarget:self action:@selector(bookNow:) forControlEvents:UIControlEventTouchUpInside];
    [bookNow setTitleColor:[DesignObj quikYellow] forState:UIControlStateNormal];
    [popView addSubview:bookNow];
    
    closeView = [UIButton buttonWithType:UIButtonTypeCustom];
    closeView.frame = CGRectMake(popView.frame.size.width-30,0,30,30);
    [closeView addTarget:self action:@selector(closePopView) forControlEvents:UIControlEventTouchUpInside];
    [closeView setImage:[UIImage imageNamed:@"close.png"] forState:UIControlStateNormal];
    [popView addSubview:closeView];
    
    
    
}


-(IBAction)showPic:(NSInteger)tag{
    
    MHGalleryItem *landschaft1 = [[MHGalleryItem alloc]initWithImage:profimg.image];
    MHGalleryItem* landschaft2 =[MHGalleryItem itemWithImage:profimg2.image];
    MHGalleryItem* landschaft3 =[MHGalleryItem itemWithImage:profimg3.image];
    MHGalleryItem* landschaft4 =[MHGalleryItem itemWithImage:profimg4.image];
    

    NSArray *galleryItem = @[landschaft1,landschaft2,landschaft3,landschaft4];
    
    MHGalleryController *gallery = [MHGalleryController galleryWithPresentationStyle:MHGalleryViewModeImageViewerNavigationBarHidden];
    gallery.galleryItems = galleryItem;
    gallery.presentingFromImageView = nil;
    gallery.presentationIndex = 0;
    gallery.UICustomization.showOverView =NO;
    gallery.UICustomization.barStyle = UIBarStyleBlackTranslucent;
    gallery.UICustomization.hideShare = YES;
//    gallery.imageViewerViewController.view.backgroundColor = [UIColor blackColor];
//    gallery.imageViewerViewController.view.frame = CGRectMake(0, 0, screenWidth, screenHeight);
    //  gallery.galleryDelegate = self;
    //  gallery.dataSource = self;
    gallery.UICustomization.barTintColor = [DesignObj quikRed];
    gallery.UICustomization.barButtonsTintColor = [DesignObj quikYellow];
    
    __weak MHGalleryController *blockGallery = gallery;
    
    gallery.finishedCallback = ^(NSInteger currentIndex,UIImage *image,MHTransitionDismissMHGallery *interactiveTransition,MHGalleryViewMode viewMode){
        if (viewMode == MHGalleryViewModeOverView) {
            [blockGallery dismissViewControllerAnimated:YES completion:^{
                [self setNeedsStatusBarAppearanceUpdate];
            }];
        }else{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [blockGallery dismissViewControllerAnimated:YES dismissImageView:nil completion:^{
                    
                    [self setNeedsStatusBarAppearanceUpdate];
                                    }];
            });
        }
    };
    [self presentMHGalleryController:gallery animated:YES completion:nil];
    
}

#pragma mark - tableView Functions.

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return objectsArr.count;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    // This will create a "invisible" footer
    return 0.001;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *MyIdentifier = @"MyIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:MyIdentifier];
    }
    
    for (UIView * view in cell.contentView.subviews)
    {
        [view removeFromSuperview];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
//    _scrView = [[UIScrollView alloc]init];
//    _scrView.frame = CGRectMake(0, 0, cell.contentView.frame.size.width, cell.contentView.frame.size.height);
//    [cell.contentView addSubview:_scrView];
//    
//    if (screenWidth > 320) {
//        
//        _scrView.contentSize = CGSizeMake(cell.contentView.frame.size.width+200, cell.contentView.frame.size.height);
//    }

        GlobalObjects * globalObj = (GlobalObjects*)[objectsArr objectAtIndex:indexPath.row];
        
//        UIImage*userImage =[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:globalObj.userPhotoUrl]]];
//
        cell.imageView.frame = CGRectMake(cell.contentView.frame.origin.x+ 5, cell.contentView.frame.origin.y+5, 50, 50);
     //   cell.imageView.image = userImage;
    [cell.imageView sd_setImageWithURL:[NSURL URLWithString:globalObj.userPhotoUrl] placeholderImage:[UIImage imageNamed:@"user.png"]];
        [cell.contentView addSubview:cell.imageView];
    
        UILabel * titlelbl = [[UILabel alloc]initWithFrame:CGRectMake(cell.imageView.frame.origin.x+50 +40, 10, 150, 30)];
        titlelbl.font = [UIFont boldSystemFontOfSize:15.0];
        [cell.contentView addSubview:titlelbl];
        
//        UILabel * amountlbl = [[UILabel alloc]initWithFrame:CGRectMake(200, 30, screenWidth-100, 30)];
//        amountlbl.font = [UIFont systemFontOfSize:16.0];
//        [_scrView addSubview:amountlbl];
    
//        UILabel * duelbl = [[UILabel alloc]initWithFrame:CGRectMake(200, 60, screenWidth-100, 30)];
//        duelbl.textColor = [DesignObj quikRed];
//        duelbl.font = [UIFont systemFontOfSize:16.0];
//        [_scrView addSubview:duelbl];
    
//    cell.contentView.frame.size.width-50
    
        UILabel *distncelbl = [[UILabel alloc]initWithFrame:CGRectMake(listtableView.frame.size.width-100, 10, 100, 30)];
        distncelbl.textColor = [DesignObj quikRed];
        distncelbl.font = [UIFont systemFontOfSize:16.0];
        [cell.contentView addSubview:distncelbl];
    
        UILabel * addresslbl = [[UILabel alloc]initWithFrame:CGRectMake(100, 90, screenWidth-100, 30)];
        addresslbl.font = [UIFont boldSystemFontOfSize:15.0];
        
        [cell.contentView addSubview:addresslbl];
        
        titlelbl.text = globalObj.username;
//        amountlbl.text = [NSString stringWithFormat:@"%@",globalObj.userPhone];
//        duelbl.text = globalObj.usermailid;
        distncelbl.text = [NSString stringWithFormat:@"%@Miles",globalObj.userDistance];

    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

        [self ShowDetailView:indexPath.row];
        
    
        

}


- (void)didSelectedOption:(NSIndexPath*)indexPath withTitle:(NSString*)title{
    
    [self closeMenu];
    
    if (indexPath.row == 0) {
        
        [self showAlert:@"Will be available in Next link"];
        
    }else if (indexPath.row == 1){
        [self showAlert:@"Will be available in Next link"];
    
    }else if (indexPath.row == 2) {
        
        [self showAlert:@"Will be available in Next link"];
        
    }else if (indexPath.row == 3){

        [self showAlert:@"Will be available in Next link"];
    
    }else if (indexPath.row == 4) {
        
        [self showAlert:@"Will be available in Next link"];
        
    }else if (indexPath.row == 5) {
        
        [self.navigationController popToRootViewControllerAnimated:YES];
        
        
    }else if (indexPath.row == 6) {
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@"Message"
                                      message:@"Do you really want to quit the QuikMov?"
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                               
                                 exit(0);
                                 
                             }];
        
        UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 
                                 [self dismissViewControllerAnimated:YES completion:^{
                                     
                                 }];
                                 
                             }];
        
        
        [alert addAction:ok];
        [alert addAction:cancel];
        
        [self presentViewController:alert animated:YES completion:nil];
    
//
    
    }
}





-(IBAction)bookNow:(id)sender{
    
//    [self.panGest setEnabled:NO];
    
    bookshadowView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screenWidth, screenHeight)];
    bookshadowView.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.5f];
    [self.view addSubview:bookshadowView];
    
    bookshadowButton = [DesignObj initWithButton:CGRectMake(0, 0, screenWidth, screenHeight) tittle:@"" img:@""];
    [bookshadowButton setBackgroundColor:[UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.5f]];
    [bookshadowButton addTarget:self action:@selector(closebookPopView) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:bookshadowButton];
    
    bookpopView = [[UIView alloc] initWithFrame:CGRectMake((screenWidth - (screenWidth - 40))/2, (screenHeight - (screenHeight - 150))/2,screenWidth - 40, screenHeight - 150)];
    bookpopView.layer.cornerRadius = 8;
    [bookpopView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg.png"]]];
    [self.view addSubview:bookpopView];
    
    _scrView = [[UIScrollView alloc]init];
    _scrView.frame = CGRectMake(0, 0, bookpopView.frame.size.width, bookpopView.frame.size.height);
    [bookpopView addSubview:_scrView];
    _scrView.contentSize = CGSizeMake(bookpopView.frame.size.width, bookpopView.frame.size.height+100);
    
    txtbgImg1 = [DesignObj initWithImage:CGRectMake((bookpopView.frame.size.width-300)/2,80,300,60) img:@"text_box.png"];
    [_scrView addSubview:txtbgImg1];
    
    deliveryArea = [DesignObj initWithTextfield:CGRectMake((bookpopView.frame.size.width-300)/2,80,300,60) placeholder:@"" tittle:@"Delivery Point" delegate:self font:16];
    deliveryArea.textAlignment = NSTextAlignmentCenter;
    deliveryArea.keyboardType = UIKeyboardTypeDefault;
    [deliveryArea addTarget:self action:@selector(getlatandlong:) forControlEvents:UIControlEventEditingDidEndOnExit];
    deliveryArea.returnKeyType = UIReturnKeyDone;
    [_scrView addSubview:deliveryArea];
    
    txtbgImg2 = [DesignObj initWithImage:CGRectMake((bookpopView.frame.size.width-300)/2,deliveryArea.frame.origin.y+60 +20,300,40) img:@"text_box.png"];
    [_scrView addSubview:txtbgImg2];
    
    shipmentType = [DesignObj initWithTextfield:CGRectMake((bookpopView.frame.size.width-300)/2,deliveryArea.frame.origin.y+60 +20,300,40) placeholder:@"" tittle:@"Shipment Type" delegate:self font:16];
    shipmentType.textAlignment = NSTextAlignmentCenter;
    shipmentType.keyboardType = UIKeyboardTypeDefault;
    shipmentType.returnKeyType = UIReturnKeyDone;
    [_scrView addSubview:shipmentType];
    
    txtbgImg3 = [DesignObj initWithImage:CGRectMake((bookpopView.frame.size.width-300)/2,shipmentType.frame.origin.y+ 40+20,300,60) img:@"text_box.png"];
    [_scrView addSubview:txtbgImg3];
    
    Description = [DesignObj initWithTextfield:CGRectMake((bookpopView.frame.size.width-300)/2,shipmentType.frame.origin.y+ 40+20,300,60) placeholder:@"" tittle:@"Description" delegate:self font:16];
    Description.textAlignment = NSTextAlignmentCenter;
    Description.keyboardType = UIKeyboardTypeDefault;
    Description.returnKeyType = UIReturnKeyDone;
    [_scrView addSubview:Description];
    
    
    confirmBooking = [DesignObj initWithButton:CGRectMake((bookpopView.frame.size.width-175)/2, Description.frame.origin.y+60 +20, 175, 40) tittle:@"Confirm Booking" img:@"button.png"];
    [confirmBooking addTarget:self action:@selector(confirmBook:) forControlEvents:UIControlEventTouchUpInside];
    [confirmBooking setTitleColor:[DesignObj quikYellow] forState:UIControlStateNormal];
    [_scrView addSubview:confirmBooking];
    
    bookcloseView = [UIButton buttonWithType:UIButtonTypeCustom];
    bookcloseView.frame = CGRectMake(bookpopView.frame.size.width-30,0,30,30);
    [bookcloseView addTarget:self action:@selector(closebookPopView) forControlEvents:UIControlEventTouchUpInside];
    [bookcloseView setImage:[UIImage imageNamed:@"close.png"] forState:UIControlStateNormal];
    [bookpopView addSubview:bookcloseView];
    
    
}

- (IBAction)getlatandlong:(UITextField *)sender {
    
      
    /*

    if ([deliveryArea resignFirstResponder]) {
        
        [self getlatandlong:sender];
    }
    if(forwardGeocoder == nil)
    {
        forwardGeocoder = [[CLGeocoder alloc] init];
    }
    NSString *address = deliveryArea.text;
    [forwardGeocoder geocodeAddressString:address completionHandler:^(NSArray *placemarks, NSError *error) {
        
        if(placemarks.count > 0)
        {
            CLPlacemark *placemark = [placemarks objectAtIndex:0];
//            self.outputLabel.text = placemark.location.description;
            forwardlat = [NSString stringWithFormat:@"%f",placemark.location.coordinate.latitude];
            forwardlong = [NSString stringWithFormat:@"%f",placemark.location.coordinate.longitude];
            NSLog(@"%@",placemark.location.description);
        }
        else if (error.domain == kCLErrorDomain)
        {
            switch (error.code)
            {
                case kCLErrorDenied:
                    [self showAlert:@"checking"];
                    break;
                case kCLErrorNetwork:
                    [self showAlert:@"No Network"];
                    break;
                case kCLErrorGeocodeFoundNoResult:
                    [self showAlert:@"No Result Found"];
                    break;
                default:
                    [self showAlert:[NSString stringWithFormat:@"%@",error.localizedDescription]];
                    break;
            }
        }
        else
        {
            [self showAlert:[NSString stringWithFormat:@"%@",error.localizedDescription]];
            
        }
        
    }];

    */
}




-(void)closebookPopView{
    [bookpopView removeFromSuperview];
    [bookshadowButton removeFromSuperview];
    [bookcloseView removeFromSuperview];
    [bookshadowView removeFromSuperview];
    [deliveryArea removeFromSuperview];
    [shipmentType removeFromSuperview];
    [Description removeFromSuperview];

    popView.hidden = YES;
    
}


-(void)closePopView{
    [closeView removeFromSuperview];
    [popView removeFromSuperview];
    [shadowView removeFromSuperview];
    [shadowButton removeFromSuperview];
}

-(IBAction)confirmBook:(id)sender{
    
    [deliveryArea resignFirstResponder];
    [shipmentType resignFirstResponder];
    [Description resignFirstResponder];
    
    if (deliveryArea.text.length ==0) {
        [self showAlert:@"Please enter the Delivery Point"];
        
    }else if (shipmentType.text.length ==0) {
        [self showAlert:@"Please enter the ShipmentType"];
        
    }else if (Description.text.length ==0) {
        [self showAlert:@"Please enter the Description"];
    }else{
        
    [self bookService];
    }
}

-(void)bookService{
    
    if ([Reachability reachabilityForInternetConnection]) {
        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
        [SVProgressHUD show];
        
        NSURL* url = [NSURL URLWithString:@"http://quikmov.quikmovapp.com/auth/booking"];
        
        NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession* session = [NSURLSession sessionWithConfiguration:config ];
        
        NSMutableURLRequest*request = [[NSMutableURLRequest alloc]initWithURL:url];
        request.HTTPMethod = @"POST";
        
        
        NSUserDefaults* userStoredetails = [NSUserDefaults standardUserDefaults];
        userID = [userStoredetails objectForKey:@"user_id"];
        NSLog(@"%@",userID);
        
       // inputs : userid,truckerid, pickupfromlat,pickupfromlon,deliverytolat.deliverytolon
        
        NSString* bodycntnt = [NSString stringWithFormat:@"userid=%@&truckerid=%@&pickupfromlat=%@&pickupfromlon=%@&deliverytolat=%@&deliverytolon=%@&shipmenttypeid=%@&description=%@",userID,trukrId,lat,log,forwardlat,forwardlong,shipmentType.text,Description.text];
        
        NSData *data = [bodycntnt dataUsingEncoding:NSUTF8StringEncoding];
        
        
        NSURLSessionUploadTask *uploadtask = [session uploadTaskWithRequest:request fromData:data completionHandler:^(NSData*data, NSURLResponse*response, NSError*error){
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                
                NSString* str = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
                NSError* error;
                
                NSDictionary* dic = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                NSLog(@"%@******%@,%@",str,[dic allValues],[dic objectForKey:@"response"]);
                
                // userId = [[dic objectForKey:@"message"] stringValue];
                NSString * Result = [dic objectForKey:@"status"];
                
                NSLog(@"%@",Result);
                if ([Result isEqualToString:@"success"]){
                    
                    
                    UIAlertController * alert=   [UIAlertController
                                                  alertControllerWithTitle:@"Message"
                                                  message:@"Booking Successful\nIf you doesn't get Respose from the Trucker\nPlease contact the Trucker in the next 5 Minutes"
                                                  preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* ok = [UIAlertAction
                                         actionWithTitle:@"OK"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action)
                                         {
                                            
                                             [bookpopView removeFromSuperview];
                                             [bookshadowButton removeFromSuperview];
                                             [bookcloseView removeFromSuperview];
                                             [bookshadowView removeFromSuperview];
                                             [deliveryArea removeFromSuperview];
                                             [shipmentType removeFromSuperview];
                                             [Description removeFromSuperview];
                                             [closeView removeFromSuperview];
                                             [popView removeFromSuperview];
                                             [shadowView removeFromSuperview];
                                             [shadowButton removeFromSuperview];
                                             
                                             
                                         }];
                    
                    
                    [alert addAction:ok];
                    
                    [self presentViewController:alert animated:YES completion:nil];
                    
                    
                }else{
                    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Message" message:[NSString stringWithFormat:@"%@",[dic objectForKey:@"result"]] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                    [alert show];
                }
                
            });
        }];
        [uploadtask resume];
    }else{
        [self showAlert:@"No internet connectivity"];
    }
    
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)showAlert:(NSString *)message{
    
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Message"
                                 message:message
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [alert dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    
    [alert addAction:ok];
    
    [self presentViewController:alert animated:YES completion:nil];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


@end


