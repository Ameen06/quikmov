//
//  TruckProfileObj.h
//  QuikMov
//
//  Created by CSCS on 13/04/16.
//  Copyright © 2016 CSCS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TruckProfileObj : NSObject

//Truck Profile

@property(nonatomic,copy)NSString* truckWidth;
@property(nonatomic,copy)NSString* truckHeight;
@property(nonatomic,copy)NSString* truckLength;
@property(nonatomic,copy)NSString* truckType;
@property(nonatomic,copy)NSString* sizeOfTruck;
@property(nonatomic,copy)NSString* serviceCatogories;
@property(nonatomic,copy)NSString* regNo;
@property(nonatomic,copy)NSString* raTe;
@property(nonatomic,copy)NSString* insurance;
@property(nonatomic,copy)NSString* equip;
@property(nonatomic,copy)NSString* truckImag;

@end
