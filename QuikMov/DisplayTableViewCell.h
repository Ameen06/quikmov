//
//  DisplayTableViewCell.h
//  QuikMov
//
//  Created by Ajmal Khan on 3/3/16.
//  Copyright © 2016 CSCS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DisplayTableViewCell : UITableViewCell

@property(nonatomic, strong) UILabel* titleLabel;

@end
