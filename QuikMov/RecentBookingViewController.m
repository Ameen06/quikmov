//
//  RecentBookingViewController.m
//  QuikMov
//
//  Created by CSCS on 12/05/16.
//  Copyright © 2016 CSCS. All rights reserved.
//

#import "RecentBookingViewController.h"
#import "AppDelegate.h"
#import "DesignObj.h"
#import "GlobalObjects.h"
#import "MBProgressHUD.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "UIImageView+WebCache.h"

@interface RecentBookingViewController ()<UITableViewDataSource,UITableViewDelegate>{
    
    UITableView * recentBookView;
    NSMutableArray * datastoreArr;
    NSString * userID;
    GlobalObjects * globleObj;
}

@property(nonatomic)UIScrollView * scrView;

@end

@implementation RecentBookingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"QuikMov";
    
    self.navigationController.navigationBarHidden = NO;
    
    self.navigationController.navigationBar.tintColor = [DesignObj quikYellow];
    
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [DesignObj quikYellow]}];
    self.navigationController.navigationBar.barTintColor = [DesignObj quikRed];
    
    self.navigationController.navigationBar.tintColor = [DesignObj quikYellow];
    
    UIImageView*bgView = [DesignObj initWithImage:CGRectMake(0, 0, screenWidth, screenHeight) img:@"bg.png"];
    [self.view addSubview:bgView];
    
    
    _scrView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, screenHeight )];
    _scrView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:_scrView];
    
    _scrView.contentSize = CGSizeMake(screenWidth, screenHeight + 100);
    
    globleObj = [[GlobalObjects alloc]init];

    // Do any additional setup after loading the view.
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:YES];
    
    [self getRecentBookings];
    
}

-(void)getRecentBookings{
    
    MBProgressHUD* hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = @"Loading...";
    hud.labelColor = [DesignObj quikYellow];
    hud.animationType = MBProgressHUDAnimationZoomIn;
    hud.color = [DesignObj quikRed];
    hud.alpha = 0.8;
    
    NSURL* url = [NSURL URLWithString:@"http://quikmov.quikmovapp.com/auth/getBookingHistory"];
    NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession* session = [NSURLSession sessionWithConfiguration:config ];
    
    NSMutableURLRequest*request = [[NSMutableURLRequest alloc]initWithURL:url];
    request.HTTPMethod = @"POST";
    
    NSUserDefaults* userStoredetails = [NSUserDefaults standardUserDefaults];
    userID = [userStoredetails objectForKey:@"user_id"];
    NSLog(@"%@",userID);
    
    NSString* bodycntnt = [NSString stringWithFormat:@"userid=%@",userID];
    
    NSData *data = [bodycntnt dataUsingEncoding:NSUTF8StringEncoding];
    
    NSURLSessionUploadTask *uploadtask = [session uploadTaskWithRequest:request fromData:data completionHandler:^(NSData*data, NSURLResponse*response, NSError*error){
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [MBProgressHUD hideHUDForView:self.view animated:YES];

            [self serviceSuccess:data];
        });
    }];
    
    [uploadtask resume];
}

- (void)serviceSuccess:(NSData *)data{
    if (data == nil) {
        return;
    }
    NSString* str = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
    NSError * error;
    NSDictionary* dic = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    NSLog(@"%@******%@,%@",str,[dic allValues],[dic objectForKey:@"response"]);
    
    NSString * Result = [dic objectForKey:@"status"];
    
    NSLog(@"%@",Result);
    
    if ([Result isEqualToString:@"success"]){
        NSArray * arr = [dic objectForKey:@"result"];
        [datastoreArr removeAllObjects];
        datastoreArr = [NSMutableArray new];
        
        for ( int i = 0; i < [arr count]; i++) {
            
            //
            
            NSDictionary * objDic = [arr objectAtIndex:i];
            
            globleObj.usernme = [objDic objectForKey:@"latitude"];
            
            globleObj.ratepermile = [objDic objectForKey:@"longitude"];
            
            globleObj.image = [NSString stringWithFormat:@"http://quikmov.quikmovapp.com%@",[objDic objectForKey:@"userfilepath"]];
            
            globleObj.userId = [objDic objectForKey:@"user_id"];
        
            [datastoreArr addObject:globleObj];
            
            }
        
        [self loadmainView];
        
    }else{
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@"Message"
                                      message:[NSString stringWithFormat:@"%@",[dic objectForKey:@"result"]]
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
        
        
        [alert addAction:ok];
        
        [self presentViewController:alert animated:YES completion:nil];

    }
    
}

-(void)loadmainView{
    
    UIView * recentBooktitle = [[UIView alloc]initWithFrame:CGRectMake(0, 64, screenWidth, 120)];
    recentBooktitle.backgroundColor = [UIColor clearColor];
    [self.view addSubview:recentBooktitle];
    
    UIImageView * settingsIcon = [DesignObj initWithImage:CGRectMake(((screenWidth - 120)/2)-15 -5, 60, 25, 25) img:@"recent_book.png"];
    [recentBooktitle addSubview:settingsIcon];
    
    UILabel * title = [DesignObj initWithLabel:CGRectMake((screenWidth - 140)/2 + 20, 57, 140, 30) title:@"Recent Booking" font:18 txtcolor:[DesignObj quikRed]];
    title.textAlignment = NSTextAlignmentCenter;
    [recentBooktitle addSubview:title];

    
    recentBookView = [DesignObj initWithTableView:self frame:CGRectMake(0, CGRectGetMaxY(recentBooktitle.frame), screenWidth, screenHeight)];
    recentBookView.dataSource = self;
    recentBookView.backgroundColor = [UIColor clearColor];
    recentBookView.rowHeight = 120.0f;
    recentBookView.separatorColor = [DesignObj quikRed];
    [self.view addSubview:recentBookView];
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [datastoreArr count];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *MyIdentifier = @"MyIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:MyIdentifier];
    }
    
    /*
    for (UIView * view in cell.contentView.subviews)
    {
        [view removeFromSuperview];
    }
     */
    
    cell.backgroundColor = [UIColor clearColor];
    
    GlobalObjects * globalInit = (GlobalObjects *)[datastoreArr objectAtIndex:indexPath.row];
    
    UIImageView * userCellimage = [DesignObj initWithImage:CGRectMake(20, (CGRectGetHeight(cell.contentView.frame) - 30)/2, 30, 30) img:@""];
     dispatch_async(dispatch_get_main_queue(), ^{
        [userCellimage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",globalInit.image]] placeholderImage:[UIImage imageNamed:@"user.png"]];
    });
    userCellimage.layer.cornerRadius = userCellimage.frame.size.width / 2;
    userCellimage.layer.borderWidth = 2.0f;
    userCellimage.layer.borderColor = [DesignObj quikRed].CGColor;
    [cell.contentView addSubview:userCellimage];
    
    UIView * titleView = [[UIView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(userCellimage.frame) + 20, 0, 70, cell.contentView.frame.size.height)];
    [cell.contentView addSubview:titleView];
    
    UIView * nexttitleView = [[UIView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(titleView.frame) + 10, 0, 120, cell.contentView.frame.size.height)];
    [cell.contentView addSubview:nexttitleView];
    
    UILabel * nameLbl = [DesignObj initWithLabel:CGRectMake(5,5,65,30) title:[NSString stringWithFormat:@"Name:"] font:18 txtcolor:[DesignObj quikRed]];
    [titleView addSubview:nameLbl];
    
    UILabel * rateLbl = [DesignObj initWithLabel:CGRectMake(5,CGRectGetHeight(titleView.frame) - 35,65,30) title:[NSString stringWithFormat:@"Rate/Mile:"] font:18 txtcolor:[DesignObj quikRed]];
    [titleView addSubview:rateLbl];
    
    UILabel * nextnameLbl = [DesignObj initWithLabel:CGRectMake(5,5,65,30) title:[NSString stringWithFormat:@"%@",globalInit.usernme] font:18 txtcolor:[DesignObj quikRed]];
    [nexttitleView addSubview:nextnameLbl];
    
    UILabel * nextrateLbl = [DesignObj initWithLabel:CGRectMake(5,CGRectGetHeight(nexttitleView.frame) - 35,65,30) title:[NSString stringWithFormat:@"%@",globalInit.ratepermile] font:18 txtcolor:[DesignObj quikRed]];
    [nexttitleView addSubview:nextrateLbl];
    
    
    return cell;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
