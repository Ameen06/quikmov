//
//  BaseViewController.h
//  QuikMov
//
//  Created by CSCS on 12/04/16.
//  Copyright © 2016 CSCS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseViewController : UIViewController

- (void)showAlert:(NSString*)message;

@end
