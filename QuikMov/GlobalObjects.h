//
//  GlobalObjects.h
//  QuikMov
//
//  Created by CSCS on 1/25/16.
//  Copyright © 2016 CSCS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface GlobalObjects : NSObject

//Truck Images
//@property(nonatomic,strong)NSString* truckPhotoUrl1;
//@property(nonatomic,strong)NSString* truckPhotoUrl2;
//@property(nonatomic,strong)NSString* truckPhotoUrl3;
//@property(nonatomic,strong)NSString* truckPhotoUrl4;
//
@property(nonatomic,copy)NSMutableArray * truckPhotos;
@property(nonatomic,copy)NSString* truckTypesid;
@property(nonatomic,copy)NSString* truckTypesparentid;
@property(nonatomic,copy)NSString* truckTypes;
@property(nonatomic,copy)NSString* subtruckTypes;
@property(nonatomic,copy)NSString* userStatus;
@property(nonatomic,copy)NSString* userId;
@property(nonatomic,copy)NSString* bookName;
@property(nonatomic,copy)NSString* bookImageurl;
@property(nonatomic,copy)NSString* pickupLat;
@property(nonatomic,copy)NSString* pickupLong;
@property(nonatomic,copy)NSString* deliveryLat;
@property(nonatomic,copy)NSString* deliveryLong;
@property(nonatomic,copy)NSString* bookingStatus;
@property(nonatomic,copy)NSString* shipmentType;
@property(nonatomic,copy)NSString* decription;
@property(nonatomic,copy)NSString* truckerId;
@property(nonatomic,copy)NSString* updatedOn;
@property(nonatomic,copy)NSString* bookingId;

// Profile View for users
@property(nonatomic,copy)NSString* dob;
@property(nonatomic,copy)NSString* lname;
@property(nonatomic,copy)NSString* mname;
@property(nonatomic,copy)NSString* fname;
@property(nonatomic,copy)NSString* user_mail;
@property(nonatomic,copy)NSString* passrd;
@property(nonatomic,copy)NSString* phno;
@property(nonatomic,copy)NSString* cpassrd;

// Truck Profile

@property(nonatomic,copy)NSString* truckWidth;
@property(nonatomic,copy)NSString* truckHeight;
@property(nonatomic,copy)NSString* truckLength;
@property(nonatomic,copy)NSString* truckType;
@property(nonatomic,copy)NSString* sizeOfTruck;
@property(nonatomic,copy)NSString* serviceCatogories;
@property(nonatomic,copy)NSString* regNo;
@property(nonatomic,copy)NSString* raTe;
@property(nonatomic,copy)NSString* insurance;
@property(nonatomic,copy)NSString* equip;
@property(nonatomic,copy)NSString* truckImag;

//

@property(nonatomic,strong)NSString* currentUsertype;
@property(nonatomic,strong)NSString* latitudee;
@property(nonatomic,strong)NSString* longitudee;
@property(nonatomic,strong)NSString* username;
@property(nonatomic,strong)NSString* usermailid;
@property(nonatomic,strong)NSString* userPhone;
@property(nonatomic,strong)NSString* userDistance;
@property(nonatomic,strong)NSString* userPhotoUrl;

//Recent Booking

@property(nonatomic,strong)NSString* usernme;
@property(nonatomic,strong)NSString* ratepermile;
@property(nonatomic,strong)NSString* image;



@end
