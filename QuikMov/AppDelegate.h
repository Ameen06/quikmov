//
//  AppDelegate.h
//  QuikMov
//
//  Created by CSCS on 1/21/16.
//  Copyright © 2016 CSCS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>

#define  screenWidth ([UIScreen mainScreen].bounds.size.width)
#define  screenHeight ([UIScreen mainScreen].bounds.size.height)
#define  APP_KEY ([NSString stringWithFormat:@"kCvEUbnCpWuJqDqujHQg5VLGaMcnK9d7"])

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property(nonatomic,strong)CLLocationManager* locationManager;
@property(nonatomic,copy)NSString * lat;
@property(nonatomic,copy)NSString * log;
@property(nonatomic,copy)NSString * strdevicetoken;


- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;


@end

