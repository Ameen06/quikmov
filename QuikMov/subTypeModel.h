//
//  subTypeModel.h
//  QuikMov
//
//  Created by Ajmal Khan on 3/19/16.
//  Copyright © 2016 CSCS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface subTypeModel : NSObject

@property (nonatomic, strong) NSString * trucktypeid;
@property (nonatomic, strong) NSString * trucktype;
@property (nonatomic, assign) NSInteger parent_id;
@property (nonatomic, strong) NSString * subcat;

@end
